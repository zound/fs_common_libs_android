/******************************************************************************
 * Copyright 2012 by Frontier Silicon Ltd. All rights reserved.
 *
 * No part of this software, either material or conceptual may be copied or
 * distributed, transmitted, transcribed, stored in a retrieval system or
 * translated into any human or computer language in any form by any means,
 * electronic, mechanical, manual or otherwise, or disclosed to third parties
 * without the express written permission of Frontier Silicon Ltd.
 * 137 Euston Road, London, NW1 2AA.
 ******************************************************************************/
package com.frontier_silicon.NetRemoteLib;

import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class EventHelper<IndexType, CallbackType> {
	public class CallbackRecord {
		public boolean mCallOnMainThread;
		public CallbackType mCallback;
		public Object mTag;
		
		public CallbackRecord(CallbackType callback, Object tag, boolean callOnMainThread) {
			this.mCallback = callback;
			this.mTag = tag;
			this.mCallOnMainThread = callOnMainThread;
		}
		
		public boolean equals(CallbackRecord otherRecord) {
			boolean callsOnMainThreadMath = (otherRecord.mCallOnMainThread == this.mCallOnMainThread);

			boolean callbacksMatch = (((otherRecord.mCallback == null) && (this.mCallback == null))
					               || ((otherRecord.mCallback != null) && otherRecord.mCallback.equals(this.mCallback)));
			
			return (callsOnMainThreadMath && callbacksMatch);
		}
	}
	
	private Map<IndexType, List<CallbackRecord>> RegisteredCallbacks = new HashMap<>();
	
	public void registerCallback(IndexType index, boolean callbackOnMainThread, CallbackType callback, Object tag) {
		synchronized (this.RegisteredCallbacks) {
			/* Is this the first time we've registered a notification for this node? */
			List<CallbackRecord> list;
			if (!(this.RegisteredCallbacks.containsKey(index))) {
				/* Yes, create new callback list. */
				list = new ArrayList<CallbackRecord>();
			} else {
				/* No, add to existing list. */
				list = this.RegisteredCallbacks.get(index);
			}
			
			/* Add to the list, and put back in the map. */
			CallbackRecord record = new CallbackRecord(callback, tag, callbackOnMainThread);
			
			/* Warning if the record is already in the list. */
			for (CallbackRecord existingRecord : list) {
				if (existingRecord.equals(record)) {
					NetRemote.log(LogLevel.Error, "Registering duplicate callback: " + callback.toString());
				}
			}
			
			list.add(record);
			NetRemote.log(NetRemote.TRACE_EVENTHELPER_BIT, LogLevel.Info, "Registering callback " + callback.toString() + " against index " + index.toString());
			this.RegisteredCallbacks.put(index, list);
		}
	}

	public void registerCallback(IndexType index, boolean callbackOnMainThread, CallbackType callback) { this.registerCallback(index, callbackOnMainThread, callback, null); }

	public void registerCallbacks(IndexType[] indices, boolean callbackOnMainThread, CallbackType callback, Object tag) {
		for (IndexType index : indices) {
			this.registerCallback(index, callbackOnMainThread, callback, tag);
		}
	}

	public void registerCallbacks(IndexType[] indices, boolean callbackOnMainThread, CallbackType callback) { this.registerCallbacks(indices, callbackOnMainThread, callback, null); }
	
	public void deregisterCallback(IndexType index, CallbackType callback) {
		synchronized (this.RegisteredCallbacks) {
			/* See if we have an entry for this node? */
			if (!(this.RegisteredCallbacks.containsKey(index))) {
				NetRemote.log(LogLevel.Error, "Attempt to deregister callback using index that has no records against it.");
				return;
			}
			
			/* See if the list contains the callback. */
			List<CallbackRecord> list = this.RegisteredCallbacks.get(index);
			CallbackRecord record = new CallbackRecord(callback, null, false);
			
			boolean found = false;
			for (CallbackRecord item : list) {
				if (((record.mCallback == item.mCallback)) || item.equals(record)) {
					NetRemote.log(NetRemote.TRACE_EVENTHELPER_BIT, LogLevel.Info, "Removing callback " + item.mCallback.toString() + " against index " + index.toString());
					list.remove(item);
					found = true;
					break;
				}
			}
			
			if (!found) {
				NetRemote.log(LogLevel.Error, "Attempt to deregister callback for a callback that has not been registered.");
			}
		}
	}
	
	public void deregisterCallbacks(Class type) {
		String typeName = NetRemote.getClassName(type);
		
		synchronized (this.RegisteredCallbacks) {
			NetRemote.log(NetRemote.TRACE_EVENTHELPER_BIT, LogLevel.Info, "Removing all callbacks for class name" + type.toString());
			for (List<CallbackRecord> list : this.RegisteredCallbacks.values()) {
				/* Repeatedly go through the list removing callbacks, until there are none left. */
				boolean changesMade;			
				do {
					changesMade = false;
					
					for (CallbackRecord item : list) {
						Class itemType = item.mCallback.getClass();
						String itemTypeName = NetRemote.getClassName(itemType);

						if (typeName.equals(itemTypeName)) {
							list.remove(item);
							changesMade = true;
							break;
						}
					}
					
				} while (changesMade);

			}				
		}
	}
	
	public Collection<CallbackRecord> getCallbacks(IndexType index) {
		synchronized (this.RegisteredCallbacks) {
			if (!(this.RegisteredCallbacks.containsKey(index))) {
				/* No callbacks, return empty list. */
				return new ArrayList<CallbackRecord>();
			} else {
				/* Return the list. */
				return Collections.unmodifiableCollection(new ArrayList<>(this.RegisteredCallbacks.get(index)));
			}
		}
	}
	
	public interface CallCallbackCodeFragment {
		void run(Object callback, Object tag);
	}
	
	public void callCallbacks(IndexType index, final CallCallbackCodeFragment code) {
		Collection<CallbackRecord> callbacks;
		synchronized (this.RegisteredCallbacks) {
			callbacks = this.getCallbacks(index);
		}

		for (final EventHelper.CallbackRecord record : callbacks) {
			NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
				@Override
				public void run() {
					code.run(record.mCallback, record.mTag);
				}
			}, record.mCallOnMainThread);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateTag(IndexType index, CallbackType callback, Object newTagValue) {
		synchronized (this.RegisteredCallbacks) {
			for (EventHelper.CallbackRecord record : this.getCallbacks(index)) {
				if (record.mCallback.equals(callback)) {
					record.mTag = newTagValue;
				}
			}
		}
	}
	
	public void clearAllCallbacks() {
        NetRemote.log(NetRemote.TRACE_EVENTHELPER_BIT, LogLevel.Info, "Removing ALL callbacks.");
		synchronized (this.RegisteredCallbacks) {
			this.RegisteredCallbacks.clear();
		}
	}
}
