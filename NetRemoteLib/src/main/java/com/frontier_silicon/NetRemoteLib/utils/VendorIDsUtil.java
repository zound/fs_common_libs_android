package com.frontier_silicon.NetRemoteLib.utils;

import android.text.TextUtils;

/**
 * Created by lsuhov on 08/12/2016.
 */

public class VendorIDsUtil {

    public static boolean isInVendorIDsList(String vendorID, String[] vendorsIdList) {
        boolean isVendorIDOk = false;

        if (!TextUtils.isEmpty(vendorID)) {
            for (String validVendorID : vendorsIdList) {
                if (validVendorID.contains(vendorID)) {
                    isVendorIDOk = true;
                    break;
                }
            }
        }

        return isVendorIDOk;
    }
}
