package com.frontier_silicon.NetRemoteLib;

/**
 * Created by mnisipeanu on 10/11/2016.
 */

public interface IWifiConnection {
    void onConnected();
    void onDisconnected();
}
