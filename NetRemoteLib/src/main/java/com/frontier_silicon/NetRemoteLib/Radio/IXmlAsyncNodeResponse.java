package com.frontier_silicon.NetRemoteLib.Radio;

import org.w3c.dom.Document;

/**
 * Created by lsuhov on 19/01/2017.
 *
 * Interface used to handle async node requests
 */

interface IXmlAsyncNodeResponse {
    void onResponse(Document document);
    void onErrorResponse(ErrorResponse errorResponse);
}
