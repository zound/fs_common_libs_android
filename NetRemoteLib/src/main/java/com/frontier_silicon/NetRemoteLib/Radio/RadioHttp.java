/******************************************************************************
 * Copyright 2012 by Frontier Silicon Ltd. All rights reserved.
 *
 * No part of this software, either material or conceptual may be copied or
 * distributed, transmitted, transcribed, stored in a retrieval system or
 * translated into any human or computer language in any form by any means,
 * electronic, mechanical, manual or otherwise, or disclosed to third parties
 * without the express written permission of Frontier Silicon Ltd.
 * 137 Euston Road, London, NW1 2AA.
 ******************************************************************************/
package com.frontier_silicon.NetRemoteLib.Radio;

import android.os.Looper;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.IRadioNotificationState;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeB32;
import com.frontier_silicon.NetRemoteLib.Node.NodeC;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs;
import com.frontier_silicon.NetRemoteLib.Node.NodeE8;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeS16;
import com.frontier_silicon.NetRemoteLib.Node.NodeS32;
import com.frontier_silicon.NetRemoteLib.Node.NodeS8;
import com.frontier_silicon.NetRemoteLib.Node.NodeU;
import com.frontier_silicon.NetRemoteLib.Node.NodeU16;
import com.frontier_silicon.NetRemoteLib.Node.NodeU32;
import com.frontier_silicon.NetRemoteLib.Node.NodeU8;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse.FSStatusCode;
import com.frontier_silicon.loggerlib.LogLevel;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This is the derived radio class for accessing radios over HTTP.
 */
public class RadioHttp extends Radio {
	private static final Boolean DisableNotificationMonitor = false;

	private URI mApiBase = null;
	private String mSessionId = null;

    private final int MAX_TIMEOUTS_BEFORE_CONNECTION_LOST = 5;
	private int mNumTimeouts = 0;
	private String mDeviceInfoLocation;

    private List<IRadioNotificationState> mNotificationState = new CopyOnWriteArrayList<>();

    /********************************
     * Radio management facilities. *
     ********************************/
    private String mFriendlyName = null;
    private String mSN = "";
    private String mUDN = "";
    private boolean mIsMultiroomCapable = false;
    private String mMultiroomVersion = "";
    private boolean mIsSG = false;
    private String mVendorID = "";
    private String mModelName = "";
    private String mModelNumber = "";
    private String mFirmwareVersion = "";
    private URI mImageURL = null;
    private boolean mIsMinuet = false;
    private boolean mIsVenice = false;
    private boolean mIsStereoCapable = false;
    private boolean mHasGoogleCast = false;
    private boolean mHasAVS = false;

    public RadioHttp(String serialNumber, String friendlyName, String apiBase) {
        initCriticalData(serialNumber, friendlyName, "", apiBase);
    }

	public RadioHttp(String serialNumber, String friendlyName, String udn, String apiBase) {
		initCriticalData(serialNumber, friendlyName, udn, apiBase);
	}

    public RadioHttp(DeviceRecord record) {
        applyDataFromDeviceRecord(record);
    }

    public void applyDataFromDeviceRecord(DeviceRecord record) {
        initCriticalData(record.serialNumber, record.friendlyName, record.UDN, record.fsApiLocation);

        mIsMultiroomCapable = record.isMultiroomCapable;
        mMultiroomVersion = record.multiroomVersion;
        mVendorID = record.vendorID;
        mModelName = record.modelName;
        mModelNumber = record.modelNumber;
        mImageURL = RadioHttpUtil.formImageUrl(mApiBase.getScheme() + "://" + mApiBase.getHost() + ":8080", record.imageURL);
        mDeviceInfoLocation = record.deviceInfoLocation;

        mDeviceRecord = record;
        mIsMinuet = record.isMinuet;
        mIsVenice = record.isVenice;
        mIsSG = record.isSG;
        mIsStereoCapable = record.isStereoCapable;
        mHasGoogleCast = record.hasGoogleCast;
        mFirmwareVersion = record.firmwareVersion;
        mHasAVS = record.hasAVS;
    }

    private void initCriticalData(String serialNumber, String friendlyName, String udn, String apiBase) {
        mSN = serialNumber;
        mFriendlyName = friendlyName;

        try {
            mApiBase = new URI(apiBase);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            NetRemote.log(LogLevel.Error, e.getMessage());
        }

        mUDN = udn;
    }

	/**
	 * Attempt a connection to the radio. "connect()" is called when first connecting to a radio, but is
	 * also called to reconnect to a radio if the connection has been lost.
	 *
	 * @param callback				The object to be called-back with the result.
	 */
	protected void connectInternal(boolean synchronous, final IConnectionCallback callback) {

        /* Get ready to create the session. */
        final String requestURL = RadioHttpUtil.formAccessUrl(mApiBase.toString(), mPIN, "CREATE_SESSION", null);

        /* Do HTTP request to get a new session ID. */
        NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, "Requesting new Session ID.");

        if (Looper.myLooper() == Looper.getMainLooper() && synchronous) {
            NetRemote.log(LogLevel.Error, ">>> MAIN THREAD Requesting: " + requestURL);
        } else {
            NetRemote.log(LogLevel.Info, ">>> Requesting: " + requestURL + " synchronous: " + synchronous);
        }

        if (synchronous) {
            XmlSyncNodeResponse xmlSyncNodeResponse = new XmlSyncNodeRequest().doRequest(requestURL, true, this);
            if (xmlSyncNodeResponse.document != null) {
                onGetSessionResponse(xmlSyncNodeResponse.document, requestURL, callback);
            } else {
                onGetSessionIdError(xmlSyncNodeResponse.errorResponse, requestURL, callback);
            }

        } else {
            (new XmlAsyncNodeRequest()).enqueue(requestURL, true, this, new IXmlAsyncNodeResponse() {
                @Override
                public void onResponse(Document document) {
                    onGetSessionResponse(document, requestURL, callback);
                }

                @Override
                public void onErrorResponse(ErrorResponse errorResponse) {
                    onGetSessionIdError( errorResponse, requestURL, callback);
                }
            });
        }
	}

	void onGetSessionIdError(ErrorResponse errorResponse, String requestURL, IConnectionCallback callback) {
        ConnectionErrorResponse connectionErrorResponse = (ConnectionErrorResponse) errorResponse;

		NetRemote.log(LogLevel.Error, "Request error: " + connectionErrorResponse.getErrorCode() + " " + requestURL);

		callback.onConnectionError(RadioHttp.this, connectionErrorResponse);
	}

	void onGetSessionResponse(Document response, String url, IConnectionCallback callback) {

		/* The doc should be like this:
		<fsapiResponse>
			<status>FS_OK</status>
			<sessionId>12345</sessionId>
		</fsapiResponse>
		 */

        setDeviceIsAlive();

		mInvalidPIN = false;
		mNumTimeouts = 0;

		/* Check that the status is ok. */
		final FSStatusCode status = RadioHttpUtil.getStatus(response, true);
		if (status == FSStatusCode.FS_OK) {
        	/* Get the sessionId value. */
            NodeList nodes = response.getElementsByTagName("sessionId");
            if (nodes.getLength() == 1) {
                String id = nodes.item(0).getFirstChild().getNodeValue();
                mSessionId = id;
                NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, "New Session ID of " + id);

                if (!DisableNotificationMonitor) {
                    notificationMonitorStart();
                }

                NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, "(RadioHTTP) Connected to radio at " + url);
                callback.onConnectionSuccess(this);
                return;
            } else {
                callback.onConnectionError(this, new ConnectionErrorResponse(false, ConnectionErrorResponse.ErrorCode.ParseError, null));
                return;
            }
        } else {
            callback.onConnectionError(this, new ConnectionErrorResponse(false, ConnectionErrorResponse.ErrorCode.Error, status, null));
            return;
        }
	}

	private NotificationThreadWrapper mNotificationMonitor = null;

	private void notificationMonitorStart() {
		if (this.mNotificationMonitor != null) {
			this.notificationMonitorStop();
		}

		this.mNotificationMonitor = new NotificationThreadWrapper(this);
	}

	private void notificationMonitorStop() {
		if (this.mNotificationMonitor != null) {
			this.mNotificationMonitor.Cancel();
			this.mNotificationMonitor = null;
		}
	}

	/**
	 * Disconnect from the radio.
	 */
	protected void disconnectInternal() {
		this.notificationMonitorStop();
	}

	/**
	 * Read a node.
	 *
	 * @param nodeType		The class/type of the node to be read
	 * @param callback		The object to be called-back with the result
	 */
	protected void getNodeInternal(Class nodeType, boolean synchronous, IGetNodeCallback callback) {
		/* Is it a list node? */
		if (NodeDefs.IsListNode(nodeType)) {
			this.getListNodeInternal(nodeType, synchronous, "-1", NodeDefs.MaxItemsForList, callback);
			return;
		}

		/* Normal node access. */
		/* Build something of the form: http://192.168.1.2/fsapi/GET/netRemote.info.version?pin=1234&sid=foo */
		String url = RadioHttpUtil.formAccessUrl(mApiBase.toString(), mPIN, "GET/" + NodeDefs.Instance().GetNodeName(nodeType), null);

		if (isCheckingAvailability() || !isAvailable()) {
			url = null;
		}

		if (url == null) {
			callback.getNodeError(nodeType, new NodeErrorResponse(false, ErrorResponse.ErrorCode.Error, null, null));
        } else {
			this.getNodeHelper(nodeType, synchronous, callback, url);
		}
	}

	/**
	 * Read a node.
	 *
	 * @param nodeType		The class/type of the node to be read
	 * @param callback		The object to be called-back with the result
	 */
	protected void getListNodeInternal(Class nodeType, boolean synchronous, String key, int maxItems, IGetNodeCallback callback) {
		/* Is it a list node? */
		NetRemote.assertTrue(com.frontier_silicon.NetRemoteLib.Node.NodeList.class.isAssignableFrom(nodeType));

		/* Build something of the form: http://192.168.1.2/fsapi/LIST_GET/netRemote.nav.list/2?pin=1234&sid=foo */
		String url = RadioHttpUtil.formAccessUrl(mApiBase.toString(), mPIN, "LIST_GET_NEXT/" + NodeDefs.Instance().GetNodeName(nodeType) + "/" + key, null, ("maxItems=" + maxItems));

		//TEST
		if (isCheckingAvailability() || !isAvailable()) {
			url = null;
		}
		//TEST

		if (url == null) {
			callback.getNodeError(nodeType, new NodeErrorResponse(false, ErrorResponse.ErrorCode.Error, null, null));
			return;
		} else {
			this.getNodeHelper(nodeType, synchronous, callback, url);
		}
	}

	protected void getNodesMultipleInternal(Class nodeTypes[], boolean synchronous, IGetNodesCallback callback) {
		/* Multiple nodes access. */
		/* Build something of the form: http://192.168.1.2/fsapi/GET_MULTIPLE?pin=1234&node=netremote.foo&node=netremote.bar */
		String nodesQueryParams = "";
		for (int i = 0; i < nodeTypes.length; i++) {
			String nodeName = NodeDefs.Instance().GetNodeName(nodeTypes[i]);
			nodesQueryParams = nodesQueryParams + "node=" + nodeName + "&";
		}

		String url = RadioHttpUtil.formAccessUrl(mApiBase.toString(), mPIN, "GET_MULTIPLE", null, nodesQueryParams);

		//TEST
		if (isCheckingAvailability() || !isAvailable()) {
			url = null;
		}
		//TEST

		if (url == null) {
			callback.onResult(getAllNodeErrors(nodeTypes, new NodeErrorResponse(false, ErrorResponse.ErrorCode.Error, null, null)));
		} else {
			getMultipleNodesHelper(nodeTypes, synchronous, callback, url);
		}
	}

	private void getMultipleNodesHelper(final Class[] nodeTypes, boolean synchronous, final IGetNodesCallback callback, final String requestURL) {

		if (mInvalidPIN && !mAllowRequestsForInvalidPIN) {
			notifyInvalidPINError(nodeTypes, callback, requestURL);
			return;
		}

		if (Looper.myLooper() == Looper.getMainLooper() && synchronous) {
			NetRemote.log(LogLevel.Error, ">>> MAIN THREAD Requesting: " + requestURL + " synchronous: " + true);
		} else {
			NetRemote.log(LogLevel.Info, ">>> Requesting: " + requestURL + " synchronous: " + synchronous);
		}

        if (synchronous) {
            XmlSyncNodeResponse xmlSyncNodeResponse = new XmlSyncNodeRequest().doRequest(requestURL, false, this);
            if (xmlSyncNodeResponse.document != null) {
                onGetMultipleNodesInfoResponse(xmlSyncNodeResponse.document, nodeTypes, callback);
            } else {
                onGetMultipleNodesInfoError(xmlSyncNodeResponse.errorResponse, callback, nodeTypes, requestURL);
            }

        } else {
            (new XmlAsyncNodeRequest()).enqueue(requestURL, false, this, new IXmlAsyncNodeResponse() {
                @Override
                public void onResponse(Document document) {
                    onGetMultipleNodesInfoResponse(document, nodeTypes, callback);
                }

                @Override
                public void onErrorResponse(ErrorResponse errorResponse) {
                    onGetMultipleNodesInfoError(errorResponse, callback, nodeTypes, requestURL);
                }
            });
        }
	}

	private Map<Class, NodeResponse> getAllNodeErrors(Class[] nodeTypes, NodeErrorResponse error) {
		HashMap<Class, NodeResponse> errorResponses = new HashMap<>();

		for (Class nodeClass : nodeTypes) {
			NodeResponse nodeResponse = new NodeResponse();
			nodeResponse.mType = NodeResponse.NodeResponseType.ERROR;
			nodeResponse.mError = error;
			errorResponses.put(nodeClass, nodeResponse);
		}

		return errorResponses;
	}

	void onGetMultipleNodesInfoResponse(Document response, Class[] nodeTypes, IGetNodesCallback callback) {

		/* The doc should be like this:
			<fsapiGetMultipleResponse>

				<fsapiResponse>
					<node>netremote.sys.info.friendlyname</node>
					<status>FS_OK</status>
					<value><c8_array>radio name</c8_array></value>
				</fsapiResponse>

				<fsapiResponse>
					<node>netremote.sys.audio.mute</node>
					<status>FS_OK</status>
					<value><u8>0</u8></value>
				</fsapiResponse>

			</fsapiGetMultipleResponse>
		*/

        setDeviceIsAlive();

		mInvalidPIN = false;
		mNumTimeouts = 0;

		Map<Class, NodeResponse> nodeResponses = new HashMap<>();

		NodeList nodes = response.getElementsByTagName("fsapiResponse");
		if (nodes.getLength() > 0) {
			for (int i = 0; i < nodes.getLength(); i++) {
				Node xmlNodeResponse = nodes.item(i);
				addNodeResponses(nodeTypes, nodeResponses, xmlNodeResponse);
			}
		} else {
			NetRemote.log(LogLevel.Error, "getNodesMultipleInternal Error no node response!");
			nodeResponses = null;
		}

		if (nodeResponses != null) {
			callback.onResult(nodeResponses);
		} else {
			callback.onResult(getAllNodeErrors(nodeTypes, new NodeErrorResponse(false, ErrorResponse.ErrorCode.ParseError, null, null)));
		}
	}

	private void addNodeResponses(Class[] nodeTypes, Map<Class, NodeResponse> nodeResponses, Node xmlNode) {
		Node value = RadioHttpUtil.getNodeValueFromXML(xmlNode);
		String responseNodeName = RadioHttpUtil.getNodeNameFromXML(xmlNode);
		FSStatusCode status = RadioHttpUtil.getStatusCodeFromXML(xmlNode);

		if (status == FSStatusCode.FS_OK) {
			Class nodeTypeClass = RadioHttpUtil.getNodeTypeClass(nodeTypes, responseNodeName);
			if (nodeTypeClass != null && value != null) {
				// create valid node response
				NodeResponse nodeResponse = new NodeResponse();
				nodeResponse.mType = NodeResponse.NodeResponseType.NODE;
				nodeResponse.mNodeInfo = NodeDefs.GetNodeFromXml(nodeTypeClass, value);

				nodeResponses.put(nodeTypeClass, nodeResponse);
			}
		} else {
			NetRemote.log(LogLevel.Error, "getNodesMultipleInternal Error " + responseNodeName + " status: " + status);

			Class nodeTypeClass = RadioHttpUtil.getNodeTypeClass(nodeTypes, responseNodeName);
			if (nodeTypeClass != null) {
				// create error response
				nodeResponses.put(nodeTypeClass, getNodeResponseError(status));
			}
		}
	}

	private NodeResponse getNodeResponseError(FSStatusCode status) {
		NodeResponse nodeResponse = new NodeResponse();
		nodeResponse.mType = NodeResponse.NodeResponseType.ERROR;
		nodeResponse.mError = new NodeErrorResponse(false, ErrorResponse.ErrorCode.FS_Error, status, null);

		return nodeResponse;
	}

	void onGetMultipleNodesInfoError(ErrorResponse errorResponse, IGetNodesCallback callback,
                                     Class[] nodeTypes, String requestURL) {
		NetRemote.log(LogLevel.Error, "Request error: " + errorResponse + " " + requestURL);

		callback.onResult(getAllNodeErrors(nodeTypes, (NodeErrorResponse) errorResponse));
	}

	private void getNodeHelper(final Class nodeType, boolean synchronous, final IGetNodeCallback callback, final String requestURL) {

		if (mInvalidPIN && !mAllowRequestsForInvalidPIN) {
			notifyInvalidPINError(nodeType, callback, requestURL);
			return;
		}

		if (Looper.myLooper() == Looper.getMainLooper() && synchronous) {
			NetRemote.log(LogLevel.Error, ">>> MAIN THREAD Requesting: " + requestURL + " synchronous: " + true);
		} else {
			NetRemote.log(LogLevel.Info, ">>> Requesting: " + requestURL + " synchronous: " + synchronous);
		}

        if (synchronous) {
            XmlSyncNodeResponse xmlSyncNodeResponse = new XmlSyncNodeRequest().doRequest(requestURL, false, this);
            if (xmlSyncNodeResponse.document != null) {
                onGetNodeInfoResponse(xmlSyncNodeResponse.document, nodeType, callback);
            } else {
                callback.getNodeError(nodeType, (NodeErrorResponse) xmlSyncNodeResponse.errorResponse);
            }

        } else {
            (new XmlAsyncNodeRequest()).enqueue(requestURL, false, this, new IXmlAsyncNodeResponse() {
                @Override
                public void onResponse(Document document) {
                    onGetNodeInfoResponse(document, nodeType, callback);
                }

                @Override
                public void onErrorResponse(ErrorResponse errorResponse) {
                    onGetNodeInfoError(errorResponse, callback, nodeType, requestURL);
                }
            });
        }
	}

	TransportErrorDetail getTimeoutTransportError() {
		TransportErrorDetail errorDetail = new TransportErrorDetail(TransportErrorDetail.ErrorCode.ConnectionError, false);

		mNumTimeouts = mNumTimeouts + 1;
		if (mNumTimeouts >= MAX_TIMEOUTS_BEFORE_CONNECTION_LOST) {
            errorDetail.UnderlyingConnectionFatallyErrored = true;
        }

		return errorDetail;

	}

	private void notifyInvalidPINError(Class nodeType, IGetNodeCallback callback, String requestURL) {
		callback.getNodeError(nodeType, getErrorResponseInvalidPIN(requestURL));
	}

	private void notifyInvalidPINError(Class[] nodeTypes, IGetNodesCallback callback, String requestURL) {
		callback.onResult(getAllNodeErrors(nodeTypes, getErrorResponseInvalidPIN(requestURL)));
	}

	private NodeErrorResponse getErrorResponseInvalidPIN(String requestURL) {
		NetRemote.log(LogLevel.Warning, "Request ignored: " + requestURL + " because of invalid mPIN");

		TransportErrorDetail errorDetail = new TransportErrorDetail(TransportErrorDetail.ErrorCode.PinError, false);
		errorDetail.UnderlyingConnectionFatallyErrored = true;
		return new NodeErrorResponse(false, ErrorResponse.ErrorCode.NetworkProblem, null, errorDetail);
	}

	void onGetNodeInfoError(ErrorResponse errorResponse, IGetNodeCallback callback, Class nodeType, String requestURL) {
		NetRemote.log(LogLevel.Error, "Request error: " + errorResponse + " " + requestURL);

		callback.getNodeError(nodeType, (NodeErrorResponse) errorResponse);
	}

	void onGetNodeInfoResponse(Document response, final Class nodeType, final IGetNodeCallback callback) {

		/* The doc should be like this:
			<fsapiResponse>
				<status>FS_OK</status>
				<value>
					<c8_array>ir-gui</c8_array>
				</value>
			</fsapiResponse>
		 */

        setDeviceIsAlive();

		mInvalidPIN = false;
		mNumTimeouts = 0;

		/* Check that the status is ok. */
		FSStatusCode status = RadioHttpUtil.getStatus(response, true);
		if (status == FSStatusCode.FS_OK) {
			NodeInfo result = null;

			/* Is it a list node? */
			if (NodeDefs.IsListNode(nodeType)) {
						/* Find the response. */
				Node firstNode = response.getFirstChild();
				if (firstNode != null) {
					if (firstNode.getNodeName().equals("fsapiResponse")) {
						result = NodeDefs.GetNodeFromXml(nodeType, firstNode);
					}
				}
			} else {
				/* Extract the value of a regular node. */
				NodeList nodes = response.getElementsByTagName("value");
				if (nodes.getLength() >= 1) {
					Node value = nodes.item(0);
					result = NodeDefs.GetNodeFromXml(nodeType, value);
				}
			}

			if (result != null) {
				callback.getNodeResult(result);
            } else {
				callback.getNodeError(nodeType, new NodeErrorResponse(false, ErrorResponse.ErrorCode.ParseError, null, null));
            }
		} else {
			callback.getNodeError(nodeType, new NodeErrorResponse(false, ErrorResponse.ErrorCode.FS_Error, status, null));
        }
	}

	/**
	 * Set a node
	 *
	 * @param node			The node and its value to be written
	 * @param callback		The object to call back with the result
	 */
	protected void setNodeInternal(final NodeInfo node, boolean synchronous, final ISetNodeCallback callback) {
		/* Determine the "long" value of the node. */
		Object value;
		if (node instanceof NodeU8) {
			value = ((NodeU8)node).getValue();
		} else if (node instanceof NodeU16) {
			value = ((NodeU16)node).getValue();
		} else if (node instanceof NodeU32) {
			value = ((NodeU32)node).getValue();
		} else if (node instanceof NodeS8) {
			value = ((NodeS8)node).getValue();
		} else if (node instanceof NodeS16) {
			value = ((NodeS16)node).getValue();
		} else if (node instanceof NodeS32) {
			value = ((NodeS32)node).getValue();
		} else if (node instanceof NodeE8) {
			value = ((NodeE8)node).getValue();
		} else if (node instanceof NodeB32) {
			value = ((NodeB32)node).getValue();
		} else if (node instanceof NodeC) {
			value = URLEncoder.encode(((NodeC)node).getValue());
		} else if (node instanceof NodeU) {
			value = URLEncoder.encode(((NodeU)node).getValue());
		} else {
			/* We do not support the set operation on this type of node. */
			callback.setNodeError(node, new NodeErrorResponse(false, ErrorResponse.ErrorCode.UnsupportedOperation, null, null));
			return;
		}

		/* Build something of the form: http://192.168.1.2/fsapi/SET/netRemote.sys.mode?pin=1234&value=3&sid=foo */
		String url = RadioHttpUtil.formAccessUrl(mApiBase.toString(), mPIN, "SET/" + NodeDefs.Instance().GetNodeName(node.getClass()), null, "value=" + value.toString());

        setNodeHelper(node, synchronous, callback, url);
	}

	private void setNodeHelper(final NodeInfo node, boolean synchronous, final ISetNodeCallback callback, final String requestURL) {

		if (mInvalidPIN && !mAllowRequestsForInvalidPIN) {
			notifyInvalidPINError(node, callback, requestURL);
			return;
		}

		if (Looper.myLooper() == Looper.getMainLooper() && synchronous) {
			NetRemote.log(LogLevel.Error, ">>> MAIN THREAD Requesting: " + requestURL + " synchronous: " + true);
		} else {
			NetRemote.log(LogLevel.Info, ">>> Requesting: " + requestURL + " synchronous: " + synchronous);
		}

        if (synchronous) {
            XmlSyncNodeResponse xmlSyncNodeResponse = new XmlSyncNodeRequest().doRequest(requestURL, false, this);
            if (xmlSyncNodeResponse.document != null) {
                onSetNodeInfoResponse(xmlSyncNodeResponse.document, node, callback);
            } else {
                onSetNodeInfoError(xmlSyncNodeResponse.errorResponse, callback, node, requestURL);
            }

        } else {
            (new XmlAsyncNodeRequest()).enqueue(requestURL, false, this, new IXmlAsyncNodeResponse() {
                @Override
                public void onResponse(Document document) {
                    onSetNodeInfoResponse(document, node, callback);
                }

                @Override
                public void onErrorResponse(ErrorResponse errorResponse) {
                    onSetNodeInfoError(errorResponse, callback, node, requestURL);
                }
            });
        }
	}

	private void notifyInvalidPINError(NodeInfo node, ISetNodeCallback callback, String requestURL) {
		callback.setNodeError(node, getErrorResponseInvalidPIN(requestURL));
	}

	void onSetNodeInfoError(ErrorResponse error, ISetNodeCallback callback, NodeInfo node, String requestURL) {
		NetRemote.log(LogLevel.Error, "Request error: " + error + " " + requestURL);
		callback.setNodeError(node, (NodeErrorResponse) error);
	}

	void onSetNodeInfoResponse(Document response, NodeInfo node, ISetNodeCallback callback) {

		/* The doc should be like this:
			<fsapiResponse>
				<status>FS_OK</status>
			</fsapiResponse>
		 */

        setDeviceIsAlive();

		mInvalidPIN = false;
		mNumTimeouts = 0;

		/* Check that the status is ok. */
		FSStatusCode status = RadioHttpUtil.getStatus(response, true);
		if (status == FSStatusCode.FS_OK) {
            callback.setNodeSuccess(node);
        } else {
            callback.setNodeError(node, new NodeErrorResponse(false, ErrorResponse.ErrorCode.FS_Error, status, null));
        }
	}

	protected void setNodesMultipleInternal(final NodeInfo[] nodes, boolean synchronous, final ISetNodesCallback callback) {
		String url = getSetMultipleUri(nodes, callback);

        setMultipleNodesHelper(nodes, synchronous, callback, url);
	}

	private void setMultipleNodesHelper(final NodeInfo[] nodes, boolean synchronous, final ISetNodesCallback callback, final String requestURL) {

		if (mInvalidPIN && !mAllowRequestsForInvalidPIN) {
			notifyInvalidPINError(nodes, callback, requestURL);
			return;
		}

		if (Looper.myLooper() == Looper.getMainLooper() && synchronous) {
			NetRemote.log(LogLevel.Error, ">>> MAIN THREAD Requesting: " + requestURL + " synchronous: " + true);
		} else {
			NetRemote.log(LogLevel.Info, ">>> Requesting: " + requestURL + " synchronous: " + synchronous);
		}

        if (synchronous) {
            XmlSyncNodeResponse xmlSyncNodeResponse = new XmlSyncNodeRequest().doRequest(requestURL, false, this);
            if (xmlSyncNodeResponse.document != null) {
                onSetMultipleNodesInfoResponse(xmlSyncNodeResponse.document, nodes, callback);
            } else {
                onSetMultipleNodesInfoError(xmlSyncNodeResponse.errorResponse, callback, nodes, requestURL);
            }

        } else {
            (new XmlAsyncNodeRequest()).enqueue(requestURL, false, this, new IXmlAsyncNodeResponse() {
                @Override
                public void onResponse(Document document) {
                    onSetMultipleNodesInfoResponse(document, nodes, callback);
                }

                @Override
                public void onErrorResponse(ErrorResponse errorResponse) {
                    onSetMultipleNodesInfoError(errorResponse, callback, nodes, requestURL);
                }
            });
        }
	}

	private void notifyInvalidPINError(NodeInfo[] nodes, ISetNodesCallback callback, String requestURL) {
		callback.onResult(getAllNodeErrors(nodes, getErrorResponseInvalidPIN(requestURL)));
	}

	void onSetMultipleNodesInfoResponse(Document response, NodeInfo[] nodes, ISetNodesCallback callback) {

		/* The doc should be like this:
			<fsapiSetMultipleResponse>

				<fsapiResponse>
					<node>netremote.sys.audio.mute</node>
					<status>FS_OK</status>
				</fsapiResponse>

				<fsapiResponse>
					<node>netremote.sys.net.keepconnected</node>
					<status>FS_OK</status>
				</fsapiResponse>

			</fsapiSetMultipleResponse>
		 */

        setDeviceIsAlive();

		mInvalidPIN = false;
		mNumTimeouts = 0;

		Map<Class, NodeResponse> nodeResponseMap = new HashMap<>();

		NodeList nodesXML = response.getElementsByTagName("fsapiResponse");
		if (nodesXML.getLength() > 0) {
			for (int i = 0; i < nodesXML.getLength(); i++) {
				Node nodeResponseXML = nodesXML.item(i);
				addNodeResponses(nodes, nodeResponseXML, nodeResponseMap);
			}
		} else {
			nodeResponseMap = null;
		}

		if (nodeResponseMap != null) {
			callback.onResult(nodeResponseMap);
		} else {
			callback.onResult(getAllNodeErrors(nodes, RadioHttpUtil.getNodeDoesNotExistError()));
		}
	}

	private void addNodeResponses(NodeInfo[] nodes, Node nodeResponseXML, Map<Class, NodeResponse> nodeResponseMap) {
		String responseNodeName = RadioHttpUtil.getNodeNameFromXML(nodeResponseXML);
		FSStatusCode status = RadioHttpUtil.getStatusCodeFromXML(nodeResponseXML);
		NodeInfo node = RadioHttpUtil.getNodeInfo(nodes, responseNodeName);

		if (node != null) {
			if (status == FSStatusCode.FS_OK) {
				NodeResponse nodeResponse = new NodeResponse();
				nodeResponse.mType = NodeResponse.NodeResponseType.NODE;
				nodeResponse.mNodeInfo = node;
				nodeResponseMap.put(node.getClass(), nodeResponse);
			} else {
				NetRemote.log(LogLevel.Error, "setNodesMultipleInternal Error " + responseNodeName + " status: " + status);
				nodeResponseMap.put(node.getClass(), getNodeResponseError(status));
			}
		}
	}

	void onSetMultipleNodesInfoError(ErrorResponse errorResponse, ISetNodesCallback callback, NodeInfo[] nodes, String requestURL) {
		NetRemote.log(LogLevel.Error, "Request error: " + errorResponse + " " + requestURL);

		callback.onResult(getAllNodeErrors(nodes, (NodeErrorResponse) errorResponse));
	}

	private String getSetMultipleUri(NodeInfo[] nodes, ISetNodesCallback callback) {
		String nodesQueryParams = "";

		for (int i = 0; i < nodes.length; i++) {
			NodeInfo node = nodes[i];

			/* Determine the "long" value of the node. */
			Object value;
			if (node instanceof NodeU8) {
				value = ((NodeU8) node).getValue();
			} else if (node instanceof NodeU16) {
				value = ((NodeU16) node).getValue();
			} else if (node instanceof NodeU32) {
				value = ((NodeU32) node).getValue();
			} else if (node instanceof NodeS8) {
				value = ((NodeS8) node).getValue();
			} else if (node instanceof NodeS16) {
				value = ((NodeS16) node).getValue();
			} else if (node instanceof NodeS32) {
				value = ((NodeS32) node).getValue();
			} else if (node instanceof NodeE8) {
				value = ((NodeE8) node).getValue();
			} else if (node instanceof NodeB32) {
				value = ((NodeB32) node).getValue();
			} else if (node instanceof NodeC) {
				value = URLEncoder.encode(((NodeC) node).getValue());
			} else if (node instanceof NodeU) {
				value = URLEncoder.encode(((NodeU) node).getValue());
			} else {
				/* We do not support the set operation on this type of node. */
				callback.onResult(getAllNodeErrors(nodes, new NodeErrorResponse(false, ErrorResponse.ErrorCode.UnsupportedOperation, null, null)));
				return null;
			}

			String nodeName = NodeDefs.Instance().GetNodeName(node.getClass());
			nodesQueryParams = nodesQueryParams + "node=" + nodeName + "&" + "value=" + value.toString() + "&";
		}

		/* Build something of the form: http://192.168.1.2/fsapi/SET_MULTIPLE?pin=1234&node=foo&value=0&node=bar&value=1 */
		return RadioHttpUtil.formAccessUrl(mApiBase.toString(), mPIN, "SET_MULTIPLE", null, nodesQueryParams);
	}



	/**
	 * Retrieve the friendly name of the radio.
	 *
	 * @return		The friendly name of the radio
	 */
	public String getFriendlyName() {
		/* Try to get the name from what was provided by SSDP. */
		if (mFriendlyName != null) {
			return mFriendlyName;
		}
		
		/* Or else just return a default string */
		return "WiFi radio";
	}

	/**
	 * Update the friendly name of the radio.
	 *
	 * @param newName		The new name
	 */
	public void setFriendlyName(String newName) {
		mFriendlyName = newName;
	}

	/**
	 * Retrieve a detailed description of the radio.
	 *
	 * @return		The detailed description
	 */
	public String getDetailDescription() {
		return ("Radio at " + mApiBase.getHost());
	}

    /**
     * Retrieve the IP of the radio.
     *
     * @return  IP address
     */
    public String getIpAddress() {
        return mApiBase.getHost();
    }

	@Override
	public String getDDXMLLocation() {
		return mDeviceInfoLocation;
	}

    public void setDDXMLLocation(String ddxmlLocation) {
        mDeviceInfoLocation = ddxmlLocation;
    }

	/**
	 * Get the serial number of the radio.
	 *
	 * @return		The serial number
	 */
	public String getSN() {
		return this.mSN;
	}

	/**
	 * Get the mUDN of the radio
	 *
	 * @return The mUDN
	 */
	public String getUDN() {
		return this.mUDN;
	}

	/**
	 *  Check if the radio is multiroom capable
	 *
	 * @return weather the radio has multiroom capabilities
	 */
	public boolean isMultiroomCapable() {
		return this.mIsMultiroomCapable;
	}

	/**
	 *  Get the multiroom ver. of the radio
	 *
	 * @return the multiroom ver. of the radio. Empty if multiroom not supported
	 */
	public String getMultiroomVersion() {
		return mMultiroomVersion;
	}

    /**
     * Check if radio has Single-Group variant of multiroom technology
     */
    @Override
    public boolean isSG() {
        return mIsSG;
    }

    /**
     * Check if radio has stereo capability
     */
    @Override
    public boolean isStereoCapable() {
        return mIsStereoCapable;
    }

    @Override
    public boolean hasGoogleCast() {
        return mHasGoogleCast;
    }

    /**
	 * Get the model name of the radio
	 *
	 * @return The model name
	 */
	public String getModelName() {
		return mModelName;
	}

	/**
	 * Get the model number of the radio
	 *
	 * @return The model number
	 */
	public String getModelNumber() {
		return mModelNumber;
	}


	/**
	 * Get the image URL of the radio
	 *
	 * @return The image URL (largest image, png mime type)
	 */
	public URI getImageURL() {
		return this.mImageURL;
	}

    @Override
    public String getFirmwareVersion() {
        return mFirmwareVersion;
    }

    /**
     * Speaker has Alexa Voice Service
     */
    @Override
    public boolean hasAVS() {
        return mHasAVS;
    }

    /**
	 * Determine whether or not the radio requires a mPIN to be accessed.
	 *
	 * @return			True if a mPIN is required
	 */
	public boolean requiresPIN() {
		return true;
	}

	public void pause() {
		if (this.mNotificationMonitor != null) {
			this.mNotificationMonitor.Pause();

            // notify
            for (IRadioNotificationState item: mNotificationState){
                item.onChange(true);
            }
		}
	}

	public void resume() {
		if (this.mNotificationMonitor != null) {
			this.mNotificationMonitor.Resume();

            // notify
            for (IRadioNotificationState item: mNotificationState){
                item.onChange(false);
            }
		}
	}

    public boolean addNotificationStateListener(IRadioNotificationState listener) {
        return !mNotificationState.contains(listener) && mNotificationState.add(listener);
    }

    public boolean removeNotificationStateListener(IRadioNotificationState listener) {
        return mNotificationState.remove(listener);
    }

	/**
	 * Get the current session ID of the connection with the radio. Useful for testing if the session has changed since
	 * last talking to the radio.
	 *
	 * @return		The current session ID
	 */
	public String getSessionId() {
		return mSessionId;
	}

	/**
	 *  Get the SSDP vendor ID of the radio
	 *
	 * @return the SSDP vendor ID of the radio
	 */
	@Override
	public String getVendorID() {
		return mVendorID;
	}

	public String getApiBase() {
		return mApiBase.toString();
	}

    @Override
    public int compareTo(Object another) {
        return compareToImpl((Radio) another);
    }

	@Override
	public boolean getIsMinuet() {
		return mIsMinuet;
	}

	@Override
	public boolean getIsVenice() {
		return mIsVenice;
	}
}
