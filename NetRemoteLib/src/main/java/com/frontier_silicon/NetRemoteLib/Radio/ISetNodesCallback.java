package com.frontier_silicon.NetRemoteLib.Radio;

import java.util.Map;

/**
 * Callback interface for writing multiple nodes.
 */
public interface ISetNodesCallback {
    void onResult(Map<Class, NodeResponse> nodeResponses);
}
