package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.LogLevel;

import org.w3c.dom.Document;

import java.io.IOException;
import java.net.SocketTimeoutException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by lsuhov on 18/01/2017.
 *
 * Class used to handle async node requests
 */

class XmlAsyncNodeRequest {

    void enqueue(final String requestURL, final boolean connectionRequest, final RadioHttp radioHttp,
                 final IXmlAsyncNodeResponse callback) {

        Request request = new Request.Builder().url(requestURL).build();

        NetRemote.getOkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                ErrorResponse errorResponse;

                e.printStackTrace();
                NetRemote.log(LogLevel.Error, "Request error: " + e + " " + requestURL);

                if (e instanceof SocketTimeoutException) {

                    errorResponse = RadioHttpUtil.getTimeoutError(connectionRequest, radioHttp);
                } else {
                    errorResponse = RadioHttpUtil.getGenericError(connectionRequest);
                }

                callback.onErrorResponse(errorResponse);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String content = response.body().string();
                    response.close();

                    RadioHttpUtil.sendToLog(requestURL, content);
                    Document document = RadioHttpUtil.getDocument(content);

                    if (document != null) {
                        callback.onResponse(document);
                    } else {
                        callback.onErrorResponse(RadioHttpUtil.getParseError(connectionRequest));
                    }
                } else {
                    int code = response.code();

                    NetRemote.log(LogLevel.Error, "Request error for " + requestURL + ", code " + code);

                    ErrorResponse errorResponse = RadioHttpUtil.getErrorBasedOnHttpCode(code, connectionRequest);
                    if (errorResponse.Transport.Error == TransportErrorDetail.ErrorCode.PinError) {
                        radioHttp.setInvalidPIN(true);
                    }

                    callback.onErrorResponse(errorResponse);
                }
            }
        });
    }
}
