package com.frontier_silicon.NetRemoteLib.Radio;

import java.util.Map;

/**
 * Callback interface for reading multiple nodes.
 */
public interface IGetNodesCallback {
    void onResult(Map<Class, NodeResponse> nodeResponses);
}
