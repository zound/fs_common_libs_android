package com.frontier_silicon.NetRemoteLib.Radio;

/**
 * This class is for reporting errors occurring during a connection attempt.
 */
public class ConnectionErrorResponse extends ErrorResponse {

    /**
     * Construct an error response.
     *
     * @param success			Whether or not the response represents a success or failure
     * @param error				The specific connection error code
     * @param fsStatus			The underlying FSAPI result code, or null if not known
     * @param transport			The underlying transport details, or null if not known
     */
    public ConnectionErrorResponse(boolean success, ErrorCode error, FSStatusCode fsStatus, TransportErrorDetail transport) {
        super(success, fsStatus, transport);
        this.Error = error;
    }

    /**
     * This is an overloaded variant of {@link #ConnectionErrorResponse(boolean, ErrorCode, FSStatusCode, TransportErrorDetail)},
     * which contains a full description of all parameters.
     */
    @SuppressWarnings("javadoc")
    public ConnectionErrorResponse(boolean success, ErrorCode error, TransportErrorDetail transport) {
        super(success, transport);
        this.Error = error;
    }

    /**
     * Whether or not this error response represent a success or failure.
     *
     * @return		True if operation was successful
     */
    public boolean getOverallSuccess() { return (Error == ErrorCode.OK); }

    /**
     * Get the error code describing this error.
     *
     * @return		The error code
     */
    public ErrorCode getErrorCode() { return this.Error; }
}
