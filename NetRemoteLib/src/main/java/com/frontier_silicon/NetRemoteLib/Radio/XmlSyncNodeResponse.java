package com.frontier_silicon.NetRemoteLib.Radio;

import org.w3c.dom.Document;

/**
 * Created by lsuhov on 18/01/2017.
 *
 * Used to keep sync node responses
 */

class XmlSyncNodeResponse {
    Document document;
    ErrorResponse errorResponse;
}
