package com.frontier_silicon.NetRemoteLib.Radio;

/**
 * A base class for returning errors.
 */
public class ErrorResponse {

    /**
     * An enumeration representing all possible types of write errors.
     */
    public enum ErrorCode {
        /**
         * The operation was successful.
         */
        OK,
        /**
         * A network time out occurred.
         */
        NetworkTimeout, /**
         * The session ID used for the operation was invalid.
         */
        InvalidSession, /**
         * The specific operation requested is not supported.
         */
        UnsupportedOperation, /**
         * An unspecified internal error occurred.
         */
        Error, /**
         * Tried to write to a read-only node.
         */
        NodeIsReadOnly, /**
         * Tried to write an invalid value to a node.
         */
        InvalidValue, /**
         * Tried to perform an operation while not connected to the radio.
         */
        NotConnected, /**
         * An unspecified network error occurred.
         */
        NetworkProblem, /**
         * A parsing error occurred while communicating with the radio.
         */
        ParseError, /**
         * Tried to read beyond the end of a list.
         */
        EndOfList, /**
         * An FSAPI error occurred.
         */
        FS_Error,
        /**
         * The radio is already connected - tried to connect twice.
         */
        AlreadyConnected,
    }

    /**
     * An enumeration representing FSAPI return codes.
     */
    public enum FSStatusCode {
        /**
         * Operation was successful.
         */
        FS_OK,

        /**
         * Operation failed for an unspecified reason.
         */
        FS_FAIL,

        /**
         * Request did not have correct parameters.
         */
        FS_PACKET_BAD,

        /**
         * The node name was not found.
         */
        FS_NODE_DOES_NOT_EXIST,

        /**
         * The list item was not found.
         */
        FS_ITEM_DOES_NOT_EXIST,

        /**
         * The operation timed out.
         */
        FS_TIMEOUT,

        /**
         * Tried to access beyond the end of a list.
         */
        FS_LIST_END,

        /**
         * Access to the requested node is blocked due to some other state.
         */
        FS_NODE_BLOCKED,

        /**
         * Unable to parse reply received from radio.
         */
        FS_PARSE_FAIL
    }

    boolean Success = false;
    FSStatusCode FSStatus = null;
    TransportErrorDetail Transport = null;
    ErrorCode Error;

    /**
     * Whether or not this error response represent a success or failure.
     *
     * @return		True if operation was successful
     */
    public boolean getOverallSuccess() { return this.Success; }

    /**
     * If available, get the underlying FSAPI result code.
     *
     * @return		The underlying FSAPI result code, or null
     */
    public FSStatusCode getFSStatus() { return this.FSStatus; }

    /**
     * If available, get the transport detail of the operation. Ie. in the case of a network attached
     * radio, get the HTTP transport details.
     *
     * @return		The transport details
     */
    public TransportErrorDetail getTransportDetail() { return this.Transport; }

    /**
     * Does the error represent a permanently failure. Ie. all future operations on the radio will fail,
     * and therefore the connection may as well be aborted.
     *
     * @return		True if the failure is permanent
     */
    public boolean isUnderlyingConnectionFatallyErrored() {
        return ((this.Transport != null) && (this.Transport.isUnderlyingConnectionFatallyErrored()));
    }

    /**
     * Construct a new error response.
     *
     * @param success		Whether or not the response represents a success or failure
     * @param fsStatus		The underlying FSAPI result code, or null if not known
     * @param transport		The underlying transport details, or null if not known
     */
    public ErrorResponse(boolean success, FSStatusCode fsStatus, TransportErrorDetail transport) {
        this.Success = success;
        this.FSStatus = fsStatus;
        this.Transport = transport;
    }

    /**
     * This is an overloaded variant of {@link ErrorResponse#ErrorResponse(boolean, FSStatusCode, TransportErrorDetail)}, which
     * contains a full description of all parameters.
     */
    @SuppressWarnings("javadoc")
    public ErrorResponse(boolean success, TransportErrorDetail transport) {
        this.Success = success;
        this.Transport = transport;
    }

    /**
     * Returns a human readable string representation of this error response.
     *
     * @return		The formatted string
     */
    public String toString() {
        if (this.Success) {
            return "Operation Successful";
        } else {
            String result = "";

            result += "OverallSuccess = " + this.getOverallSuccess();

            if (this.FSStatus != null) {
                if (result.length() > 0) { result += ", "; }
                result += "FSStatus = " + this.FSStatus.toString();
            }

            if (this.Transport != null) {
                if (result.length() > 0) { result += ", "; }
                result += "Transport status = " + this.Transport.toString();
            }

            return ("Error (" + result +")");
        }
    }
}
