package com.frontier_silicon.NetRemoteLib.Radio.nodeCommunication;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Radio.ISetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

/**
 * Created by lsuhov on 22/04/2017.
 */

public class NodeSyncSetter {

    private Radio mRadio;
    private NodeInfo mNodeInfo;
    public boolean mResult;
    public NodeErrorResponse mErrorResponse;

    public NodeSyncSetter(Radio radio, NodeInfo nodeInfo) {
        mRadio = radio;
        mNodeInfo = nodeInfo;
    }

    public boolean set() {
        if (mRadio == null) {
            dispose();
            mResult = false;

            return false;
        }

        mRadio.setNode(mNodeInfo, true, new ISetNodeCallback() {
            public void setNodeSuccess(NodeInfo node) {
                FsLogger.log("setNodeSuccess: " + node.getName() + " value= " + node.toString());
                mResult = true;
            }

            public void setNodeError(NodeInfo node, NodeErrorResponse error) {
                FsLogger.log("setNodeError: " + node.getName() + " err= " + error.toString(), LogLevel.Error);
                mResult = false;
                mErrorResponse = error;
            }
        });

        dispose();

        return mResult;
    }

    private void dispose() {
        mRadio = null;
        mNodeInfo = null;
    }
}
