package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;

public class NodeResponse {
    public enum NodeResponseType {
        NODE,
        ERROR
    }

    public NodeResponseType mType;

    public NodeInfo mNodeInfo;
    public NodeErrorResponse mError;
}
