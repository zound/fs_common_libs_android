package com.frontier_silicon.NetRemoteLib.Radio;

/**
 * Callback interface for a connection attempt to a radio.
 */
public interface IConnectionCallback {

    /**
     * This method will be called-back when a connection has been successfully made.
     *
     * @param radio		The radio
     */
    void onConnectionSuccess(Radio radio);

    /**
     * This method will be called-back if there is a problem connecting to the radio.
     *
     * @param radio		The radio
     * @param error		Details of the error
     */
    void onConnectionError(Radio radio, ConnectionErrorResponse error);

    /**
     * This method will be called-back if, after successfully connecting to a radio, the connection is
     * then later lost. This would occur, for example, if another device took over control of the radio.
     *
     * @param radio		The radio
     * @param error		Details of the error
     */
    void onDisconnected(Radio radio, ConnectionErrorResponse error);
}
