package com.frontier_silicon.NetRemoteLib.Radio;


import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.loggerlib.LogLevel;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class RadioHttpUtil {

    public static String formAccessUrl(String apiBase, String pin, String method, String sessionId, String extraQueryParameters) {
        String result;

        if (extraQueryParameters.length() > 0) {
            extraQueryParameters = ("&" + extraQueryParameters);
        }

        if (TextUtils.isEmpty(sessionId)) {
            result = apiBase + "/" + method + "?pin=" + pin + extraQueryParameters;
        } else {
            result = apiBase + "/" + method + "?pin=" + pin + "&sid=" + sessionId + extraQueryParameters;
        }

        return result;
    }

    public static String formAccessUrl(String apiBase, String pin, String method, String sessionId) {
        return formAccessUrl(apiBase, pin, method, sessionId, "");
    }

    public static URI formImageUrl(String apiBase, String iconURL) {
        URI result = null;
        try {
            if (iconURL != null) {
                result = new URI(apiBase + iconURL);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void sendToLog(String url, String data) {
        NetRemote.log(LogLevel.Info, "Response for " + url + "\n" + data);
    }

    public static Document getDocument(String response) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(response.getBytes("UTF-8")));
            return doc;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Document emptyDoc = db.newDocument();

        return emptyDoc;
    }

    public static Document getDocument(InputStream inputStream) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
            Document doc = db.parse(inputStream);
            return doc;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Document emptyDoc = db.newDocument();

        return emptyDoc;
    }

    public static ErrorResponse.FSStatusCode getStatus(Document doc) {
		/* Find the status node. */
        NodeList nodes = doc.getElementsByTagName("status");
        if (nodes.getLength() == 1) {
            try {
				/* Determine its value. */
                String value = nodes.item(0).getFirstChild().getNodeValue();
                ErrorResponse.FSStatusCode code = ErrorResponse.FSStatusCode.valueOf(value);
                return code;
            } catch (Exception e) {
            }
        }

        return ErrorResponse.FSStatusCode.FS_PARSE_FAIL;
    }

    public static ErrorResponse.FSStatusCode getStatus(Document doc, Boolean checkFsApiResponse) {
        if (checkFsApiResponse) {
            Node firstNode = doc.getFirstChild();
            if (firstNode != null) {
                if (!(firstNode.getNodeName().equals("fsapiResponse"))) {
                    return ErrorResponse.FSStatusCode.FS_PARSE_FAIL;
                }
            } else {
                return ErrorResponse.FSStatusCode.FS_PARSE_FAIL;
            }
        }

        return getStatus(doc);
    }

    public static ErrorResponse.FSStatusCode getStatusCodeFromXML(Node node) {
        ErrorResponse.FSStatusCode status = ErrorResponse.FSStatusCode.FS_PARSE_FAIL;

        NodeList children = node.getChildNodes();
        for (int idx = 0; idx < children.getLength(); idx++) {
            Node child = children.item(idx);

            String name = child.getNodeName();
            if (!TextUtils.isEmpty(name)) {
                if (name.equalsIgnoreCase("status")) {
                    try {
                        String statusStrValue = child.getFirstChild().getNodeValue();
                        status = ErrorResponse.FSStatusCode.valueOf(statusStrValue);
                    } catch (Exception e) {
                    }
                }
            }
        }

        return status;
    }

    public static String getNodeNameFromXML(Node node) {
        String nodeName = null;

        NodeList children = node.getChildNodes();
        for (int idx = 0; idx < children.getLength(); idx++) {
            Node child = children.item(idx);

            String name = child.getNodeName();
            if (!TextUtils.isEmpty(name)) {
                if (name.equalsIgnoreCase("node")) {
                    try {
                        nodeName = child.getFirstChild().getNodeValue();
                    } catch (Exception e) {
                    }
                }
            }
        }

        return nodeName;
    }

    public static Node getNodeValueFromXML(Node node) {
        Node value = null;

        NodeList children = node.getChildNodes();
        for (int idx = 0; idx < children.getLength(); idx++) {
            Node child = children.item(idx);

            String name = child.getNodeName();
            if (!TextUtils.isEmpty(name)) {
                if (name.equalsIgnoreCase("value")) {
                    value = child;
                }
            }
        }

        return value;
    }

    public static Class getNodeTypeClass(Class[] nodeTypes, String nodeName) {
        for (int i = 0; i < nodeTypes.length; i++) {
            if (!TextUtils.isEmpty(nodeName)) {
                if (nodeName.equalsIgnoreCase(NodeDefs.Instance().GetNodeName(nodeTypes[i]))) {
                    return nodeTypes[i];
                }
            }
        }

        return null;
    }

    public static NodeInfo getNodeInfo(NodeInfo[] nodes, String nodeName) {
        for (int i = 0; i < nodes.length; i++) {
            if (!TextUtils.isEmpty(nodeName)) {
                if (nodeName.equalsIgnoreCase(nodes[i].getName())) {
                    return nodes[i];
                }
            }
        }

        return null;
    }

    public static ErrorResponse getErrorBasedOnHttpCode(int httpStatusCode, boolean connectionRequest) {
        ErrorResponse error;
        ErrorResponse.ErrorCode errorCode = ErrorResponse.ErrorCode.NetworkProblem;
        TransportErrorDetail errorDetail = new TransportErrorDetail(TransportErrorDetail.ErrorCode.ConnectionError, false);

        if (httpStatusCode == HttpURLConnection.HTTP_FORBIDDEN) { //403
            errorDetail.Error = TransportErrorDetail.ErrorCode.PinError;
            errorDetail.UnderlyingConnectionFatallyErrored = true;
        } else if (httpStatusCode == HttpURLConnection.HTTP_NOT_FOUND) { //404
            errorDetail.Error = TransportErrorDetail.ErrorCode.InvalidSession;
            errorDetail.UnderlyingConnectionFatallyErrored = true;
            errorCode = ErrorResponse.ErrorCode.InvalidSession;
        }

        if (connectionRequest) {
            error = new ConnectionErrorResponse(false, errorCode, null, errorDetail);
        } else {
            error = new NodeErrorResponse(false, errorCode, null, errorDetail);
        }

        return error;
    }

    public static ErrorResponse getTimeoutError(boolean connectionRequest, RadioHttp radioHttp) {
        ErrorResponse errorResponse;

        if (connectionRequest) {
            TransportErrorDetail errorDetail = new TransportErrorDetail(TransportErrorDetail.ErrorCode.ConnectionError, true);

            errorResponse = new ConnectionErrorResponse(false, ErrorResponse.ErrorCode.NetworkTimeout, null, errorDetail);

        } else {
            errorResponse = new NodeErrorResponse(false, ErrorResponse.ErrorCode.NetworkTimeout,
                    null, radioHttp.getTimeoutTransportError());
        }

        return errorResponse;
    }

    public static ErrorResponse getNetworkError(boolean connectionRequest, RadioHttp radioHttp) {
        ErrorResponse errorResponse;

        if (connectionRequest) {
            TransportErrorDetail errorDetail = new TransportErrorDetail(TransportErrorDetail.ErrorCode.ConnectionError, true);

            errorResponse = new ConnectionErrorResponse(false, ErrorResponse.ErrorCode.NetworkProblem, null, errorDetail);

        } else {
            errorResponse = new NodeErrorResponse(false, ErrorResponse.ErrorCode.NetworkProblem, null, null);
        }

        return errorResponse;
    }

    public static ErrorResponse getGenericError(boolean connectionRequest) {
        ErrorResponse errorResponse;

        if (connectionRequest) {
            errorResponse = new ConnectionErrorResponse(false,
                    ConnectionErrorResponse.ErrorCode.Error, null, null);
        } else {
            errorResponse = new NodeErrorResponse(false, ErrorResponse.ErrorCode.Error, null, null);
        }

        return errorResponse;
    }

    public static ErrorResponse getParseError(boolean connectionRequest) {
        ErrorResponse errorResponse;

        if (connectionRequest) {
            errorResponse = new ConnectionErrorResponse(false,
                    ConnectionErrorResponse.ErrorCode.ParseError, null, null);
        } else {
            errorResponse = new NodeErrorResponse(false, ErrorResponse.ErrorCode.ParseError, null, null);
        }

        return errorResponse;
    }

    public static NodeErrorResponse getNodeDoesNotExistError() {
        return new NodeErrorResponse(false, ErrorResponse.ErrorCode.FS_Error, ErrorResponse.FSStatusCode.FS_NODE_DOES_NOT_EXIST, null);
    }
}
