package com.frontier_silicon.NetRemoteLib.Radio;


import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;

import java.util.HashMap;
import java.util.Map;

class NodeCacher {

    private final Map<Class, NodeInfo> NodeCache = new HashMap<>();
    private final Map<String, NodeInfo> ListNodeCache = new HashMap<>();

    protected NodeInfo GetCacheEntry(Class type) {
        NodeInfo result = null;

        /* Get the entry from the cache. */
        String node = NodeDefs.Instance().GetNodeName(type);
        Class nodeType = NodeDefs.Instance().GetNodeClass(node);

        NetRemote.assertTrue(nodeType != null);

        synchronized (this.NodeCache) {
            result = this.NodeCache.get(nodeType);
        }

        return result;
    }

    void updateCacheEntry(NodeInfo node) {
		/* Check it's cacheable. */
        NetRemote.assertTrue(node.IsCacheable() || node.IsNotifying());
        if (!(node.IsCacheable() || node.IsNotifying())) {
            return;
        }

		/* See if it's a base type, if it is we need to convert it because we always store full node types in the cache. */
        if (NodeDefs.IsBaseType(node.getClass())) {
            node = NodeDefs.Instance().ConvertBaseToNode(node);
        }

		/* Update the cache. */
        synchronized (this.NodeCache) {
            if (this.NodeCache.containsKey(node.getClass())) {
                this.NodeCache.remove(node.getClass());
            }

            this.NodeCache.put(node.getClass(), node);
        }
    }

    protected String GetListNodeCacheKey(Class type, String startKey, int maxItems) {
        return type.getName() + ":" + startKey + ":" + maxItems;
    }

    protected NodeInfo GetListNodesCacheEntry(String key) {
        NodeInfo result = null;

        synchronized (this.ListNodeCache) {
            result = this.ListNodeCache.get(key);
        }

        return result;
    }

    void updateListNodesCacheEntry(String key, NodeInfo node) {
		/* Check it's cacheable. */
        NetRemote.assertTrue(node.IsCacheable());
        if (!node.IsCacheable()) {
            return;
        }

		/* See if it's a base type, if it is we need to convert it because we always store full node types in the cache. */
        if (NodeDefs.IsBaseType(node.getClass())) {
            node = NodeDefs.Instance().ConvertBaseToNode(node);
        }

		/* Update the cache. */
        synchronized (this.ListNodeCache) {
            if (this.ListNodeCache.containsKey(key)) {
                this.ListNodeCache.remove(key);
            }

            this.ListNodeCache.put(key, node);
        }
    }

    private void clearListNodesCache() {
        synchronized (this.ListNodeCache) {
            this.ListNodeCache.clear();
        }
    }

    /**
     * Use this method to clear the internal cache of node values.
     */
    public void ClearNodeCache() {
        NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "Node cache cleared.");

        synchronized (this.NodeCache) {
            this.NodeCache.clear();
        }
    }

    /**
     * Use this method to clear the internal cache of node values, for specific nodes.

     * @param nodeTypes		The nodes to be cleared
     *
     */
    public void ClearNodeCache(Class[] nodeTypes) {
        synchronized (this.NodeCache) {
            for (Class type : nodeTypes) {
                if (this.NodeCache.containsKey(type)) {
                    this.NodeCache.remove(type);
                }
            }
        }
    }


}
