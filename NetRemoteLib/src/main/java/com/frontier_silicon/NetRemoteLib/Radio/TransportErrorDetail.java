package com.frontier_silicon.NetRemoteLib.Radio;

/**
 * A base class for conveying details about the transport layer of an operation.
 */
public class TransportErrorDetail {
    /**
     * An error code representing problems that can occur with operations.
     */
    public enum ErrorCode {
        /**
         * Couldn't connect to radio.
         */
        ConnectionError,

        /**
         * A protocol error happen in the transport layer.
         */
        ProtocolError,

        /**
         * A response could not be parsed.
         */
        ParseError,

        /**
         * The PIN number for the radio was incorrect.
         */
        PinError,

        /**
         * An unknown IO error occurred.
         */
        IOProblem,

        /**
         * The session ID for the operation was invalid - probably stolen by another controller.
         */
        InvalidSession,

        /**
         * Timeout on notification thread
         */
        NotificationTimeout
    }

    ErrorCode Error = null;
    boolean UnderlyingConnectionFatallyErrored = false;

    /**
     * Get the error code for this transport detail.
     *
     * @return		The error code
     */
    public ErrorCode getErrorCode() { return this.Error; }

    /**
     * Does the error represent a permanently failure. Ie. all future operations on the radio will fail,
     * and therefore the connection may as well be aborted.
     *
     * @return		True if the failure is permanent
     */
    public boolean isUnderlyingConnectionFatallyErrored() { return this.UnderlyingConnectionFatallyErrored; }

    TransportErrorDetail(ErrorCode errorCode, boolean fatal) {
        this.Error = errorCode;
        this.UnderlyingConnectionFatallyErrored = fatal;
    }

    /**
     * Returns a human readable string representation of this transport detail.
     *
     * @return		The formatted string
     */
    public String toString() {
        String result = "";

        result += "UnderlyingConnectionFatallyErrored = " + (this.isUnderlyingConnectionFatallyErrored()?"true":"false");

        if (this.getErrorCode() != null) {
            if (result.length() > 0) { result += ", "; }
            result += "ErrorCode = " + this.getErrorCode().toString();
        }

        return result;
    }
}
