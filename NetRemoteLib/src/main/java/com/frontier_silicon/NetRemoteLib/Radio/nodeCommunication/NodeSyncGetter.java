package com.frontier_silicon.NetRemoteLib.Radio.nodeCommunication;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

/**
 * Created by lsuhov on 22/11/2016.
 */

public class NodeSyncGetter {

    private Radio mRadio;
    private Class mNodeClass;
    private boolean mForceUncached;
    NodeInfo mResult;
    public NodeErrorResponse mErrorResponse;

    public NodeSyncGetter(Radio radio, Class nodeClass, boolean forceUncached) {
        mRadio = radio;
        mNodeClass = nodeClass;
        mForceUncached = forceUncached;
    }

    public NodeInfo get() {
        if (mRadio == null) {
            FsLogger.log("getNodeInfo: radio is null", LogLevel.Error);

            dispose();
            return null;
        }

        mRadio.getNode(mNodeClass, true, mForceUncached, new IGetNodeCallback() {

            @Override
            public void getNodeResult(NodeInfo node) {
                mResult = node;

                FsLogger.log("getNodeResult: " + node.getName() + " value= " + node.toString());
            }

            @Override
            public void getNodeError(Class nodeType, NodeErrorResponse error) {
                mResult = null;
                mErrorResponse = error;
                FsLogger.log("getNodeError: " + nodeType.toString() + " err= " + error.toString(), LogLevel.Error);
            }
        });

        NodeInfo result = mResult;

        dispose();

        return result;
    }

    private void dispose() {
        mRadio = null;
        mNodeClass = null;
        mResult = null;
    }
}
