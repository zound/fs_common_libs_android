package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;

/**
 * Callback interface for node notifications.
 */
public interface INodeNotificationCallback {
    /**
     * This callback is called whenever the given node has changed value.
     *
     * @param node		The node and its new value
     */
    void onNodeUpdated(NodeInfo node);
}

