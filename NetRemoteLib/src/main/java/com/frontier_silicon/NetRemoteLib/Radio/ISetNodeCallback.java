package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;

/**
 * Callback interface for setting the value of a node.
 */
public interface ISetNodeCallback {

    /**
     * This callback is called with the result of a successful write to a node.
     *
     * @param node		The node written and its value
     */
    void setNodeSuccess(NodeInfo node);

    /**
     * This callback is called if an error occurs while trying to write to a node.
     *
     * @param node		The node that failed to write
     * @param error		An error indication
     */
    void setNodeError(NodeInfo node, NodeErrorResponse error);
}
