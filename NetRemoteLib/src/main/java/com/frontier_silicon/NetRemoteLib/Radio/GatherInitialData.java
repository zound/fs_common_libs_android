package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavActionDabScan;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayFrequency;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoName;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoText;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayRepeat;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayScrobble;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayServiceIdsFmRdsPi;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayShuffle;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioMute;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioVolume;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsDabFreqList;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsEqPresets;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsVolumeSteps;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoFriendlyName;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysMode;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysPower;

import org.w3c.dom.Document;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

class GatherInitialData implements IGetNodeCallback {

    private Radio mRadio;
    private boolean mSynchronous = false;
    private boolean mIsOnMainThread = false;
    private IConnectionCallback Callback;

    GatherInitialData(Radio radio, boolean synchronous, boolean isOnMainThread, IConnectionCallback callback) {
        mRadio = radio;
        mSynchronous = synchronous;
        mIsOnMainThread = isOnMainThread;
        Callback = callback;
    }

    void Begin() {

        /* The underlying radio implementation connected, so now try to read the various bits of information we
         * always need to collect from a radio at start of day. */
        
        mRadio.getListNode(NodeSysCapsValidModes.class, "-1", NodeDefs.MaxItemsForList, mSynchronous, true, this);
    }

    private void Finished(final NodeErrorResponse error) {
        if ((error == null) || error.getOverallSuccess()) {
            mRadio.HaveWorkingConnection = true;

            /* Call the callback. */
            NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                @Override
                public void run() {
                    Callback.onConnectionSuccess(GatherInitialData.this.mRadio);
                }
            }, mIsOnMainThread);
        } else {
            mRadio.close();
            NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                @Override
                public void run() {
                    Callback.onConnectionError(GatherInitialData.this.mRadio,
                            (ConnectionErrorResponse) RadioHttpUtil.getGenericError(true));
                }
            }, mIsOnMainThread);

        }
    }

    @Override
    public void getNodeResult(NodeInfo node) {
        if (node.getClass().equals(NodeSysCapsValidModes.class)) {

            //Override selectable value for DMR making it true
            NodeSysCapsValidModes validModes = (NodeSysCapsValidModes)node;
            /* Remember this node - it never changes. */
            mRadio.mValidModes = validModes;

            mRadio.getListNode(NodeSysCapsEqPresets.class, mSynchronous, this);
        } else if (node.getClass().equals(NodeSysCapsEqPresets.class)) {

            // get just one node at first using GET_MULTIPLE => fix crash on IR 2.2
            mRadio.getNodes(new Class[]{NodeSysPower.class}, mSynchronous, new IGetNodesCallback() {
                @Override
                public void onResult(Map<Class, NodeResponse> nodeResponses) {
                    mRadio.getNodes(new Class[]{NodeSysMode.class, NodePlayInfoName.class, NodePlayInfoText.class, NodePlayStatus.class,
                            NodePlayRepeat.class, NodePlayShuffle.class, NodeSysCapsVolumeSteps.class,
                            NodeSysAudioVolume.class, NodeSysAudioMute.class
                    }, mSynchronous, true, new IGetNodesCallback() {
                        @Override
                        public void onResult(Map<Class, NodeResponse> nodeResponses) {
                            // This is the last node in our sequence, so finish
                            GatherInitialData.this.Finished(null);
                        }
                    });
                }
            });
        }
    }

    @Override
    public void getNodeError(Class nodeType, NodeErrorResponse error) {
        try {
            /* For faking list node results. */
            org.w3c.dom.Node fakeXml = null;
            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(new ByteArrayInputStream("<fakeXml/>".getBytes("UTF-8")));
                fakeXml = doc.getFirstChild();
            } catch (Exception e) {
            }

            /* It's ok for some nodes to fail on certain older devices. */
            if ((error != null) && (error.getFSStatus() == ErrorResponse.FSStatusCode.FS_NODE_DOES_NOT_EXIST)) {
                /* Pretend it was successfully read because we were only interested in whether or not it existed, which the
                   internals will have now noted. */
                if (nodeType.equals(NodePlayFrequency.class)) {
                    this.getNodeResult(new NodePlayFrequency(0L));
                    return;
                } else if (nodeType.equals(NodePlayServiceIdsFmRdsPi.class)) {
                    this.getNodeResult(new NodePlayServiceIdsFmRdsPi(0L));
                    return;
                } else if (nodeType.equals(NodePlayScrobble.class)) {
                    this.getNodeResult(new NodePlayScrobble(0L));
                    return;
                } else if (nodeType.equals(NodeSysInfoFriendlyName.class)) {
                    this.getNodeResult(new NodeSysInfoFriendlyName(""));
                    return;
                } else if (nodeType.equals(NodeNavActionDabScan.class)) {
                    this.getNodeResult(new NodeNavActionDabScan(0L));
                    return;
                } else if (nodeType.equals(NodeSysCapsDabFreqList.class)) {
                    this.getNodeResult(new NodeSysCapsDabFreqList(fakeXml));
                    return;
                } else if (nodeType.equals(NodeSysCapsEqPresets.class)) {
                    this.getNodeResult(new NodeSysCapsEqPresets(fakeXml));
                    return;
                }
            }

            /* On certain older devices, nodes can fail in this way too. But the framework doesn't spot that the node is not
               supported, so we have to help it out here. */
            if ((error != null) && (error.getFSStatus() == ErrorResponse.FSStatusCode.FS_FAIL)) {
                if (nodeType.equals(NodeSysCapsDabFreqList.class) || nodeType.equals(NodeSysCapsEqPresets.class)) {
						/* Add to nodes that do not exist. */
                    synchronized (mRadio.mNodesThatDoNotExist) {
                        if (!(mRadio.mNodesThatDoNotExist.contains(nodeType))) {
                            mRadio.mNodesThatDoNotExist.add(nodeType);
                        }
                    }

                    /* Continue gathering. */
                    NodeInfo node;
                    try {
                        Constructor con = nodeType.getConstructor(new Class[] {org.w3c.dom.Node.class});
                        Object[] args = new Object[] {fakeXml};
                        node = (NodeInfo)(con.newInstance(args));
                        this.getNodeResult(node);
                        return;
                    } catch (SecurityException e) {
                    } catch (NoSuchMethodException e) {
                    } catch (IllegalArgumentException e) {
                    } catch (InstantiationException e) {
                    } catch (IllegalAccessException e) {
                    } catch (InvocationTargetException e) {
                    }
                }
            }
        } catch (NodeDefs.NodeParseErrorException e) {
        }

        /* Report the failure. */
        this.Finished(error);
    }
}

