package com.frontier_silicon.NetRemoteLib.Radio;


/**
 * This class is for reporting errors occurring while trying to write the value of a node.
 */
public class NodeErrorResponse extends ErrorResponse {

    /**
     * Construct an error response.
     *
     * @param success			Whether or not the response represents a success or failure
     * @param error				The specific write error code
     * @param fsStatus			The underlying FSAPI result code, or null if not known
     * @param transport			The underlying transport details, or null if not known
     */
    public NodeErrorResponse(boolean success, ErrorCode error, FSStatusCode fsStatus, TransportErrorDetail transport) {
        super(success, fsStatus, transport);
        this.Error = error;
    }

    /**
     * Whether or not this error response represent a success or failure.
     *
     * @return		True if operation was successful
     */
    public boolean getOverallSuccess() { return (Error == ErrorResponse.ErrorCode.OK); }

    /**
     * Get the error code describing this error.
     *
     * @return		The error code
     */
    public ErrorCode getErrorCode() { return this.Error; }

}
