/******************************************************************************
 * Copyright 2012 by Frontier Silicon Ltd. All rights reserved.
 *
 * No part of this software, either material or conceptual may be copied or
 * distributed, transmitted, transcribed, stored in a retrieval system or
 * translated into any human or computer language in any form by any means,
 * electronic, mechanical, manual or otherwise, or disclosed to third parties
 * without the express written permission of Frontier Silicon Ltd.
 * 137 Euston Road, London, NW1 2AA.
 ******************************************************************************/
package com.frontier_silicon.NetRemoteLib.Radio;

import android.os.Looper;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.IRadioNotificationState;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.BaseSysCapsValidModes.ListItem;
import com.frontier_silicon.NetRemoteLib.Node.BaseSysPower.Ord;
import com.frontier_silicon.NetRemoteLib.Node.NodeC;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeInteger;
import com.frontier_silicon.NetRemoteLib.Node.NodeList;
import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysFactoryReset;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysMode;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysPower;
import com.frontier_silicon.NetRemoteLib.Node.NodeU;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse.FSStatusCode;
import com.frontier_silicon.NetRemoteLib.Radio.nodeCommunication.NodeSyncGetter;
import com.frontier_silicon.NetRemoteLib.Radio.nodeCommunication.NodeSyncSetter;
import com.frontier_silicon.NetRemoteLib.Radio.nodeCommunication.NodesSyncGetter;
import com.frontier_silicon.loggerlib.LogLevel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This base class is for accessing radios. It provides access to high-level information about the radio, and provides
 * mechanisms for connecting to the radio and to access it's nodes. It is an abstract base class, so does nothing by itself,
 * but should be used in conjunction with a derived type like, RadioHttp.
 */
public abstract class Radio implements Comparable {

	boolean mSupportsMultipleGetSet = true;

	private List<IConnectionCallback> mConnectionListeners = new CopyOnWriteArrayList<>();

	private List<INodeNotificationCallback> mNodeNotificationListeners = new CopyOnWriteArrayList<>();

	boolean HaveWorkingConnection = false;

	NodeCacher mNodeCacher = new NodeCacher();

	private boolean mIsAvailable = false;
	private boolean mIsCheckingAvailability = false;
	private final String MODE_DMR= "DMR";
    protected DeviceRecord mDeviceRecord;
    protected String mPIN = "1234";
    protected boolean mInvalidPIN = false;
    protected boolean mAllowRequestsForInvalidPIN = false;

    final List<Class> mNodesThatDoNotExist = new ArrayList<>();

    /**
	 * This method is used to read the value of a node. The class of the node to be read is provided, along with a callback
	 * to which the result should be sent. The operation can run synchronously or asynchronously.
     * This is useful, for example, to ensure that callbacks are called in a context from which it is safe to perform UI operations.
	 *
	 * Certain nodes from the radio can be cached locally, but the 'forceUncached' parameter can be used to indicate that
	 * you don't want the cache to be used.
	 *
	 * @param nodeType				The type of node to be read
	 * @param synchronous			Whether to perform a blocking operation, and wait for the result to be retrieved
	 * @param forceUncached		Whether to use or not the cache
	 * @param callback				The callback to be called
	 */
	public void getNode(final Class nodeType, final boolean synchronous, boolean forceUncached, final IGetNodeCallback callback) {

		/* Warn if the class is NodeList */
		if (NodeList.class.isAssignableFrom(nodeType)) {
			NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Warning, "Attempting to read list node " + nodeType.toString() + " with getNode method. Use getListNode and fetch your items in batches of 1-20 items.");
		}

		/* See if the node is in the cache. */
		if (!forceUncached) {
			final NodeInfo cachedEntry = mNodeCacher.GetCacheEntry(nodeType);
			if (cachedEntry != null && (!cachedEntry.IsNotifying() || isConnectionOk())) {
				NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getNode(" + nodeType.toString() + ") called, and value returned from cache. Value = " + cachedEntry.toString());

                callback.getNodeResult(cachedEntry);

				return;
			}
		}

        if (!isNodeSupported(nodeType)) {
            NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getNode(" + nodeType.toString() + ") called, but value returned FS_NODE_DOES_NOT_EXIST from cache");

            callback.getNodeError(nodeType, RadioHttpUtil.getNodeDoesNotExistError());

            return;
        }

        final boolean isOnMainThread = NetRemote.getMainThreadExecutor().isOnMainThread();

		/* Get the node. */
		Radio.this.getNodeInternal(nodeType, synchronous, new IGetNodeCallback() {
                    public void getNodeResult(final NodeInfo node) {
				/* Update the cache. */
				if (node.IsCacheable() || node.IsNotifying()) {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getNode(" + nodeType.toString() + ") called, value fetched successfully, and cache updated. Value = " + node.toString());
					mNodeCacher.updateCacheEntry(node);
				} else {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getNode(" + nodeType.toString() + ") called, value fetched successfully (cache NOT updated). Value = " + node.toString());
				}

				/* Call the callback. */
				NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.getNodeResult(node);
                    }
                }, isOnMainThread);
			}

			public void getNodeError(final Class nodeType, final NodeErrorResponse error) {

				if (!((error != null) && (error.getFSStatus() != null))) {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "getNode(" + nodeType.toString() + ") called but error occured.");
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "  error = " + error.toString());
				}

                processNodeErrorToSeeIfNodeDoesntExist(nodeType, error);

                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.getNodeError(nodeType, error);
                    }
                }, isOnMainThread);

                if (error.isUnderlyingConnectionFatallyErrored()) {
                    handleLostConnection(error);
                }
			}
		});

	}

	/**
	 * This is an overloaded variant of {@link #getNode(Class, boolean, boolean, IGetNodeCallback)}, which
	 * contains a full description of all parameters.
     *
     * forceUncached is set to false
	 */
	public void getNode(Class nodeType, boolean synchronous, IGetNodeCallback callback) {
        this.getNode(nodeType, synchronous, false, callback);
	}

    /**
     * This is an overloaded variant of {@link #getNode(Class, boolean, boolean, IGetNodeCallback)}, which
     * contains a full description of all parameters.
     *
     * synchronous and forceUncached are set to false
     */
    public void getNode(Class nodeType, IGetNodeCallback callback) {
        this.getNode(nodeType, false, false, callback);
    }

    /**
     * This method is used to read synchronously the value of a node. You can use this method in order
     * to eliminate the callback.
     *
     * Certain nodes from the radio can be cached locally, but the 'forceUncached' parameter can be used to indicate that
     * you don't want the cache to be used.
     *
     * @param nodeType				The type of node to be read
     * @param forceUncached		Whether to use or not the cache
     * @return NodeSyncGetter
     */
    public NodeSyncGetter getNodeSyncGetter(Class nodeType, boolean forceUncached) {
        return new NodeSyncGetter(Radio.this, nodeType, forceUncached);
    }

    /**
     * This is an overloaded variant of {@link #getNodeSyncGetter(Class, boolean)}, which
     * contains a full description of all parameters.
     *
     * forceUncached is set to false
     */
    public NodeSyncGetter getNodeSyncGetter(Class nodeClass) {
        return getNodeSyncGetter(nodeClass, false);
    }

    /**
	 * Often you need to read multiple nodes to gather several pieces of information. This method is a handy wrapper
	 * for {@link #getNode(Class, boolean, boolean, IGetNodeCallback)}, and allows multiple nodes to be
	 * read in one go. The types of nodes required should be passed in an array, and a callback is passed for the
	 * results.
	 *
	 * The operation can run synchronously or asynchronously, and the callback can be
	 * called in a specific context, specified by 'callbackContext'. This is useful, for example, to ensure that callbacks
	 * are called in a context from which it is safe to perform UI operations.
	 * @param nodeTypesToRequest        List of node types to be fetched
	 * @param nodeCachedResponses       Map with nodes that were found in the cache
     * @param synchronous               Whether to perform a blocking operation, and wait for the result to be retrieved
     * @param callback                  The callback to be called
     */

	private void getNodesSequential(final Class[] nodeTypesToRequest, final Map<Class, NodeResponse> nodeCachedResponses,
                                    final boolean synchronous, final IGetNodesCallback callback) {

        final class GetNodesSequentialHelper implements IGetNodeCallback {
			private final boolean mForceCallbackOnMainThread = NetRemote.getMainThreadExecutor().isOnMainThread();
			Map<Class, NodeResponse> mNodeResults = new HashMap<>();

			private Queue<Class> mNodeTypes;

			public GetNodesSequentialHelper() {
				/* Convert the array to a list for easier subsequent manipulation. */
				mNodeTypes = new LinkedList<>();
				Collections.addAll(mNodeTypes, nodeTypesToRequest);

                mNodeResults.putAll(nodeCachedResponses);

				/* Kick things off, by fetching the first node. */
				getNextNode();
			}

			private void getNextNode() {
				Class nodeType = mNodeTypes.poll();
				if (nodeType != null) {
					getNode(nodeType, synchronous, this);
				} else {
					/* no more nodes => notify listener result is ready */
					NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(mNodeResults);
                        }
                    }, mForceCallbackOnMainThread);
				}
			}

			@Override
			public void getNodeResult(NodeInfo node) {
				/* Add to results and on to next node. */
				if (mNodeResults.containsKey(node.getClass())) {
					NetRemote.log(LogLevel.Error, "getNodes() called with same node type specified more than once (which can't be returned in a map).");
				} else {
					NodeResponse nodeResponse = new NodeResponse();
					nodeResponse.mType = NodeResponse.NodeResponseType.NODE;
					nodeResponse.mNodeInfo = node;
					mNodeResults.put(node.getClass(), nodeResponse);
				}

				getNextNode();
			}

			@Override
			public void getNodeError(Class nodeType, NodeErrorResponse error) {
				/* Add error response to results and get the next node */
				NetRemote.log(LogLevel.Error, "getNodes() " + nodeType + " " + error);

				NodeResponse nodeResponse = new NodeResponse();
				nodeResponse.mType = NodeResponse.NodeResponseType.ERROR;
				nodeResponse.mError = error;
				mNodeResults.put(nodeType, nodeResponse);

				getNextNode();
			}
		}

		new GetNodesSequentialHelper();
	}

    /**
     ** Often you need to read multiple nodes to gather several pieces of information. This method allows multiple nodes to be
     * read in one go. The types of nodes required should be passed in an array, and a callback is passed for the
     * results. The nodes are requested via a single HTTP request (GET_MULTIPLE).
     *
     * The operation is run synchronously without the need of callbacks.
     *
     * @param nodeTypes			List of node types to be fetched
     * @param forceUncached     Whether to use or not the cache
     */
    public NodesSyncGetter getNodesSyncGetter(Class[] nodeTypes, boolean forceUncached) {
        return new NodesSyncGetter(Radio.this, nodeTypes, forceUncached);
    }

    /**
     * This is an overloaded variant of {@link #getNodesSyncGetter(Class[], boolean)}, which
     * contains a full description of all parameters.
     *
     * forceUncached is set to false
     */
    public NodesSyncGetter getNodesSyncGetter(Class[] nodeTypes) {
        return getNodesSyncGetter(nodeTypes, false);
    }

	/**
	 * This is an overloaded variant of {@link #getNodes(Class[], boolean, IGetNodesCallback)}, which
	 * contains a full description of all parameters.
	 */
	public void getNodes(Class[] nodeTypes, boolean synchronous, IGetNodesCallback callback) {
        this.getNodes(nodeTypes, synchronous, false, callback);
	}

	/**
	 * This is an overloaded variant of {@link #getNodes(Class[], boolean, IGetNodesCallback)}, which
	 * contains a full description of all parameters.
	 */
	public void getNodes(Class[] nodeTypes, IGetNodesCallback callback) {
        this.getNodes(nodeTypes, false, false, callback);
	}


	/**
	 ** Often you need to read multiple nodes to gather several pieces of information. This method allows multiple nodes to be
	 * read in one go. The types of nodes required should be passed in an array, and a callback is passed for the
	 * results. The nodes are requested via a single HTTP request (GET_MULTIPLE).
	 *
	 * The operation can run synchronously or asynchronously. This is useful, for example, to ensure that callbacks
	 * are called in a context from which it is safe to perform UI operations.
	 *
	 * @param nodeTypes			List of node types to be fetched
	 * @param synchronous		Whether to perform a blocking operation, and wait for the result to be retrieved
     * @param forceUncached		Whether to use or not the cache
	 * @param callback			The callback to be called
	 */
	public void getNodes(final Class[] nodeTypes, final boolean synchronous, final boolean forceUncached, final IGetNodesCallback callback) {

        final boolean forceCallbackOnMainThread = NetRemote.getMainThreadExecutor().isOnMainThread();

        /* Get cached nodes */
        List<Class> nodeTypesToRequest = new ArrayList<>();
        final Map<Class, NodeResponse> nodeCachedResponses = new HashMap<>();

        for (Class nodeType : nodeTypes) {
            final NodeInfo cachedEntry = mNodeCacher.GetCacheEntry(nodeType);
            if (cachedEntry != null && !forceUncached && (!cachedEntry.IsNotifying() || isConnectionOk())) {
                NetRemote.log(LogLevel.Info, "getNodes(" + nodeType.toString() + ") called, and value returned from cache. Value = " + cachedEntry.toString());

                NodeResponse cachedResponse = new NodeResponse();
                cachedResponse.mType = NodeResponse.NodeResponseType.NODE;
                cachedResponse.mNodeInfo = cachedEntry;
                nodeCachedResponses.put(nodeType, cachedResponse);

            } else if (!isNodeSupported(nodeType)) {
                NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getNodes(" + nodeType.toString() + ") called, but value returned FS_NODE_DOES_NOT_EXIST from cache");

                NodeResponse doesNotExistResponse = new NodeResponse();
                doesNotExistResponse.mType = NodeResponse.NodeResponseType.ERROR;
                doesNotExistResponse.mError = RadioHttpUtil.getNodeDoesNotExistError();

                nodeCachedResponses.put(nodeType, doesNotExistResponse);
            } else {
                nodeTypesToRequest.add(nodeType);
            }
        }

        if (nodeTypesToRequest.size() == 0) {
            NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                @Override
                public void run() {
                    callback.onResult(nodeCachedResponses);
                }
            }, forceCallbackOnMainThread);
            return;
        }

        final Class[] nodeTypesLeft = nodeTypesToRequest.toArray(new Class[nodeTypesToRequest.size()]);

		if (mSupportsMultipleGetSet) {

			/* Get the nodes. */
			Radio.this.getNodesMultipleInternal(nodeTypesLeft, synchronous, new IGetNodesCallback() {
				@Override
				public void onResult(final Map<Class, NodeResponse> nodeResponses) {
					debugPrintAllErrors(nodeResponses);

					if (checkMultipleGetSetSupported(nodeResponses)) {
                        updateNodeCache(nodeResponses);

                        nodeResponses.putAll(nodeCachedResponses);

                        NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(nodeResponses);
                            }
                        }, forceCallbackOnMainThread);

                        ErrorResponse fatalError = getFatalErrors(nodeResponses);
                        if (fatalError != null) {
                            Radio.this.handleLostConnection(fatalError);
                        }

					} else {
						NetRemote.log(LogLevel.Error, "getNodes multiple not supported! Using old method.");
						mSupportsMultipleGetSet = false;
						getNodesSequential(nodeTypesLeft, nodeCachedResponses, synchronous, callback);
					}
				}
			});
		} else {
			NetRemote.log(LogLevel.Error, "getNodes multiple not supported! Using old method.");
			getNodesSequential(nodeTypesLeft, nodeCachedResponses, synchronous, callback);
		}
	}


    /**
     * Needed by Device info retrieval component for Zound. This function doesn't fallback to sequential GET
     * if GET_MULTIPLE is not supported.
     * @param nodeTypes
     * @param synchronous
     * @param callback
     */
    public void getNodesMultiple(final Class[] nodeTypes, final boolean synchronous, final IGetNodesCallback callback) {

        final boolean forceCallbackOnMainThread = NetRemote.getMainThreadExecutor().isOnMainThread();

        /* Get the nodes. */
        Radio.this.getNodesMultipleInternal(nodeTypes, synchronous, new IGetNodesCallback() {
            @Override
            public void onResult(final Map<Class, NodeResponse> nodeResponses) {
                debugPrintAllErrors(nodeResponses);

                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onResult(nodeResponses);
                    }
                }, forceCallbackOnMainThread);

            }
        });
    }

	void debugPrintAllErrors(Map<Class, NodeResponse> nodeResponses) {
		Set<Class> keys = nodeResponses.keySet();
		for (Class key : keys) {
			NodeResponse nodeResponse = nodeResponses.get(key);
			if (nodeResponse.mType == NodeResponse.NodeResponseType.ERROR) {
				if (nodeResponse.mError.isUnderlyingConnectionFatallyErrored()) {
					if (!((nodeResponse.mError != null) && (nodeResponse.mError.getFSStatus() != null))) {
						NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "getNodesMultiple(" + key.toString() + ") called but error occured.");
						NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "  error = " + nodeResponse.mError.toString());
					}
				}
			}
		}
	}

	ErrorResponse getFatalErrors(Map<Class, NodeResponse> nodeResponses) {
		ErrorResponse fatalError = null;

		Collection<NodeResponse> responses = nodeResponses.values();
		for (NodeResponse nodeResponse : responses) {
			if (nodeResponse.mType == NodeResponse.NodeResponseType.ERROR) {
				if (nodeResponse.mError.isUnderlyingConnectionFatallyErrored()) {
					fatalError = nodeResponse.mError;
					break;
				}
			}
		}

		return fatalError;
	}

	boolean checkMultipleGetSetSupported(Map<Class, NodeResponse> nodeResponses) {
		boolean multipleGetSetSupported = true;

		Collection<NodeResponse> responses = nodeResponses.values();
		for (NodeResponse nodeResponse : responses) {
			if (nodeResponse.mType == NodeResponse.NodeResponseType.ERROR) {
				if (nodeResponse.mError.Transport != null && nodeResponse.mError.Transport.Error == TransportErrorDetail.ErrorCode.InvalidSession) {
					multipleGetSetSupported = false;
					break;
				}
			}
		}

		return multipleGetSetSupported;
	}

	/**
	 * Often you need to write multiple nodes in one go. This method is a handy wrapper
	 * for {@link #setNode(NodeInfo, boolean, ISetNodeCallback)}, and allows multiple nodes to be
	 * written in one go. The values of nodes to be written should be passed in an array, and a callback is passed for the
	 * results. The nodes will be written in sequence one after another.
	 *
	 * The operation can run synchronously or asynchronously, and the callback can be
	 * called in a specific context, specified by 'callbackContext'. This is useful, for example, to ensure that callbacks
	 * are called in a context from which it is safe to perform UI operations.
	 *
	 * @param nodes					List of node types to be written
	 * @param synchronous			Whether to perform a blocking operation, and wait for the result to be written
	 * @param callback				The callback to be called
	 */
    void setNodesSequential(final NodeInfo[] nodes, final boolean synchronous, final ISetNodesCallback callback) {
		final class SetNodesSequentialHelper implements ISetNodeCallback {

			private Queue<NodeInfo> mNodes;
			private Map<Class, NodeResponse> mNodeResults = new HashMap<>();
			private final boolean mForceCallbackOnMainThread;

			public SetNodesSequentialHelper(NodeInfo[] nodes) {
				/* Convert the array to a list for easier subsequent manipulation. */
				mNodes = new LinkedList<>();
				Collections.addAll(mNodes, nodes);

				mForceCallbackOnMainThread = NetRemote.getMainThreadExecutor().isOnMainThread();

				/* Kick things off, by fetching the first node. */
				setNextNode();
			}

			private void setNextNode() {
				NodeInfo node = mNodes.poll();
				if (node != null) {
					setNode(node, synchronous, this);
				} else {
					/* no more nodes => notify listener result is ready */
					NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(mNodeResults);
                        }
                    }, mForceCallbackOnMainThread);
				}
			}

			@Override
			public void setNodeSuccess(NodeInfo node) {
				/* Add to results and on to next node. */
				if (mNodeResults.containsKey(node.getClass())) {
					NetRemote.log(LogLevel.Error, "setNodes() called with same node type specified more than once (which can't be returned in a map).");
				} else {
					NodeResponse nodeResponse = new NodeResponse();
					nodeResponse.mType = NodeResponse.NodeResponseType.NODE;
					nodeResponse.mNodeInfo = node;
					mNodeResults.put(node.getClass(), nodeResponse);
				}

				/* On to next node. */
				setNextNode();
			}

			@Override
			public void setNodeError(final NodeInfo node, final NodeErrorResponse error) {
				/* Add error response to results and get the next node */
				NetRemote.log(LogLevel.Error, "setNodes() " + node.getClass() + " " + error);

				NodeResponse nodeResponse = new NodeResponse();
				nodeResponse.mType = NodeResponse.NodeResponseType.ERROR;
				nodeResponse.mError = error;
				mNodeResults.put(node.getClass(), nodeResponse);

				/* On to next node. */
				setNextNode();
			}
		}

		new SetNodesSequentialHelper(nodes);
	}


	protected Map<Class, NodeResponse> getAllNodeErrors(NodeInfo[] nodes, NodeErrorResponse error) {
		HashMap<Class, NodeResponse> errorResponses = new HashMap<>();

		for (NodeInfo node : nodes) {
			NodeResponse nodeResponse = new NodeResponse();
			nodeResponse.mType = NodeResponse.NodeResponseType.ERROR;
			nodeResponse.mError = error;
			errorResponses.put(node.getClass(), nodeResponse);
		}

		return errorResponses;
	}

	/**
	 * Often you need to write multiple nodes in one go. This method is a handy wrapper
	 * for {@link #setNode(NodeInfo, boolean, ISetNodeCallback)}, and allows multiple nodes to be
	 * written in one go. The values of nodes to be written should be passed in an array, and a callback is passed for the
	 * results. The nodes are written via a single HTTP request (SET_MULTIPLE).
	 *
	 * The operation can run synchronously or asynchronously. This is useful, for example, to ensure that callbacks
	 * are called in a context from which it is safe to perform UI operations.
	 *
	 * @param nodes					List of node types to be written
	 * @param synchronous			Whether to perform a blocking operation, and wait for the result to be written
	 * @param callback				The callback to be called
	 */
	public void setNodes(final NodeInfo[] nodes, final boolean synchronous, final ISetNodesCallback callback) {

		if (mSupportsMultipleGetSet) {
			/* Check we are connected. */
			/*if (!(this.mConnected)) {
				NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, com.frontier_silicon.loggerlib.LogLevel.Error, "SetNodesMultiple called but not connected.");
				callback.SetNodesError(nodes[0], new ISetNodeCallback.ErrorResponse(false, ISetNodeCallback.ErrorResponse.ErrorCode.NotConnected, null, null), userData);
			}*/

			final boolean forceCallbackOnMainThread = NetRemote.getMainThreadExecutor().isOnMainThread();

			for (int i = 0; i < nodes.length; i++) {
				final NodeInfo node = nodes[i];

				/* Check the node is not read-only. */
				if (node.IsReadOnly()) {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "SetNodesMultiple(" + node.getClass().toString() + ") called but node is read-only.");

                    callback.onResult(getAllNodeErrors(nodes, new NodeErrorResponse(false, ErrorResponse.ErrorCode.NodeIsReadOnly, null, null)));
				}

				/* Check that the node contents are sensible. */
				if (node instanceof NodeInteger) {
					NodeInteger ni = (NodeInteger) node;
					if ((ni.getValue() > ni.getMaxPossibleValue()) || (ni.getValue() < ni.getMinPossibleValue())) {
						NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "SetNodesMultiple(" + node.getClass().toString() + ") called but data is not valid.");

                        callback.onResult(getAllNodeErrors(nodes, new NodeErrorResponse(false, ErrorResponse.ErrorCode.InvalidValue, null, null)));
					}
				} else if (node instanceof NodeC) {
					NodeC nc = (NodeC) node;
					if (nc.getValue().length() > nc.getMaximumLength()) {
						NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "SetNodesMultiple(" + node.getClass().toString() + ") called but data is not valid.");

                        callback.onResult(getAllNodeErrors(nodes, new NodeErrorResponse(false, ErrorResponse.ErrorCode.InvalidValue, null, null)));
					}
				} else if (node instanceof NodeU) {
					NodeU nu = (NodeU) node;
					if (nu.getValue().length() > nu.getMaximumLength()) {
						NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "SetNodesMultiple(" + node.getClass().toString() + ") called but data is not valid.");

                        callback.onResult(getAllNodeErrors(nodes, new NodeErrorResponse(false, ErrorResponse.ErrorCode.InvalidValue, null, null)));
					}
				}

				/* Is it an enum type? */
				try {
					if (node.getClass().getMethod("getValueEnum") != null) {
						Method method = node.getClass().getMethod("getValueEnum");
						if (method.invoke(node) == null) {
							NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "SetNodesMultiple(" + node.getClass().toString() + ") called but data is not valid.");

                            callback.onResult(getAllNodeErrors(nodes, new NodeErrorResponse(false, ErrorResponse.ErrorCode.InvalidValue, null, null)));
						}
					}
				} catch (SecurityException | IllegalArgumentException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
				}
            }

			/* get nodes classes */
			Class[] nodeTypes = new Class[nodes.length];
			for (int i = 0; i < nodes.length; i++) {
				nodeTypes[i] = nodes.getClass();
			}

			/* Do the set. */
			Radio.this.setNodesMultipleInternal(nodes, synchronous, new ISetNodesCallback() {

				@Override
				public void onResult(final Map<Class, NodeResponse> nodeResponses) {
					debugPrintAllErrors(nodeResponses);

					if (checkMultipleGetSetSupported(nodeResponses)) {

                        updateNodeCache(nodeResponses);

                        NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(nodeResponses);
                            }
                        }, forceCallbackOnMainThread);

                        ErrorResponse fatalError = getFatalErrors(nodeResponses);
                        if (fatalError != null) {
                            Radio.this.handleLostConnection(fatalError);
                        }

					} else {
						NetRemote.log(LogLevel.Error, "setNodes multiple not supported! Using old method.");
						mSupportsMultipleGetSet = false;
						setNodesSequential(nodes, synchronous, callback);
					}
				}
			});
		} else {
			NetRemote.log(LogLevel.Error, "setNodes multiple not supported! Using old method.");
			setNodesSequential(nodes, synchronous, callback);
		}
	}

    /**
     * This is an overloaded variant of {@link #setNodes(NodeInfo[], boolean, ISetNodesCallback)}, which
     * contains a full description of all parameters.
     */
    public void setNodes(NodeInfo[] nodes, boolean synchronous) {
        setNodes(nodes, synchronous, new ISetNodesCallback() {
            @Override
            public void onResult(Map<Class, NodeResponse> nodeResponses) {}
        });
    }

	void updateNodeCache(Map<Class, NodeResponse> nodeResponses) {

        for (Map.Entry<Class, NodeResponse> pair : nodeResponses.entrySet()) {
            NodeResponse nodeResponse = pair.getValue();

            if (nodeResponse.mType == NodeResponse.NodeResponseType.NODE) {
				/* Update the cache. */
                if (nodeResponse.mNodeInfo.IsCacheable() || nodeResponse.mNodeInfo.IsNotifying()) {
                    NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "setNodes(" + nodeResponse.mNodeInfo.getClass().toString() + ") called, written successfully, and cache updated. Value = " + nodeResponse.mNodeInfo.toString());
                    mNodeCacher.updateCacheEntry(nodeResponse.mNodeInfo);
                } else {
                    NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "setNodes(" + nodeResponse.mNodeInfo.getClass().toString() + ") called, written successfully (cache NOT updated). Value = " + nodeResponse.mNodeInfo.toString());
                }
            } else {
                processNodeErrorToSeeIfNodeDoesntExist(pair.getKey(), nodeResponse.mError);
            }
        }
	}

	/**
	 * This method is used to read the value of a list node. The class of the list node to be read is provided, along with a callback
	 * to which the result should be sent. The operation can run synchronously or asynchronously.
	 *
	 * The 'startKey' for the first list item to be read should be specified, along with the maximum number of list items to be
	 * retrieved.
	 *
	 * @param nodeType				The type of node to be read
	 * @param startKey				The key of the first list item to be retrieved
	 * @param maxItems				The maximum number of list items to be retrieved
	 * @param synchronous			Whether to perform a blocking operation, and wait for the result to be retrieved
     * @param forceUncached		    Whether to use or not the cache
	 * @param callback				The callback to be called
	 */
	public void getListNode(final Class nodeType, final String startKey, final int maxItems, boolean synchronous, boolean forceUncached, final IGetNodeCallback callback) {

		final boolean forceCallbackOnMainThread = NetRemote.getMainThreadExecutor().isOnMainThread();

		if (!forceUncached) {
			/* See if the node is in the cache. */
			final NodeInfo cachedEntry = mNodeCacher.GetListNodesCacheEntry(mNodeCacher.GetListNodeCacheKey(nodeType, startKey, maxItems));
			if (cachedEntry != null && (!cachedEntry.IsNotifying() || isConnectionOk())) {
				NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getListNode(" + nodeType.toString() + ") called, and value returned from cache. Value = " + cachedEntry.toString());

				callback.getNodeResult(cachedEntry);

				return;
			}
		}

        if (!isNodeSupported(nodeType)) {
            NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getListNode(" + nodeType.toString() + ") called, but value returned from cache is does_not_exist");

            callback.getNodeError(nodeType, RadioHttpUtil.getNodeDoesNotExistError());

            return;
        }

		/* Get the list. */
		Radio.this.getListNodeInternal(nodeType, synchronous, startKey, maxItems, new IGetNodeCallback() {
			@Override
            public void getNodeResult(final NodeInfo node) {

				if (node instanceof NodeSysCapsValidModes) {
					for (NodeListItem item : ((NodeSysCapsValidModes) node).getEntries()) {
						NodeSysCapsValidModes.ListItem modeItem = (NodeSysCapsValidModes.ListItem) item;
						if ((modeItem).getId().equalsIgnoreCase(MODE_DMR)) {
							modeItem.setSelectable();
						}
					}
				}
				/* Update the cache. */
				if (node.IsCacheable()) {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getListNode(" + nodeType.toString() + ") called, value fetched successfully, and cache updated. Value = " + node.toString());
					mNodeCacher.updateListNodesCacheEntry(mNodeCacher.GetListNodeCacheKey(nodeType, startKey, maxItems), node);
				} else {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "getListNode(" + nodeType.toString() + ") called, value fetched successfully (cache NOT updated). Value = " + node.toString());
				}

				NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.getNodeResult(node);
                    }
                }, forceCallbackOnMainThread);
			}

			@Override
			public void getNodeError(final Class nodeType, final NodeErrorResponse error) {

				if (!((error != null) && (error.getFSStatus() != null))) {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "getListNode(" + nodeType.toString() + ") called but error occured.");
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "  error = " + error.toString());
				}

                Radio.this.processNodeErrorToSeeIfNodeDoesntExist(nodeType, error);
                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.getNodeError(nodeType, error);
                    }
                }, forceCallbackOnMainThread);

                if (error.isUnderlyingConnectionFatallyErrored()) {
                    Radio.this.handleLostConnection(error);
                }

			}
		});

	}

	/**
	 * This is an overloaded variant of {@link #getListNode(Class, String, int, boolean, IGetNodeCallback)}, which
	 * contains a full description of all parameters.
	 */
	public void getListNode(Class nodeType, String startKey, int maxItems, boolean synchronous, IGetNodeCallback callback) {
        this.getListNode(nodeType, startKey, maxItems, synchronous, false, callback);
	}

	/**
	 * This is an overloaded variant of {@link #getListNode(Class, String, int, boolean, IGetNodeCallback)}, which
	 * contains a full description of all parameters.
	 */
	public void getListNode(Class nodeType, IGetNodeCallback callback) {
        this.getListNode(nodeType, "-1", NodeDefs.MaxItemsForList, false, false, callback);
	}

	/**
	 * This is an overloaded variant of {@link #getListNode(Class, String, int, boolean, IGetNodeCallback)}, which
	 * contains a full description of all parameters.
	 */
	public void getListNode(Class nodeType, int maxItems, IGetNodeCallback callback) {
        this.getListNode(nodeType, "-1", maxItems, false, false, callback);
	}

	/**
	 * This is an overloaded variant of {@link #getListNode(Class, String, int, boolean, IGetNodeCallback)}, which
	 * contains a full description of all parameters.
	 */
	public void getListNode(Class nodeType, boolean synchronous, IGetNodeCallback callback) {
        this.getListNode(nodeType, "-1", NodeDefs.MaxItemsForList, synchronous, false, callback);
	}


	/**
	 * This method is used subsequent to calls of {@link #getListNode(Class, String, int, boolean, IGetNodeCallback)}, to
	 * retrieve further list items from the same list as a previous call. The original node (not class) should be passed so that
	 * further items can be retrieved starting at the end of the previous list, and the  maximum number of list items to be
	 * retrieved should also be specified.
	 *
	 * The operation can run synchronously or asynchronously. This is useful, for example, to ensure that callbacks
	 * are called in a context from which it is safe to perform UI operations.
	 *
	 * @param node					The original node, starting from which, further list items should be retrieved
	 * @param maxItems				The maximum number of list items to be retrieved in this operation
	 * @param synchronous			Whether to perform a blocking operation, and wait for the result to be retrieved
	 * @param callback				The callback to be called
	 */
	public void getListNodeNext(final NodeList node, int maxItems, boolean synchronous, final IGetNodeCallback callback) {
		/* Is this the end of the list? */
		if (node.IsEndOfList()) {
			NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "getListNodeNext(" + node.getClass().toString() + ") called but at end of list.");
			callback.getNodeError(node.getClass(), new NodeErrorResponse(false, ErrorResponse.ErrorCode.EndOfList, null, null));
		}

		/* Find out last key. */
		String key = "-1";

		NodeListItem lastEntry = (NodeListItem)(node.getLastItem());
		if (lastEntry != null) {
			key = lastEntry.getKey().toString();
		}

		/* Do the get. */
		this.getListNode(node.getClass(), key, maxItems, synchronous, false, callback);
	}

	/**
	 * This is an overloaded variant of {@link #getListNodeNext(NodeList, int, boolean, IGetNodeCallback)}, which
	 * contains a full description of all parameters.
	 */
	public void getListNodeNext(NodeList node, int maxItems, IGetNodeCallback callback) {
        this.getListNodeNext(node, maxItems, false, callback);
	}


	/**
	 * This method is used to set the value of a node. The value of the node to be written is provided, along with a callback
	 * to which the result should be sent. The operation can run synchronously or asynchronously. This is useful,
     * for example, to ensure that callbacks are called in a context from which it is safe to perform UI operations.
	 *
	 * Certain nodes can be cached locally, and the cache will be updated after a successful write operation.
	 *
	 * @param node					The node and value to be written
	 * @param synchronous			Whether to perform a blocking operation, and wait for the value to be written
	 * @param callback				The callback to be called
	 */
	public void setNode(final NodeInfo node, boolean synchronous, final ISetNodeCallback callback) {

		final boolean forceCallbackOnMainThread = NetRemote.getMainThreadExecutor().isOnMainThread();

		/* Check the node is not read-only. */
		if (node.IsReadOnly()) {
			NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "setNode(" + node.getClass().toString() + ") called but node is read-only.");

            callback.setNodeError(node, new NodeErrorResponse(false, ErrorResponse.ErrorCode.NodeIsReadOnly, null, null));
		}

		/* Check that the node contents are sensible. */
		if (node instanceof NodeInteger) {
			NodeInteger ni = (NodeInteger)node;
			if ((ni.getValue() > ni.getMaxPossibleValue()) || (ni.getValue() < ni.getMinPossibleValue())) {
				NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "setNode(" + node.getClass().toString() + ") called but data is not valid.");

                callback.setNodeError(node, new NodeErrorResponse(false, ErrorResponse.ErrorCode.InvalidValue, null, null));
			}
		} else if (node instanceof NodeC) {
			NodeC nc = (NodeC)node;
			if (nc.getValue().length() > nc.getMaximumLength()) {
				NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "setNode(" + node.getClass().toString() + ") called but data is not valid.");

                callback.setNodeError(node, new NodeErrorResponse(false, ErrorResponse.ErrorCode.InvalidValue, null, null));
			}
		} else if (node instanceof NodeU) {
			NodeU nu = (NodeU)node;
			if (nu.getValue().length() > nu.getMaximumLength()) {
				NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "setNode(" + node.getClass().toString() + ") called but data is not valid.");

                callback.setNodeError(node, new NodeErrorResponse(false, ErrorResponse.ErrorCode.InvalidValue, null, null));
			}
		}

		/* Is it an enum type? */
		try {
			if (node.getClass().getMethod("getValueEnum") != null) {
				Method method = node.getClass().getMethod("getValueEnum");
				if (method.invoke(node) == null) {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "setNode(" + node.getClass().toString() + ") called but data is not valid.");

                    callback.setNodeError(node, new NodeErrorResponse(false, ErrorResponse.ErrorCode.InvalidValue, null, null));
				}
			}
		} catch (SecurityException | InvocationTargetException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException e) {
		}

		/* Do the set. */
		setNodeInternal(node, synchronous, new ISetNodeCallback() {
			@Override
            public void setNodeSuccess(final NodeInfo node) {
				/* Update the cache. */
				if (node.IsCacheable() || node.IsNotifying()) {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "setNode(" + node.getClass().toString() + ") called, written successfully, and cache updated. Value = " + node.toString());
					mNodeCacher.updateCacheEntry(node);
				} else {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, "setNode(" + node.getClass().toString() + ") called, written successfully (cache NOT updated). Value = " + node.toString());
				}

				/* Call the callback. */
				NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.setNodeSuccess(node);
                    }
                }, forceCallbackOnMainThread);
			}

			@Override
			public void setNodeError(final NodeInfo node, final NodeErrorResponse error) {
				if (!((error != null) && (error.getFSStatus() != null))) {
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "setNode(" + node.getClass().toString() + ") called but error occured.");
					NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "  error = " + error.toString());
				}

                processNodeErrorToSeeIfNodeDoesntExist(node.getClass(), error);

                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.setNodeError(node, error);
                    }
                }, forceCallbackOnMainThread);

                if (error.isUnderlyingConnectionFatallyErrored()) {

                    NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error,
                            " error isUnderlyingConnectionFatallyErrored " + error.toString() +
                                    " when setting node " + node.getClass().toString());

                    handleLostConnection(error);
                } else {
                    NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error,
                            " error isNotUnderlyingConnectionFatallyErrored " + error.toString() +
                                    " when setting node " + node.getClass().toString());
                }

			}
		});

	}

	/**
	 * This is an overloaded variant of {@link #setNode(NodeInfo, boolean, ISetNodeCallback)}, which
	 * contains a full description of all parameters.
	 */
	public void setNode(NodeInfo node, ISetNodeCallback callback) {
        this.setNode(node, false, callback);
	}

    /**
     * This is an overloaded variant of {@link #setNode(NodeInfo, boolean, ISetNodeCallback)}, which
     * contains a full description of all parameters.
     *
     * This can be used when the result of SET is not important.
     */
    public void setNode(NodeInfo node, boolean synchronous) {
        this.setNode(node, synchronous, new ISetNodeCallback() {
            @Override
            public void setNodeSuccess(NodeInfo node) {}

            @Override
            public void setNodeError(NodeInfo node, NodeErrorResponse error) {}
        });
    }

    /**
     * Function that enables a sync set, while hiding the callback and returning the result of the SET
     * @param node to set
     * @return NodeSyncSetter - object on which you have call .set() in order to send the command to the speaker.
     */
    public NodeSyncSetter getNodeSyncSetter(NodeInfo node) {
        return new NodeSyncSetter(Radio.this, node);
    }

	/**
	 *  Add a node notification listener that will be called when a node is updated
	 *  (must be connected with session in order to recv notif)
	 *
	 * @param listener	node notification listener
	 */
	public void addNotificationListener(INodeNotificationCallback listener) {
        mNodeNotificationListeners.add(listener);
	}

	/**
	 *  Remove node notification listener
	 *
	 * @param listener	node notification listener
	 */
	public void removeNotificationListener(INodeNotificationCallback listener) {
        if (!mNodeNotificationListeners.remove(listener)) {
            NetRemote.log(LogLevel.Error, "Attempt to deregister unregistered callback " + listener);
        }
	}

	private void clearNotificationListeners() {
        mNodeNotificationListeners.clear();
	}


    /**
     * Helper function to set the mode of the radio using the predefined enumeration from the NodeSysCapsValidModes node.
     *
     * @param mode					The required mode
     * @param synchronous			Whether to perform a blocking operation, and wait for the value to be written
     */
	public void setMode(final NodeSysCapsValidModes.Mode mode, final boolean synchronous) {
        setMode(mode, synchronous, new ISetNodeCallback() {
            @Override
            public void setNodeSuccess(NodeInfo node) {
            }

            @Override
            public void setNodeError(NodeInfo node, NodeErrorResponse error) {
            }
        });
    }

	/**
	 * Helper function to set the mode of the radio using the predefined enumeration from the NodeSysCapsValidModes node.
	 *
	 * @param mode					The required mode
	 * @param synchronous			Whether to perform a blocking operation, and wait for the value to be written
	 * @param callback				The callback to be called
	 */
	public void setMode(final NodeSysCapsValidModes.Mode mode, final boolean synchronous, final ISetNodeCallback callback) {
		if (mValidModes != null) {
            setModeInternal(mode, synchronous, callback);
		} else {
			getValidModesListAsync(new IModesListRetrieverListener() {
                @Override
                public void onValidModesListRetrieved(NodeSysCapsValidModes validModesNode) {
                    if (validModesNode != null) {
                        setModeInternal(mode, synchronous, callback);
                    } else {
                        NetRemote.log(LogLevel.Error, "SetMode() called but cannot run because mValidModes is null.");
                        if (callback != null) {
                            callback.setNodeError(new NodeSysMode((long) 0), new NodeErrorResponse(false, ErrorResponse.ErrorCode.Error, null, null));
                        }
                    }
                }
            });
		}
	}

    private void setModeInternal(NodeSysCapsValidModes.Mode mode, boolean synchronous, final ISetNodeCallback callback) {
        final NodeSysMode setMode = mValidModes.GetNodeForSettingMode(mode);
        if (setMode != null) {
            this.setNode(setMode, synchronous, callback);
        } else {
            NetRemote.log(LogLevel.Error, "SetMode() called but cannot run because we didn't know about the requested mode.");
            if (callback != null) {
                callback.setNodeError(new NodeSysMode((long) 0), new NodeErrorResponse(false, ErrorResponse.ErrorCode.Error, null, null));
            }
        }
    }

    public interface IRadioModeCallback {
		void getCurrentMode(NodeSysCapsValidModes.Mode currentMode);
	}

	public interface IRadioModeAsListItemCallback {
		void getCurrentModeAsListItem(NodeSysCapsValidModes.ListItem currentMode);
	}

	/**
	 * Gets the current mode of the radio.
	 *
	 * @return		The current mode of the radio
	 */
	public void getMode(final IRadioModeCallback callback) {
		boolean synchronous = false;

		getNode(NodeSysMode.class, synchronous, new IGetNodeCallback() {
            @Override
			public void getNodeResult(final NodeInfo node) {
				if (mValidModes != null) {
					callback.getCurrentMode(mValidModes.GetModeAsEnum((NodeSysMode) node));
				} else {
					getValidModesListAsync(new IModesListRetrieverListener() {
                        @Override
                        public void onValidModesListRetrieved(NodeSysCapsValidModes validModesNode) {

                            if (node != null && mValidModes != null) {
                                callback.getCurrentMode(mValidModes.GetModeAsEnum((NodeSysMode) node));
                            } else {
                                callback.getCurrentMode(NodeSysCapsValidModes.Mode.UnknownMode);
                            }
                        }
                    });
				}
			}
            @Override
			public void getNodeError(Class nodeType, NodeErrorResponse error) {
				callback.getCurrentMode(NodeSysCapsValidModes.Mode.UnknownMode);
			}
		});

	}

    public NodeSysCapsValidModes.Mode getModeSync() {
        mGetModeResult = null;
        boolean synchronous = true;

        getNode(NodeSysMode.class, synchronous, new IGetNodeCallback() {
            public void getNodeResult(NodeInfo node) {
                if (mValidModes != null) {
                    mGetModeResult = mValidModes.GetModeAsEnum((NodeSysMode) node);
                } else {
                    getValidModesListAsync(null);
                }
            }

            public void getNodeError(Class nodeType, NodeErrorResponse error) {
            }
        });

        return mGetModeResult;
    }

	/**
	 * Gets the current mode of the radio as a NodeSysCapsValidModes.ListItem node.
	 */
	public void getModeAsListItemSync(final IRadioModeAsListItemCallback callback) {
		getModeListItemSync(callback, true);
	}
	public void getModeAsListItemAsync(final IRadioModeAsListItemCallback callback) {
		getModeListItem(callback, false);
	}

	private void getModeListItemSync(final IRadioModeAsListItemCallback callback, boolean synchronous) {
		getNode(NodeSysMode.class, synchronous, new IGetNodeCallback(){
			public void getNodeResult(final NodeInfo node) {
				if (mValidModes != null) {
					callback.getCurrentModeAsListItem((ListItem) mValidModes.GetModeAsListItem((NodeSysMode) node));
				} else {
					getValidModesListSync(new IModesListRetrieverListener() {
						@Override
						public void onValidModesListRetrieved(NodeSysCapsValidModes validModesNode) {
							if (validModesNode != null) {
								callback.getCurrentModeAsListItem((ListItem) mValidModes.GetModeAsListItem((NodeSysMode) node));
							} else {
								callback.getCurrentModeAsListItem(null);
							}
						}
					});
				}
			}

			public void getNodeError(Class nodeType, NodeErrorResponse error) {
				callback.getCurrentModeAsListItem(null);
			}
		});
	}

	private void getModeListItem(final IRadioModeAsListItemCallback callback, boolean synchronous) {
		getNode(NodeSysMode.class, synchronous, new IGetNodeCallback(){
			public void getNodeResult(final NodeInfo node) {
				if (mValidModes != null) {
					callback.getCurrentModeAsListItem((ListItem) mValidModes.GetModeAsListItem((NodeSysMode) node));
				} else {
					getValidModesListAsync(new IModesListRetrieverListener() {
                        @Override
                        public void onValidModesListRetrieved(NodeSysCapsValidModes validModesNode) {
                            if (validModesNode != null) {
                                callback.getCurrentModeAsListItem((ListItem) mValidModes.GetModeAsListItem((NodeSysMode) node));
                            } else {
                                callback.getCurrentModeAsListItem(null);
                            }
                        }
                    });
				}
			}

			public void getNodeError(Class nodeType, NodeErrorResponse error) {
				callback.getCurrentModeAsListItem(null);
			}
		});
	}

    /**
     * Function that returns a node list with all available modes on that speaker.
     * Be aware that if there is no active connection, a sync operation will be done on the network
     *
     * @return NodeSysCapsValidModes
     */
    public NodeSysCapsValidModes getValidModes() {
        if (mValidModes == null) {
            getValidModesListSync(new IModesListRetrieverListener() {
                @Override
                public void onValidModesListRetrieved(NodeSysCapsValidModes validModesNode) {
                    mValidModes = validModesNode;
                }
            });
        }
        return mValidModes;
    }


	private interface IModesListRetrieverListener {
        void onValidModesListRetrieved(NodeSysCapsValidModes validModesNode);
    }


	private void getValidModesListSync(final IModesListRetrieverListener listener) {
		if (mValidModes == null) {
			getListNode(NodeSysCapsValidModes.class, true, new IGetNodeCallback() {
				@Override
				public void getNodeResult(NodeInfo node) {
					mValidModes = (NodeSysCapsValidModes) node;
					if (listener != null) {
						listener.onValidModesListRetrieved(mValidModes);
					}
				}

				@Override
				public void getNodeError(Class nodeType, NodeErrorResponse error) {
					mValidModes = null;
					if (listener != null) {
						listener.onValidModesListRetrieved(null);
					}
				}
			});
		}
	}


	private void getValidModesListAsync(final IModesListRetrieverListener listener) {
		if (mValidModes == null) {
            getListNode(NodeSysCapsValidModes.class, false, new IGetNodeCallback() {
				@Override
				public void getNodeResult(NodeInfo node) {
					mValidModes = (NodeSysCapsValidModes) node;
                    if (listener != null) {
                        listener.onValidModesListRetrieved(mValidModes);
                    }
				}

				@Override
				public void getNodeError(Class nodeType, NodeErrorResponse error) {
					mValidModes = null;
                    if (listener != null) {
                        listener.onValidModesListRetrieved(null);
                    }
				}
			});
		}
	}

	/**
	 * See if the radio is in standby.
	 *
	 * @return			True if in standby.
	 */
	public boolean isInStandby() {
		/* The power node is read at start of day, and should be cached from then on (even if it changes). So
		   we can read it here. */
		NodeSysPower power = (NodeSysPower)(mNodeCacher.GetCacheEntry(NodeSysPower.class));
		if (power != null) {
			return (power.getValueEnum() == Ord.OFF);
		}

		return false;
	}

	public void clearNodeCache(Class[] nodeTypes) {
		mNodeCacher.ClearNodeCache(nodeTypes);
	}


	protected abstract void connectInternal(boolean synchronous, IConnectionCallback callback);

	/**
	 * Attempt a connection to the radio. "connect()" is called when first connecting to a radio, but is
	 * also called to reconnect to a radio if the connection has been lost.
	 * @param synchronous			Whether to perform a blocking operation, and wait for the operation to complete
	 * @param callback				The callback to be called
	 */
	public void connect(final boolean synchronous, final IConnectionCallback callback) {

		/* add connection listener */
		mConnectionListeners.add(callback);

		/* See if we're already connected. */
		if (this.HaveWorkingConnection) {
			callback.onConnectionSuccess(Radio.this);
		} else {
            final boolean isOnMainThread = Looper.myLooper() == Looper.getMainLooper();

			/* Try to do the connect. */
			this.connectInternal(synchronous, new IConnectionCallback() {
				public void onConnectionSuccess(final Radio radio) {
					setIsAvailable(true);
					GatherInitialData initialData = new GatherInitialData(radio, synchronous, isOnMainThread, callback);
					initialData.Begin();
				}

				public void onConnectionError(final Radio radio, final ConnectionErrorResponse error) {
					Radio.this.close();

                    NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onConnectionError(radio, error);
                        }
                    }, isOnMainThread);
				}

				public void onDisconnected(final Radio radio, final ConnectionErrorResponse error) {
					Radio.this.close();

                    NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onDisconnected(radio, error);
                        }
                    }, isOnMainThread);
				}
			});
		}

	}

    /**
     * This is an overloaded variant of {@link #connect(boolean, IConnectionCallback)}, which
     * contains a full description of all parameters.
     *
     * synchronous is set to false
     */
    public void connect(IConnectionCallback callback) {
        this.connect(false, callback);
    }


    /**
	 * Determine whether or not the connection to the radio is ok or if, for example, it has been lost.
	 *
	 * @return			True if the connection is ok
	 */
	public boolean isConnectionOk() {
		return HaveWorkingConnection;
	}

	protected void handleLostConnection(ErrorResponse error) {
        this.HaveWorkingConnection = false;

        NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error, "  Radio -  handleLostConnection");

        ConnectionErrorResponse connectionError;
        if (error == null) {
            connectionError = new ConnectionErrorResponse(false,
                    ErrorResponse.ErrorCode.Error, null);
        } else if (error instanceof ConnectionErrorResponse) {
            connectionError = (ConnectionErrorResponse)error;
        } else {
            connectionError = new ConnectionErrorResponse(false, error.Error, error.FSStatus, error.getTransportDetail());
        }

		if (error != null && error.getTransportDetail() != null) {
			NetRemote.log(NetRemote.TRACE_NODE_REQUESTS_BIT, LogLevel.Error,
					"  Radio -  handleLostConnection - we have: " + error.getTransportDetail().getErrorCode().name());
		}

        notifyConnectionListenersOnDisconnected(connectionError);

        this.close();
	}

	private void notifyConnectionListenersOnDisconnected(ConnectionErrorResponse error) {
		for (IConnectionCallback callback : mConnectionListeners) {
			callback.onDisconnected(Radio.this, error);
		}
	}

	/**
	 * Any class implementing Radio should implement this helper function to disconnect from a radio.
	 */
	protected abstract void disconnectInternal();


	public void close() {
		disconnectInternal();

		mNodeCacher.ClearNodeCache();

		clearConnectionListeners();

		clearNotificationListeners();

		HaveWorkingConnection = false;
	}

	private void clearConnectionListeners() {
		mConnectionListeners.clear();
	}


	protected abstract void getNodeInternal(Class nodeType, boolean synchronous, IGetNodeCallback callback);

	protected abstract void getListNodeInternal(Class nodeType, boolean synchronous, String startKey, int maxItems, IGetNodeCallback callback);

	protected abstract void getNodesMultipleInternal(Class nodeTypes[], boolean synchronous, IGetNodesCallback callback);


	protected abstract void setNodeInternal(NodeInfo node, boolean synchronous, ISetNodeCallback callback);

	protected abstract void setNodesMultipleInternal(final NodeInfo[] nodes, boolean synchronous, final ISetNodesCallback callback);


	/***********************
	 * Node notifications. *
	 ***********************/

    void updateNodeCache(NodeInfo node) {
        NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification received for node: " + node.getName() + " = " + node.toString());

        /* Update the cache. */
        mNodeCacher.updateCacheEntry(node);
    }

	void notifyOnNodeUpdated(final NodeInfo node) {

        NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
            @Override
            public void run() {
                for (INodeNotificationCallback callback : mNodeNotificationListeners) {
                    callback.onNodeUpdated(node);
                }
            }
        }, true);
	}

	/*********************
	 * Node availability *
	 *********************/

	/**
	 * Determine whether or not the radio supports the given node.
	 *
	 * @param nodeType		The type of node
	 * @return				True if the radio supports the node
	 */
	public boolean isNodeSupported(Class nodeType) {
		synchronized (this.mNodesThatDoNotExist) {
            return !this.mNodesThatDoNotExist.contains(nodeType) ||
                    NodeDefs.Instance().NodeThatNotExistShouldBeIgnored(nodeType);
        }
	}

	void processNodeErrorToSeeIfNodeDoesntExist(Class nodeType, NodeErrorResponse error)
	{
		nodeType = NodeDefs.Instance().GetNodeTypeFromBaseType(nodeType);

		if (nodeType != null && error != null && error.Success == false
                && error.FSStatus == FSStatusCode.FS_NODE_DOES_NOT_EXIST) {

            synchronized (this.mNodesThatDoNotExist) {
                if (!(this.mNodesThatDoNotExist.contains(nodeType))) {
                    this.mNodesThatDoNotExist.add(nodeType);
                }
            }
		}
	}

	/********************************
	 * Radio management facilities. *
	 ********************************/

	/**
	 * pause the radio, including all worker threads.
	 */
	abstract public void pause();

	/**
	 * resume the radio, and un-pause all worker threads.
	 */
	abstract public void resume();

    abstract public boolean addNotificationStateListener(IRadioNotificationState listener);

    abstract public boolean removeNotificationStateListener(IRadioNotificationState listener);

	/**
	 * Retrieve the friendly name of the radio.
	 *
	 * @return		The friendly name of the radio
	 */
	public abstract String getFriendlyName();

	/**
	 * Update the friendly name of the radio.
	 *
	 * @param newName		The new name
	 */
	public abstract void setFriendlyName(String newName);

	/**
	 * Check if radio type is minuet.
	 *
	 * @return	weather radio is minuet
	 */
	public abstract boolean getIsMinuet();

	/**
	 * Check if radio type is venice.
	 *
	 * @return	weather radio is venice
	 */
	public abstract boolean getIsVenice();

	/**
	 * Retrieve a detailed description of the radio.
	 *
	 * @return		The detailed description
	 */
	public abstract String getDetailDescription();

	/**
	 * Get the serial number of the radio.
	 *
	 * @return		The serial number
	 */
	public abstract String getSN();

	/**
	 * Get the UDN of the radio
	 *
	 * @return The UDN
	 */
	public abstract String getUDN();

	/**
	 *  Check if the radio is multiroom capable
	 *
	 * @return weather the radio has multiroom capabilities 
	 */
	public abstract boolean isMultiroomCapable();

	/**
	 *  Get the multiroom ver. of the radio
	 *
	 * @return the multiroom ver. of the radio. Empty if multiroom not supported
	 */
	public abstract String getMultiroomVersion();

    /**
     * Check if radio has Single-Group variant of multiroom technology
     */
    public abstract boolean isSG();

    /**
     * Check if radio has stereo capability
     */
    public abstract boolean isStereoCapable();

    /**
     * Check if Radio has Google Cast capability. This should be present in the list of modes too.
     */
    public abstract boolean hasGoogleCast();

    /**
	 *  Get the SSDP vendor ID of the radio
	 *
	 * @return the SSDP vendor ID of the radio
	 */
	public abstract String getVendorID();

	/**
	 *  Get the model name of the radio
	 *
	 * @return the model name of the radio
	 */
	public abstract String getModelName();

	/**
	 *  Get the model number of the radio
	 *
	 * @return the model number of the radio
	 */
	public abstract String getModelNumber();

	/**
	 *  Get the image URL of the radio
	 *
	 * @return the image URL of the radio
	 */
	public abstract URI getImageURL();

    /**
     * Get the firmware version of the speaker
     */
	public abstract String getFirmwareVersion();

    /**
     * Speaker has Alexa Voice Service
     */
    public abstract boolean hasAVS();

	/**
	 * Get the radio as a string.
	 *
	 * @return		The friendly name of the radio
	 */
	public String toString() {
		return this.getFriendlyName();
	}

	NodeSysCapsValidModes mValidModes = null;

	private NodeSysCapsValidModes.Mode mGetModeResult = null;

	/**
	 * Test whether or not two radio objects are equal (ie. represent the same radio).
	 *
	 * @return			True if they are the same radio
	 */
	public boolean equals(Object other) {
		if (other != null) {
			if (other instanceof Radio) {
				if (((Radio)other).getSN().equals(this.getSN())) {
					return true;
				}
			}
		}

		return false;
	}

    public boolean areContentsTheSame(Radio newRadio) {
        if (!equals(newRadio)) {
            return false;
        }
        if (!getFriendlyName().equals(newRadio.getFriendlyName())) {
            return false;
        }
        if (!getIpAddress().equals(newRadio.getIpAddress())) {
            return false;
        }

        return true;
    }

    /**
	 * Determine whether or not the radio requires a PIN to be accessed.
	 *
	 * @return			True if a PIN is required
	 */
	public abstract boolean requiresPIN();

	/**
	 * Get the current session ID of the connection with the radio. Useful for testing if the session has changed since
	 * last talking to the radio.
	 *
	 * @return		The current session ID
	 */
	public abstract String getSessionId();

	/**
	 * Retrieve the IP of the radio.
	 *
	 * @return  IP address
	 */
	public abstract String getIpAddress();

	/**
	 * Retrieve the location of dd.xml
	 * @return URL of dd.xml
     */
	public abstract String getDDXMLLocation();

    /**
     * Compares this object to the specified object to determine their relative
     * order.
     *
     * @param anotherRadio the object to compare to this instance.
     * @return a negative integer if this instance is less than {@code another};
     * a positive integer if this instance is greater than
     * {@code another}; 0 if this instance has the same order as
     * {@code another}.
     * @throws ClassCastException if {@code another} cannot be converted into something
     *                            comparable to {@code this} instance.
     */
    protected int compareToImpl(Radio anotherRadio) {
        String friendlyName = getFriendlyName();
        String anotherFriendlyName = anotherRadio.getFriendlyName();

        return friendlyName.compareToIgnoreCase(anotherFriendlyName);
    }

	/**
	 *  Resets radio to factory settings
	 */
	public void factoryReset(final ISetNodeCallback listener) {
		setNode(new NodeSysFactoryReset(NodeSysFactoryReset.Ord.RESET), new ISetNodeCallback() {
			@Override
			public void setNodeSuccess(NodeInfo node) {
				handleLostConnection(null);

				if (listener != null) {
					listener.setNodeSuccess(node);
				}
			}

			@Override
			public void setNodeError(NodeInfo node, NodeErrorResponse error) {
				if (listener != null) {
					listener.setNodeError(node, error);
				}
			}
		});
	}

	public boolean isAvailable() {
		return mIsAvailable;
	}

	public void setIsAvailable(boolean isAvailable) {
		mIsAvailable = isAvailable;
	}

	public boolean isCheckingAvailability() {
		return mIsCheckingAvailability;
	}

	public void setIsCheckingAvailability(boolean isChecking) {
		mIsCheckingAvailability = isChecking;
	}

    public boolean isMissing() {
        return !isAvailable() && !isCheckingAvailability();
    }

    /**
     * Used to signal that the speaker is active and doesn't need pinging
     */
    public void setDeviceIsAlive() {
        if (mDeviceRecord != null) {
            mDeviceRecord.refreshTimeStamp();
        }
        setIsAvailable(true);
    }

    public void storeDeviceRecord(DeviceRecord deviceRecord) {
        mDeviceRecord = deviceRecord;
    }

    public DeviceRecord getDeviceRecord() {
        return mDeviceRecord;
    }

    public abstract void applyDataFromDeviceRecord(DeviceRecord record);

    /**
     * Get the current value of the mPIN.
     *
     * @return the mPIN
     */
    public String getPIN() {
        return mPIN;
    }

    /**
     * Set the value of the mPIN.
     *
     * @param pin		The mPIN to set
     */
    public void setPIN(String pin) {
        mPIN = pin;
        mInvalidPIN = false;
    }

    /**
     * Function that can be used to set if pin is valid or not
     * @param invalidPIN
     */
    protected void setInvalidPIN(boolean invalidPIN) {
        mInvalidPIN = invalidPIN;
    }

    /**
     * Set to true if requests to radio need to be made even if the mPIN was detected as invalid
     * @param allowRequests
     */
    public void allowRequestsForInvalidPIN(boolean allowRequests) {
        mAllowRequestsForInvalidPIN = allowRequests;
    }
}


