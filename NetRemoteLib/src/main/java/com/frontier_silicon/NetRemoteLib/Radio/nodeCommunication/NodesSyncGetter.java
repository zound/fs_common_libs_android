package com.frontier_silicon.NetRemoteLib.Radio.nodeCommunication;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodesCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.NodeResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lsuhov on 13/12/2016.
 */

public class NodesSyncGetter {
    private Radio mRadio;
    private Class[] mNodes;
    private boolean mForceUncached;
    Map<Class, NodeInfo> mGenericNodesMap = new HashMap<>();
    public Map<Class, NodeErrorResponse> nodesErrorMap = new HashMap<>();

    public NodesSyncGetter(Radio radio, Class[] nodes, boolean forceUncached) {
        mRadio = radio;
        mNodes = nodes;
        mForceUncached = forceUncached;
    }

    public Map<Class, NodeInfo> get() {
        if (mRadio == null) {
            FsLogger.log("get: radio is null", LogLevel.Error);

            dispose();
            return null;
        }

        mRadio.getNodes(mNodes, true, mForceUncached, new IGetNodesCallback() {

            @Override
            public void onResult(Map<Class, NodeResponse> nodeResponses) {


                for (Class key : nodeResponses.keySet()) {
                    NodeResponse response = nodeResponses.get(key);
                    if (response.mType == NodeResponse.NodeResponseType.NODE) {
                        mGenericNodesMap.put(key, response.mNodeInfo);
                    } else { //is error
                        nodesErrorMap.put(key, response.mError);
                        FsLogger.log("GetNodesResult: Error " + key + " " + response.mError, LogLevel.Error);
                    }
                }

                for (NodeInfo node : mGenericNodesMap.values()) {
                    FsLogger.log("GetNodesResult: " + node.getName() + " value= " + node.toString());
                }
            }
        });

        Map<Class, NodeInfo> nodes = mGenericNodesMap;

        dispose();

        return nodes;
    }

    private void dispose() {
        mRadio = null;
        mNodes = null;
        mGenericNodesMap = null;
    }
}
