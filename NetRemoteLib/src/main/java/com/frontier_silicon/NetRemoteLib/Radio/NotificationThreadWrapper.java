package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.Discovery.ping.PingUtil;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.utils.MutableBoolean;
import com.frontier_silicon.loggerlib.LogLevel;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Request;
import okhttp3.Response;

class NotificationThreadWrapper implements Runnable {

    private final String mApiBase;
    private final String mPIN;
    private final String mSessionId;
    private Thread OurThread = null;
    private boolean Aborted = false;
    private RadioHttp mRadio = null;

    synchronized public void Cancel() {
        this.Aborted = true;

        if (this.OurThread != null) {
            NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, LogLevel.Info, "Cancelling NetRemoteLib Notification Monitor.");
            this.Aborted = true;
            this.Resume();
            this.OurThread.interrupt();
            this.OurThread = null;
        }
    }

    private MutableBoolean Paused = new MutableBoolean(false);

    public void Pause() {
        NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification monitor pausing.");
        synchronized (this.Paused) {
            NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification monitor paused.");

            this.Paused.setTrue();
            this.Paused.notify();

            NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification monitor really paused.");
        }
    }

    public void Resume() {
        NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification monitor resuming.");
        synchronized (this.Paused) {
            NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification monitor resumed.");

            this.Paused.setFalse();
            this.Paused.notify();

            NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification monitor really resumed.");
        }
    }

    private void CheckPaused() {
        /* See if we've been paused? */
        synchronized (this.Paused) {
            while (this.Paused.getValue()) {
                try {
                    this.Paused.wait();
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
    }

    public NotificationThreadWrapper(RadioHttp radio) {
        mRadio = radio;
        mApiBase = radio.getApiBase();
        mPIN = radio.getPIN();
        mSessionId = radio.getSessionId();

        this.OurThread = new Thread(this, "NetRemoteLib Notification Monitor");
        this.OurThread.start();
    }

    public void run() {
        Thread.currentThread().setName("NetRemoteLib Notification Monitor");
        NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification monitor started.");

        while (!(this.Aborted)) {

            /* See if we've been paused? */
            this.CheckPaused();

            /* Build something of the form: http://192.168.1.2/fsapi/GET_NOTIFIES?pin=1234&sid=12345678 */
            String url = RadioHttpUtil.formAccessUrl(mApiBase, mPIN, "GET_NOTIFIES", mSessionId);

            NetRemote.log(LogLevel.Info, ">>>>> START NOTIF URL: " + url);

            Request request = new Request.Builder().url(url).build();

            Document document = null;
            ConnectionErrorResponse errorResponse = null;

            try {
                Response response = NetRemote.getOkHttpClientForNotifications().newCall(request).execute();

                if (response.isSuccessful()) {
                    String message = response.body().string();
                    //releasing resource due to a strict mode warning
                    response.close();

                    RadioHttpUtil.sendToLog(url, message);

                    document = RadioHttpUtil.getDocument(message);
                } else {
                    int code = response.code();
                    NetRemote.log(LogLevel.Error, "Notification error, code " + code);

                    errorResponse = (ConnectionErrorResponse) RadioHttpUtil.getErrorBasedOnHttpCode(code, true);
                }
            } catch (SocketTimeoutException e) {
                e.printStackTrace();

                if (new PingUtil().isLocationAvailable(mApiBase)) {
                    NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Node timeout. Retry ping success.");
                    mRadio.setDeviceIsAlive();
                } else {
                    NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Node timeout. Retry ping failed");
                    errorResponse = new ConnectionErrorResponse(false, ErrorResponse.ErrorCode.NetworkTimeout, null,
                            new TransportErrorDetail(TransportErrorDetail.ErrorCode.NotificationTimeout, true));
                }
            } catch (IOException e) {
                e.printStackTrace();

                errorResponse = (ConnectionErrorResponse) RadioHttpUtil.getGenericError(true);
            }

            /* Test if we've been cancelled. */
            if (this.Aborted) {
                NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification handler cancelled.");
                break;
            }

            /* See if we've been paused? */
            this.CheckPaused();

            /* Parse notif. response */
            if (document != null) {
                ErrorResponse.FSStatusCode status = RadioHttpUtil.getStatus(document, true);
                if (status == ErrorResponse.FSStatusCode.FS_OK) {
                    notifyNodeListeners(document);
                    mRadio.setDeviceIsAlive();
                } else if (status == ErrorResponse.FSStatusCode.FS_TIMEOUT) {
                    /* Timeout is ok, just try again. */
                    mRadio.setDeviceIsAlive();
                } else {
                    NetRemote.log(LogLevel.Error, "Notification received status that was not OK? " + status);
                }
            } else {
                if (errorResponse != null) {
                    NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, LogLevel.Warning, "Notification handler failed due to socket timeout, so stopping.");
                    mRadio.handleLostConnection(errorResponse);
                    break;
                }
            }
        }

        NetRemote.log(NetRemote.TRACE_NOTIFICATIONS_BIT, "Notification monitor ended.");
    }

    private void notifyNodeListeners(Document doc) {
        List<NodeInfo> nodeInfoList = new ArrayList<>();
        /* Now find all the notify nodes. */
        NodeList nodes = doc.getElementsByTagName("notify");
        for (int loop = 0; loop < nodes.getLength(); loop += 1) {
            Node notifyNode = nodes.item(loop);

            /* Get the name of the node. */
            Node nameNode = notifyNode.getAttributes().getNamedItem("node");
            if (nameNode != null) {
                String name = nameNode.getNodeValue();
                NetRemote.assertTrue(name != null);

                /* See if the node name is one that we understand. */
                Class nodeType = NodeDefs.Instance().GetNodeClass(name);
                if (nodeType != null) {

                    /* Extract the value node. */
                    Node valueNode = notifyNode.getFirstChild();
                    if ((valueNode != null) && (valueNode.getNodeName().equals("value"))) {
                        NodeInfo result = NodeDefs.GetNodeFromXml(nodeType, valueNode);
                        if (result != null) {
                            mRadio.updateNodeCache(result);
                            nodeInfoList.add(result);
                        } else {
                            NetRemote.log(LogLevel.Error, "Notification received corrupted message - could not parse node value for node with name = " + name + ".");
                        }
                    } else {
                        NetRemote.log(LogLevel.Error, "Notification received corrupted message (no value node inside notify node).");
                    }
                } else {
                    NetRemote.log(LogLevel.Error, "Notification received message for unknown node (" + name + ").");
                }
            } else {
                NetRemote.log(LogLevel.Error, "Notification received corrupted message (no node name attribute).");
            }
        }

        for (int i = 0; i < nodeInfoList.size(); i++) {
            mRadio.notifyOnNodeUpdated(nodeInfoList.get(i));
        }
        nodeInfoList.clear();
    }
}
