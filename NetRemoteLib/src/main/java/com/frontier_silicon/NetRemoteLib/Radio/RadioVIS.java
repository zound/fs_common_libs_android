/******************************************************************************
 * Copyright 2012 by Frontier Silicon Ltd. All rights reserved.
 *
 * No part of this software, either material or conceptual may be copied or
 * distributed, transmitted, transcribed, stored in a retrieval system or
 * translated into any human or computer language in any form by any means,
 * electronic, mechanical, manual or otherwise, or disclosed to third parties
 * without the express written permission of Frontier Silicon Ltd.
 * 137 Euston Road, London, NW1 2AA.
 ******************************************************************************/
package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.EventHelper;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayFrequency;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoName;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayServiceIdsDabEnsembleId;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayServiceIdsDabScids;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayServiceIdsDabServiceId;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayServiceIdsEcc;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayServiceIdsFmRdsPi;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes.Mode;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysMode;
import com.frontier_silicon.NetRemoteLib.utils.MutableBoolean;
import com.frontier_silicon.loggerlib.LogLevel;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


/**
 * This helper class connects to a radio and provides RadioVIS images and URLs for the current programme. 
 */
@SuppressWarnings("rawtypes")
public class RadioVIS implements IConnectionCallback, IGetNodesCallback, INodeNotificationCallback {
	private Radio TheRadio;
	private Boolean Enabled = false;
	private String PreviousInfoName = null;
	private String Frequency;
	private int ECC;
	private int PiCode;
	private String CountryCode;
	private int AutoCountryPos;
	private int AutoCountryECC;
	private int DabServId;
	private int DabEnsId;
	private int DabScIds;
	private String DnsFQDN;
	
	/**
	 * Construct a new RadioVIS helper object.
	 * 
	 * @param radio		The radio to be monitored
	 */
	public RadioVIS(Radio radio) {
		this.TheRadio = radio;	
		this.setEnabled(true, null);
	}

	/**
	 * Construct a new RadioVIS helper object, specifying the PIN access code for the radio.
	 * 
	 * @param radio		The radio to be monitored
	 * @param pin		The PIN for the radio
	 */
	public RadioVIS(Radio radio, String pin) {
		this.TheRadio = radio;
		this.TheRadio.setPIN(pin);
		this.setEnabled(true, null);
	}
	
	/**
	 * Get the radio being used by the RadioVIS helper.
	 * 
	 * @return		The radio object
	 */
	public Radio getRadio() {
		return this.TheRadio;
	}
	
	/**
	 * close this RadioVIS helper object.
	 */
	public synchronized void close() {
		/* Disable the main client. */
		this.resume();
		this.setEnabled(false, Callback.ErrorCode.Closed);
		this.TheRadio = null;
	}

	public void setRadio(Radio radio) {
		this.TheRadio = radio;
	}

	/**
	 * Callback interface for events from RadioVIS. 
	 */
	public interface Callback {
		/**
		 * This class is for reporting errors occurring during RadioVIS processing. 
		 */
        enum ErrorCode {
			/**
			 * The operation was successful.
			 */
			OK,
			
			/**
			 * An error occurred while trying to access a node on the radio.
			 */
			NodeAccessError,
			
			/**
			 * A network time out occurred.
			 */
			NetworkTimeout,
			
			/**
			 * An error occurred during STOMP protocol processing.
			 */
			StompError,
			
			/**
			 * An error occurred while trying to do a DNS lookup.
			 */
			DnsError,
			
			/**
			 * The RadioVIS helper object has been closed.
			 */
			Closed,
			
			/**
			 * An error occurred while trying to connect to the radio.
			 */
			ConnectionError,
			
			/**
			 * The connection to the radio was lost.
			 */
			ConnectionLost,
			
			/**
			 * The RadioVIS helper object has been disabled at the request of the user.
			 */
			UserRequested,
		}
		
		/**
		 * This method will be called-back when the URL changes for the RadioVIS image.
		 * 
		 * @param newUrl		The new URL
		 */
        void radioVisUrlUpdate(String newUrl);
		
		/**
		 * This method will be called-back whenever the enabled state of RadioVIS changes. If for example,
		 * RadioVIS loses it connection to a radio, it will disable itself and trigger appropriate callbacks.
		 * User code can then later re-enable RadioVIS when it is safe to resume. The RadioVIS helper will
		 * automatically reconnect itself when it is re-enabled.
		 * 
		 * @param enabled		Whether or not RadioVis is now enabled
		 * @param reason		When becoming disabled, reason gives a hint as to why
		 */
        void radioVisEnablementChanged(boolean enabled, ErrorCode reason);
	}
	
	private EventHelper<Long, Callback> Callbacks = new EventHelper<Long, Callback>();
	
	/**
	 * Register to receive callbacks when RadioVIS information changes. The callback can be
	 * called in a specific context, specified by 'callbackContext'. This is useful, for example, to ensure that callbacks
	 * are called in a context from which it is safe to perform UI operations.
	 *
	 * @param callbackOnMainThread	If callbacks shoudl be called on Main Thread
	 * @param callback				The callback to be called
	 */
	public void registerCallback(boolean callbackOnMainThread, Callback callback) {
		this.Callbacks.registerCallback(0L, callbackOnMainThread, callback);
	}
	
	/**
	 * This is an overloaded variant of {@link #registerCallback(boolean, Callback)}, which
	 * contains a full description of all parameters.
	 */
	@SuppressWarnings("javadoc")
	public void registerCallback(Callback callback) { this.Callbacks.registerCallback(0L, false, callback); }
	
	/**
	 * Deregister a previously registered callback.
	 * 
	 * @param callback				The callback to be deregistered, as originally passed to the registration function
	 */
	public void deregisterCallback(Callback callback) {
		this.Callbacks.deregisterCallback(0L, callback);
	}
	
	/**
	 * Get the current URL (if there is one) for the current programme.
	 * 
	 * @return		The current URL
	 */
	public String getCurrentImageURL() {
		return this.CurrentImageURL;
	}

	/**
	 * Enable or disable this RadioVIS helper object.
	 * 
	 * @param enabled		True to enable
	 */
	public void setEnabled(boolean enabled) {
		this.setEnabled(enabled, Callback.ErrorCode.UserRequested);
	}
	
	private synchronized void setEnabled(final boolean enabled, final Callback.ErrorCode reason) {
		boolean stateChanged = (this.Enabled != enabled);
		this.Enabled = enabled;
		
		if (this.TheRadio != null) {
			if (stateChanged) {
				if (enabled) {
					this.TheRadio.connect(this);
				} else {
					/* Clear all resources. */
					this.cancelAll();
					this.updateURL("");
					if (this.TheRadio != null) {
						this.TheRadio.removeNotificationListener(this);
						//this.TheRadio.close();
						this.TheRadio = null;
					}
                    this.NumberOfGetErrorsInSuccession = 0;
				}
				
				/* Notify of state change. */
				NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, "RadioVIS calling callbacks to notify that enabled state has changed to " + enabled + ".");
				this.Callbacks.callCallbacks(0L, new EventHelper.CallCallbackCodeFragment(){
					public void run(Object callbackObject, Object tag) {
						Callback callback = (Callback)callbackObject;
						callback.radioVisEnablementChanged(enabled, reason);
					}
				});
			} else {
				if (enabled) {
					this.refreshInfo();
				}
			}
		}
	}

	/**
	 * An internal function.
	 */
	public synchronized void onConnectionSuccess(Radio radio) {

		/* Register interest in certain notifications. */
		this.TheRadio.addNotificationListener(this);
		
		/* Kick off a RadioVIS attempt. */
		this.refreshInfo();
	}
	
	private void refreshInfo() {
		if (TheRadio == null || !TheRadio.isConnectionOk()) {
			return;
		}

        TheRadio.getMode(new Radio.IRadioModeCallback() {
            @Override
            public void getCurrentMode(Mode currentMode) {
                /* Get nodes appropriate for current mode. */
                if (currentMode != null) {
                    NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, "RadioVIS refreshing info for mode " + currentMode.toString());

                    switch (currentMode) {
                        case FM: {
					        /* Certain nodes that we require are not available on older radios. */
                            if (TheRadio.isNodeSupported(NodePlayFrequency.class) &&
                                    TheRadio.isNodeSupported(NodePlayServiceIdsFmRdsPi.class)) {

                                TheRadio.getNodes(new Class[]{NodePlayFrequency.class, NodePlayServiceIdsFmRdsPi.class,
                                                NodePlayServiceIdsEcc.class},
                                        RadioVIS.this);
                            }
                            break;
                        }
                        case DAB: {
                            if (TheRadio.isNodeSupported(NodePlayServiceIdsFmRdsPi.class)) {
                                TheRadio.getNodes(new Class[]{NodePlayServiceIdsFmRdsPi.class,
                                                NodePlayServiceIdsDabServiceId.class, NodePlayServiceIdsDabEnsembleId.class,
                                                NodePlayServiceIdsDabScids.class, NodePlayServiceIdsEcc.class},
                                        RadioVIS.this);
                            }
                            break;
                        }
                    }
                }
            }
        });
	}
	
	private void clearCacheForInfo() {
		if (this.TheRadio != null) {
			this.TheRadio.clearNodeCache(new Class[]{NodePlayServiceIdsFmRdsPi.class,
																   NodePlayServiceIdsDabServiceId.class,
																   NodePlayServiceIdsDabEnsembleId.class,
																   NodePlayServiceIdsDabScids.class,
																   NodePlayServiceIdsEcc.class,
																   NodePlayFrequency.class});
		}
	}

	/**
	 * An internal function.
	 */
	public synchronized void onConnectionError(Radio radio, ConnectionErrorResponse error) {
		NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Error, "RadioVIS failed to connect to radio.");
		this.setEnabled(false, Callback.ErrorCode.ConnectionError);
	}

	/**
	 * An internal function.
	 */
	public synchronized void onDisconnected(Radio radio, ConnectionErrorResponse error) {
		NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Warning, "RadioVIS lost connection to radio.");
		this.setEnabled(false, Callback.ErrorCode.ConnectionLost);
	}

	/**
	 * An internal function.
	 */
	@SuppressWarnings("incomplete-switch")
    @Override
	public void onResult(Map<Class, NodeResponse> nodeResponses) {
        if (this.TheRadio == null) {
            return;
        } // To protect if we are closed while waiting on a result.

		/* Set some defaults. */
		this.Frequency = null;
		this.CountryCode = null;
		this.PiCode = -1;
		this.ECC = -1;
		this.DabEnsId = -1;
		this.DabServId = -1;
		this.DabScIds = -1;
		this.AutoCountryPos = -1;
//		this.DnsFQDN = null;

        boolean errorFound = false;
        boolean underlyingErrorFound = false;

		Map<Class, NodeInfo> nodes = new HashMap<>();
		for (Class key : nodeResponses.keySet()) {
			NodeResponse response = nodeResponses.get(key);

            if (response.mType == NodeResponse.NodeResponseType.NODE) {
				nodes.put(key, response.mNodeInfo);

            } else { //is error
                errorFound = true;
				if (response.mError.isUnderlyingConnectionFatallyErrored()) {
                    underlyingErrorFound = true;
                }
			}
		}

        if (errorFound) {
            this.NumberOfGetErrorsInSuccession += 1;

            /* See if it's a permanent failure? */
            if (underlyingErrorFound || (this.NumberOfGetErrorsInSuccession > 10) || (this.TheRadio == null)) {
                this.setEnabled(false, Callback.ErrorCode.NodeAccessError);
                return;
            } else {
                /* Retry. */
                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshInfo();
                    }
                }, true);
            }

        } else {
            this.NumberOfGetErrorsInSuccession = 0;
        }
		
		/* Determine what mode was used for the original request by whether or not the Frequency node is in the results. We do this
		   rather than inspect the current mode because the mode could change after the node request went out. */
		Mode mode = (nodes.containsKey(NodePlayFrequency.class))?Mode.FM:Mode.DAB;
		
		/* Node handling code that is common to all modes. */
		NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, "RadioVIS got info with " + nodes.size() + " nodes.");

		/* Read the PI Code. */
		if (nodes.containsKey(NodePlayServiceIdsFmRdsPi.class)) {
	    	int numericCode = 0;
			try {
				numericCode = ((NodePlayServiceIdsFmRdsPi) nodes.get(NodePlayServiceIdsFmRdsPi.class)).getValue().intValue();
			} catch (NullPointerException e) {
			}

	    	/* This if-statement is important because we don't move on to the next node until we have valid RDS. And
	           because FMRDSPI is a notifying node we don't have to worry about re-getting it. We will just read the
	           value here when it becomes available. */
	    	if (numericCode != 0) {
	    		this.PiCode = numericCode;
	    		this.ECC = -1; // Clear this, if we are in monitoring mode we don't want to re-use the old value.
	    	}
	    }
		
		/* Mode specific node handling code. */
		switch (mode) {
			case FM: {
				/* Read the frequency. */
				{
					this.Frequency = ((NodePlayFrequency)nodes.get(NodePlayFrequency.class)).getFrequencyForRadioDNS();
				}
				
				/* Handle ECC. */
				{
					this.ECC = nodes.containsKey(NodePlayServiceIdsEcc.class) ?
                            ((NodePlayServiceIdsEcc)nodes.get(NodePlayServiceIdsEcc.class)).getValue().intValue() : -1;

					if (this.PiCode != -1 ) {
						/* Out with the old. */
						this.CountryCode = null;

						/* If the ECC is 0, we need to use a country code instead. */
						if (this.ECC == 0) {
							/* Get the country code from the PI code. */
							int countryId = ((this.PiCode >> 12) & 0xf);

							/* Configure DNS to cycle through relevant countries. */
							this.AutoCountryPos = 0;
							this.AutoCountryECC = countryId;
							this.CountryCode = this.lookUpCountryCode(countryId, this.AutoCountryPos);
							NetRemote.assertTrue(this.CountryCode != null); // Can't be null because we should always get au and us as a minimum.
						} else {
							/* No need to do anything, we can use the ECC code as is. */
							int countryId = ((this.PiCode >> 12) & 0xf);
							this.AutoCountryPos = -1;
							this.CountryCode = String.format("%x%02x", countryId, (this.ECC & 0xff));
						}
					}
				}
				
				break;
			}
			case DAB: {
				try {
					/* Service nodes. */
					this.DabServId = ((NodePlayServiceIdsDabServiceId) nodes.get(NodePlayServiceIdsDabServiceId.class)).getValue().intValue();
					this.DabEnsId = ((NodePlayServiceIdsDabEnsembleId) nodes.get(NodePlayServiceIdsDabEnsembleId.class)).getValue().intValue();
					this.DabScIds = ((NodePlayServiceIdsDabScids) nodes.get(NodePlayServiceIdsDabScids.class)).getValue().intValue();

					/* Handle ECC. */
					{
						int numericCode = ((NodePlayServiceIdsEcc) nodes.get(NodePlayServiceIdsEcc.class)).getValue().intValue();
						this.ECC = numericCode;

		            	/* Out with the old. */
						this.CountryCode = null;

						int countryId = ((this.DabServId >> 12) & 0xf);

						this.AutoCountryPos = -1;
						this.CountryCode = String.format("%x%02x", countryId, (numericCode & 0xff));
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

				break;
			}
		}

		new Thread(new Runnable() {
            @Override
            public void run() {
                beginRadioVIS();
            }
        }).start();
	}

	private int NumberOfGetErrorsInSuccession = 0;
	
	private Timer RefreshDelayer = null;
	
	/**
	 * An internal function.
	 */
	public void onNodeUpdated(final NodeInfo node) {

		if (node instanceof NodeSysMode ||
			node instanceof NodePlayInfoName ||
			node instanceof NodePlayServiceIdsEcc ||
			node instanceof NodePlayServiceIdsFmRdsPi ||
			node instanceof NodePlayFrequency) {

			NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "RadioVIS node changed, node: " + node.getName() + ", so seeing if we have changed program.");
			if (this.TheRadio == null) {
				return;
			} // To protect if we are closed while waiting on a result.

			this.TheRadio.getMode(new Radio.IRadioModeCallback() {
                @Override
                public void getCurrentMode(Mode currentMode) {

                    if (TheRadio == null) {
                        return;
                    } // To protect if we are closed while waiting on a result.

                    /* See if it's the Play Info. */
                    if (node.getClass().equals(NodePlayInfoName.class)) {
                        /* Here we determine if the program has changed. But in some situations we have to introduce a delay
                           here. This is because in FM mode when scanning through frequencies the InfoName node changes
                           rapidly in quick succession. So we want to wait for things to settle so we don't do lots of costly
                           operations like reading the mode uncached, or clearing the cache for certain nodes. */

                        /* cancel any previous delayed refresh. */
                        if (RefreshDelayer != null) {
                            RefreshDelayer.cancel();
                            RefreshDelayer.purge();
                            RefreshDelayer = null;
                        }

                        if (currentMode == Mode.FM) {
                            /* Delay. */
                            /* Issue new request after delay. */
                            RefreshDelayer = new Timer();
                            RefreshDelayer.schedule(new TimerTask() {
                                public void run() {
                                    RadioVIS.this.examinePotentialProgramChange((NodePlayInfoName) node);
                                }
                            }, 500);
                        } else {
                            examinePotentialProgramChange((NodePlayInfoName) node);
                        }
                    }

	    	        /* For any other change to refetch all information. */
                    else {
                        NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "RadioVIS node changed, node: " + node.getName() + ", so refreshing info.");

                        if ((TheRadio != null) && ((node instanceof NodePlayServiceIdsEcc) || (node instanceof NodePlayServiceIdsFmRdsPi))) {
                            TheRadio.clearNodeCache(new Class[]{NodePlayServiceIdsDabServiceId.class,
                                    NodePlayServiceIdsDabEnsembleId.class,
                                    NodePlayServiceIdsDabScids.class});
                        }

                        updateURL("");
                        cancelAll();
                        refreshInfo();
                    }
                }
            });
		}
	}
	
	private void examinePotentialProgramChange(NodePlayInfoName nameNode) {
		if (this.TheRadio == null || nameNode == null) {
			return;
		}
		
        final String name = nameNode.getValue();
        
        /* Have to refresh the mode node in the cache, in case the NodePlayInfoName notification came in ahead of the mode change notification. */
        this.TheRadio.getNode(NodeSysMode.class, false, false/*true*/, new IGetNodeCallback() {
            @Override
			public void getNodeResult(NodeInfo node) {

                if (TheRadio == null || !TheRadio.isConnectionOk()) {
                    return;
                }

                TheRadio.getMode(new Radio.IRadioModeCallback() {
                    @Override
                    public void getCurrentMode(Mode currentMode) {
                        /* Have we just changed program? */
                        boolean justChangedProgram = false;
                        if (currentMode == Mode.FM) {
                            justChangedProgram = ((RadioVIS.this.PreviousInfoName == null) || (name.endsWith("MHz") && (!(RadioVIS.this.PreviousInfoName.equals(name)))));
                        } else if (currentMode == Mode.DAB) {
                            justChangedProgram = ((RadioVIS.this.PreviousInfoName == null) || (!(RadioVIS.this.PreviousInfoName.equals(name))));
                        }

		                /* Remember name for next time. */
                        RadioVIS.this.PreviousInfoName = null;

                        if (name != null) {
                            RadioVIS.this.PreviousInfoName = name;
                        }

		                /* If we've just changed program we need to refresh all our other details. */
                        if (justChangedProgram) {
                            NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "RadioVIS Program change detected.");
                            RadioVIS.this.cancelAll();
                            RadioVIS.this.clearCacheForInfo();
                            RadioVIS.this.updateURL("");
                            RadioVIS.this.refreshInfo();
                        } else {
                            NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "RadioVIS Program change NOT detected.");
                        }
                    }
                });
			}
			
			public void getNodeError(Class nodeType, NodeErrorResponse error) {
			}
		});
	}
	
	private void cancelAll() {
	    this.cancelDns();
	    this.updateURL("");
	}
	
	/* Turn numeric ECC (country code) to its string representation. */
	private String lookUpCountryCode(int numericCode, int index) {
		for (CountryCode entry : CountryCodeLookup) {
			if (((entry.Code == 0) || (numericCode == entry.Code)) && (index-- == 0)) {
				return entry.Country;
			}
		}
		
		return null;
	}
	
	private static List<CountryCode> CountryCodeLookup;
	static {
		CountryCodeLookup = new ArrayList<CountryCode>();
		CountryCodeLookup.add(new CountryCode(0xC, "gb"));
		CountryCodeLookup.add(new CountryCode(0xD, "de"));
		CountryCodeLookup.add(new CountryCode(0x1, "de"));
		CountryCodeLookup.add(new CountryCode(0x4, "ch"));
		CountryCodeLookup.add(new CountryCode(0xF, "fr"));
		CountryCodeLookup.add(new CountryCode(0x5, "it"));
		CountryCodeLookup.add(new CountryCode(0xE, "se"));
		CountryCodeLookup.add(new CountryCode(0x9, "dk"));
		CountryCodeLookup.add(new CountryCode(0x0, "au"));
		CountryCodeLookup.add(new CountryCode(0x0, "us"));
	}
	
	private static class CountryCode {
		public int Code;
		public String Country;
		public CountryCode(int code, String country) {
			this.Code = code;
			this.Country = country;
		}
	}
	
	private boolean hasAllInfoToDoLookup(Mode mode) {
	    boolean result = false;
	    String debugOutput = "RadioVis: Missing info -";
	    
	    if (mode == Mode.FM) {
	        if (this.Frequency == null) {
	            debugOutput += " Frequency";
	        }
	        if (this.CountryCode == null) {
	            debugOutput += " CountryCode";            
	        }
	        if (this.PiCode == -1) {
	            debugOutput += " PiCode";            
	        }
	        if (this.ECC == -1) {
	            debugOutput += " ECC";            
	        }
	        result = ((this.Frequency != null) && (this.CountryCode != null) && (this.PiCode != -1) && (this.ECC != -1));
	    } else if (mode == Mode.DAB) {
	        if (this.CountryCode == null) {
	            debugOutput += " CountryCode";            
	        }
	        if (this.ECC == -1) {
	            debugOutput += " ECC";            
	        }
	        if (this.DabEnsId == -1) {
	            debugOutput += " dabEnsId";            
	        }
	        if (this.DabScIds == -1) {
	            debugOutput += " dabSciIds";
	        }
	        if (this.DabServId == -1) {
	            debugOutput += " dabServId";            
	        }
	        
	        result = ((this.CountryCode != null) && (this.ECC != -1) && (this.DabEnsId != -1) && (this.DabScIds != -1) && (this.DabServId != -1));
	    }
	   
	    if (!result) {
	        NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, debugOutput);
	    }

	    return result;
	}
	
	private void beginRadioVIS() {

        NodeSysCapsValidModes.Mode mode;

        synchronized (RadioVIS.this) {
            if (this.TheRadio == null || !this.TheRadio.isConnectionOk()) {
                return;
            } // To protect if we are closed while waiting on a result.
            mode = this.TheRadio.getModeSync();
        }

	    if (this.hasAllInfoToDoLookup(mode)) {
	        /* Check it's a supported mode. */
	        if (mode == Mode.FM) {
	            /* Construct the domain name for the RadioDNS lookup. */
	            NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "Have the required info to begin RadioDNS.");
	            this.DnsFQDN = String.format("%s.%04x.%s.%s.radiodns.org", this.Frequency, this.PiCode, this.CountryCode, mode.name().toLowerCase());
	            this.beginDns();
	        } else if (mode == Mode.DAB) {
	            /* Construct the domain name for the RadioDNS lookup. */
	        	NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "Have the required info to begin RadioDNS.");
	            this.DnsFQDN = String.format("%01x.%04x.%04x.%s.%s.radiodns.org", this.DabScIds, this.DabServId, this.DabEnsId, this.CountryCode, mode.name().toLowerCase());
	            this.beginDns();
	        }
	    } else {
	    	NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "Do not have sufficient information to begin RadioDNS.");
	    }
	}
	
	/**
	 * pause this RadioVIS instance, to be called when your application is put in the background.
	 */
	public void pause() {
		if (this.OurWorker != null) {
			this.OurWorker.pause();
			NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, "RadioVIS paused.");
		}
	}
	
	/**
	 * resume this RadioVIS instance, to be called when your application returns from the background.
	 */
	public void resume() {
		if (this.OurWorker != null) {
			this.OurWorker.resume();
			NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, "RadioVIS resumed.");
		}		
	}
	
	private void cancelDns() {
		if (this.OurWorker != null) {
			NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "Cancelling DNS.");
			this.OurWorker.cancel();
			this.OurWorker = null;
		}
	}
	
	private Worker OurWorker = null;
	
	private class Worker implements Runnable {
		public boolean Aborted = false;
		private Thread OurThread = null;
		private NodeSysCapsValidModes.Mode CurrentMode = null;
		private Radio mRadio;
		
		public Worker(Radio radio) {
			this.mRadio = radio;
			this.CurrentMode = this.mRadio.getModeSync();
			
			this.OurThread = new Thread(this, "RadioVIS Worker");
			this.OurThread.start();
		}
		
		
		private MutableBoolean Paused = new MutableBoolean(false);
		
		public void pause() {
			synchronized (this.Paused) {
				this.Paused.setTrue();
				this.Paused.notify();
			}
		}

		public void resume() {
			synchronized (this.Paused) {
				this.Paused.setFalse();
				this.Paused.notify();
			}
		}
		
		private void checkPaused() {
			/* See if we've been paused? */
			synchronized (this.Paused) {
				while (this.Paused.getValue() == true) {
					try {
						this.Paused.wait();
					} catch (InterruptedException e) {
						break;
					}
				}
			}
		}
		
		public void run() {
			NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "RadioVIS Worker beginning.");

			if (!(this.Aborted)) {
				Callback.ErrorCode reason = this.doWork();
	
				/* We only set enabled to false (and therefore notify the app), if the reason for closing wasn't routine - ie. some
				   part of RadioVIS just calling cancelAll(). */
				if (!(this.Aborted)) {
					synchronized (RadioVIS.this) {
						if (RadioVIS.this.Enabled) {
							RadioVIS.this.setEnabled(false, reason);
						}					
					}
				}
			
				/* We've stopped so clear the URL. */
				RadioVIS.this.updateURL("");
			}
						
			NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "RadioVIS Worker reached end of run().");
			synchronized (RadioVIS.this) {
				synchronized (Worker.this) {
					this.OurThread = null;
				}
			}
		}
		
		public void cancel() {
            synchronized (Worker.this) { // TODO Deadlock seen here.
                if (this.OurThread != null) {
                    NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "Cancelling RadioVIS Worker.");
                    this.Aborted = true;
                    this.resume();
                    this.OurThread.interrupt();
                    Thread.yield();
                    if (this.StompInput != null) {
                        try {
                            this.StompInput.close();
                        } catch (IOException e) {
                        }
                    }
                    this.OurThread = null;
                }
            }
		}
		
		private String cleanUpDnsName(String name) {
			name = name.trim();
			
			if (name.endsWith(".")) {
				name = name.substring(0, (name.length() - 1));
			}
			
			return name;
		}
		
		private int DnsFailureCount;
		
		private Callback.ErrorCode doWork() {
			this.DnsFailureCount = 0;
			
			while (!(this.Aborted)) {
				/* See if we need to pause. */
				this.checkPaused();
				
				try {
					/* Get the CNAME record. */
					NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "Looking up CNAME record for " + RadioVIS.this.DnsFQDN);
					org.xbill.DNS.Record[] records = null;
					
					try {
						records = new org.xbill.DNS.Lookup(RadioVIS.this.DnsFQDN, org.xbill.DNS.Type.CNAME).run();
					} catch (NullPointerException e) {}
					
					/* See if our thread has been cancelled. */
					if (this.Aborted) { return Callback.ErrorCode.Closed; }
					
					/* Examine the result. */
					String cname = null;
					if (records != null) {
						for (org.xbill.DNS.Record record : records) {
							if (record instanceof org.xbill.DNS.CNAMERecord) {
								org.xbill.DNS.CNAMERecord cnameRecord = (org.xbill.DNS.CNAMERecord)record;
								cname = cnameRecord.getTarget().toString();
								if (cname != null) {
									cname = this.cleanUpDnsName(cname);
									break;
								}
							}
						}
					}
					
					if (cname != null) {
						NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "CNAME record for " + RadioVIS.this.DnsFQDN + " is " + cname);
						
						/* Now get the SRV record. */
						records = new org.xbill.DNS.Lookup(("_radiovis._tcp." + cname), org.xbill.DNS.Type.SRV).run();
						
						/* See if our thread has been cancelled. */
						if (this.Aborted) { return Callback.ErrorCode.Closed; }
						
						/* Examine the result. */
						String stompHost = null;
						int stompPort = 0;
						if (records != null) {
							for (org.xbill.DNS.Record record : records) {
								if (record instanceof org.xbill.DNS.SRVRecord) {
									org.xbill.DNS.SRVRecord srvRecord = (org.xbill.DNS.SRVRecord)record;
									stompHost = this.cleanUpDnsName(srvRecord.getTarget().toString());
									stompPort = srvRecord.getPort();
									if (stompHost != null) { break; }
								}
							}
						}
						
						/* Have we got what we need to begin STOMP? */
						if (stompHost != null) {
							NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "Stomp host is " + stompHost + ":" + (Long.valueOf(stompPort)).toString());
							
							/* Call STOMP. */
							return this.beginStomp(stompHost, stompPort);
						} else {
							NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Warning, "Unable to get SRV record for " + cname + " / " + RadioVIS.this.DnsFQDN);
						}
					} else {
						/* The FQDN->CNAME lookup failed for some reason. */
						NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Warning, "Unable to get CNAME record for " + RadioVIS.this.DnsFQDN);
						
						/* Check we've not had too many failures. */
						this.DnsFailureCount += 1;
						if (this.DnsFailureCount > 20) {
							NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Warning, "Too many DNS failures - giving up.");
							return Callback.ErrorCode.DnsError;
						}
						
						/* See if we are not doing auto cycling through countries. */
						if (RadioVIS.this.AutoCountryPos != -1) {
							/* On to the next country. */
							RadioVIS.this.AutoCountryPos += 1;
					        String countryString = RadioVIS.this.lookUpCountryCode(RadioVIS.this.AutoCountryECC, RadioVIS.this.AutoCountryPos);
					        if (countryString != null) {
					            /* We have got the next entry to try. */
					        	RadioVIS.this.CountryCode = countryString;
					        } else {
					            /* We have got to the end of the list, wrap round to the start. */
					            RadioVIS.this.AutoCountryPos = 0;
					            RadioVIS.this.CountryCode = RadioVIS.this.lookUpCountryCode(RadioVIS.this.AutoCountryECC, RadioVIS.this.AutoCountryPos);
					        }
					        
					        if (this.CurrentMode == Mode.FM) {
					            /* Construct the domain name for the RadioDNS lookup. */
					        	RadioVIS.this.DnsFQDN = String.format("%s.%04x.%s.%s.radiodns.org", RadioVIS.this.Frequency, RadioVIS.this.PiCode, RadioVIS.this.CountryCode, this.CurrentMode.toString().toLowerCase());
					        } else if (this.CurrentMode == Mode.DAB) {
					        	RadioVIS.this.DnsFQDN = String.format("%01x.%04x.%04x.%s.%s.radiodns.org", RadioVIS.this.DabScIds, RadioVIS.this.DabServId, RadioVIS.this.DabEnsId, RadioVIS.this.CountryCode, this.CurrentMode.toString().toLowerCase());
					        }
					        
					        continue; // Continue immediately without delay.
						}
					}
				} catch (org.xbill.DNS.TextParseException e) {
					NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Warning, "Problem during DNS operations for RadioVIS on " + RadioVIS.this.DnsFQDN);
					return Callback.ErrorCode.DnsError; // Fail immediately as a retry is unlikely to succeed.
				}
					
				/* See if our thread has been cancelled. */
				if (this.Aborted) { return Callback.ErrorCode.Closed; }
				
				/* Sleep for a bit before retrying by going round the loop. */
				try {
					Thread.sleep(10 * 1000);
				} catch (InterruptedException e) {
				}
			}
			
			return Callback.ErrorCode.Closed;
		}
		
		private void stompWrite(String message) {
			this.StompWriter.write(message);
			this.StompWriter.write("\n");
			this.StompWriter.write(0);
			this.StompWriter.flush();
		}
		
		private PrintWriter StompWriter;
		
		private InputStream StompInput = null;
		
		private Callback.ErrorCode beginStomp(String host, int port) {
			while (!(this.Aborted)) {
				try {
					Socket sock = new Socket(host, port);
					this.StompInput = sock.getInputStream();
					//BufferedReader input = new BufferedReader(new InputStreamReader(sock.getInputStream()));
					this.StompWriter = new PrintWriter(sock.getOutputStream());
					NetRemote.log(NetRemote.TRACE_STOMP_BIT, LogLevel.Info, "Opened STOMP connection to " + host);
					
					/* Send the connect message. */
					this.stompWrite("CONNECT\n");
					
					/* Main packet processing loop. */
					while (!(this.Aborted)) {
						/* See if we need to pause. */
						this.checkPaused();
						
						/* Keep reading data until we get a null which signifies the end of a packet. */
						String msg = "";
						do {
							int data = this.StompInput.read();
							if (data > 0) {
								msg += (char)data;
							} else {
								break;
							}
						} while (!(this.Aborted));
						
						/* See if we need to pause. */
						this.checkPaused();

						/* See if our thread has been cancelled. */
						if (this.Aborted) {
							try {
								this.StompInput.close();
							} catch (Exception e) {}
							try {
								this.StompWriter.close();
								sock.close();
							} catch (Exception e) {}
							return Callback.ErrorCode.Closed;
						}
						
						/* Process the message. */
						NetRemote.log(NetRemote.TRACE_STOMP_BIT, LogLevel.Info, "STOMP RX: " + msg);
						
						this.processStompMessage(msg);
					}
				} catch (UnknownHostException e) {
					NetRemote.log(NetRemote.TRACE_STOMP_BIT, LogLevel.Warning, "STOMP problem host '" + host + "' is unknown.");
					
					/* We used to stop STOMP and return an error here, but bug #13637 demonstrated that this is not the correct thing to do.
					   For a Bluetooth connected radio, the connection to the radio can be completely stable but the Wi-Fi connection which
					   is needed for RadioVIS could come and go. If the problem is transient, stopping STOMP would not allow RadioVIS to
					   resume once the network has recovered. */
					//return Callback.ErrorCode.StompError; // No point in retrying from this exception, so return.
				} catch (Exception e) {
					NetRemote.log(NetRemote.TRACE_STOMP_BIT, LogLevel.Warning, "STOMP IO problem while accessing host '" + host + "', exception = " + e.toString());
				}
				
				/* See if our thread has been cancelled. */
				if (this.Aborted) { return Callback.ErrorCode.Closed; }
				
				/* If we get here there has been some kind of error, so sleep for a bit before retrying. */
				try {
					Thread.sleep(20 * 1000);
				} catch (InterruptedException e) {
					return Callback.ErrorCode.Closed;
				}
			}
			
			return Callback.ErrorCode.Closed;
		}
		
		@SuppressWarnings("unused")
		private String StompSessionID;
		
		private void processStompMessage(String msg) {
            synchronized (Worker.this) {
				if (this.mRadio == null) { return; }
				
				/* Clean up the message. Turn any \r's in to \n's, and then get rid of double \n's, and then any leading \n's. */
			    msg = msg.replace("\r", "\n");
			    msg = msg.replace("\n\n", "\n");
			    msg = msg.trim();
			    
			    /* Split in to lines. */
			    String[] lines = msg.split("\n");
			    
			    /* Process message. */
			    int numberOfLines = lines.length;
			    if (numberOfLines >= 1) {
			        String action = lines[0];
			        /* Process message of form:
			             CONNECTED
			             session:ID:rad0304-33090-1307974249056-3:508301
			         
			             or just session without 'ID:', ie:
			         
			             CONNECTED
			             session:rad0304-33090-1307974249056-3:508301
			         */
			        if (action.toLowerCase().startsWith("connected")) {
			            /* See if there is an (optional) session ID. */
			            String idLine = this.findLineWithPrefix("session:", lines);
			            if (idLine != null) {
			                /* Read the session ID, and subscribe. */
			                if (idLine.toLowerCase().startsWith("session:id:")) {
			                    this.StompSessionID = idLine.substring(11);
			                } else {
			                    this.StompSessionID = idLine.substring(8);
			                }
			            }
	
			            /* Send ack. */
			            this.stompWrite("SUBSCRIBE\ndestination: " + this.getCurrentTopic() + "\nack: auto\n");
			        }
			        
			        /* Process message of form:
			            MESSAGE
			            content-type:text/plain; charset=UTF-8
			            link:http://www.bbc.co.uk/radio2/
			            message-id:ID:rad0304-33090-1307974249056-3:509694:-1:1:42
			            destination:/topic/fm/gb/c202/08880/image
			            timestamp:1312467554885
			            expires:0
			            content-length:96
			            priority:0
			            trigger-time:NOW
			            
			            SHOW http://radiodns-www.prototype0.net/radiovis_slideshow/radio2/Radio2_2011-08-04-14-05-50.png
			        */
			        else if (action.toLowerCase().startsWith("message")) {
			        	/* See if there's a show line with a URL. */
			        	String url = null;
			        	boolean triggerNow = false;
			        	for (String line : lines) {
			        		if (line.toLowerCase().startsWith("show ")) {
			        			url = line.substring(5).trim();
			        		} else if (line.toLowerCase().startsWith("trigger-time:") && line.toLowerCase().endsWith("now")) {
			        			triggerNow = true;
			        		}
			        	}
	
			        	if (url != null) {
			        		if (triggerNow) {
			        			if (!this.Aborted) {
			        				NetRemote.log(NetRemote.TRACE_STOMP_BIT, LogLevel.Info, "STOMP - New URL to display: " + url);
			        				RadioVIS.this.updateURL(url);
			        			} else {
			        				NetRemote.log(NetRemote.TRACE_STOMP_BIT, LogLevel.Info, "STOMP - Ignoring URL because aborted.");
			        			}
			        		} else {
			        			NetRemote.log(NetRemote.TRACE_STOMP_BIT, LogLevel.Warning, "Stomp - Not showing URL because trigger-time was not NOW, URL was: " + url);
			        		}
			        	}
			        }
			    }
            }
		}
		    
		private String findLineWithPrefix(String prefix, String[] lines) {
			for (String line : lines) {
				if (line.toLowerCase().startsWith(prefix)) {
					return line;
				}
			}

			return null;
		}
		
		private String getCurrentTopic() {
			NodeSysCapsValidModes.Mode mode = this.CurrentMode;
			
		    if (mode == Mode.FM) {
		        /* FM. */
		        return String.format("/topic/fm/%s/%04x/%s/image", RadioVIS.this.CountryCode, RadioVIS.this.PiCode, RadioVIS.this.Frequency);            
		    } else if (mode == Mode.DAB) {
		        /* DAB. */
		        return String.format("/topic/dab/%s/%04x/%04x/%01x/image", RadioVIS.this.CountryCode, RadioVIS.this.DabEnsId, RadioVIS.this.DabServId, RadioVIS.this.DabScIds);
		    }
		    
		    return "";
		}
	}
	
	private void beginDns() {
		/* Clear out any previous operation. */
        this.cancelDns();

        /* Kick off a new worker. */
		this.OurWorker = new Worker(this.TheRadio);
	}
	
	private String CurrentImageURL = "";
	
	private void updateURL(final String url) {
		synchronized (RadioVIS.this) {
			synchronized (this.CurrentImageURL) {
				boolean valueHasChanged = !(url.equals(this.CurrentImageURL));
	
				/* Remember the new. */
				this.CurrentImageURL = url;
	
				/* Notify anyone who wants to know. */
				if (valueHasChanged) {
					NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, LogLevel.Info, "RadioVIS, new url = " + url);

                    /* New URL event. */
                    NetRemote.log(NetRemote.TRACE_RADIOVIS_BIT, "RadioVIS calling callbacks with new URL = " + ((url == null)?"null":url));
                    this.Callbacks.callCallbacks(0L, new EventHelper.CallCallbackCodeFragment(){
                        public void run(Object callbackObject, Object tag) {
                            Callback callback = (Callback)callbackObject;
                            callback.radioVisUrlUpdate(url);
                        }
                    });
				}
			}
		}
	}
	
	/**
	 * A static helper function to determine if the RadioVIS helper library would be able to operate correctly on the given radio.
	 * 
	 * @param radio			The radio to be tested
	 * @return				True if the RadioVIS helper could use the radio
	 */
	public static boolean doesRadioSupportRadioVIS(Radio radio) {
		return radio.isNodeSupported(NodePlayServiceIdsFmRdsPi.class);
	}
}
