package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;

/**
 * Callback interface for reading the value of a node.
 *
 */
public interface IGetNodeCallback {
    /**
     * This callback is called with the result of a successful read of a node.
     *
     * @param node			The node read and its value
     */
    void getNodeResult(NodeInfo node);

    /**
     * This callback is called if an error occurs while trying to read a node.
     *
     * @param nodeType		The type of node being read
     * @param error			An error indication
     */
    void getNodeError(Class nodeType, NodeErrorResponse error);
}


