package com.frontier_silicon.NetRemoteLib.Radio;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.LogLevel;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by lsuhov on 18/01/2017.
 *
 * Used to handle sync node requests
 */

class XmlSyncNodeRequest {

    XmlSyncNodeResponse doRequest(String requestURL, boolean connectionRequest, RadioHttp radioHttp) {
        XmlSyncNodeResponse xmlSyncNodeResponse = new XmlSyncNodeResponse();

        Request request = new Request.Builder().url(requestURL).build();

        try {
            Response response = NetRemote.getOkHttpClient().newCall(request).execute();

            if (response.isSuccessful()) {
                String content = response.body().string();
                response.close();

                RadioHttpUtil.sendToLog(requestURL, content);
                xmlSyncNodeResponse.document = RadioHttpUtil.getDocument(content);

                if (xmlSyncNodeResponse.document == null) {
                    xmlSyncNodeResponse.errorResponse = RadioHttpUtil.getParseError(connectionRequest);
                }
            } else {
                int code = response.code();

                NetRemote.log(LogLevel.Error, "Request error for " + requestURL + ", code " + code);

                xmlSyncNodeResponse.errorResponse = RadioHttpUtil.getErrorBasedOnHttpCode(code, connectionRequest);
                if (xmlSyncNodeResponse.errorResponse.Transport.Error == TransportErrorDetail.ErrorCode.PinError) {
                    radioHttp.setInvalidPIN(true);
                }
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();

            NetRemote.log(LogLevel.Error, "Request error: " + e + " " + requestURL);

            xmlSyncNodeResponse.errorResponse = RadioHttpUtil.getTimeoutError(connectionRequest, radioHttp);

        } catch (SocketException e) {
            e.printStackTrace();

            NetRemote.log(LogLevel.Error, "Request error: " + e + " " + requestURL);

            xmlSyncNodeResponse.errorResponse = RadioHttpUtil.getNetworkError(connectionRequest, radioHttp);

        } catch (IOException e) {
            e.printStackTrace();

            NetRemote.log(LogLevel.Error, "Request error: " + e + " " + requestURL);

            xmlSyncNodeResponse.errorResponse = RadioHttpUtil.getGenericError(connectionRequest);
        }

        return xmlSyncNodeResponse;
    }
}
