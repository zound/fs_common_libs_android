package com.frontier_silicon.NetRemoteLib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.frontier_silicon.loggerlib.LogLevel;

/**
 * Receiver for network changes. It is interested in WiFi and Ethernet changes
 *
 * @author viftime
 */
public class NetworkStateReceiver extends BroadcastReceiver {

    private String mPrevWiFiSSID = "";
    private IWifiConnection callback;
    private NetworkInfo.State lastNetworkKnownState;

    public NetworkStateReceiver() {

        initWifiState();
    }

    private void initWifiState() {

        if (AccessPointUtil.isConnectedToWiFi()) {
            mPrevWiFiSSID = AccessPointUtil.getSSIDOfCurrentConnection();
        }
    }

    public void setConnectionListener(IWifiConnection listener) {
        this.callback = listener;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        final String action = intent.getAction();
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (networkInfo == null) {
            return;
        }

        if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION) && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {

            if (networkInfo.isConnected()) {
                // state: CONNECTED/CONNECTED

                NetRemote.log(LogLevel.Info, "Received network NETWORK_STATE_CHANGED_ACTION true : " + networkInfo.toString());

                if (isNetworkChanged()) {
                    NetRemote.log(LogLevel.Info, "Sending NETWORK_STATE_CHANGED_ACTION: " + networkInfo.toString());
                    callback.onConnected();
                    lastNetworkKnownState = networkInfo.getState();
                }

            } else if (!networkInfo.isConnectedOrConnecting()  && networkInfo.getState() != lastNetworkKnownState) {
                // state: DISCONNECTED/DISCONNECTED

                NetRemote.log(LogLevel.Info, "Received network NETWORK_STATE_CHANGED_ACTION false : " + networkInfo.toString());

                mPrevWiFiSSID = "";
                callback.onDisconnected();
                lastNetworkKnownState = networkInfo.getState();
            }

       }
       //     else if (action.equals("android.net.conn.CONNECTIVITY_CHANGE") && networkInfo.getType() == ConnectivityManager.TYPE_ETHERNET) {
//            if (networkInfo.isConnected()) {
//                callback.onConnected();
//
//            } else if (!networkInfo.isConnectedOrConnecting()) {
//                callback.onDisconnected();
//            }
//        }

    }

    private boolean isNetworkChanged() {

        String newWiFiSSID = AccessPointUtil
                .sanitizeSSID(AccessPointUtil.getSSIDOfCurrentConnection());

		if (!newWiFiSSID.equals(mPrevWiFiSSID)) {
			NetRemote.log(LogLevel.Info, "Prev:" + mPrevWiFiSSID + " new:" + newWiFiSSID + " isChanged");
			mPrevWiFiSSID = newWiFiSSID;
			return true;
		}

		NetRemote.log(LogLevel.Info, "Prev:" + mPrevWiFiSSID + " new:" + newWiFiSSID + " not changed");
		return false;
	}

}
