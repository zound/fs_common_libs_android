package com.frontier_silicon.NetRemoteLib;

/**
 * Created by mnisipeanu on 25/05/2017.
 */

public interface IRadioNotificationState {

    void onChange(boolean isPaused);
}
