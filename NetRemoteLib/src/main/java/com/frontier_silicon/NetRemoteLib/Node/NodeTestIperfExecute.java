package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.test.iperf.execute node.
 *
 * On set starts or stop all instances running as specified. On get returns START
 * if at least 1 instance is running, otherwise STOP.
 */

public class NodeTestIperfExecute extends BaseTestIperfExecute {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeTestIperfExecute(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeTestIperfExecute(Ord value) {
		super(value);
	}
}
