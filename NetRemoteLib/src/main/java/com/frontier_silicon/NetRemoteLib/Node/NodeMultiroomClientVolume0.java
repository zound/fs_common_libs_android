package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.volume0 node.
 * 
 * Handle client 0 volume level
 */
public class NodeMultiroomClientVolume0 extends BaseMultiroomClientVolume0 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomClientVolume0(Long value) {
		super(value);
	}
}
