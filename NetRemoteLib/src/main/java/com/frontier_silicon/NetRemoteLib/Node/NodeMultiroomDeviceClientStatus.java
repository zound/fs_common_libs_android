package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.device.clientStatus node.
 *
 * Client status
 */

public class NodeMultiroomDeviceClientStatus extends BaseMultiroomDeviceClientStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomDeviceClientStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomDeviceClientStatus(Ord value) {
		super(value);
	}
}
