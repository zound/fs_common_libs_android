package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.status node.
 *
 * Current playback status.  Values mean:
 * 
 * !======================================== ! Value ! Meaning
 * 
 * ! IDLE ! no playback is in progress
 * 
 * ! BUFFERING ! a track is starting, but no audio has yet played for the current
 * track
 * 
 * ! PLAYING ! a track is being played - audio is being played out
 * 
 * ! PAUSED ! the current track is paused.
 * 
 * ! REBUFFERING ! a track as started, but the audio has underrun so it is
 * rebuffering
 * 
 * ! ERROR ! an error has occurred playing the track.  The play.errorStr node is
 * updated with a user displayable string
 * 
 * ! STOPPED ! the playback has stopped. !========================================
 * 
 * As a special case, when in iPod mode without and iPod docked, the status will
 * become ERROR
 */

public class NodePlayStatus extends BasePlayStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayStatus(Ord value) {
		super(value);
	}
}
