package com.frontier_silicon.NetRemoteLib.Node;

/**
 * This is the base class of a U32 node.
 * 
 */
public abstract class NodeU32 extends NodeInteger {
	/**
	 * Constructs a node with the given value.
	 * 
	 * @param value		The numeric value of the node
	 */
	protected NodeU32(Long value) {
		super(value, 0xffffffffL, 0);
	}
	
	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	NodeU32() {
	}
}
