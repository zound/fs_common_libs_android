package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.cast.usageReport node.
 *
 * ACTIVE indicates usage data will be sent to googles servers
 */

public class NodeCastUsageReport extends BaseCastUsageReport {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeCastUsageReport(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeCastUsageReport(Ord value) {
		super(value);
	}
}
