package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.addClient node.
 * 
 * Instruct the client to join the group
 */
public class NodeMultiroomGroupAddClient extends BaseMultiroomGroupAddClient {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomGroupAddClient(String value) {
		super(value);
	}
}
