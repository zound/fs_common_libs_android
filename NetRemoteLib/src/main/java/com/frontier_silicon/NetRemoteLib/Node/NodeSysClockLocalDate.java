package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.clock.localDate node.
 * 
 * Query or set the local date. If set when netRemote.sys.clock.source is nonzero
 * (i.e. not User) then the user setting will be overwritten when the clock is next
 * updated from the source.
 */
public class NodeSysClockLocalDate extends BaseSysClockLocalDate {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysClockLocalDate(String value) {
		super(value);
	}
}
