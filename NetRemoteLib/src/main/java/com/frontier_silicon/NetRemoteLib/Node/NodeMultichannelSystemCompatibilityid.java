package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.system.compatibilityid node.
 * 
 * Streaming compatibility id
 */
public class NodeMultichannelSystemCompatibilityid extends BaseMultichannelSystemCompatibilityid {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultichannelSystemCompatibilityid(String value) {
		super(value);
	}
}
