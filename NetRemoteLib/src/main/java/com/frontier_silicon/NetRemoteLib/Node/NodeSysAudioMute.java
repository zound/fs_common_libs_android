package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.mute node.
 *
 * Mute audio
 */

public class NodeSysAudioMute extends BaseSysAudioMute {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysAudioMute(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysAudioMute(Ord value) {
		super(value);
	}

	/**
	 * Get a new node representing the toggled/opposite value of this node.
	 * 
	 * @return		A new node with the toggled value
	 */
	public NodeSysAudioMute ToggleValue() {
		if (this.getValueEnum() == Ord.MUTE) {
			return new NodeSysAudioMute(Ord.NOT_MUTE);
		} else {
			return new NodeSysAudioMute(Ord.MUTE);
		}
	}
}
