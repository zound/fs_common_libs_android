package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.state node.
 *
 * Enable navigation subsystem - this navigation context is not started until this
 * node is enabled.  Access to other nav nodes are blocked which this is disabled.
 * This node resets to off when the mode is changed.  Setting this node to off and
 * back to on restarts the navigation at the root for the current mode.
 */

public class NodeNavState extends BaseNavState {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavState(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavState(Ord value) {
		super(value);
	}
}
