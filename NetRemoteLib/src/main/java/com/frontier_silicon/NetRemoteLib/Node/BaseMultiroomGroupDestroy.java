package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.multiroom.group.destroy node.
 *
 * Destroy the current group
 */
public class BaseMultiroomGroupDestroy extends NodeE8<BaseMultiroomGroupDestroy.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.multiroom.group.destroy"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10910200; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseMultiroomGroupDestroy(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseMultiroomGroupDestroy() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseMultiroomGroupDestroy(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value IDLE.
         */
        IDLE, 
        /**
         * Enumeration value DESTROY.
         */
        DESTROY, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case IDLE: return (long)0;
			case DESTROY: return (long)1;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.IDLE;
			case 1: return Ord.DESTROY;
		}
		return null;
	}
}
