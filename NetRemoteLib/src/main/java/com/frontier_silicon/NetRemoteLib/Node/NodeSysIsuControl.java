package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.isu.control node.
 *
 * This node is available only in application mode. CHECK - Checks for a newer
 * software version and UPDATE_FIRMWARE reboots to the update mode
 */

public class NodeSysIsuControl extends BaseSysIsuControl {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysIsuControl(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysIsuControl(Ord value) {
		super(value);
	}
}
