package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.scan node.
 *
 * Scan for access point
 */

public class NodeSysNetWlanScan extends BaseSysNetWlanScan {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanScan(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanScan(Ord value) {
		super(value);
	}
}
