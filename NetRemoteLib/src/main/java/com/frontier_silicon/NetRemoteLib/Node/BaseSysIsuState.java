package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.isu.state node.
 *
 * Used to get/notify various isu states
 */
public class BaseSysIsuState extends NodeE8<BaseSysIsuState.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.isu.state"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10108100; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return true; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return true; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseSysIsuState(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysIsuState() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseSysIsuState(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value IDLE.
         */
        IDLE, 
        /**
         * Enumeration value CHECK_IN_PROGRESS.
         */
        CHECK_IN_PROGRESS, 
        /**
         * Enumeration value UPDATE_AVAILABLE.
         */
        UPDATE_AVAILABLE, 
        /**
         * Enumeration value UPDATE_NOT_AVAILABLE.
         */
        UPDATE_NOT_AVAILABLE, 
        /**
         * Enumeration value CHECK_FAILED.
         */
        CHECK_FAILED, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case IDLE: return (long)0;
			case CHECK_IN_PROGRESS: return (long)1;
			case UPDATE_AVAILABLE: return (long)2;
			case UPDATE_NOT_AVAILABLE: return (long)3;
			case CHECK_FAILED: return (long)4;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.IDLE;
			case 1: return Ord.CHECK_IN_PROGRESS;
			case 2: return Ord.UPDATE_AVAILABLE;
			case 3: return Ord.UPDATE_NOT_AVAILABLE;
			case 4: return Ord.CHECK_FAILED;
		}
		return null;
	}
}
