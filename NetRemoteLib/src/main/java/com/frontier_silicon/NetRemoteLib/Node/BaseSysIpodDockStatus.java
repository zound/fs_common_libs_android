package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.ipod.dockStatus node.
 *
 * iPod dock status
 */
public class BaseSysIpodDockStatus extends NodeE8<BaseSysIpodDockStatus.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.ipod.dockStatus"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x101f0100; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return true; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return true; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseSysIpodDockStatus(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysIpodDockStatus() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseSysIpodDockStatus(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value NOT_DOCKED.
         */
        NOT_DOCKED, 
        /**
         * Enumeration value DOCKED_STILL_CONNECTING.
         */
        DOCKED_STILL_CONNECTING, 
        /**
         * Enumeration value DOCKED_ONLINE_READY.
         */
        DOCKED_ONLINE_READY, 
        /**
         * Enumeration value DOCKED_UNSUPPORTED_IPOD.
         */
        DOCKED_UNSUPPORTED_IPOD, 
        /**
         * Enumeration value DOCKED_UNKNOWN_DEVICE.
         */
        DOCKED_UNKNOWN_DEVICE, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case NOT_DOCKED: return (long)0;
			case DOCKED_STILL_CONNECTING: return (long)1;
			case DOCKED_ONLINE_READY: return (long)2;
			case DOCKED_UNSUPPORTED_IPOD: return (long)3;
			case DOCKED_UNKNOWN_DEVICE: return (long)4;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.NOT_DOCKED;
			case 1: return Ord.DOCKED_STILL_CONNECTING;
			case 2: return Ord.DOCKED_ONLINE_READY;
			case 3: return Ord.DOCKED_UNSUPPORTED_IPOD;
			case 4: return Ord.DOCKED_UNKNOWN_DEVICE;
		}
		return null;
	}
}
