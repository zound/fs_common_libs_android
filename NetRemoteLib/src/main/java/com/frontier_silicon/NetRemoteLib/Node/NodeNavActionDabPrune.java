package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.action.dabPrune node.
 *
 * When set to PRUNE all the entries in the ensemble, service and component lists
 * that are considered no longer valid are removed. A notification is generated
 * when the DAB prune completes.
 */

public class NodeNavActionDabPrune extends BaseNavActionDabPrune {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavActionDabPrune(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavActionDabPrune(Ord value) {
		super(value);
	}
}
