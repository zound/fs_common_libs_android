package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.isu.mandatory node.
 *
 * Read-only status of the new firmware regarding the update importance. The node
 * returns boolean values( yes/no ).
 */

public class NodeSysIsuMandatory extends BaseSysIsuMandatory {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysIsuMandatory(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysIsuMandatory(Ord value) {
		super(value);
	}
}
