package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.extStaticDelay node.
 * 
 * AudioSync external hardware delay (usecs)
 */
public class NodeSysAudioExtStaticDelay extends BaseSysAudioExtStaticDelay {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAudioExtStaticDelay(Long value) {
		super(value);
	}
}
