package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.shuffleStatus node.
 *
 * Indicate if the last shuffle change was successful or an error occurred.
 */

public class NodePlayShuffleStatus extends BasePlayShuffleStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayShuffleStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayShuffleStatus(Ord value) {
		super(value);
	}
}
