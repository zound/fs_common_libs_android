package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.alarm.current node.
 * 
 * 0-based current firing alarm. -1 if it is not alarming.
 */
public class NodeSysAlarmCurrent extends BaseSysAlarmCurrent {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAlarmCurrent(Long value) {
		super(value);
	}
}
