package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.rsa.publicKey node.
 * 
 * The 1024-bit RSA public key (modulus and exponent pair). The data is encoded as
 * a hexadecimal string of the followin format: [exponent] + [:] + [modulus]. For
 * example (public key truncated ofr clarity): "10001:E0C329B68C41F6E40FA8"
 */
public class NodeSysRsaPublicKey extends BaseSysRsaPublicKey {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysRsaPublicKey(String value) {
		super(value);
	}
}
