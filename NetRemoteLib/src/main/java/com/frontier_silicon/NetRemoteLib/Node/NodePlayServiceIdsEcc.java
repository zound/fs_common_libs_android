package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.serviceIds.ecc node.
 * 
 * Extended country code (ECC) valid in both FM and DAB.  Notifies when available
 * in FM
 */
public class NodePlayServiceIdsEcc extends BasePlayServiceIdsEcc {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayServiceIdsEcc(Long value) {
		super(value);
	}
}
