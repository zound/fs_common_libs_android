package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.listversion node.
 * 
 * 
 */
public class NodeNavPresetListversion extends BaseNavPresetListversion {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetListversion(Long value) {
		super(value);
	}
}
