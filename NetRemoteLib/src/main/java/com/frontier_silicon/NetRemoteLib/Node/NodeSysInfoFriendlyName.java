package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.friendlyName node.
 * 
 * Friendly name of device.
 */
public class NodeSysInfoFriendlyName extends BaseSysInfoFriendlyName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoFriendlyName(String value) {
		super(value);
	}
}
