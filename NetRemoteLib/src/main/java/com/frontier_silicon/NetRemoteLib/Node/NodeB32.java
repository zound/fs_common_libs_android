package com.frontier_silicon.NetRemoteLib.Node;

import java.util.List;

/*********************************************************************************
 * Base classes and interfaces that form the foundation of all node definitions. *
 *********************************************************************************/

/**
 * This is the base class for a 32-bit bit field node.
 * 
 * @param <T>		Enumeration type for the values of the bit field.
 */
@SuppressWarnings("rawtypes")
public abstract class NodeB32<T extends Enum> extends NodeToken<T> {
	protected long Value = 0;
	
	/**
	 * Constructs a node with the given value.
	 * 
	 * @param value		The numeric value of the node
	 */
	protected NodeB32(Long value) {
		super(32);
		this.Value = value;
	}
	
	/**
	 * Constructs a node with a value specified by a list of enumeration values.
	 * 
	 * @param values	The list of enumeration values
	 */
	protected NodeB32(List<T> values) {
		super(32);
		this.Value = 0;
		for (T value : values) {
			this.Value |= this.GetValueFromEnum(value); 
		}
	}
	
	/**
	 * Gets the numeric value of the node.
	 * 
	 * @return			The numeric value of the node
	 */
	public Long getValue() { return this.Value; }
	
	/**
	 * All derived classes must implement a method to return the enum values of this node.
	 * 
	 * @return		The list of possible enum values
	 */
	public abstract List<T> getValuesEnum();
	
	/**
	 * Returns whether or not the bit is set.
	 * 
	 * @param bit		The bit of interest
	 * @return			True if bit is set
	 */
	public boolean isSet(T bit) { return ((this.Value & (this.GetValueFromEnum(bit))) != 0); }

	/**
	 * All derived classes must implement a method to indicate whether or not unknown bits are set.
	 * 
	 * @return		True if bits are set that are unknown to the library
	 */
	public abstract boolean AreUnknownBitsSet();
	
	/**
	 * Gets a string representing the node value.
	 * 
	 * @return			A string representing the node value
	 */
	public String toString() {
		List<T> values = this.getValuesEnum();
		
		String result = "";
		
		for (T item : values) {
			if (result.length() > 0) {
				result += (" | " + item.toString());
			} else {
				result += item.toString();
			}
		}
		
		if (this.AreUnknownBitsSet()) {
			if (result.length() > 0) {
				result += (" + Unknown value(s)");
			} else {
				result = "Unknown value(s)";
			}
		}
		
		return ("<" + result + ">");
	}
	
	/**
	 * Test the equality of two nodes.
	 * 
	 * @param otherNode		The other node to test
	 * @return				True if the value of the two nodes are equal
	 */	
	public boolean equals(NodeInfo otherNode) {
		if (otherNode instanceof NodeB32<?>) {
			return this.Value == ((NodeB32<?>)otherNode).Value;
		}
		
		return false;
	}
}
