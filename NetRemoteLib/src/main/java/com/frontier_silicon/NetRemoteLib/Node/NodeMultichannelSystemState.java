package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.system.state node.
 *
 * The state of the device
 */

public class NodeMultichannelSystemState extends BaseMultichannelSystemState {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelSystemState(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelSystemState(Ord value) {
		super(value);
	}
}
