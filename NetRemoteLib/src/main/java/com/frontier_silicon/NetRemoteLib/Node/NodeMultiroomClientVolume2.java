package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.volume2 node.
 * 
 * Handle client 2 volume level
 */
public class NodeMultiroomClientVolume2 extends BaseMultiroomClientVolume2 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomClientVolume2(Long value) {
		super(value);
	}
}
