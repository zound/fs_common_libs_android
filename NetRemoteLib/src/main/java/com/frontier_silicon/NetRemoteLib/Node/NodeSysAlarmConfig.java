package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.alarm.config node.
 * 
 * Configuration alarm on/off, time, duration, source, preset (if DAB or FM),
 * repeat type, once alarm date, and volume.
 */
public class NodeSysAlarmConfig extends BaseSysAlarmConfig {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysAlarmConfig(Node value) throws NodeParseErrorException {
		super(value);
	}
}
