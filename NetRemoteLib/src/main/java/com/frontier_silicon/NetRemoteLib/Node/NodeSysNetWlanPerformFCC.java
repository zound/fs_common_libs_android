package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.performFCC node.
 *
 * Enable the FCC test
 */

public class NodeSysNetWlanPerformFCC extends BaseSysNetWlanPerformFCC {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanPerformFCC(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanPerformFCC(Ord value) {
		super(value);
	}
}
