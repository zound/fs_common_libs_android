package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.name node.
 * 
 * Return the group name
 */
public class NodeMultiroomGroupName extends BaseMultiroomGroupName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomGroupName(String value) {
		super(value);
	}
}
