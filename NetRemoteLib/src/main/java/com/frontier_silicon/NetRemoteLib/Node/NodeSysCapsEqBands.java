package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.caps.eqBands node.
 * 
 * Define the EQ bands that the radio supports, with user displayable label, and
 * range.  This list effectively describes the meanings and range of the items in
 * the netRemote.sys.audio.eq list node.
 * 
 * An exmaple list of EQ bands is: !======================================== !
 * Label ! Min ! Max
 * 
 * ! Bass ! -20 ! 20
 * 
 * ! Treble ! -20 ! 20 !========================================
 */
public class NodeSysCapsEqBands extends BaseSysCapsEqBands {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysCapsEqBands(Node value) throws NodeParseErrorException {
		super(value);
	}
}
