package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.action.context node.
 * 
 * Activate context menu for item. The value written must be the the index of item
 * with contextMenu set to TRUE.
 */
public class NodeNavActionContext extends BaseNavActionContext {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavActionContext(Long value) {
		super(value);
	}
}
