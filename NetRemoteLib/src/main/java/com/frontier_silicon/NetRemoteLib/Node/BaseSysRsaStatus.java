package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.rsa.status node.
 *
 * Inidicates whether the RSA key has been generated. Generation of the key can
 * take &gt; 1 minute.
 */
public class BaseSysRsaStatus extends NodeE8<BaseSysRsaStatus.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.rsa.status"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x1010a100; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return true; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseSysRsaStatus(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysRsaStatus() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseSysRsaStatus(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value GENERATION_IN_PROGRESS.
         */
        GENERATION_IN_PROGRESS, 
        /**
         * Enumeration value KEY_AVAILABLE.
         */
        KEY_AVAILABLE, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case GENERATION_IN_PROGRESS: return (long)0;
			case KEY_AVAILABLE: return (long)1;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.GENERATION_IN_PROGRESS;
			case 1: return Ord.KEY_AVAILABLE;
		}
		return null;
	}
}
