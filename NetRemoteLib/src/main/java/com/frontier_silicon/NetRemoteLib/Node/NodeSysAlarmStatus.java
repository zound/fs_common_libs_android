package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.alarm.status node.
 *
 * Alarm status: idle/alarming/snoozing.
 */

public class NodeSysAlarmStatus extends BaseSysAlarmStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysAlarmStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysAlarmStatus(Ord value) {
		super(value);
	}
}
