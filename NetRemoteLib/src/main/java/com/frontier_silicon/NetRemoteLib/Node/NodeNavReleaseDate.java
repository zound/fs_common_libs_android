package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.releaseDate node.
 * 
 * Album release date
 */
public class NodeNavReleaseDate extends BaseNavReleaseDate {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
    public NodeNavReleaseDate(Long value) {
        super(value);
    }
}
