package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.ipod.dockStatus node.
 *
 * iPod dock status
 */

public class NodeSysIpodDockStatus extends BaseSysIpodDockStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysIpodDockStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysIpodDockStatus(Ord value) {
		super(value);
	}
}
