package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.wpsPinRead node.
 * 
 * Reads the PIN for user display
 */
public class NodeSysNetWlanWpsPinRead extends BaseSysNetWlanWpsPinRead {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanWpsPinRead(String value) {
		super(value);
	}
}
