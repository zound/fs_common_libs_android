package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.ipConfig.dnsPrimary node.
 * 
 * Primary DNS for the interface
 */
public class NodeSysNetIpConfigDnsPrimary extends BaseSysNetIpConfigDnsPrimary {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetIpConfigDnsPrimary(Long value) {
		super(value);
	}
}
