package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.errorStr node.
 * 
 * A user displayable error string which explains the cause of the most recent
 * error, or an empty string.  This is updated when the status becomes FAIL or
 * FATAL_ERR
 */
public class NodeNavErrorStr extends BaseNavErrorStr {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavErrorStr(String value) {
		super(value);
	}
}
