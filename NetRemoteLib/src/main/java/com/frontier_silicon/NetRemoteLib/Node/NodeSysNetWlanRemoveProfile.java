package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.removeProfile node.
 * 
 * SSID extracted from the wlan.profiles node
 */
public class NodeSysNetWlanRemoveProfile extends BaseSysNetWlanRemoveProfile {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanRemoveProfile(String value) {
		super(value);
	}
}
