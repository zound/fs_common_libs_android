package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.airableQuality node.
 *
 * Airable audio quality level
 */

public class NodeSysAudioAirableQuality extends BaseSysAudioAirableQuality {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysAudioAirableQuality(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysAudioAirableQuality(Ord value) {
		super(value);
	}
}
