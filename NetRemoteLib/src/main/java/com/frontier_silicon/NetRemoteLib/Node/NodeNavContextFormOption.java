package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.nav.context.form.option node.
 * 
 * List of comboboxes used by forms
 */
public class NodeNavContextFormOption extends BaseNavContextFormOption {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeNavContextFormOption(Node value) throws NodeParseErrorException {
		super(value);
	}
}
