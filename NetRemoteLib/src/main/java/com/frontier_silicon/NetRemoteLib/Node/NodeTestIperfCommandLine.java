package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.test.iperf.commandLine node.
 * 
 * On set prepare a new iperf instance with the specified command line arguments.
 * Each time the node is set a new instance of the iperf is created.  The iperf
 * instances are only started when netRemote.test.iperf.execute is set to START.
 * To remove all current iperf instances set the node to "delete" when execute is
 * STOP.  If "delete" is set when one or more iperf instances are still running the
 * set will return FS_FAIL.  For example: "-c x.x.x.x -u -i 1 -b 1M -p 5003 -t 40"
 * where x.x.x.x is the iperf server IP address. On get returns a string containing
 * the list of all iperf instance command line arguments with the following format:
 * " ---- Task [x] command line: [iperf arg]" where [x] is the instance number and
 * [iperf arg] is the arguments.
 */
public class NodeTestIperfCommandLine extends BaseTestIperfCommandLine {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeTestIperfCommandLine(String value) {
		super(value);
	}
}
