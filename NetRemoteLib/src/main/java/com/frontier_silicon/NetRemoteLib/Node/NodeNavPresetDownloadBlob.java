package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.download.blob node.
 * 
 * 
 */
public class NodeNavPresetDownloadBlob extends BaseNavPresetDownloadBlob {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetDownloadBlob(String value) {
		super(value);
	}
}
