package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.alerttone node.
 *
 * Request to play a short alert tone, at alert volume level, regardless of the
 * active mode.
 */

public class NodePlayAlerttone extends BasePlayAlerttone {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayAlerttone(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayAlerttone(Ord value) {
		super(value);
	}
}
