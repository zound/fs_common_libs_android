package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.misc.fsDebug.component node.
 * 
 * FsDebug Component
 */
public class NodeMiscFsDebugComponent extends BaseMiscFsDebugComponent {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMiscFsDebugComponent(Long value) {
		super(value);
	}
}
