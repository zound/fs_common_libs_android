package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.connectedSSID node.
 * 
 * The name of the Wireless LAN to which this station is connected
 */
public class NodeSysNetWlanConnectedSSID extends BaseSysNetWlanConnectedSSID {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanConnectedSSID(String value) {
		super(value);
	}
}
