package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.caps.clockSourceList node.
 * 
 * Return clock sources
 */
public class NodeSysCapsClockSourceList extends BaseSysCapsClockSourceList {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysCapsClockSourceList(Node value) throws NodeParseErrorException {
		super(value);
	}
}
