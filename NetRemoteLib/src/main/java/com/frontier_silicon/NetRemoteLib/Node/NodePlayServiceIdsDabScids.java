package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.serviceIds.dabScids node.
 * 
 * DAB SCIDS
 */
public class NodePlayServiceIdsDabScids extends BasePlayServiceIdsDabScids {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayServiceIdsDabScids(Long value) {
		super(value);
	}
}
