package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.caps.fmFreqRange.upper node.
 * 
 * Upper frequency (in KHz) range upto which the FM tuner support
 */
public class NodeSysCapsFmFreqRangeUpper extends BaseSysCapsFmFreqRangeUpper {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysCapsFmFreqRangeUpper(Long value) {
		super(value);
	}
}
