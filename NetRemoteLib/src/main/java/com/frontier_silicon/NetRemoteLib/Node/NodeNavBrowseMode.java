package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.browseMode node.
 * 
 * Mode used for browsing the netRemote.nav.* nodes. When the mode is changed the
 * device will start navigating the new mode from the root tree. The value is an
 * index into the netRemote.sys.caps.validModes list - indicating the current
 * browse mode, with the special case that the invalid key means that the mode
 * specified in netRemote.sys.mode is currently selected.
 */
public class NodeNavBrowseMode extends BaseNavBrowseMode {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavBrowseMode(Long value) {
		super(value);
	}
}
