package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.addPresetStatus node.
 *
 * Node to provide preset availability
 */

public class NodePlayAddPresetStatus extends BasePlayAddPresetStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayAddPresetStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayAddPresetStatus(Ord value) {
		super(value);
	}
}
