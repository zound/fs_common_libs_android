package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.volume3 node.
 * 
 * Handle client 3 volume level
 */
public class NodeMultiroomClientVolume3 extends BaseMultiroomClientVolume3 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomClientVolume3(Long value) {
		super(value);
	}
}
