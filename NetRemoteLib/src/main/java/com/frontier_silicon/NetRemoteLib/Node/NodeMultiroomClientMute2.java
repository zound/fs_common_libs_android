package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.mute2 node.
 *
 * Handle client 2 mute
 */

public class NodeMultiroomClientMute2 extends BaseMultiroomClientMute2 {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientMute2(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientMute2(Ord value) {
		super(value);
	}
}
