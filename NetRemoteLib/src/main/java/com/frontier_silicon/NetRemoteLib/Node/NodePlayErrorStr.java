package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.errorStr node.
 * 
 * A user displayable error string which explains the cause of the most play recent
 * error, or an empty string.  This is updated when the status becomes ERROR
 */
public class NodePlayErrorStr extends BasePlayErrorStr {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayErrorStr(String value) {
		super(value);
	}
}
