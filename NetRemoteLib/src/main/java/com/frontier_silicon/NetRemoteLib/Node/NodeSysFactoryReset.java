package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.factoryReset node.
 *
 * Node for factory resetting the radio from the application.  When set to RESET
 * the module is rebooted and all persistent nodes are reset to their default
 * state.  Setting to NONE has no effect.
 */

public class NodeSysFactoryReset extends BaseSysFactoryReset {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysFactoryReset(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysFactoryReset(Ord value) {
		super(value);
	}
}
