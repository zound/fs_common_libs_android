package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.nav.form.option node.
 * 
 * List of comboboxes used by forms
 */
public class NodeNavFormOption extends BaseNavFormOption {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeNavFormOption(Node value) throws NodeParseErrorException {
		super(value);
	}
}
