package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.keepConnected node.
 *
 * To keep the network connection even in other modes like FM/DAB etc.
 */

public class NodeSysNetKeepConnected extends BaseSysNetKeepConnected {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetKeepConnected(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetKeepConnected(Ord value) {
		super(value);
	}
}
