package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.test.iperf.commandLine node.
 *
 * On set prepare a new iperf instance with the specified command line arguments.
 * Each time the node is set a new instance of the iperf is created.  The iperf
 * instances are only started when netRemote.test.iperf.execute is set to START.
 * To remove all current iperf instances set the node to "delete" when execute is
 * STOP.  If "delete" is set when one or more iperf instances are still running the
 * set will return FS_FAIL.  For example: "-c x.x.x.x -u -i 1 -b 1M -p 5003 -t 40"
 * where x.x.x.x is the iperf server IP address. On get returns a string containing
 * the list of all iperf instance command line arguments with the following format:
 * " ---- Task [x] command line: [iperf arg]" where [x] is the instance number and
 * [iperf arg] is the arguments.
 */
public class BaseTestIperfCommandLine extends NodeC implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.test.iperf.commandLine"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10710100; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.C8, 256));
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

	/**
	 * Construct a new node using a string value.
	 *
	 * @param value      The value of the node
	 */
	public BaseTestIperfCommandLine(String value) {
		super(256, value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseTestIperfCommandLine() {
		super();
	}
}
