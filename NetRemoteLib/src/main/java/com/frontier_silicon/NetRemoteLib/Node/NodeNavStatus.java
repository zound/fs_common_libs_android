package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.status node.
 *
 * The current status of the navigation context. The meanings of the individual
 * states are:
 * 
 * !======================================== ! Value ! Meaning
 * 
 * ! WAITING ! the list items are currently being fetched, and the list isn't
 * available.
 * 
 * ! READY ! the list items are available (from netRemote.nav.list).
 * 
 * ! FAIL ! the list navigation has failed, but the previous list is still
 * available.  The navigation operation can be retried
 * 
 * ! FATAL_ERR  ! a fatal error has occurred in navigation, retrying will not help.
 * The list items are not available.
 * 
 * ! READY_ROOT ! the list items are available (as READY), but navigation has
 * reverted to the root. !========================================
 */

public class NodeNavStatus extends BaseNavStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavStatus(Ord value) {
		super(value);
	}
}
