package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.id node.
 * 
 * Get the device's group id
 */
public class NodeMultiroomGroupId extends BaseMultiroomGroupId {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomGroupId(String value) {
		super(value);
	}
}
