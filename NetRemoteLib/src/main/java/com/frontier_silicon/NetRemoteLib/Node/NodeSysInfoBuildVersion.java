package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.buildVersion node.
 * 
 * Incremental Build Version string of the firmware software.
 */
public class NodeSysInfoBuildVersion extends BaseSysInfoBuildVersion {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoBuildVersion(String value) {
		super(value);
	}
}
