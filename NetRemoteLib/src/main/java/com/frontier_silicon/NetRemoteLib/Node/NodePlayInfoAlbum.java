package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.info.album node.
 * 
 * Album for current audio, if known and appropriate
 */
public class NodePlayInfoAlbum extends BasePlayInfoAlbum {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayInfoAlbum(String value) {
		super(value);
	}
}
