package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.upload.blob node.
 * 
 * 
 */
public class NodeNavPresetUploadBlob extends BaseNavPresetUploadBlob {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetUploadBlob(String value) {
		super(value);
	}
}
