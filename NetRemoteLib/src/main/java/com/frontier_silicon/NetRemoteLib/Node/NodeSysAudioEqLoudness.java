package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.eqLoudness node.
 *
 * Enable loudness function in EQ chip.  Note that this setting is only used if the
 * first (custom) EQ preset is selected
 */

public class NodeSysAudioEqLoudness extends BaseSysAudioEqLoudness {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysAudioEqLoudness(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysAudioEqLoudness(Ord value) {
		super(value);
	}
}
