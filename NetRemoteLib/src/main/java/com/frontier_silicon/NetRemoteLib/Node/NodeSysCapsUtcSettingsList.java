package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.caps.utcSettingsList node.
 * 
 * Return UTC timezone settings available
 */
public class NodeSysCapsUtcSettingsList extends BaseSysCapsUtcSettingsList {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysCapsUtcSettingsList(Node value) throws NodeParseErrorException {
		super(value);
	}
}
