package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.formData node.
 * 
 * Data to fill up an airable form, in conjunction with a list item of type
 * FORM_ITEM.
 */
public class NodeNavFormData extends BaseNavFormData {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavFormData(String value) {
		super(value);
	}
}
