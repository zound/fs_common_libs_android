package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.download.artworkUrl node.
 * 
 * 
 */
public class NodeNavPresetDownloadArtworkUrl extends BaseNavPresetDownloadArtworkUrl {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetDownloadArtworkUrl(String value) {
		super(value);
	}
}
