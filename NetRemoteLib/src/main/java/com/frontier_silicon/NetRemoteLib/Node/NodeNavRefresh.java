package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.refresh node.
 *
 * Navigation node to force refresh of the item list after the issue of an airable
 * action or message
 */

public class NodeNavRefresh extends BaseNavRefresh {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavRefresh(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavRefresh(Ord value) {
		super(value);
	}
}
