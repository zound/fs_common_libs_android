package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.net.wlan.scanList node.
 * 
 * List of access points available
 */
public class NodeSysNetWlanScanList extends BaseSysNetWlanScanList {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysNetWlanScanList(Node value) throws NodeParseErrorException {
		super(value);
	}
}
