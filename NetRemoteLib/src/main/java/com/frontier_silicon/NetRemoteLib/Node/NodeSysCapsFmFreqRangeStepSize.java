package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.caps.fmFreqRange.stepSize node.
 * 
 * size of frequency step (In the UK and Europe this is 50 kHz &amp; in the US it is
 * 100 kHz)
 */
public class NodeSysCapsFmFreqRangeStepSize extends BaseSysCapsFmFreqRangeStepSize {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysCapsFmFreqRangeStepSize(Long value) {
		super(value);
	}
}
