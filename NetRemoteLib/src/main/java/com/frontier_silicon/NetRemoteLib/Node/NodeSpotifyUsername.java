package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.spotify.username node.
 * 
 * Read only node that returns the username of the Spotify account
 */
public class NodeSpotifyUsername extends BaseSpotifyUsername {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSpotifyUsername(String value) {
		super(value);
	}
}
