package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.uap.interfaceEnable node.
 *
 * uAP global state enable/disable
 */

public class NodeSysNetUapInterfaceEnable extends BaseSysNetUapInterfaceEnable {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetUapInterfaceEnable(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetUapInterfaceEnable(Ord value) {
		super(value);
	}
}
