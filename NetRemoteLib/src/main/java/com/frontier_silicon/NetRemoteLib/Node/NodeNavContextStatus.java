package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.context.status node.
 *
 * The current status of the navigation context. For the meanings of the individual
 * states see: nav.status
 */

public class NodeNavContextStatus extends BaseNavContextStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavContextStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavContextStatus(Ord value) {
		super(value);
	}
}
