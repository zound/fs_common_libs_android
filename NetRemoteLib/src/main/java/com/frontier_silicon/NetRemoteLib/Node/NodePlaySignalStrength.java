package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.signalStrength node.
 * 
 * Returns an estimation of the current SNR level in cB (1dB = 10 cB)  (for dab) or
 * current signal strength in -dBm (for FM)
 */
public class NodePlaySignalStrength extends BasePlaySignalStrength {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlaySignalStrength(Long value) {
		super(value);
	}
}
