package com.frontier_silicon.NetRemoteLib.Node;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.LogLevel;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.HashMap;
import java.util.Map;

/**
 * This class defines a single list item used by list nodes.
 * 
 */
public abstract class NodeListItem {
	protected Map<NodeListItem.Field, Object> Value = new HashMap<>(Field.values().length);

	/**
	 * This helper defines all possible fields that can be used in an item/entry within a list node.
	 * 
	 */
	protected enum Field {
		key, id, selectable, label, min, max, langlabel, index, freq, ssid, privacy, wpscapability, 
		name, type, subtype, on, time, duration, source, preset, repeat, date, volume, udn,
		friendlyname, ipaddress, audiosyncversion, groupid, groupname, grouprole, clientnumber,
		streamable, region, modetype, devicestate, devicename, uniqid, rssi, blob, artworkurl,
        description, optionscount, graphicuri, contextmenu, formitemtype, artist, systemid, systemrole,
        systemname, playlisturl
	}

	/**
	 * This helper defines the possible values of the "Repeat" field used by list items.
	 * 
	 */
	public enum FieldRepeat {
		/**
		 * no repeat
		 */
		Empty,
		/**
		 * repeat everyday
		 */
		Everyday,
		/**
		 * just once
		 */
		Once,
		/**
		 * repeat in weekend only
		 */
		Weekend,
		/**
		 * repeat weekdays only
		 */
		Weekday
	}
	
	/**
	 * This helper defines the possible values of the "Source" field used by list items.
	 * 
	 */
	public enum FieldSource {
		/**
		 * Manual source
		 */
		Manual,
		/**
		 * DAB source
		 */
		DAB,
		/**
		 * FM source
		 */
		FM,
		/**
		 * SNTP source (Simple Network Time Protocol)
		 */
		SNTP
	}
	
	/**
	 * This helper defines the possible values of the "Type" field used by list items.
	 * 
	 */
	public enum FieldType {
		/**
		 * A directory.
		 */
		Directory,
		/**
		 * A playable item.
		 */
		PlayableItem,
		/**
		 * An item that will pop up a search dialogue, and then present the results as a directory.
		 */
		SearchDirectory,
		/**
		 * An item on unknown type.
		 */
		Unknown,
		/**
		 * An item that will contain forms data.
		 */
		FormItem,
		/**
		 * An item that will contain the data of a message
		 */
		MessageItem,
		/**
		 * An item that will contain the information that we need to login on Amazon
		 */
		AmazonLogin,
		/**
		 * An error occurred while retrieving the item.
		 */
		FetchErrItem
	}

	/**
	 * This helper defines the possible values of the "SubType" field used by list items.
	 * 
	 */
	public enum FieldSubType {
		/**
		 * Type unknown.
		 */
		None,
		/**
		 * A station.
		 */
		Station,
		/**
		 * A podcast.
		 */
		Podcast,
		/**
		 * A track.
		 */
		Track,
		/**
		 * Type TEXT.
		 */
		Text,
		/**
		 * A PASSWORD.
		 */
		Password,
		/**
		 * An option.
		 */
		Options,
		/**
		 * submit.
		 */
		Submit,
		/**
		 * A button.
		 */
		Button,
		/**
		 *  Unknown Airable track
		 */
		Disabled
	}
	/**
	 * Created by adanaila on 10/7/2016.
	 * List of types for Form Items
	 */

	public enum FormItemType {
		STRING,
		PASSWORD,
		COMBOBOX,
		BUTTON,
		DESCRIPTION
	}
	
	/**
	 * Device list Is server selectable field
	 */
	public enum FieldGroupRole {
		/**
		 * No Group
		 */
		No_Group,
		/**
		 * Is Client
		 */
		Client,
		/**
		 * Is Server
		 */
		Server
	}

    /**
     * Returns MODE_SPECIFIC or GENERIC depending on mode specifications in multiroom.
     * This can be used for example in music player if in a group a specific client has USB device connected
     */
    public enum ModeType {
        Generic,
        ModeSpecific
    }

	/**
	* the values for contextMenu, if it is present on item or not.
	 */
	public enum FieldContextMenu{
		False,
		True
	}

    /**
     * netRemote.bluetooth.connectedDevices
     */
    public enum DeviceState {
        Disconnected,
        Connected,
        Paired
    }

    public enum AlarmSource {
        Buzzer,
        DAB,
        FM,
        Ipod
    }

    /**
     *  Field from netRemote.multiroom.device.listAll. Indicates the state of stereo speakers
     *  */
    public enum SystemRole {
        Independent,
        Primary,
        Secondary
    }
	
	/**
	 * Constructs a node list entry from the given XML.
	 * 
	 * @param xml		The XML specifying the fields of the node list entry
	 * @throws NodeParseErrorException 
	 */
	protected NodeListItem(Node item) throws NodeDefs.NodeParseErrorException {
		/* Expect to get something like:
		   		<item key="0">
		   			<field name="id"><c8_array>DAB</c8_array></field>
		   			<field name="selectable"><u8>1</u8></field>
		   			<field name="label"><c8_array>DAB</c8_array></field>
		   		</item>
		*/
		
		NamedNodeMap attr = item.getAttributes();
		if (attr != null) {
			Node key = attr.getNamedItem("key");
			if (key != null) {
				String keyValue = key.getNodeValue();
				if (keyValue != null) {
					/* Remember the key. */
					this.Value.put(NodeListItem.Field.key, Long.valueOf(keyValue));
					
					/* Do the other fields. */
					org.w3c.dom.NodeList fields = item.getChildNodes();
					for (int loop = 0; loop < fields.getLength(); loop += 1) { 
						Node field = fields.item(loop);
						String name = field.getNodeName(); 
						if (name.equals("field")) {
							attr = field.getAttributes();
							if (attr != null) {
								Node fieldNameAttr = attr.getNamedItem("name");
								if (fieldNameAttr != null) {
									String fieldName = fieldNameAttr.getNodeValue();
									if (fieldName != null) {
										/* We've got the field name in a form which is appropriate for our map. */
										fieldName = fieldName.toLowerCase();
										
										/* Check it corresponds to something in the enumeration. */
										NodeListItem.Field fieldAsEnum = null;
										try {
											fieldAsEnum = NodeListItem.Field.valueOf(fieldName);
										} catch (Exception e) { }
										
										if (fieldAsEnum != null) {
											/* Now get the value. */
											Node child = NodeDefs.GetFirstChildOfInterest(field);
											if (child != null) {
												Object valueAsObject = NodeDefs.ParseValue(child);
												
												/* Check it was understood / parsed as something. */
												if (!(Node.class.isAssignableFrom(valueAsObject.getClass()))) {
													this.Value.put(fieldAsEnum, valueAsObject);
												} else {
													throw new NodeDefs.NodeParseErrorException();													
												}
											} else {
												throw new NodeDefs.NodeParseErrorException();
											}
										} else {
											NetRemote.log(LogLevel.Error, fieldName + " field is not implemented yet in NetRemote nodes");
										}
									} else {
										throw new NodeDefs.NodeParseErrorException();
									}
								} else {
									throw new NodeDefs.NodeParseErrorException();
								}
							} else {
								throw new NodeDefs.NodeParseErrorException();
							}
						}
					}
					
					return;
				}
			}
		}
		
		throw new NodeDefs.NodeParseErrorException();
	}

    protected String getFieldByNameAsString(NodeListItem.Field field) {
        String value = null;
        if (this.Value.containsKey(field)) {
            value = (String)this.Value.get(field);
        }
        return value;
    }

    protected Long getFieldByNameAsLong(NodeListItem.Field field) {
        Long value = null;
        if (this.Value.containsKey(field)) {
            value = (Long) this.Value.get(field);
        }
        return value;
    }
	
	protected Object getFieldByName(NodeListItem.Field field) {
		if (this.Value.containsKey(field)) {
			Object value = this.Value.get(field);
			
			switch (field) {
				case streamable:
				case selectable:
				case privacy:
				case wpscapability: {
					if (value == null)
						return false;
					return (((Long)value) != 0);
				}
				case type: {
					int number = ((Long)value).intValue();
					switch (number) {
						case 0: return FieldType.Directory;
						case 1: return FieldType.PlayableItem;
						case 2: return FieldType.SearchDirectory;
						case 3: return FieldType.Unknown;
						case 4: return FieldType.FormItem;
						case 5: return FieldType.MessageItem;
						case 6: return FieldType.AmazonLogin;
						case 255: return FieldType.FetchErrItem;
					}
					break;
				}
				case subtype: {
					int number = ((Long)value).intValue();
					switch (number) {
						case 0: return FieldSubType.None;
						case 1: return FieldSubType.Station;
						case 2: return FieldSubType.Podcast;
						case 3: return FieldSubType.Track;
						case 4: return FieldSubType.Text;
						case 5: return FieldSubType.Password;
						case 6: return FieldSubType.Options;
						case 7: return FieldSubType.Submit;
						case 8: return FieldSubType.Button;
						case 9: return FieldSubType.Disabled;
					}
					break;
				}
				case repeat: {
					int number = ((Long)value).intValue();
					switch (number) {
						case 0: return FieldRepeat.Empty;
						case 1: return FieldRepeat.Everyday;
						case 2: return FieldRepeat.Once;
						case 3: return FieldRepeat.Weekend;
						case 4: return FieldRepeat.Weekday;
					}
					break;
				}
				case grouprole: {
					int number = ((Long)value).intValue();
					switch (number) {
						case 0: return FieldGroupRole.No_Group;
						case 1: return FieldGroupRole.Client;
						case 2: return FieldGroupRole.Server;
					}
					break;
				}
                case source: {
                    String strSource = (String)value;
                    if (strSource.contentEquals("DAB")) {
                        return FieldSource.DAB;
                    } else if (strSource.contentEquals("FM")) {
                        return FieldSource.FM;
                    } else if (strSource.contentEquals("NETWORK")) {
                        return FieldSource.SNTP;
                    }else if (strSource.contentEquals("SNTP")) {
						return FieldSource.SNTP;
					} else if (strSource.contentEquals("NO_UPDATE")) {
                        return FieldSource.Manual;
                    }
					break;
                }
                case modetype: {
                    int number = ((Long) value).intValue();
                    switch (number) {
                        case 0: return ModeType.Generic;
                        case 1: return ModeType.ModeSpecific;
                    }
                    break;
                }
                case devicestate: {
                    int number = ((Long) value).intValue();
                    switch (number) {
                        case 0: return DeviceState.Disconnected;
                        case 1: return DeviceState.Connected;
                        case 2: return DeviceState.Paired;
                    }
                    break;
                }

                case formitemtype: {
                    int number = ((Long)value).intValue();
                    switch (number) {
                        case 0: return FormItemType.STRING;
                        case 1: return FormItemType.PASSWORD;
                        case 2: return FormItemType.COMBOBOX;
                        case 3: return FormItemType.BUTTON;
                        case 4: return FormItemType.DESCRIPTION;
                    }
                    break;
                }

				case contextmenu:{
					int number = ((Long)value).intValue();
					switch (number) {
						case 0:
							return FieldContextMenu.False;
						case 1:
							return FieldContextMenu.True;
					}
					break;
				}

                case systemrole: {
                    int number = ((Long)value).intValue();
                    switch (number) {
                        case 0:
                            return SystemRole.Independent;
                        case 1:
                            return SystemRole.Primary;
                        case 2:
                            return SystemRole.Secondary;
                    }
                    break;
                }

                default: {
					return value;	
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Get the key of this entry.
	 * 
	 * @return		The key
	 */
    public Long getKey() { return (Long)(this.getFieldByName(NodeListItem.Field.key)); }
    protected boolean getSelectable() { return (Boolean)(this.getFieldByName(NodeListItem.Field.selectable)); }

	/**
	 * Allows the selectable field of a radio mode to be enabled
 	 */
	protected void setSelectable() { this.Value.put(NodeListItem.Field.selectable, 1L); }
	protected String getSSID() {
        Object value = this.getFieldByName(NodeListItem.Field.ssid);
        return (String)value;
    }
	protected boolean getPrivacy() { return (Boolean)(this.getFieldByName(NodeListItem.Field.privacy)); }
	protected boolean getWpsCapability() { return (Boolean)(this.getFieldByName(NodeListItem.Field.wpscapability)); }
	protected FieldType getEType() { return (FieldType)(this.getFieldByName(NodeListItem.Field.type)); }
	protected FieldSubType getSubType() { return (FieldSubType)(this.getFieldByName(NodeListItem.Field.subtype)); }
	protected boolean getOn() { return (Boolean)(this.getFieldByName(NodeListItem.Field.on)); }
	protected FieldContextMenu getContextMenu(){ return (FieldContextMenu)(this.getFieldByName(Field.contextmenu));}
    protected FormItemType getFormItemType(){ return (FormItemType)(this.getFieldByName(Field.formitemtype));}
	protected FieldSource getSource() { return (FieldSource)(this.getFieldByName(NodeListItem.Field.source)); }
	protected FieldRepeat getRepeat() { return (FieldRepeat)(this.getFieldByName(NodeListItem.Field.repeat)); }
	protected FieldGroupRole getGroupRole() { return (FieldGroupRole)(this.getFieldByName(NodeListItem.Field.grouprole)); }
    protected SystemRole getSystemRole() { return (SystemRole)this.getFieldByName(Field.systemrole); }
	protected boolean getStreamable() {
		Boolean isStreamable = (Boolean) this.getFieldByName(NodeListItem.Field.streamable);
		if (isStreamable == null) {
			isStreamable = false;
		}		
		return isStreamable;
	}
    protected ModeType getModeType() { return (ModeType)this.getFieldByName(Field.modetype); }
    protected DeviceState getDeviceState() { return (DeviceState)this.getFieldByName(Field.devicestate); }
    protected AlarmSource getAlarmSource() {
        if (this.Value.containsKey(Field.source)) {
            Object value = this.Value.get(Field.source);
            int number = ((Long)value).intValue();
            switch (number) {
                case 0:
                    return AlarmSource.Buzzer;
                case 1:
                    return AlarmSource.DAB;
                case 2:
                    return AlarmSource.FM;
                case 3:
                    return AlarmSource.Ipod;
            }
        }
        return null;
    }
	
	public String toString() {
		String label = this.getFieldByNameAsString(Field.label);
		if (label != null) {
			return label;
		}
		
		return super.toString();
	}
}
