package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.version node.
 * 
 * Release Version string of the firmware software.
 */
public class NodeSysInfoVersion extends BaseSysInfoVersion {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoVersion(String value) {
		super(value);
	}
}
