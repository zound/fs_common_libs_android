package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.misc.nvs.data node.
 * 
 * NVS area to store information for APPs, like the speaker setup mode
 */
public class NodeMiscNvsData extends BaseMiscNvsData {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMiscNvsData(String value) {
		super(value);
	}
}
