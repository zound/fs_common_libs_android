package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.rsa.status node.
 *
 * Inidicates whether the RSA key has been generated. Generation of the key can
 * take &gt; 1 minute.
 */

public class NodeSysRsaStatus extends BaseSysRsaStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysRsaStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysRsaStatus(Ord value) {
		super(value);
	}
}
