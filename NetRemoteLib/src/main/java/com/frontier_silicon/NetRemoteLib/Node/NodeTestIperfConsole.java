package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.test.iperf.console node.
 * 
 * Read only node that returns any new output combined from all the running
 * instances. On a get clears the log of the output.
 */
public class NodeTestIperfConsole extends BaseTestIperfConsole {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeTestIperfConsole(String value) {
		super(value);
	}
}
