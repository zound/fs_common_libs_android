package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.device.transportOptimisation node.
 *
 * Node used to disable or enable the transport optimisation
 */

public class NodeMultiroomDeviceTransportOptimisation extends BaseMultiroomDeviceTransportOptimisation {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomDeviceTransportOptimisation(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomDeviceTransportOptimisation(Ord value) {
		super(value);
	}
}
