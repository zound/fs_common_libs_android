package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.status1 node.
 *
 * Client 1 status
 */

public class NodeMultiroomClientStatus1 extends BaseMultiroomClientStatus1 {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientStatus1(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientStatus1(Ord value) {
		super(value);
	}
}
