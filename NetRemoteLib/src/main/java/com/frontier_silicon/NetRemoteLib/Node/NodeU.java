package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The abstract base class for byte array nodes.
 */
public abstract class NodeU {
	protected String Value = "";
	
	protected long MaximumSize;
	
	/**
	 * Get the maximum length allowable for data held by this node.
	 * 
	 * @return			The maximum length of the data
	 */
	public long getMaximumLength() { return this.MaximumSize; }
	
	protected NodeU(long size, String value) {
		this.MaximumSize = size;
		
		if (value != null) {
			//this.Value = value.substring(0, Math.min((int)size, value.length()));
			this.Value = value; // Size check is done in setNode.
		}
	}
	
	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	NodeU() {
	}
	
	/**
	 * The value of this node.
	 * 
	 * @return			The node value
	 */
	public String getValue() { return this.Value; }
	
	/**
	 * Gets a string representing the node value.
	 * 
	 * @return			A string representing the node value
	 */
	public String toString() { return this.Value; }
	
	/**
	 * Test the equality of two nodes.
	 * 
	 * @param otherNode		The other node to test
	 * @return				True if the value of the two nodes are equal
	 */	
	public boolean equals(NodeInfo otherNode) {
		if (otherNode instanceof NodeU) {
			return this.Value.equals(((NodeC)otherNode).Value);
		}
		
		return false;
	}
}
