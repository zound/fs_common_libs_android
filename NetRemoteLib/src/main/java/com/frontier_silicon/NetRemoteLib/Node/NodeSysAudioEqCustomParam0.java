package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.eqCustom.param0 node.
 * 
 * Setting for first custom EQ parameter (if defined)
 */
public class NodeSysAudioEqCustomParam0 extends BaseSysAudioEqCustomParam0 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAudioEqCustomParam0(Long value) {
		super(value);
	}
}
