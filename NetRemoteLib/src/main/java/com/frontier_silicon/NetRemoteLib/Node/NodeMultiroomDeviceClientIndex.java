package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.device.clientIndex node.
 * 
 * Node used to retrive the client index in the group context
 */
public class NodeMultiroomDeviceClientIndex extends BaseMultiroomDeviceClientIndex {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomDeviceClientIndex(Long value) {
		super(value);
	}
}
