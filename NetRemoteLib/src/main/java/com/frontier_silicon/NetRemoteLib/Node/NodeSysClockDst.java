package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.clock.dst node.
 *
 * Whether daylight savings is enabled or not. (For netRemote.sys.clock.source ==
 * 3(SNTP))
 */

public class NodeSysClockDst extends BaseSysClockDst {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysClockDst(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysClockDst(Ord value) {
		super(value);
	}
}
