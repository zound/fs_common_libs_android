package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.play.info.name node.
 *
 * Headline name of current audio - e.g. station name, podcast name, or track
 * title.  Notifies on change.  This node always returns a useful string if audio
 * is playing (with the exception of aux-in, where no useful information is
 * available)
 */
public class BasePlayInfoName extends NodeC implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.play.info.name"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10304010; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return true; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return true; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return true; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.C8, 65));
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

	/**
	 * Construct a new node using a string value.
	 *
	 * @param value      The value of the node
	 */
	public BasePlayInfoName(String value) {
		super(65, value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BasePlayInfoName() {
		super();
	}
}
