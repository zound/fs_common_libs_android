package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.interfaceEnable node.
 *
 * To enable the wireless interface
 */

public class NodeSysNetWlanInterfaceEnable extends BaseSysNetWlanInterfaceEnable {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanInterfaceEnable(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanInterfaceEnable(Ord value) {
		super(value);
	}
}
