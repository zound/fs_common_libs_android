package com.frontier_silicon.NetRemoteLib.Node;

import java.text.DecimalFormat;

/**
 * The definition for the netRemote.play.frequency node.
 * 
 * For FM, tune to a specific frequency in KHz. For DAB, enter manual tune screen
 * and tune to the frequency corrensponding to the specified Key. Use the invalid
 * key (0xFFFFFFFF) to leave the manual tune state.  Notifies whenever it changes.
 */
public class NodePlayFrequency extends BasePlayFrequency {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayFrequency(Long value) {
		super(value);
	}

	/**
	 * Get the value of this frequency node in MHz.
	 * 
	 * @return		The frequency in MHz
	 */
	public double getFrequencyInMHz() {
		return ((double)this.Value) / 1000.0;
	}
	
	/**
	 * Get the value of this frequency node in MHz, returned as a string.
	 * 
	 * @return		The frequency in MHz as a string
	 */
	public String getFrequencyInMHzAsString() {
		double frequency = this.getFrequencyInMHz();
		DecimalFormat formatter = new DecimalFormat("#.00");
		String result = formatter.format(frequency);
		return result;
	}
	
	/**
	 * Get the value of this frequency node in a form suitable for use by RadioVIS.
	 * 
	 * @return		The frequency in MHz in a form suitable for RadioVIS
	 */
	public String getFrequencyForRadioDNS() {
		double frequency = this.getFrequencyInMHz();
		DecimalFormat formatter = new DecimalFormat("000.00");
		String frequencyAsString = formatter.format(frequency);
		
		return frequencyAsString.substring(0, 3) + frequencyAsString.substring(4, 6);
	}
}
