package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.context.numItems node.
 * 
 * Number of items available in current list.  A value of 0 means that the list is
 * valid, but empty.  A value of -1 means                  that the list is valid,
 * but the number of items is not known.
 */
public class NodeNavContextNumItems extends BaseNavContextNumItems {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavContextNumItems(Long value) {
		super(value);
	}
}
