package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.commitChanges node.
 *
 * Commit the new configuration and reboot. This field is really W/o, so it will
 * always read back as 'NO'.
 */

public class NodeSysNetCommitChanges extends BaseSysNetCommitChanges {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetCommitChanges(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetCommitChanges(Ord value) {
		super(value);
	}
}
