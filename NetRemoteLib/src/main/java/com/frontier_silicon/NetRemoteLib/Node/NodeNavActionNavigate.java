package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.action.navigate node.
 * 
 * Navigate into a directory. The value written must be the index of an item of
 * type DIRECTORY, or SEARCH_DIRECTORY.  The current list is replaced with the
 * child list.  Use the invalid key (0xffffffff) to navigate up.
 */
public class NodeNavActionNavigate extends BaseNavActionNavigate {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavActionNavigate(Long value) {
		super(value);
	}
}
