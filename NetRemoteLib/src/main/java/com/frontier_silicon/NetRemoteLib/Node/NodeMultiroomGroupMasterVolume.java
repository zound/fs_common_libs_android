package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.masterVolume node.
 * 
 * Master volume
 */
public class NodeMultiroomGroupMasterVolume extends BaseMultiroomGroupMasterVolume {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomGroupMasterVolume(Long value) {
		super(value);
	}
}
