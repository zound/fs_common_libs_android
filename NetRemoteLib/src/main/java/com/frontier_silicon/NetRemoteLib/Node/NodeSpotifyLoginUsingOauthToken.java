package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.spotify.loginUsingOauthToken node.
 * 
 * Spotify login using specified authentication token
 */
public class NodeSpotifyLoginUsingOauthToken extends BaseSpotifyLoginUsingOauthToken {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSpotifyLoginUsingOauthToken(String value) {
		super(value);
	}
}
