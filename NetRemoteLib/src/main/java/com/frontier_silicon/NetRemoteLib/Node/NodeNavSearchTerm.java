package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.searchTerm node.
 * 
 * Term to use for a search, in conjunction with a list item of type
 * SEARCH_DIRETORY
 */
public class NodeNavSearchTerm extends BaseNavSearchTerm {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavSearchTerm(String value) {
		super(value);
	}
}
