package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.spotify.playlist.uri node.
 * 
 * Current playing playlist uri
 */
public class NodeSpotifyPlaylistUri extends BaseSpotifyPlaylistUri {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSpotifyPlaylistUri(String value) {
		super(value);
	}
}
