package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.controllerName node.
 * 
 * Controllername could be a combination of App/Device name and will be used to
 * detect if another app is in control of the device.
 */
public class NodeSysInfoControllerName extends BaseSysInfoControllerName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoControllerName(String value) {
		super(value);
	}
}
