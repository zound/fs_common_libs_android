package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.info.artist node.
 * 
 * Artist for current audio, if known and appropriate
 */
public class NodePlayInfoArtist extends BasePlayInfoArtist {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayInfoArtist(String value) {
		super(value);
	}
}
