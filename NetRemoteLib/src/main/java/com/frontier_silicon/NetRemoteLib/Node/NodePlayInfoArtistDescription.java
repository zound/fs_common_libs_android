package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.info.artistDescription node.
 * 
 * Artist metadata (up to 4kB)
 */
public class NodePlayInfoArtistDescription extends BasePlayInfoArtistDescription {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayInfoArtistDescription(String value) {
		super(value);
	}
}
