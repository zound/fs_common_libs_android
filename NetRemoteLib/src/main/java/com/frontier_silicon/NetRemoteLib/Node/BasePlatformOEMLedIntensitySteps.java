package com.frontier_silicon.NetRemoteLib.Node;
/**
 * The definition for the base class for the netRemote.platform.OEM.ledIntensitySteps node.
 */
public class BasePlatformOEMLedIntensitySteps extends NodeU8 implements NodeInfo {
    /**
     * Get the name of the node.
     *
     * @return     The name of the node.
     */
    public String getName() { return "netRemote.platform.OEM.ledIntensitySteps"; }
    /**
     * Get the address of the node which is used in the dock protocol.
     *
     * @return     The 32-bit address.
     */
    public long getAddress() { return 0; }
    /**
     * Get whether or not the node can be cached.
     *
     * @return     True if the node can be cached.
     */
    public boolean IsCacheable() { return false; }
    /**
     * Get whether or not the node generates notifications.
     *
     * @return     True if the node generates notifications.
     */
    public boolean IsNotifying() { return false; }
    /**
     * Get whether or not the node is read-only.
     *
     * @return      True if the node is read-only.
     */
    public boolean IsReadOnly() { return true; }

    private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.U8));
    /**
     * Get a description of the data elements within the node.
     *
     * @return      The node prototype
     */
    public NodePrototype getPrototype() {
        return Prototype;
    }

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
    public BasePlatformOEMLedIntensitySteps(Long value) {
        super(value);
    }

    /**
     * Construct a new node with no value. This is normally a no-no, but the constructor is only
     * allowed to be used inside the package. It is needed so that an instance of a node can be
     * created to examine the properties above.
     */
    BasePlatformOEMLedIntensitySteps() {
        super();
    }
}
