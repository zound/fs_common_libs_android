package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setEncType node.
 *
 * Set encryption type of the AP (used with net.wlan.setSSID)
 */

public class NodeSysNetWlanSetEncType extends BaseSysNetWlanSetEncType {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanSetEncType(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanSetEncType(Ord value) {
		super(value);
	}
}
