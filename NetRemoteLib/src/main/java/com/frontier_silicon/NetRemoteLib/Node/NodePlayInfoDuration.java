package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.info.duration node.
 * 
 * Duration of current track - if applicable and known, otherwise 0.  Note that
 * this can actually change during playback, e.g. for VBR audio.
 */
public class NodePlayInfoDuration extends BasePlayInfoDuration {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayInfoDuration(Long value) {
		super(value);
	}
}
