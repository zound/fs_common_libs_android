package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.caps.eqPresets node.
 * 
 * Define the EQ presets that the radio supports, with user displayable label.
 * Note that the first entry (i.e. key of 0) is special - as it is the custom
 * preset, which means that custom settings are used.
 */
public class NodeSysCapsEqPresets extends BaseSysCapsEqPresets {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysCapsEqPresets(Node value) throws NodeParseErrorException {
		super(value);
	}
}
