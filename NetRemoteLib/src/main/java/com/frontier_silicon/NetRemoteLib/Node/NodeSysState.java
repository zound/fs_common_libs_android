package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.state node.
 *
 * netRemote node to enter the Soft AP Update mode and also to report is mode is
 * active or not.
 */

public class NodeSysState extends BaseSysState {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysState(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysState(Ord value) {
		super(value);
	}
}
