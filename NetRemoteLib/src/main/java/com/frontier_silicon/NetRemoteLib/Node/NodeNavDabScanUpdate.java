package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.dabScanUpdate node.
 * 
 * Returns the progress information (Number of station found)
 */
public class NodeNavDabScanUpdate extends BaseNavDabScanUpdate {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavDabScanUpdate(Long value) {
		super(value);
	}
}
