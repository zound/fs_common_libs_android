package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.caps.protocolVersion node.
 * 
 * Protocol version
 */
public class NodeMultiroomCapsProtocolVersion extends BaseMultiroomCapsProtocolVersion {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomCapsProtocolVersion(Long value) {
		super(value);
	}
}
