package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.ipConfig.subnetMask node.
 * 
 * Subnet mask for the interface
 */
public class NodeSysNetIpConfigSubnetMask extends BaseSysNetIpConfigSubnetMask {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetIpConfigSubnetMask(Long value) {
		super(value);
	}
}
