package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.spotify.lastError node.
 * 
 * Read only node that returns the last error reported for Spotify connection.
 */
public class NodeSpotifyLastError extends BaseSpotifyLastError {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSpotifyLastError(Long value) {
		super(value);
	}
}
