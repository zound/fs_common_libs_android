package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.device.serverStatus node.
 *
 * Server status
 */

public class NodeMultiroomDeviceServerStatus extends BaseMultiroomDeviceServerStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomDeviceServerStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomDeviceServerStatus(Ord value) {
		super(value);
	}
}
