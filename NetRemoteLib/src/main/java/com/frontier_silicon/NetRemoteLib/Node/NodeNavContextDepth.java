package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.context.depth node.
 * 
 * Node used to inform about the state of the navigation depth for IR/MP browsing.
 */
public class NodeNavContextDepth extends BaseNavContextDepth {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavContextDepth(Long value) {
		super(value);
	}
}
