package com.frontier_silicon.NetRemoteLib.Node;


import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the base class for the netRemote.sys.alarm.config node.
 *
 * Configuration alarm on/off, time, duration, source, preset (if DAB or FM),
 * repeat type, once alarm date, and volume.
 */
public class BaseSysAlarmConfig extends NodeList implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.alarm.config"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x1010b700; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private static NodePrototype Prototype = null;

        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
        if (Prototype == null) {
        	ArrayList<NodePrototype.Arg> args = new ArrayList<NodeInfo.NodePrototype.Arg>();
        	args.add(new NodePrototype.Arg("key", NodePrototype.Arg.ArgDataType.U32, 1));
        	args.add(new NodePrototype.Arg("on", NodePrototype.Arg.ArgDataType.E8, 1));
        	args.add(new NodePrototype.Arg("time", NodePrototype.Arg.ArgDataType.C8, 7));
        	args.add(new NodePrototype.Arg("duration", NodePrototype.Arg.ArgDataType.U8, 1));
        	args.add(new NodePrototype.Arg("source", NodePrototype.Arg.ArgDataType.E8, 1));
        	args.add(new NodePrototype.Arg("preset", NodePrototype.Arg.ArgDataType.U8, 1));
        	args.add(new NodePrototype.Arg("repeat", NodePrototype.Arg.ArgDataType.E8, 1));
        	args.add(new NodePrototype.Arg("date", NodePrototype.Arg.ArgDataType.C8, 9));
        	args.add(new NodePrototype.Arg("volume", NodePrototype.Arg.ArgDataType.U8, 1));
        	Prototype = new NodePrototype(args);
        }
        
        return Prototype;
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysAlarmConfig() {
		super();
	}

	/**
	 * The definition for a list item for the "netRemote.sys.alarm.config" node.
	 * 
	 */
	public static class ListItem extends NodeListItem {
		/**
		 * Constructs a node list entry from the given XML.
		 * 
		 * @param xml		The XML specifying the fields of the node list entry
		 * @throws NodeParseErrorException 
		 */
		public ListItem(Node xml) throws NodeParseErrorException {
			super(xml);
		}

		public boolean getOn() { return super.getOn(); }
		public String getTime() { return super.getFieldByNameAsString(NodeListItem.Field.time); }
		public Long getDuration() { return super.getFieldByNameAsLong(NodeListItem.Field.duration); }
		public AlarmSource getAlarmSource() { return super.getAlarmSource(); }
		public Long getPreset() { return super.getFieldByNameAsLong(NodeListItem.Field.preset); }
		public FieldRepeat getRepeat() { return super.getRepeat(); }
		public String getDate() { return super.getFieldByNameAsString(NodeListItem.Field.date); }
		public Long getVolume() { return super.getFieldByNameAsLong(NodeListItem.Field.volume); }
		public Long getKey() { return super.getFieldByNameAsLong(NodeListItem.Field.key); }
	}

	/**
	 * Constructs a node from the given XML.
	 * 
	 * @param xml		The XML specifying the fields of the node list entry
	 * @throws NodeParseErrorException 
	 */
	public BaseSysAlarmConfig(Node xml) throws NodeParseErrorException {
		super(BaseSysAlarmConfig.ListItem.class, xml);
	}
	
	/**
	 * Gets all the entries for the list.
	 * 
	 * @return		All the entries for the list
	 */
	public List<ListItem> getEntries() {
		List<ListItem> result = new ArrayList<ListItem>();
		
		for (NodeListItem item : this.Items) {
			result.add((ListItem)item);
		}
		
		return result;
	}
	
	/**
	 * Gets the item at the specified location.
	 * 
	 * @param location		The index of the location.
	 * @return				The item
	 */
	public ListItem getItem(int location) {
		if (location < this.Items.size()) {
			return (ListItem)(this.Items.get(location));
		}
		
		return null;
	}
	
	/**
	 * Get the last item in this list.
	 * 
	 * @return				The item
	 */
	public ListItem getLastItem() {
		if (!(this.Items.isEmpty())) {
			return this.getItem(this.Items.size() - 1);
		}
		
		return null;
	}
}
