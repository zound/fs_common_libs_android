package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.cast.tos node.
 *
 * Google Cast terms of service acceptance status
 */

public class NodeCastTos extends BaseCastTos {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeCastTos(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeCastTos(Ord value) {
		super(value);
	}
}
