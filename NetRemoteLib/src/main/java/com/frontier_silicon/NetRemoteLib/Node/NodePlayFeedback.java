package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.feedback node.
 *
 * Supply positive or negative feedback on the current track (e.g. Love/Ban for
 * Last.fm, or Thumbs up/down for Pandora).
 */

public class NodePlayFeedback extends BasePlayFeedback {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayFeedback(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayFeedback(Ord value) {
		super(value);
	}
}
