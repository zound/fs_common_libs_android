package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.action.selectPreset node.
 * 
 * Select a preset for playback.  The value written must be an index into the
 * preset list of a valid item
 */
public class NodeNavActionSelectPreset extends BaseNavActionSelectPreset {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavActionSelectPreset(Long value) {
		super(value);
	}
}
