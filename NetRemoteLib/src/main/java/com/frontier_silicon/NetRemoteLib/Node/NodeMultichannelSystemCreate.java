package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.system.create node.
 * 
 * Create a new system with the specified name
 */
public class NodeMultichannelSystemCreate extends BaseMultichannelSystemCreate {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultichannelSystemCreate(String value) {
		super(value);
	}
}
