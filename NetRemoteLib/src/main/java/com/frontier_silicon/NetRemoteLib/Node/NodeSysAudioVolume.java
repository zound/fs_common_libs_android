package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.volume node.
 * 
 * Audio volume level.  The range is from 0, to one less than the number of volume
 * steps - available from netRemote.sys.caps.volumeSteps
 */
public class NodeSysAudioVolume extends BaseSysAudioVolume {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAudioVolume(Long value) {
		super(value);
	}
}
