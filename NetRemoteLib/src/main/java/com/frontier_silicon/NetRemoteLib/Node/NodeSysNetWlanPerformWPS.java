package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.performWPS node.
 *
 * Connect the device by PIN/PBC based WPS method
 */

public class NodeSysNetWlanPerformWPS extends BaseSysNetWlanPerformWPS {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanPerformWPS(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanPerformWPS(Ord value) {
		super(value);
	}
}
