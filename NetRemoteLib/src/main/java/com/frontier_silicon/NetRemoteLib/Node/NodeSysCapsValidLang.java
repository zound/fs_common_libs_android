package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.caps.validLang node.
 * 
 * List all the languages supported
 */
public class NodeSysCapsValidLang extends BaseSysCapsValidLang {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysCapsValidLang(Node value) throws NodeParseErrorException {
		super(value);
	}
}
