package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.scrobble node.
 *
 * Scrobbling is currently enabled, if supported for this mode
 */

public class NodePlayScrobble extends BasePlayScrobble {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayScrobble(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayScrobble(Ord value) {
		super(value);
	}
}
