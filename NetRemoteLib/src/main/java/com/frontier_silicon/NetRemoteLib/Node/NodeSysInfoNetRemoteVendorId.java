package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.netRemoteVendorId node.
 * 
 * Node to return the netremote vendor id
 */
public class NodeSysInfoNetRemoteVendorId extends BaseSysInfoNetRemoteVendorId {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoNetRemoteVendorId(String value) {
		super(value);
	}
}
