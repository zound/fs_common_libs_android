package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.swap.index1 node.
 * 
 * 
 */
public class NodeNavPresetSwapIndex1 extends BaseNavPresetSwapIndex1 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetSwapIndex1(Long value) {
		super(value);
	}
}
