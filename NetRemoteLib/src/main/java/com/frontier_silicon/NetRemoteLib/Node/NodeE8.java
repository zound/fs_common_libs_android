package com.frontier_silicon.NetRemoteLib.Node;

/*********************************************************************************
 * Base classes and interfaces that form the foundation of all node definitions. *
 *********************************************************************************/

/**
 * This is the base class of an E8 node.
 * 
 * @param <T>		Enumeration type for the node
*/
@SuppressWarnings("rawtypes")
public abstract class NodeE8<T extends Enum> extends NodeToken<T> {
	protected long Value = 0;
	
	/**
	 * Constructs a node with the given value.
	 * 
	 * @param value		The numeric value of the node
	 */
	protected NodeE8(Long value) {
		super(8);
		this.Value = value;
	}
	
	/**
	 * Constructs a node with the given enumeration value
	 * 
	 * @param ord		The enumeration value
	 */
	protected NodeE8(T ord) {
		super(8);
		this.Value = this.GetValueFromEnum(ord);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	NodeE8() {
		super();
	}
	
	/**
	 * Gets the numeric value of the node.
	 * 
	 * @return			The numeric value of the node
	 */
	public Long getValue() { return this.Value; }
	
	
	/**
	 * Gets the enum value of this node.
	 * 
	 * @return		The enum value of the node
	 */
	public T getValueEnum() {
		return this.GetEnumFromValue(this.Value);
	}
	
	/**
	 * Gets a string representing the node value.
	 * 
	 * @return			A string representing the node value
	 */
	public String toString() {
		T value = this.getValueEnum();
		if (value == null) {
			return "<Unknown value>";
		} else {
			return ("<" + value.toString() + ">");
		}
	}
	
	/**
	 * Test the equality of two nodes.
	 * 
	 * @param otherNode		The other node to test
	 * @return				True if the value of the two nodes are equal
	 */	
	public boolean equals(NodeInfo otherNode) {
		if (otherNode instanceof NodeE8<?>) {
			return this.Value == ((NodeE8<?>)otherNode).Value;
		}
		
		return false;
	}
}
