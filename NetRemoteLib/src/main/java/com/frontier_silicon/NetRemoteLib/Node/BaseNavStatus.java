package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.nav.status node.
 *
 * The current status of the navigation context. The meanings of the individual
 * states are:
 * 
 * !======================================== ! Value ! Meaning
 * 
 * ! WAITING ! the list items are currently being fetched, and the list isn't
 * available.
 * 
 * ! READY ! the list items are available (from netRemote.nav.list).
 * 
 * ! FAIL ! the list navigation has failed, but the previous list is still
 * available.  The navigation operation can be retried
 * 
 * ! FATAL_ERR  ! a fatal error has occurred in navigation, retrying will not help.
 * The list items are not available.
 * 
 * ! READY_ROOT ! the list items are available (as READY), but navigation has
 * reverted to the root. !========================================
 */
public class BaseNavStatus extends NodeE8<BaseNavStatus.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.nav.status"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10203000; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return true; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return true; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseNavStatus(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseNavStatus() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseNavStatus(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value WAITING.
         */
        WAITING, 
        /**
         * Enumeration value READY.
         */
        READY, 
        /**
         * Enumeration value FAIL.
         */
        FAIL, 
        /**
         * Enumeration value FATAL_ERR.
         */
        FATAL_ERR, 
        /**
         * Enumeration value READY_ROOT.
         */
        READY_ROOT, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case WAITING: return (long)0;
			case READY: return (long)1;
			case FAIL: return (long)2;
			case FATAL_ERR: return (long)3;
			case READY_ROOT: return (long)4;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.WAITING;
			case 1: return Ord.READY;
			case 2: return Ord.FAIL;
			case 3: return Ord.FATAL_ERR;
			case 4: return Ord.READY_ROOT;
		}
		return null;
	}
}
