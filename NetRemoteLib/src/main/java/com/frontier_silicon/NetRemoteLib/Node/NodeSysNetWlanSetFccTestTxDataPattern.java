package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setFccTestTxDataPattern node.
 * 
 * Set the Tx data pattern for FCC test
 */
public class NodeSysNetWlanSetFccTestTxDataPattern extends BaseSysNetWlanSetFccTestTxDataPattern {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanSetFccTestTxDataPattern(Long value) {
		super(value);
	}
}
