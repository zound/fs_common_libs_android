package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.eqPreset node.
 * 
 * Currently selected EQ preset.  The value is an index in the
 * netRemote.sys.caps.eqPresets list
 */
public class NodeSysAudioEqPreset extends BaseSysAudioEqPreset {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAudioEqPreset(Long value) {
		super(value);
	}
}
