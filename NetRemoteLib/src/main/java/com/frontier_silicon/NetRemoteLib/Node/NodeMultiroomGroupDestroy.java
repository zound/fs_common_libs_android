package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.destroy node.
 *
 * Destroy the current group
 */

public class NodeMultiroomGroupDestroy extends BaseMultiroomGroupDestroy {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomGroupDestroy(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomGroupDestroy(Ord value) {
		super(value);
	}
}
