package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.addPreset node.
 * 
 * Preset index
 */
public class NodePlayAddPreset extends BasePlayAddPreset {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayAddPreset(Long value) {
		super(value);
	}
}
