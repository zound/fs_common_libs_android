package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.download.name node.
 * 
 * 
 */
public class NodeNavPresetDownloadName extends BaseNavPresetDownloadName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetDownloadName(String value) {
		super(value);
	}
}
