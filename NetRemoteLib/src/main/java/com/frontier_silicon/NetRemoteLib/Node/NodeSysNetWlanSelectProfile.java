package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.selectProfile node.
 * 
 * Index from the wlan.profiles list
 */
public class NodeSysNetWlanSelectProfile extends BaseSysNetWlanSelectProfile {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanSelectProfile(Long value) {
		super(value);
	}
}
