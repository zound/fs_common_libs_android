package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.nav.context.list node.
 * 
 * Current context menu items.
 */
public class NodeNavContextList extends BaseNavContextList {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeNavContextList(Node value) throws NodeParseErrorException {
		super(value);
	}
}
