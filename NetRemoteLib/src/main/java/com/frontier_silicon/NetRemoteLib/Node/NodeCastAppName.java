package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.cast.appName node.
 * 
 * indicates the currently streaming app name
 */
public class NodeCastAppName extends BaseCastAppName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeCastAppName(String value) {
		super(value);
	}
}
