package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.macAddress node.
 * 
 * MAC address of the wireless interface
 */
public class NodeSysNetWlanMacAddress extends BaseSysNetWlanMacAddress {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanMacAddress(String value) {
		super(value);
	}
}
