package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setAuthType node.
 *
 * Set authentication type of the AP (used with net.wlan.setSSID)
 */

public class NodeSysNetWlanSetAuthType extends BaseSysNetWlanSetAuthType {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanSetAuthType(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanSetAuthType(Ord value) {
		super(value);
	}
}
