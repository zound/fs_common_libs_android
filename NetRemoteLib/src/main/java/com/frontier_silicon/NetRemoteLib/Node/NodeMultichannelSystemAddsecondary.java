package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.system.addsecondary node.
 * 
 * Instruct the speaker to join the system as secondary
 */
public class NodeMultichannelSystemAddsecondary extends BaseMultichannelSystemAddsecondary {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultichannelSystemAddsecondary(String value) {
		super(value);
	}
}
