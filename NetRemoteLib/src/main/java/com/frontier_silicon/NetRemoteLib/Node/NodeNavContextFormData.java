package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.context.formData node.
 * 
 * Data to fill up an airable form, in conjunction with a list item of type
 * FORM_ITEM.
 */
public class NodeNavContextFormData extends BaseNavContextFormData {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavContextFormData(String value) {
		super(value);
	}
}
