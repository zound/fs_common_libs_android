package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.cast.tos node.
 *
 * Google Cast terms of service acceptance status
 */
public class BaseCastTos extends NodeE8<BaseCastTos.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.cast.tos"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10c01000; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseCastTos(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseCastTos() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseCastTos(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value INACTIVE.
         */
        INACTIVE, 
        /**
         * Enumeration value ACTIVE.
         */
        ACTIVE, 
        /**
         * Enumeration value UNSET.
         */
        UNSET, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case INACTIVE: return (long)0;
			case ACTIVE: return (long)1;
			case UNSET: return (long)2;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.INACTIVE;
			case 1: return Ord.ACTIVE;
			case 2: return Ord.UNSET;
		}
		return null;
	}
}
