package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.airplay.setPassword node.
 * 
 * Set the Airplay password with the specified value
 */
public class NodeAirplaySetPassword extends BaseAirplaySetPassword {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeAirplaySetPassword(String value) {
		super(value);
	}
}
