package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.net.wlan.setPassphrase node.
 *
 * Set the passphrase/key (this must be set before selecting the SSID).
 * 
 * WPA - a null terminated passphrase between 8 and 63 characters excluding the
 * null.
 * 
 * WEP/WEP64/WEP128 - a 32 byte key represented by a null terminated string where
 * the hex value of each nibble is given by it's ASCII value.
 * 
 * The passphrase must be encrypted using RSA PKCS1 v1.5 (RSAES-PKCS1-v1_5). The
 * RSA public key can be obtained from the netRemote.sys.rsa nodes
 */
public class BaseSysNetWlanSetPassphrase extends NodeC implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.net.wlan.setPassphrase"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10106206; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.C8, 257));
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

	/**
	 * Construct a new node using a string value.
	 *
	 * @param value      The value of the node
	 */
	public BaseSysNetWlanSetPassphrase(String value) {
		super(257, value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysNetWlanSetPassphrase() {
		super();
	}
}
