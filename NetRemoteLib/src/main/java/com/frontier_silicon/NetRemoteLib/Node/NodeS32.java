package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The abstract base class for S32 nodes.
 */
public abstract class NodeS32 extends NodeInteger {
	/**
	 * Constructs a node with the given value.
	 * 
	 * @param value		The numeric value of the node
	 */
	protected NodeS32(Long value) {
		super(value, 0x7fffffffL, -0x7fffffffL); 
	}
	
	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	NodeS32() {
	}
}
