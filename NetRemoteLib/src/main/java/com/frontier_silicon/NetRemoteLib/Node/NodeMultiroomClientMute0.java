package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.mute0 node.
 *
 * Handle client 0 mute
 */

public class NodeMultiroomClientMute0 extends BaseMultiroomClientMute0 {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientMute0(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientMute0(Ord value) {
		super(value);
	}
}
