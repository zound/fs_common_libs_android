package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.primary.channelmask node.
 *
 * Primary's channel mask
 */

public class NodeMultichannelPrimaryChannelmask extends BaseMultichannelPrimaryChannelmask {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelPrimaryChannelmask(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelPrimaryChannelmask(Ord value) {
		super(value);
	}
}
