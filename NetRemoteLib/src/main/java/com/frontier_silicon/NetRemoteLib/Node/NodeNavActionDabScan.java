package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.action.dabScan node.
 *
 * Dab scanning control. When read indicates the current status of the DAB scan.
 * When written controls the DAB scan. A notification is generated when the DAB
 * scan completes.
 */

public class NodeNavActionDabScan extends BaseNavActionDabScan {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavActionDabScan(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavActionDabScan(Ord value) {
		super(value);
	}
}
