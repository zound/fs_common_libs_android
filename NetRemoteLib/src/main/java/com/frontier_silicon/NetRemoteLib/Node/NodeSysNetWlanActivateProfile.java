package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.activateProfile node.
 * 
 * SSID extracted from the wlan.profiles node
 */
public class NodeSysNetWlanActivateProfile extends BaseSysNetWlanActivateProfile {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanActivateProfile(String value) {
		super(value);
	}
}
