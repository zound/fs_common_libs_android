package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.ipConfig.gateway node.
 * 
 * Gateway for the interface
 */
public class NodeSysNetIpConfigGateway extends BaseSysNetIpConfigGateway {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetIpConfigGateway(Long value) {
		super(value);
	}
}
