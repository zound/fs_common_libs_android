package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.mute1 node.
 *
 * Handle client 1 mute
 */

public class NodeMultiroomClientMute1 extends BaseMultiroomClientMute1 {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientMute1(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientMute1(Ord value) {
		super(value);
	}
}
