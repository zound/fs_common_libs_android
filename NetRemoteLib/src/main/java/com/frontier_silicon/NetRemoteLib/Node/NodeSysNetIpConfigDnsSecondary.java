package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.ipConfig.dnsSecondary node.
 * 
 * Secondary DNS for the interface
 */
public class NodeSysNetIpConfigDnsSecondary extends BaseSysNetIpConfigDnsSecondary {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetIpConfigDnsSecondary(Long value) {
		super(value);
	}
}
