package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.rate node.
 * 
 * Supports Cue, Review, Pause, Play, if appropriate.
 */
public class NodePlayRate extends BasePlayRate {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayRate(Long value) {
		super(value);
	}
}
