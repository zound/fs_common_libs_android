package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.system.removesecondary node.
 * 
 * Instruct the speaker to leave the system
 */
public class NodeMultichannelSystemRemovesecondary extends BaseMultichannelSystemRemovesecondary {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultichannelSystemRemovesecondary(String value) {
		super(value);
	}
}
