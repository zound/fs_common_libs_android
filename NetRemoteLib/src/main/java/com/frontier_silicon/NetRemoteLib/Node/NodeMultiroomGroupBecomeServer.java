package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.becomeServer node.
 *
 * Duplicate of netRemote.multiroom.group.state needed to maintain backward
 * compatibility
 */

public class NodeMultiroomGroupBecomeServer extends BaseMultiroomGroupBecomeServer {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomGroupBecomeServer(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomGroupBecomeServer(Ord value) {
		super(value);
	}
}
