package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wired.interfaceEnable node.
 *
 * To enable the wired interface
 */

public class NodeSysNetWiredInterfaceEnable extends BaseSysNetWiredInterfaceEnable {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWiredInterfaceEnable(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWiredInterfaceEnable(Ord value) {
		super(value);
	}
}
