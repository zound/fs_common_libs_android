package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.caps.volumeSteps node.
 * 
 * Number of volume steps that the radio supports - the valid range of the netRemote.sys.volume node
 * is 0 to (volumeSteps-1).
 */
public class NodeSysCapsVolumeSteps extends BaseSysCapsVolumeSteps {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysCapsVolumeSteps(Long value) {
		super(value);
	}
}
