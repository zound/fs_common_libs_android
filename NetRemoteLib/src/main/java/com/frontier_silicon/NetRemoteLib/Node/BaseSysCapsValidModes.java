package com.frontier_silicon.NetRemoteLib.Node;


import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the base class for the netRemote.sys.caps.validModes node.
 *
 * List describing the modes that are supported on the device.
 * 
 * An example list of valid modes is: !======================================== !
 * Index ! id ! selectable ! streamable ! label
 * 
 * ! 0 ! None ! No ! Yes ! Mode None
 * 
 * ! 1 ! IR ! Yes ! Yes ! Internet Radio
 * 
 * ! 2 ! MP ! Yes ! Yes ! Music Player !========================================
 */
public class BaseSysCapsValidModes extends NodeList implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.caps.validModes"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10102100; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return true; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return true; }

	private static NodePrototype Prototype = null;

        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
        if (Prototype == null) {
        	ArrayList<NodePrototype.Arg> args = new ArrayList<NodeInfo.NodePrototype.Arg>();
        	args.add(new NodePrototype.Arg("key", NodePrototype.Arg.ArgDataType.U32, 1));
        	args.add(new NodePrototype.Arg("id", NodePrototype.Arg.ArgDataType.C8, 8));
        	args.add(new NodePrototype.Arg("selectable", NodePrototype.Arg.ArgDataType.E8, 1));
        	args.add(new NodePrototype.Arg("label", NodePrototype.Arg.ArgDataType.C8, 32));
        	args.add(new NodePrototype.Arg("streamable", NodePrototype.Arg.ArgDataType.E8, 1));
        	args.add(new NodePrototype.Arg("modeType", NodePrototype.Arg.ArgDataType.E8, 1));
        	Prototype = new NodePrototype(args);
        }
        
        return Prototype;
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysCapsValidModes() {
		super();
	}

	/**
	 * The definition for a list item for the "netRemote.sys.caps.validModes" node.
	 * 
	 */
	public static class ListItem extends NodeListItem {
		/**
		 * Constructs a node list entry from the given XML.
		 * 
		 * @param xml		The XML specifying the fields of the node list entry
		 * @throws NodeParseErrorException 
		 */
		public ListItem(Node xml) throws NodeParseErrorException {
			super(xml);
		}

		public String getId() { return super.getFieldByNameAsString(NodeListItem.Field.id); }
		public boolean getSelectable() { return super.getSelectable(); }
		public String getLabel() { return super.getFieldByNameAsString(NodeListItem.Field.label); }
		public boolean getStreamable() { return super.getStreamable(); }
        public ModeType getModeType() { return super.getModeType(); }
		public Long getKey() { return super.getFieldByNameAsLong(NodeListItem.Field.key); }

		/**
		 * Allows the selectable field of a radio mode to be enabled
		 */
		public void setSelectable() { super.setSelectable();}

		/**
		 * Replaces the value of the mode with the new value
		 * @param newLabel The new label for the mode
         */
		public void setLabel(String newLabel) {
			if(Value.containsKey(NodeListItem.Field.label)) {
				Value.put(NodeListItem.Field.label, newLabel);
			}
		}
	}

	/**
	 * Constructs a node from the given XML.
	 * 
	 * @param xml		The XML specifying the fields of the node list entry
	 * @throws NodeParseErrorException 
	 */
	public BaseSysCapsValidModes(Node xml) throws NodeParseErrorException {
		super(BaseSysCapsValidModes.ListItem.class, xml);
	}
	
	/**
	 * Gets all the entries for the list.
	 * 
	 * @return		All the entries for the list
	 */
	public List<ListItem> getEntries() {
		List<ListItem> result = new ArrayList<ListItem>();
		
		for (NodeListItem item : this.Items) {
			result.add((ListItem)item);
		}
		
		return result;
	}
	
	/**
	 * Gets the item at the specified location.
	 * 
	 * @param location		The index of the location.
	 * @return				The item
	 */
	public ListItem getItem(int location) {
		if (location < this.Items.size()) {
			return (ListItem)(this.Items.get(location));
		}
		
		return null;
	}
	
	/**
	 * Get the last item in this list.
	 * 
	 * @return				The item
	 */
	public ListItem getLastItem() {
		if (!(this.Items.isEmpty())) {
			return this.getItem(this.Items.size() - 1);
		}
		
		return null;
	}
}
