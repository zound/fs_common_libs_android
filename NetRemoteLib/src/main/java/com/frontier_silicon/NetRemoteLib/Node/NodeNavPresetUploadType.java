package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.upload.type node.
 * 
 * 
 */
public class NodeNavPresetUploadType extends BaseNavPresetUploadType {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetUploadType(String value) {
		super(value);
	}
}
