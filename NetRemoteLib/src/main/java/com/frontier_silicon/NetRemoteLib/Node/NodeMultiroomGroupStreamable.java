package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.streamable node.
 *
 * Return true or false depending on current mode's capability of streaming on
 * multiroom
 */

public class NodeMultiroomGroupStreamable extends BaseMultiroomGroupStreamable {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomGroupStreamable(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomGroupStreamable(Ord value) {
		super(value);
	}
}
