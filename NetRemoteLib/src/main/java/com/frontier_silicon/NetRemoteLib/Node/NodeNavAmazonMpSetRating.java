package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.amazonMpSetRating node.
 *
 * Sending and Amazon Music Rating for the current station track
 */

public class NodeNavAmazonMpSetRating extends BaseNavAmazonMpSetRating {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavAmazonMpSetRating(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavAmazonMpSetRating(Ord value) {
		super(value);
	}
}
