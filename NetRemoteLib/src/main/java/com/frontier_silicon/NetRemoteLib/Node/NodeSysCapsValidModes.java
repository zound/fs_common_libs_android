package com.frontier_silicon.NetRemoteLib.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

import org.w3c.dom.Node;

/**
 * The definition for the netRemote.sys.caps.validModes node.
 * 
 * List describing the modes that are supported on the device.
 * 
 * An example list of valid modes is: !======================================== !
 * Index ! id ! selectable ! streamable ! label
 * 
 * ! 0 ! None ! No ! Yes ! Mode None
 * 
 * ! 1 ! IR ! Yes ! Yes ! Internet Radio
 * 
 * ! 2 ! MP ! Yes ! Yes ! Music Player !========================================
 */
public class NodeSysCapsValidModes extends BaseSysCapsValidModes {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysCapsValidModes(Node value) throws NodeParseErrorException {
		super(value);
	}
	
	/**
	 * An enumeration representing all possible modes for radios. 
	 */
	public static enum Mode {
		/**
		 * Internet Radio mode.
		 */
		IR,
		/**
		 * Spotify Connect mode.
		 */
		Spotify,
		/**
		 * Music Player mode.
		 */
		MP,
		/**
		 * Last FM mode.
		 */
		LastFM,
		/**
		 * Pandora mode.
		 */
		Pandora,
		/**
		 * DAB mode.
		 */
		DAB,
		/**
		 * FM mode.
		 */
		FM,
		/**
		 * iPod Mode.
		 */
		iPod,
		/**
		 * Aux-in mode.
		 */
		AuxIn,
		/**
		 * Aux-in mode 2.
		 */
		AuxIn2,
		/**
		 * DLNA DMR mode.
		 */
		DMR,
		/**
		 * Bluetooth mode.
		 */
		Bluetooth,
		/**
		 * Custom mode 1
		 */
		CustomMode1,
		/**
		 * Custom mode 2
		 */
		CustomMode2,
		/**
		 * Custom mode 3
		 */
		CustomMode3,
		/**
		 * Custom mode 4
		 */
		CustomMode4,
		/**
		 * Custom mode 5
		 */
		CustomMode5,
		/**
		 * Airplay mode
		 */
		AirPlay,
        /**
         * Cast
         */
        Cast,
        /**
         * Standby is reported as a mode in Minuet
         */
        Standby,
        /**
         * Audsync
         */
        Audsync,
		/**
		 * AIRABLE_DEEZER
		 */
		AIRABLE_DEEZER,
		/**
		 * AIRABLE_NAPSTER
		 */
		AIRABLE_NAPSTER,
		/**
		 * AIRABLE_QOBUZ
		 */
		AIRABLE_QOBUZ,
		/**
		 * AIRABLE_TIDAL
		 */
		AIRABLE_TIDAL,
		/**
		 * AIRABLE_ALDI
		 */
		AIRABLE_ALDI,
		/**
		 * AIRABLE_HOFER
		 */
		AIRABLE_HOFER,
		/**
		 * AIRABLE_AMAZON_MP
		 */
		AIRABLE_AMAZON_MP,
		/**
		 * CD
		 */
		CD,
		/**
		 * OPTICAL
		 */
		OPTICAL,
		/**
		 * RCA
		 */
		RCA,
		/**
		 * None mode
		 */
		None,
		/**
		 * Unknown mode
		 */
		UnknownMode;


		// This is obsolete in UNDOK
		// Don't use this for displaying in the UI or for exact name checking; instead use enum.name() method
		@Deprecated
		public String toString() {
			switch (this) {
				case IR: { return "Internet Radio"; }
				case Spotify: { return "Spotify"; }
				case MP: { return "Music Player"; }
				case LastFM: { return "Last.fm"; }
				case Pandora: { return "Pandora"; }
				case DAB: { return "DAB"; }
				case FM: { return "FM"; }
				case iPod: { return "iPod"; }
				case AuxIn: { return "AUX In"; }
				case AuxIn2: { return "AUX 2 In"; }
				case DMR: { return "DMR"; }
				case Bluetooth: { return "Bluetooth"; }
				case CustomMode1: return "Custom Mode1";
				case CustomMode2: return "Custom Mode2";
				case CustomMode3: return "Custom Mode3";
				case CustomMode4: return "Custom Mode4";
				case CustomMode5: return "Custom Mode5";
				case AirPlay: return "AirPlay";
                case Cast: return "Cast";
                case Standby: return "Standby";
                case Audsync: return "Audsync";
				case None: return "None";
				case AIRABLE_DEEZER: return  "Deezer";
				case AIRABLE_NAPSTER: return  "Napster";
				case AIRABLE_QOBUZ: return  "Qobuz";
				case AIRABLE_TIDAL: return  "TIDAL";
				case AIRABLE_ALDI: return "Aldi";
				case AIRABLE_HOFER: return "Hofer";
				case AIRABLE_AMAZON_MP: return "Amazon";
				case CD: return "CD";
				case OPTICAL: return "Optical in";
				case UnknownMode: return "Unknown Mode";
				case RCA: return "RCA";
			}
			
			return super.toString();
		}
	}
	
	/**
	 * Convert the string value of a mode to its enumeration representation.
	 * 
	 * @param id		The string value of the mode
	 * @return			The enumeration value
	 */
	public static Mode ConvertIdToEnum(String id) {
		if (id.equals("IR")) {
			return Mode.IR;
		} else if (id.equals("Spotify")) { 
			return Mode.Spotify;
		} else if (id.equals("MP") || id.equals("NETWORK")) {
			return Mode.MP;
		} else if (id.equals("LASTFM")) {
			return Mode.LastFM;
		} else if (id.equals("PANDORA")) {
			return Mode.Pandora;
		} else if (id.equals("DAB")) {
			return Mode.DAB;
		} else if (id.equals("FM")) {
			return Mode.FM;
		} else if (id.equals("IPOD")) {
			return Mode.iPod;
		} else if (id.equals("AUXIN")) {
			return Mode.AuxIn;
		} else if (id.equals("AUXIN2")) {
			return Mode.AuxIn2;
		} else if (id.equals("DMR")) {
			return Mode.DMR;
		} else if (id.equals("Bluetooth")) {
			return Mode.Bluetooth;
		} else if (id.equals("CustomMode1")) {
            return Mode.CustomMode1;
		} else if (id.equals("CustomMode2")) {
			return Mode.CustomMode2;
		} else if (id.equals("CustomMode3")) {
            return Mode.CustomMode3;
		} else if (id.equals("CustomMode4")) {
            return Mode.CustomMode4;
		} else if (id.equals("CustomMode5")) {
            return Mode.CustomMode5;
		} else if (id.equals("Airplay")) {
            return Mode.AirPlay;
        } else if (id.equals("Google Cast")) {
            return Mode.Cast;
		} else if (id.equals("Standby")) {
            return Mode.Standby;
        } else if (id.equals("Audsync")) {
            return Mode.Audsync;
        } else if (id.equals("None")) {
			return Mode.None;
		} else if (id.equals("AIRABLE_DEEZER")) {
			return Mode.AIRABLE_DEEZER;
		}else if (id.equals("AIRABLE_NAPSTER")) {
			return Mode.AIRABLE_NAPSTER;
		}else if (id.equals("AIRABLE_QOBUZ")) {
			return Mode.AIRABLE_QOBUZ;
		}else if (id.equals("AIRABLE_TIDAL")) {
			return Mode.AIRABLE_TIDAL;
		}else if (id.equals("AIRABLE_ALDI")) {
			return  Mode.AIRABLE_ALDI;
		}else if (id.equals("AIRABLE_HOFER")) {
			return  Mode.AIRABLE_HOFER;
		}else if (id.equals("AIRABLE_AMAZON_MP")) {
			return Mode.AIRABLE_AMAZON_MP;
		}else if(id.equals("CD")){
			return Mode.CD;
		}else if(id.equals("OPTICAL")){
			return Mode.OPTICAL;
		}else if (id.equals("RCA")) {
			return Mode.RCA;
		}
		return Mode.UnknownMode;
	}
	
	/**
	 * Get the node that should be used to set the radio to the given mode.
	 * 
	 * @param requiredMode		The required mode
	 * @return					The node to change to the required mode
	 */
	public NodeSysMode GetNodeForSettingMode(Mode requiredMode) {
		/* Try to find the required mode in our list of known modes. */
		for (NodeListItem item : this.Items) {
			ListItem modeItem = (ListItem)item;
			Mode mode = NodeSysCapsValidModes.ConvertIdToEnum(modeItem.getId());
			if ((mode != null) && (mode == requiredMode)) {
				return new NodeSysMode(modeItem.getKey());
			}
		}
		
		return null;
	}
	
	/**
	 * Get the enumeration value representing the mode specified in the given node.
	 * 
	 * @param modeNode		The node specifying the mode
	 * @return				The corresponding enumeration value
	 */
	public Mode GetModeAsEnum(NodeSysMode modeNode) {
		for (NodeListItem item : this.Items) {
			ListItem modeItem = (ListItem)item;
			
			if (modeItem.getKey().equals(modeNode.getValue())) {
				return NodeSysCapsValidModes.ConvertIdToEnum(modeItem.getId());
			}
		}
		
		return Mode.None;
	}
	
	/**
	 * Get the list item from valid modes list representing the given mode
	 * @param modeNode		The node specifying the mode
	 * @return				The corresponding list item node
	 */
	public NodeListItem GetModeAsListItem(NodeSysMode modeNode) {
		for (NodeListItem item : this.Items) {
			ListItem modeItem = (ListItem)item;
			
			if (modeItem.getKey().equals(modeNode.getValue())) {
				return modeItem;
			}
		}
		
		return null;
	}

	public Boolean containsMode(Mode mode) {
		for (NodeListItem item : this.Items) {
			ListItem modeItem = (ListItem) item;

			if (ConvertIdToEnum(modeItem.getId()) == mode) {
				return true;
			}
		}
		return false;
	}
}
