package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.status3 node.
 *
 * Client 3 status
 */

public class NodeMultiroomClientStatus3 extends BaseMultiroomClientStatus3 {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientStatus3(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientStatus3(Ord value) {
		super(value);
	}
}
