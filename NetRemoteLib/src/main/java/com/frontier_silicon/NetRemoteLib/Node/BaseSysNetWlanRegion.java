package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.net.wlan.region node.
 *
 * Region of the wlan device
 */
public class BaseSysNetWlanRegion extends NodeE8<BaseSysNetWlanRegion.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.net.wlan.region"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10106203; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseSysNetWlanRegion(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysNetWlanRegion() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseSysNetWlanRegion(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value INVALID.
         */
        INVALID, 
        /**
         * Enumeration value USA.
         */
        USA, 
        /**
         * Enumeration value CANADA.
         */
        CANADA, 
        /**
         * Enumeration value EUROPE.
         */
        EUROPE, 
        /**
         * Enumeration value SPAIN.
         */
        SPAIN, 
        /**
         * Enumeration value FRANCE.
         */
        FRANCE, 
        /**
         * Enumeration value JAPAN.
         */
        JAPAN, 
        /**
         * Enumeration value AUSTRALIA.
         */
        AUSTRALIA, 
        /**
         * Enumeration value Reserved8.
         */
        Reserved8, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case INVALID: return (long)0;
			case USA: return (long)1;
			case CANADA: return (long)2;
			case EUROPE: return (long)3;
			case SPAIN: return (long)4;
			case FRANCE: return (long)5;
			case JAPAN: return (long)6;
			case AUSTRALIA: return (long)7;
			case Reserved8: return (long)8;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.INVALID;
			case 1: return Ord.USA;
			case 2: return Ord.CANADA;
			case 3: return Ord.EUROPE;
			case 4: return Ord.SPAIN;
			case 5: return Ord.FRANCE;
			case 6: return Ord.JAPAN;
			case 7: return Ord.AUSTRALIA;
			case 8: return Ord.Reserved8;
		}
		return null;
	}
}
