package com.frontier_silicon.NetRemoteLib.Node;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The definition for the netRemote.play.info.name node.
 * 
 * Headline name of current audio - e.g. station name, podcast name, or track
 * title.  Notifies on change.  This node always returns a useful string if audio
 * is playing (with the exception of aux-in, where no useful information is
 * available)
 */
public class NodePlayInfoName extends BasePlayInfoName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayInfoName(String value) {
		super(value);
	}
	
	/**
	 * See if the name if of the form abc.xyMHz and can therefore be turned in to a frequency.
	 * 
	 * @return		The frequency or null if no match can be made
	 */
	public NodePlayFrequency getFrequencyFromName() {
		Pattern pattern = Pattern.compile("([0-9]+[.][0-9]+)MHz");
		Matcher matcher = pattern.matcher(this.Value);

        if (matcher.find()) {
            String freq = matcher.group(1);
            Double freqAsDouble = Double.valueOf(freq);
            Long freqAsLong = Long.valueOf((long)(freqAsDouble * (double)1000.0));
            return new NodePlayFrequency(freqAsLong);
        }
		
		return null;
	}
}
