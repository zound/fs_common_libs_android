package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.system.id node.
 * 
 * Get the device's system id
 */
public class NodeMultichannelSystemId extends BaseMultichannelSystemId {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultichannelSystemId(String value) {
		super(value);
	}
}
