package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.download.download node.
 * 
 * 
 */
public class NodeNavPresetDownloadDownload extends BaseNavPresetDownloadDownload {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetDownloadDownload(Long value) {
		super(value);
	}
}
