package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.dmruuid node.
 * 
 * Returns the UUID used in DLNA/UPnP communications. This allows a controller that
 * supports DLNA/UPnP to identify the same device.
 */
public class NodeSysInfoDmruuid extends BaseSysInfoDmruuid {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoDmruuid(String value) {
		super(value);
	}
}
