package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.spotify.bitRate node.
 * 
 * BitRate
 */
public class NodeSpotifyBitRate extends BaseSpotifyBitRate {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSpotifyBitRate(Long value) {
		super(value);
	}
}
