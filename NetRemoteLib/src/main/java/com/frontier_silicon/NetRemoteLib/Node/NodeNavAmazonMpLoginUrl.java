package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.amazonMpLoginUrl node.
 * 
 * Amazon login link
 */
public class NodeNavAmazonMpLoginUrl extends BaseNavAmazonMpLoginUrl {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavAmazonMpLoginUrl(String value) {
		super(value);
	}
}
