package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.currentPreset node.
 * 
 * Currently playing preset id. Returns UINT32_MAX if no preset is playing
 */
public class NodeNavPresetCurrentPreset extends BaseNavPresetCurrentPreset {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetCurrentPreset(Long value) {
		super(value);
	}
}
