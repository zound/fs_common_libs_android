package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.bluetooth.connectedDevicesListVersion node.
 * 
 * Bluetooth connectedDevicesList Version
 */
public class NodeBluetoothConnectedDevicesListVersion extends BaseBluetoothConnectedDevicesListVersion {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeBluetoothConnectedDevicesListVersion(Long value) {
		super(value);
	}
}
