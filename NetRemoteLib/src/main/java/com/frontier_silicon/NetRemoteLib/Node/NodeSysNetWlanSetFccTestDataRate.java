package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setFccTestDataRate node.
 *
 * Set the data rate for FCC test
 */

public class NodeSysNetWlanSetFccTestDataRate extends BaseSysNetWlanSetFccTestDataRate {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanSetFccTestDataRate(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanSetFccTestDataRate(Ord value) {
		super(value);
	}
}
