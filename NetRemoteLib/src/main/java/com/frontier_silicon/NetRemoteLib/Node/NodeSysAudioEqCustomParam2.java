package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.eqCustom.param2 node.
 * 
 * Setting for third custom EQ parameter (if defined)
 */
public class NodeSysAudioEqCustomParam2 extends BaseSysAudioEqCustomParam2 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAudioEqCustomParam2(Long value) {
		super(value);
	}
}
