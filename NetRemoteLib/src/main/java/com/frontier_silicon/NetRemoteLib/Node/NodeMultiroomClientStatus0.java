package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.status0 node.
 *
 * Client 0 status
 */

public class NodeMultiroomClientStatus0 extends BaseMultiroomClientStatus0 {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientStatus0(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientStatus0(Ord value) {
		super(value);
	}
}
