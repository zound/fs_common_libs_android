package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.regionFcc node.
 *
 * Read only node returning a bool variable accordingly to the Fcc region hardware
 * gpio setup
 */

public class NodeSysNetWlanRegionFcc extends BaseSysNetWlanRegionFcc {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanRegionFcc(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanRegionFcc(Ord value) {
		super(value);
	}
}
