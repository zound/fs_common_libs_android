package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.create node.
 * 
 * Create a new group with the specified name
 */
public class NodeMultiroomGroupCreate extends BaseMultiroomGroupCreate {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomGroupCreate(String value) {
		super(value);
	}
}
