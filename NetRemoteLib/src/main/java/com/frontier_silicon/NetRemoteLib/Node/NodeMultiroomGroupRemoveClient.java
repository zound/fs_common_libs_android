package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.removeClient node.
 * 
 * Instruct client to leave the group
 */
public class NodeMultiroomGroupRemoveClient extends BaseMultiroomGroupRemoveClient {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomGroupRemoveClient(String value) {
		super(value);
	}
}
