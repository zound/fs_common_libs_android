package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.activeSession node.
 *
 * Indicates if a session is currently active.
 */

public class NodeSysInfoActiveSession extends BaseSysInfoActiveSession {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysInfoActiveSession(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysInfoActiveSession(Ord value) {
		super(value);
	}
}
