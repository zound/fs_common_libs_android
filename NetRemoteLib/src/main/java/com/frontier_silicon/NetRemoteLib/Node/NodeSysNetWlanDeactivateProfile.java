package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.deactivateProfile node.
 * 
 * SSID extracted from the wlan.profiles node
 */
public class NodeSysNetWlanDeactivateProfile extends BaseSysNetWlanDeactivateProfile {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanDeactivateProfile(String value) {
		super(value);
	}
}
