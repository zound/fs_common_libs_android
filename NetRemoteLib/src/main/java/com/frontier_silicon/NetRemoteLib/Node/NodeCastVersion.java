package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.cast.version node.
 * 
 * indicates the current cast version
 */
public class NodeCastVersion extends BaseCastVersion {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeCastVersion(String value) {
		super(value);
	}
}
