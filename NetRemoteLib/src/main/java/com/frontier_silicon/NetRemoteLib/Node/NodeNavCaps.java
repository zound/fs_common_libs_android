package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.caps node.
 * 
 * The navigation capabilities of the current mode.  This node returns a bit-field
 * with the following meanings.
 * 
 * !======================================== ! Bit ! Meaning
 * 
 * ! 0 ! Navigation (using netRemote.nav.list node)
 * 
 * ! 1 ! Presets (using netRemote.nav.presets node)
 * !========================================
 */
public class NodeNavCaps extends BaseNavCaps {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavCaps(Long value) {
		super(value);
	}
	
	/**
	 * Enum that lists all navigation capabilities 
	 */
	public enum NodeNavCapability {
		/**
		 * Navigation capability 
		 */
		NodeNavCapabilityNavigation,
		/**
		 * Presets capability
		 */
		NodeNavCapabilityPresets
	}
	
	/**
	 * Test if certain operations are supported in the current state.
	 * @param navCapability
	 * @return True if supported
	 */
	public boolean DoesSupport(NodeNavCapability navCapability) {
		return (this.getValue() & (1 << navCapability.ordinal())) != 0;
	}
}
