package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.nav.list node.
 * 
 * Current items.  Items can only be requested from the list when the status is
 * READY or FAIL.
 */
public class NodeNavList extends BaseNavList {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeNavList(Node value) throws NodeParseErrorException {
		super(value);
	}
}
