package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.eqCustom.param3 node.
 * 
 * Setting for fourth custom EQ parameter (if defined)
 */
public class NodeSysAudioEqCustomParam3 extends BaseSysAudioEqCustomParam3 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAudioEqCustomParam3(Long value) {
		super(value);
	}
}
