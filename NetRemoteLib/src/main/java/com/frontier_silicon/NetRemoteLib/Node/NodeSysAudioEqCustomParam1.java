package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.eqCustom.param1 node.
 * 
 * Setting for second custom EQ parameter (if defined)
 */
public class NodeSysAudioEqCustomParam1 extends BaseSysAudioEqCustomParam1 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAudioEqCustomParam1(Long value) {
		super(value);
	}
}
