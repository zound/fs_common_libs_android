package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.isu.summary node.
 * 
 * Summary of changes coming with the new firmware(if the CHECK_FOR_UPDATE succeeds
 * with UPDATE_AVAILABLE)
 */
public class NodeSysIsuSummary extends BaseSysIsuSummary {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysIsuSummary(String value) {
		super(value);
	}
}
