package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.modelName node.
 * 
 * Node to return device model name
 */
public class NodeSysInfoModelName extends BaseSysInfoModelName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoModelName(String value) {
		super(value);
	}
}
