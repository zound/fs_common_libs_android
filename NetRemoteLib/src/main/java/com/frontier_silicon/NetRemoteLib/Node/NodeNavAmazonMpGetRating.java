package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.amazonMpGetRating node.
 * 
 * The ratings posibilites of the current station track.  This node returns a bit-
 * field with the following meanings.
 * 
 * !======================================== ! Bit ! Meaning
 * 
 * ! 0 ! Positive rating is available
 * 
 * ! 1 ! Neutral rating is available
 * 
 * ! 2 ! Negative rating is available
 * 
 * !========================================
 */
public class NodeNavAmazonMpGetRating extends BaseNavAmazonMpGetRating {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavAmazonMpGetRating(Long value) {
		super(value);
	}
}
