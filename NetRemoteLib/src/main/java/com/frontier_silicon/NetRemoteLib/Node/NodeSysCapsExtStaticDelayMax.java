package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.caps.extStaticDelayMax node.
 * 
 * The upper limit of the netRemote.sys.audio.extStaticDelay node
 */
public class NodeSysCapsExtStaticDelayMax extends BaseSysCapsExtStaticDelayMax {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysCapsExtStaticDelayMax(Long value) {
		super(value);
	}
}
