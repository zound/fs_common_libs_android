package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.bluetooth.connectedDevices node.
 * 
 * The list consist the names of the connected devices name to the speaker
 */
public class NodeBluetoothConnectedDevices extends BaseBluetoothConnectedDevices {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeBluetoothConnectedDevices(Node value) throws NodeParseErrorException {
		super(value);
	}
}
