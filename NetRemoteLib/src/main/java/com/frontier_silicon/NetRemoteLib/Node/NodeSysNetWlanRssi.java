package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.rssi node.
 * 
 * Returns an indication of  the received signal strength for the current Wireless
 * LAN.
 * 
 * A value of 0 means that there is no useable WiFi signal. A value of 100 means
 * that the signal is at or near to maximum.
 * 
 * Note that the RSSI is calculated whenever a wireless packet is received, it is
 * therefore only valid when connected to a Wireless LAN.
 */
public class NodeSysNetWlanRssi extends BaseSysNetWlanRssi {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanRssi(Long value) {
		super(value);
	}
}
