package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.isu.softwareUpdateProgress node.
 * 
 * netRemote node to monitor the progress during firmware update.             The
 * node is returning an integer value between 0 and 100, according to the updating
 * state. When not performing a firmware update it returns 0.
 */
public class NodeSysIsuSoftwareUpdateProgress extends BaseSysIsuSoftwareUpdateProgress {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysIsuSoftwareUpdateProgress(Long value) {
		super(value);
	}
}
