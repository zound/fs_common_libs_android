package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.status2 node.
 *
 * Client 2 status
 */

public class NodeMultiroomClientStatus2 extends BaseMultiroomClientStatus2 {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientStatus2(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientStatus2(Ord value) {
		super(value);
	}
}
