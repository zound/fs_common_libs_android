package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.download.type node.
 * 
 * 
 */
public class NodeNavPresetDownloadType extends BaseNavPresetDownloadType {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetDownloadType(String value) {
		super(value);
	}
}
