package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.device.listAllVersion node.
 * 
 * List version
 */
public class NodeMultiroomDeviceListAllVersion extends BaseMultiroomDeviceListAllVersion {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomDeviceListAllVersion(Long value) {
		super(value);
	}
}
