package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.audio.eqCustom.param4 node.
 * 
 * Setting for fifth custom EQ parameter (if defined)
 */
public class NodeSysAudioEqCustomParam4 extends BaseSysAudioEqCustomParam4 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAudioEqCustomParam4(Long value) {
		super(value);
	}
}
