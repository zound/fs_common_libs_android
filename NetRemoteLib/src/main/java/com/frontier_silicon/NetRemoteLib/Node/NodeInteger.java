package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The abstract base type of integer nodes.
 */
public class NodeInteger {
	private long Max;
	private long Min;
	
	/**
	 * Get the maximum possible value that can be stored in this type of node.
	 * 
	 * @return		The maximum value
	 */
	public long getMaxPossibleValue() { return this.Max; }
	
	/**
	 * Get the minimum possible value that can be stored in this type of node.
	 * 
	 * @return		The minimum value
	 */
	public long getMinPossibleValue() { return this.Min; }
	
	/**
	 * Constructs a node with the given value.
	 * 
	 * @param value		The numeric value of the node
	 */
	protected NodeInteger(Long value, long max, long min) {
		this.Max = max;
		this.Min = min;
//		NetRemote.assertTrue(value >= min);
//		NetRemote.assertTrue(value <= max);
		this.Value = value;
	}
	
	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	NodeInteger() {
	}
	
	/**
	 * Gets the value of the node.
	 * 
	 * @return			The numeric value of the node
	 */
	public Long getValue() { return this.Value; }
	protected long Value = 0;
	
	/**
	 * Gets a string representing the node value.
	 * 
	 * @return			A string representing the node value
	 */
	public String toString() { return ((Long)(this.Value)).toString(); }
	
	/**
	 * Test the equality of two nodes.
	 * 
	 * @param otherNode		The other node to test
	 * @return				True if the value of the two nodes are equal
	 */
	public boolean equals(NodeInfo otherNode) {
		if (otherNode instanceof NodeInteger) {
			return this.Value == ((NodeInteger)otherNode).Value;
		}
		
		return false;
	}
}
