package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.power node.
 *
 * Standby state of device.  Note that some time after entering standby, the
 * network interface will be shut-down, so communication between the net remote and
 * the radio will be lost.
 */

public class NodeSysPower extends BaseSysPower {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysPower(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysPower(Ord value) {
		super(value);
	}
}
