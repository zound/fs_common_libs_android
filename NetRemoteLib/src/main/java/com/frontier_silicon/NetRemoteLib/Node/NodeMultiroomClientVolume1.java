package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.volume1 node.
 * 
 * Handle client 1 volume level
 */
public class NodeMultiroomClientVolume1 extends BaseMultiroomClientVolume1 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomClientVolume1(Long value) {
		super(value);
	}
}
