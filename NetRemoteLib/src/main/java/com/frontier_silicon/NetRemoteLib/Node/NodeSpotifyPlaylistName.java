package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.spotify.playlist.name node.
 * 
 * Current playing playlist name
 */
public class NodeSpotifyPlaylistName extends BaseSpotifyPlaylistName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSpotifyPlaylistName(String value) {
		super(value);
	}
}
