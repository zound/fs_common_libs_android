package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.info.description node.
 * 
 * Text metadata (up to 4kB)
 */
public class NodePlayInfoDescription extends BasePlayInfoDescription {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
    public NodePlayInfoDescription(String value) {
        super(value);
    }
}
