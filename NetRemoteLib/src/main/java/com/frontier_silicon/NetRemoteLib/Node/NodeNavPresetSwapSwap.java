package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.swap.swap node.
 * 
 * Perform index1 and index2 preset swap operation
 */
public class NodeNavPresetSwapSwap extends BaseNavPresetSwapSwap {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetSwapSwap(Long value) {
		super(value);
	}
}
