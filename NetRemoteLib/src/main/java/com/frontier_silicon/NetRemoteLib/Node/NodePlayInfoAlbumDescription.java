package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.info.albumDescription node.
 * 
 * Album metadata (up to 4kB)
 */
public class NodePlayInfoAlbumDescription extends BasePlayInfoAlbumDescription {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayInfoAlbumDescription(String value) {
		super(value);
	}
}
