package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.repeat node.
 *
 * Repeat playback, if appropriate.
 */

public class NodePlayRepeat extends BasePlayRepeat {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayRepeat(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayRepeat(Ord value) {
		super(value);
	}
	
	/**
	 * Return a node set to the opposite value of this node.
	 * 
	 * @return		The toggled value
	 */
	public NodePlayRepeat getToggledValue() {
		if (this.getValueEnum() == Ord.ON) {
			return new NodePlayRepeat(Ord.OFF);
		}
		
		return new NodePlayRepeat(Ord.ON);
	}
}
