package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.clock.timeZone node.
 * 
 * Set Time Zone in TZ data format. The string must match one of the regions values
 * from netremote.sys.caps.utcsettinglist. An error is returned if the selected
 * region is not found in netremote.sys.caps.utcsettings list. In case setting
 * timezone was unsuccessful an error is returned.
 */
public class NodeSysClockTimeZone extends BaseSysClockTimeZone {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysClockTimeZone(String value) {
		super(value);
	}
}
