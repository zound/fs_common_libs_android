package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.info.text node.
 * 
 * Some contextual text for the current audio, if available.  E.g. DAB DLS, FM RDS-
 * Radiotext
 */
public class NodePlayInfoText extends BasePlayInfoText {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayInfoText(String value) {
		super(value);
	}
}
