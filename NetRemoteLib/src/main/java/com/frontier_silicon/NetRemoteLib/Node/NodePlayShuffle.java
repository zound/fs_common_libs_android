package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.shuffle node.
 *
 * Shuffle current order of playback, if appropriate.
 */

public class NodePlayShuffle extends BasePlayShuffle {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayShuffle(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayShuffle(Ord value) {
		super(value);
	}
	
	/**
	 * Return a node set to the opposite value of this node.
	 * 
	 * @return		The toggled value
	 */
	public NodePlayShuffle getToggledValue() {
		if (this.getValueEnum() == Ord.ON) {
			return new NodePlayShuffle(Ord.OFF);
		}
		
		return new NodePlayShuffle(Ord.ON);
	}
}
