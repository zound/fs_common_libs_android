package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.net.wlan.setFccTestDataRate node.
 *
 * Set the data rate for FCC test
 */
public class BaseSysNetWlanSetFccTestDataRate extends NodeE8<BaseSysNetWlanSetFccTestDataRate.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.net.wlan.setFccTestDataRate"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10106210; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseSysNetWlanSetFccTestDataRate(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysNetWlanSetFccTestDataRate() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseSysNetWlanSetFccTestDataRate(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value _1M.
         */
        _1M, 
        /**
         * Enumeration value _2M.
         */
        _2M, 
        /**
         * Enumeration value _5_5M.
         */
        _5_5M, 
        /**
         * Enumeration value _11M.
         */
        _11M, 
        /**
         * Enumeration value _22M.
         */
        _22M, 
        /**
         * Enumeration value _6M.
         */
        _6M, 
        /**
         * Enumeration value _9M.
         */
        _9M, 
        /**
         * Enumeration value _12M.
         */
        _12M, 
        /**
         * Enumeration value _18M.
         */
        _18M, 
        /**
         * Enumeration value _24M.
         */
        _24M, 
        /**
         * Enumeration value _36M.
         */
        _36M, 
        /**
         * Enumeration value _48M.
         */
        _48M, 
        /**
         * Enumeration value _54M.
         */
        _54M, 
        /**
         * Enumeration value _72M.
         */
        _72M, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case _1M: return (long)0;
			case _2M: return (long)1;
			case _5_5M: return (long)2;
			case _11M: return (long)3;
			case _22M: return (long)4;
			case _6M: return (long)5;
			case _9M: return (long)6;
			case _12M: return (long)7;
			case _18M: return (long)8;
			case _24M: return (long)9;
			case _36M: return (long)10;
			case _48M: return (long)11;
			case _54M: return (long)12;
			case _72M: return (long)13;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord._1M;
			case 1: return Ord._2M;
			case 2: return Ord._5_5M;
			case 3: return Ord._11M;
			case 4: return Ord._22M;
			case 5: return Ord._6M;
			case 6: return Ord._9M;
			case 7: return Ord._12M;
			case 8: return Ord._18M;
			case 9: return Ord._24M;
			case 10: return Ord._36M;
			case 11: return Ord._48M;
			case 12: return Ord._54M;
			case 13: return Ord._72M;
		}
		return null;
	}
}
