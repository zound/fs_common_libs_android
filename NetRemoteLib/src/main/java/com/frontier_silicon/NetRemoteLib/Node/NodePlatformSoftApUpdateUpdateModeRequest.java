package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.platform.softApUpdate.updateModeRequest node.
 *
 * Request of the update mode
 */

public class NodePlatformSoftApUpdateUpdateModeRequest extends BasePlatformSoftApUpdateUpdateModeRequest {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlatformSoftApUpdateUpdateModeRequest(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlatformSoftApUpdateUpdateModeRequest(Ord value) {
		super(value);
	}
}
