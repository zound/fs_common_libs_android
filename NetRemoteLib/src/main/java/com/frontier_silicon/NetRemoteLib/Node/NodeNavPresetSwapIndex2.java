package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.swap.index2 node.
 * 
 * 
 */
public class NodeNavPresetSwapIndex2 extends BaseNavPresetSwapIndex2 {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetSwapIndex2(Long value) {
		super(value);
	}
}
