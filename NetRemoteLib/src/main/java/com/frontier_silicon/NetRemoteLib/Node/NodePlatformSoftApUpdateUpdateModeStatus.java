package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.platform.softApUpdate.updateModeStatus node.
 *
 * Status of the update mode
 */

public class NodePlatformSoftApUpdateUpdateModeStatus extends BasePlatformSoftApUpdateUpdateModeStatus {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlatformSoftApUpdateUpdateModeStatus(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlatformSoftApUpdateUpdateModeStatus(Ord value) {
		super(value);
	}
}
