package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.airplay.clearPassword node.
 *
 * Clear the Airplay password
 */

public class NodeAirplayClearPassword extends BaseAirplayClearPassword {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeAirplayClearPassword(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeAirplayClearPassword(Ord value) {
		super(value);
	}
}
