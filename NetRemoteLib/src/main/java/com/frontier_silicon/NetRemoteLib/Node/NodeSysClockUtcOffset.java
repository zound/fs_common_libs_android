package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.clock.utcOffset node.
 * 
 * The UTC offset in seconds. (For netRemote.sys.clock.source == 3(SNTP))
 */
public class NodeSysClockUtcOffset extends BaseSysClockUtcOffset {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysClockUtcOffset(Long value) {
		super(value);
	}
}
