package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.platform.OEM.colorProduct node.
 * 
 * Color of the product
 */
public class NodePlatformOEMColorProduct extends BasePlatformOEMColorProduct {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlatformOEMColorProduct(String value) {
		super(value);
	}
}
