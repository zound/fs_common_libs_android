package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.clock.source node.
 *
 * Select which source(s) to use as a reference when updating the clock. Changing
 * the node to any nonzero value results in an immediate update. A value of User
 * (0) disables the update of the clock and the clock must be set using the
 * netRemote.sys.clock.time and netRemote.sys.clock.date nodes. The time between
 * updates is build-time configurable for each source.
 */

public class NodeSysClockSource extends BaseSysClockSource {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysClockSource(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysClockSource(Ord value) {
		super(value);
	}
}
