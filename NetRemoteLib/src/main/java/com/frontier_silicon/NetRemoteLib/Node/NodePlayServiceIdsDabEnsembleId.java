package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.serviceIds.dabEnsembleId node.
 * 
 * DAB Ensemble ID
 */
public class NodePlayServiceIdsDabEnsembleId extends BasePlayServiceIdsDabEnsembleId {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayServiceIdsDabEnsembleId(Long value) {
		super(value);
	}
}
