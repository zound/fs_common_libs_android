package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.lang node.
 * 
 * Index into the netRemote.sys.caps.validLang
 */
public class NodeSysLang extends BaseSysLang {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysLang(Long value) {
		super(value);
	}
}
