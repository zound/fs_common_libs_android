package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.caps.maxClients node.
 * 
 * Max clients number
 */
public class NodeMultiroomCapsMaxClients extends BaseMultiroomCapsMaxClients {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomCapsMaxClients(Long value) {
		super(value);
	}
}
