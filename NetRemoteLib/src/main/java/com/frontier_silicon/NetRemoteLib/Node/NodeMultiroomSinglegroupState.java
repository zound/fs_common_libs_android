package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.singlegroup.state node.
 *
 * Device state in SG-MR configuration
 */

public class NodeMultiroomSinglegroupState extends BaseMultiroomSinglegroupState {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomSinglegroupState(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomSinglegroupState(Ord value) {
		super(value);
	}
}
