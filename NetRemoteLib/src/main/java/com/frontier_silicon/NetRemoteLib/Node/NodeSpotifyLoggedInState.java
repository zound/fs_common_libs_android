package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.spotify.loggedInState node.
 *
 * Retrieve spotify logged in state
 */

public class NodeSpotifyLoggedInState extends BaseSpotifyLoggedInState {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSpotifyLoggedInState(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSpotifyLoggedInState(Ord value) {
		super(value);
	}
}
