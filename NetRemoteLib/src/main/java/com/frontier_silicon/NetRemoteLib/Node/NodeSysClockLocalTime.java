package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.clock.localTime node.
 * 
 * Query or set the local time. The time is UTC plus any local time offset. If set
 * when netRemote.sys.clock.source is nonzero (i.e. not User) then the user setting will be overwritten when
 * the clock is next updated from the source. The time is always returned in 24
 * hour format
 */
public class NodeSysClockLocalTime extends BaseSysClockLocalTime {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysClockLocalTime(String value) {
		super(value);
	}
}
