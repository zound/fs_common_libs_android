package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.alarm.duration node.
 * 
 * Current alarm duration in seconds.
 */
public class NodeSysAlarmDuration extends BaseSysAlarmDuration {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAlarmDuration(Long value) {
		super(value);
	}
}
