package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.isu.state node.
 *
 * Used to get/notify various isu states
 */

public class NodeSysIsuState extends BaseSysIsuState {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysIsuState(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysIsuState(Ord value) {
		super(value);
	}
}
