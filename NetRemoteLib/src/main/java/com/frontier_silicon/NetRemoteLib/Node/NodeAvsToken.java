package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.avs.token node.
 * 
 * Set device refresh token
 */
public class NodeAvsToken extends BaseAvsToken {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeAvsToken(String value) {
		super(value);
	}
}
