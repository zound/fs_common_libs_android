package com.frontier_silicon.NetRemoteLib.Node;

/**
 * Created by cvladu on 15/06/2017.
 */

public class NodePlatformOEMLedIntensitySteps extends BasePlatformOEMLedIntensitySteps {

    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */

    public NodePlatformOEMLedIntensitySteps(Long value) {
        super(value);
    }
}
