package com.frontier_silicon.NetRemoteLib.Node;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo.NodePrototype;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse.FSStatusCode;

import org.w3c.dom.Node;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A helper class for determining information about node classes.
 */
@SuppressWarnings("rawtypes")
public class NodeDefs {
	private static NodeDefs TheInstance = null;
	
	private final static String NodeNamespace = "com.frontier_silicon.NetRemoteLib.Node.";

    /**
     * The maximum number of items that can be held in a list node.
     */
    public static final int MaxItemsForList = 65536;

    /**
     * The default start index for list nodes
     */
    public static final int StartIndexForList = -1;

    /**
	 * Get the single static instance of this class. You should use the static instance and not
	 * create your own separate instances.
	 * 
	 * @return			The instance
	 */
	public static NodeDefs Instance() {
		if (NodeDefs.TheInstance == null) {
			NodeDefs.TheInstance = new NodeDefs();
		}
		return NodeDefs.TheInstance;
	}
	
	private Map<Class, String> NodeLookup = null;
	private Map<String, Class> ClassLookup = null;
	//private Map<Long, Class> NodeAddressLookup = null;
	//private Map<Class, Long> NodeAddressReverseLookup = null;FReleaseDate
	private Map<Long, FSStatusCode> FSStatusCodeLookup = null;
    private Set<Class> IgnoredNodesInCaseOfNodeDoesNotExist = null;


	private synchronized void InitAllNodes() {
		if (this.NodeLookup == null) {
            // Increase this in case the number of nodes is larger than the defined value. Please note
            // that base classes are added at a later step.
			this.NodeLookup = new HashMap<>(700);

			/* Start of auto-generated code. */
            this.NodeLookup.put(NodeBluetoothDiscoverableState.class, "netRemote.bluetooth.discoverableState");
            this.NodeLookup.put(NodeBluetoothConnectedDevices.class, "netRemote.bluetooth.connectedDevices");
            this.NodeLookup.put(NodeBluetoothConnectedDevicesListVersion.class, "netRemote.bluetooth.connectedDevicesListVersion");
            this.NodeLookup.put(NodeSysInfoFriendlyName.class, "netRemote.sys.info.friendlyName");
            this.NodeLookup.put(NodeSysInfoVersion.class, "netRemote.sys.info.version");
            this.NodeLookup.put(NodeSysInfoRadioId.class, "netRemote.sys.info.radioId");
            this.NodeLookup.put(NodeSysInfoRadioPin.class, "netRemote.sys.info.radioPin");
            this.NodeLookup.put(NodeSysInfoControllerName.class, "netRemote.sys.info.controllerName");
            this.NodeLookup.put(NodeSysInfoDmruuid.class, "netRemote.sys.info.dmruuid");
            this.NodeLookup.put(NodeSysInfoActiveSession.class, "netRemote.sys.info.activeSession");
            this.NodeLookup.put(NodeSysInfoModelName.class, "netRemote.sys.info.modelName");
            this.NodeLookup.put(NodeSysInfoSerialNumber.class, "netRemote.sys.info.serialNumber");
            this.NodeLookup.put(NodeSysCapsValidModes.class, "netRemote.sys.caps.validModes");
            this.NodeLookup.put(NodeSysCapsVolumeSteps.class, "netRemote.sys.caps.volumeSteps");
            this.NodeLookup.put(NodeSysCapsEqPresets.class, "netRemote.sys.caps.eqPresets");
            this.NodeLookup.put(NodeSysCapsEqBands.class, "netRemote.sys.caps.eqBands");
            this.NodeLookup.put(NodeSysCapsValidLang.class, "netRemote.sys.caps.validLang");
            this.NodeLookup.put(NodeSysCapsFmFreqRangeLower.class, "netRemote.sys.caps.fmFreqRange.lower");
            this.NodeLookup.put(NodeSysCapsFmFreqRangeUpper.class, "netRemote.sys.caps.fmFreqRange.upper");
            this.NodeLookup.put(NodeSysCapsFmFreqRangeStepSize.class, "netRemote.sys.caps.fmFreqRange.stepSize");
            this.NodeLookup.put(NodeSysCapsDabFreqList.class, "netRemote.sys.caps.dabFreqList");
            this.NodeLookup.put(NodeSysCapsExtStaticDelayMax.class, "netRemote.sys.caps.extStaticDelayMax");
            this.NodeLookup.put(NodeSysCapsUtcSettingsList.class, "netRemote.sys.caps.utcSettingsList");
            this.NodeLookup.put(NodeSysCapsClockSourceList.class, "netRemote.sys.caps.clockSourceList");
            this.NodeLookup.put(NodeSysPower.class, "netRemote.sys.power");
            this.NodeLookup.put(NodeSysMode.class, "netRemote.sys.mode");
            this.NodeLookup.put(NodeSysAudioVolume.class, "netRemote.sys.audio.volume");
            this.NodeLookup.put(NodeSysAudioMute.class, "netRemote.sys.audio.mute");
            this.NodeLookup.put(NodeSysAudioEqPreset.class, "netRemote.sys.audio.eqPreset");
            this.NodeLookup.put(NodeSysAudioEqCustomParam0.class, "netRemote.sys.audio.eqCustom.param0");
            this.NodeLookup.put(NodeSysAudioEqCustomParam1.class, "netRemote.sys.audio.eqCustom.param1");
            this.NodeLookup.put(NodeSysAudioEqCustomParam2.class, "netRemote.sys.audio.eqCustom.param2");
            this.NodeLookup.put(NodeSysAudioEqCustomParam3.class, "netRemote.sys.audio.eqCustom.param3");
            this.NodeLookup.put(NodeSysAudioEqCustomParam4.class, "netRemote.sys.audio.eqCustom.param4");
            this.NodeLookup.put(NodeSysAudioEqLoudness.class, "netRemote.sys.audio.eqLoudness");
            this.NodeLookup.put(NodeSysAudioExtStaticDelay.class, "netRemote.sys.audio.extStaticDelay");
            this.NodeLookup.put(NodeSysNetWiredInterfaceEnable.class, "netRemote.sys.net.wired.interfaceEnable");
            this.NodeLookup.put(NodeSysNetWiredMacAddress.class, "netRemote.sys.net.wired.macAddress");
            this.NodeLookup.put(NodeSysNetWlanInterfaceEnable.class, "netRemote.sys.net.wlan.interfaceEnable");
            this.NodeLookup.put(NodeSysNetWlanMacAddress.class, "netRemote.sys.net.wlan.macAddress");
            this.NodeLookup.put(NodeSysNetWlanRegion.class, "netRemote.sys.net.wlan.region");
            this.NodeLookup.put(NodeSysNetWlanScan.class, "netRemote.sys.net.wlan.scan");
            this.NodeLookup.put(NodeSysNetWlanScanList.class, "netRemote.sys.net.wlan.scanList");
            this.NodeLookup.put(NodeSysNetWlanSetPassphrase.class, "netRemote.sys.net.wlan.setPassphrase");
            this.NodeLookup.put(NodeSysNetWlanSelectAP.class, "netRemote.sys.net.wlan.selectAP");
            this.NodeLookup.put(NodeSysNetWlanConnectedSSID.class, "netRemote.sys.net.wlan.connectedSSID");
            this.NodeLookup.put(NodeSysNetWlanSetSSID.class, "netRemote.sys.net.wlan.setSSID");
            this.NodeLookup.put(NodeSysNetWlanSetEncType.class, "netRemote.sys.net.wlan.setEncType");
            this.NodeLookup.put(NodeSysNetWlanSetAuthType.class, "netRemote.sys.net.wlan.setAuthType");
            this.NodeLookup.put(NodeSysNetWlanRssi.class, "netRemote.sys.net.wlan.rssi");
            this.NodeLookup.put(NodeSysNetWlanWpsPinRead.class, "netRemote.sys.net.wlan.wpsPinRead");
            this.NodeLookup.put(NodeSysNetWlanPerformWPS.class, "netRemote.sys.net.wlan.performWPS");
            this.NodeLookup.put(NodeSysNetWlanSetFccTestChanNum.class, "netRemote.sys.net.wlan.setFccTestChanNum");
            this.NodeLookup.put(NodeSysNetWlanSetFccTestDataRate.class, "netRemote.sys.net.wlan.setFccTestDataRate");
            this.NodeLookup.put(NodeSysNetWlanSetFccTestTxDataPattern.class, "netRemote.sys.net.wlan.setFccTestTxDataPattern");
            this.NodeLookup.put(NodeSysNetWlanSetFccTestTxPower.class, "netRemote.sys.net.wlan.setFccTestTxPower");
            this.NodeLookup.put(NodeSysNetWlanPerformFCC.class, "netRemote.sys.net.wlan.performFCC");
            this.NodeLookup.put(NodeSysNetWlanSetFccTxOff.class, "netRemote.sys.net.wlan.setFccTxOff");
            this.NodeLookup.put(NodeSysNetWlanRegionFcc.class, "netRemote.sys.net.wlan.regionFcc");
            this.NodeLookup.put(NodeSysNetWlanProfiles.class, "netRemote.sys.net.wlan.profiles");
            this.NodeLookup.put(NodeSysNetWlanSelectProfile.class, "netRemote.sys.net.wlan.selectProfile");
            this.NodeLookup.put(NodeSysNetWlanRemoveProfile.class, "netRemote.sys.net.wlan.removeProfile");
            this.NodeLookup.put(NodeSysNetWlanActivateProfile.class, "netRemote.sys.net.wlan.activateProfile");
            this.NodeLookup.put(NodeSysNetWlanDeactivateProfile.class, "netRemote.sys.net.wlan.deactivateProfile");
            this.NodeLookup.put(NodeSysNetIpConfigDhcp.class, "netRemote.sys.net.ipConfig.dhcp");
            this.NodeLookup.put(NodeSysNetIpConfigAddress.class, "netRemote.sys.net.ipConfig.address");
            this.NodeLookup.put(NodeSysNetIpConfigSubnetMask.class, "netRemote.sys.net.ipConfig.subnetMask");
            this.NodeLookup.put(NodeSysNetIpConfigGateway.class, "netRemote.sys.net.ipConfig.gateway");
            this.NodeLookup.put(NodeSysNetIpConfigDnsPrimary.class, "netRemote.sys.net.ipConfig.dnsPrimary");
            this.NodeLookup.put(NodeSysNetIpConfigDnsSecondary.class, "netRemote.sys.net.ipConfig.dnsSecondary");
            this.NodeLookup.put(NodeSysNetKeepConnected.class, "netRemote.sys.net.keepConnected");
            this.NodeLookup.put(NodeSysNetCommitChanges.class, "netRemote.sys.net.commitChanges");
            this.NodeLookup.put(NodeSysNetUapInterfaceEnable.class, "netRemote.sys.net.uap.interfaceEnable");
            this.NodeLookup.put(NodeSysClockSource.class, "netRemote.sys.clock.source");
            this.NodeLookup.put(NodeSysClockLocalDate.class, "netRemote.sys.clock.localDate");
            this.NodeLookup.put(NodeSysClockLocalTime.class, "netRemote.sys.clock.localTime");
            this.NodeLookup.put(NodeSysClockUtcOffset.class, "netRemote.sys.clock.utcOffset");
            this.NodeLookup.put(NodeSysClockDst.class, "netRemote.sys.clock.dst");
            this.NodeLookup.put(NodeSysClockMode.class, "netRemote.sys.clock.mode");
            this.NodeLookup.put(NodeSysClockDateFormat.class, "netRemote.sys.clock.dateFormat");
            this.NodeLookup.put(NodeSysIsuState.class, "netRemote.sys.isu.state");
            this.NodeLookup.put(NodeSysIsuControl.class, "netRemote.sys.isu.control");
            this.NodeLookup.put(NodeSysIsuVersion.class, "netRemote.sys.isu.version");
            this.NodeLookup.put(NodeSysIsuSummary.class, "netRemote.sys.isu.summary");
            this.NodeLookup.put(NodeSysIsuSoftwareUpdateProgress.class, "netRemote.sys.isu.softwareUpdateProgress");
            this.NodeLookup.put(NodeSysIsuMandatory.class, "netRemote.sys.isu.mandatory");
            this.NodeLookup.put(NodeSysLang.class, "netRemote.sys.lang");
            this.NodeLookup.put(NodeSysRsaStatus.class, "netRemote.sys.rsa.status");
            this.NodeLookup.put(NodeSysRsaPublicKey.class, "netRemote.sys.rsa.publicKey");
            this.NodeLookup.put(NodeSysAlarmStatus.class, "netRemote.sys.alarm.status");
            this.NodeLookup.put(NodeSysAlarmCurrent.class, "netRemote.sys.alarm.current");
            this.NodeLookup.put(NodeSysAlarmDuration.class, "netRemote.sys.alarm.duration");
            this.NodeLookup.put(NodeSysAlarmSnoozing.class, "netRemote.sys.alarm.snoozing");
            this.NodeLookup.put(NodeSysAlarmSnooze.class, "netRemote.sys.alarm.snooze");
            this.NodeLookup.put(NodeSysAlarmConfigChanged.class, "netRemote.sys.alarm.configChanged");
            this.NodeLookup.put(NodeSysAlarmConfig.class, "netRemote.sys.alarm.config");
            this.NodeLookup.put(NodeSysCfgIrAutoPlayFlag.class, "netRemote.sys.cfg.irAutoPlayFlag");
            this.NodeLookup.put(NodeSysFactoryReset.class, "netRemote.sys.factoryReset");
            this.NodeLookup.put(NodeSysState.class, "netRemote.sys.state");
            this.NodeLookup.put(NodeSysSleep.class, "netRemote.sys.sleep");
            this.NodeLookup.put(NodeSysIpodDockStatus.class, "netRemote.sys.ipod.dockStatus");
            this.NodeLookup.put(NodeNavState.class, "netRemote.nav.state");
            this.NodeLookup.put(NodeNavCaps.class, "netRemote.nav.caps");
            this.NodeLookup.put(NodeNavStatus.class, "netRemote.nav.status");
            this.NodeLookup.put(NodeNavErrorStr.class, "netRemote.nav.errorStr");
            this.NodeLookup.put(NodeNavNumItems.class, "netRemote.nav.numItems");
            this.NodeLookup.put(NodeNavList.class, "netRemote.nav.list");
            this.NodeLookup.put(NodeNavPresets.class, "netRemote.nav.presets");
            this.NodeLookup.put(NodeNavSearchTerm.class, "netRemote.nav.searchTerm");
            this.NodeLookup.put(NodeNavDabScanUpdate.class, "netRemote.nav.dabScanUpdate");
            this.NodeLookup.put(NodeNavActionNavigate.class, "netRemote.nav.action.navigate");
            this.NodeLookup.put(NodeNavActionSelectItem.class, "netRemote.nav.action.selectItem");
            this.NodeLookup.put(NodeNavActionSelectPreset.class, "netRemote.nav.action.selectPreset");
            this.NodeLookup.put(NodeNavActionDabPrune.class, "netRemote.nav.action.dabPrune");
            this.NodeLookup.put(NodeNavActionDabScan.class, "netRemote.nav.action.dabScan");
            this.NodeLookup.put(NodeNavBrowseMode.class, "netRemote.nav.browseMode");
            this.NodeLookup.put(NodeNavDepth.class, "netRemote.nav.depth");
            this.NodeLookup.put(NodeNavPresetListversion.class, "netRemote.nav.preset.listversion");
            this.NodeLookup.put(NodeNavPresetDelete.class, "netRemote.nav.preset.delete");
            this.NodeLookup.put(NodeNavPresetDownloadDownload.class, "netRemote.nav.preset.download.download");
            this.NodeLookup.put(NodeNavPresetDownloadType.class, "netRemote.nav.preset.download.type");
            this.NodeLookup.put(NodeNavPresetDownloadName.class, "netRemote.nav.preset.download.name");
            this.NodeLookup.put(NodeNavPresetDownloadBlob.class, "netRemote.nav.preset.download.blob");
            this.NodeLookup.put(NodeNavPresetDownloadArtworkUrl.class, "netRemote.nav.preset.download.artworkUrl");
            this.NodeLookup.put(NodeNavPresetUploadUpload.class, "netRemote.nav.preset.upload.upload");
            this.NodeLookup.put(NodeNavPresetUploadType.class, "netRemote.nav.preset.upload.type");
            this.NodeLookup.put(NodeNavPresetUploadName.class, "netRemote.nav.preset.upload.name");
            this.NodeLookup.put(NodeNavPresetUploadBlob.class, "netRemote.nav.preset.upload.blob");
            this.NodeLookup.put(NodeNavPresetUploadArtworkUrl.class, "netRemote.nav.preset.upload.artworkUrl");
            this.NodeLookup.put(NodeNavPresetCurrentPreset.class, "netRemote.nav.preset.currentPreset");
            this.NodeLookup.put(NodeNavPresetSwapIndex1.class, "netRemote.nav.preset.swap.index1");
            this.NodeLookup.put(NodeNavPresetSwapIndex2.class, "netRemote.nav.preset.swap.index2");
            this.NodeLookup.put(NodeNavPresetSwapSwap.class, "netRemote.nav.preset.swap.swap");
            this.NodeLookup.put(NodePlayStatus.class, "netRemote.play.status");
            this.NodeLookup.put(NodePlayCaps.class, "netRemote.play.caps");
            this.NodeLookup.put(NodePlayErrorStr.class, "netRemote.play.errorStr");
            this.NodeLookup.put(NodePlayInfoName.class, "netRemote.play.info.name");
            this.NodeLookup.put(NodePlayInfoText.class, "netRemote.play.info.text");
            this.NodeLookup.put(NodePlayInfoDuration.class, "netRemote.play.info.duration");
            this.NodeLookup.put(NodePlayInfoArtist.class, "netRemote.play.info.artist");
            this.NodeLookup.put(NodePlayInfoAlbum.class, "netRemote.play.info.album");
            this.NodeLookup.put(NodePlayInfoGraphicUri.class, "netRemote.play.info.graphicUri");
            this.NodeLookup.put(NodePlayControl.class, "netRemote.play.control");
            this.NodeLookup.put(NodePlayRate.class, "netRemote.play.rate");
            this.NodeLookup.put(NodePlayPosition.class, "netRemote.play.position");
            this.NodeLookup.put(NodePlayFeedback.class, "netRemote.play.feedback");
            this.NodeLookup.put(NodePlayShuffle.class, "netRemote.play.shuffle");
            this.NodeLookup.put(NodePlayRepeat.class, "netRemote.play.repeat");
            this.NodeLookup.put(NodePlayScrobble.class, "netRemote.play.scrobble");
            this.NodeLookup.put(NodePlayFrequency.class, "netRemote.play.frequency");
            this.NodeLookup.put(NodePlaySignalStrength.class, "netRemote.play.signalStrength");
            this.NodeLookup.put(NodePlayAddPreset.class, "netRemote.play.addPreset");
            this.NodeLookup.put(NodePlayServiceIdsEcc.class, "netRemote.play.serviceIds.ecc");
            this.NodeLookup.put(NodePlayServiceIdsFmRdsPi.class, "netRemote.play.serviceIds.fmRdsPi");
            this.NodeLookup.put(NodePlayServiceIdsDabEnsembleId.class, "netRemote.play.serviceIds.dabEnsembleId");
            this.NodeLookup.put(NodePlayServiceIdsDabServiceId.class, "netRemote.play.serviceIds.dabServiceId");
            this.NodeLookup.put(NodePlayServiceIdsDabScids.class, "netRemote.play.serviceIds.dabScids");
            this.NodeLookup.put(NodePlayShuffleStatus.class, "netRemote.play.shuffleStatus");
            this.NodeLookup.put(NodePlayAddPresetStatus.class, "netRemote.play.addPresetStatus");
            this.NodeLookup.put(NodePlayAlerttone.class, "netRemote.play.alerttone");
            this.NodeLookup.put(NodeMiscFsDebugComponent.class, "netRemote.misc.fsDebug.component");
            this.NodeLookup.put(NodeMiscFsDebugTraceLevel.class, "netRemote.misc.fsDebug.traceLevel");
            this.NodeLookup.put(NodePlatformSoftApUpdateUpdateModeStatus.class, "netRemote.platform.softApUpdate.updateModeStatus");
            this.NodeLookup.put(NodePlatformSoftApUpdateUpdateModeRequest.class, "netRemote.platform.softApUpdate.updateModeRequest");
            this.NodeLookup.put(NodePlatformOEMColorProduct.class, "netRemote.platform.OEM.colorProduct");
            this.NodeLookup.put(NodeSpotifyUsername.class, "netRemote.spotify.username");
            this.NodeLookup.put(NodeSpotifyBitRate.class, "netRemote.spotify.bitRate");
            this.NodeLookup.put(NodeSpotifyStatus.class, "netRemote.spotify.status");
            this.NodeLookup.put(NodeSpotifyLastError.class, "netRemote.spotify.lastError");
            this.NodeLookup.put(NodeSpotifyLoginUsingOauthToken.class, "netRemote.spotify.loginUsingOauthToken");
            this.NodeLookup.put(NodeSpotifyLoggedInState.class, "netRemote.spotify.loggedInState");
            this.NodeLookup.put(NodeMultiroomGroupCreate.class, "netRemote.multiroom.group.create");
            this.NodeLookup.put(NodeMultiroomGroupDestroy.class, "netRemote.multiroom.group.destroy");
            this.NodeLookup.put(NodeMultiroomGroupId.class, "netRemote.multiroom.group.id");
            this.NodeLookup.put(NodeMultiroomGroupAddClient.class, "netRemote.multiroom.group.addClient");
            this.NodeLookup.put(NodeMultiroomGroupRemoveClient.class, "netRemote.multiroom.group.removeClient");
            this.NodeLookup.put(NodeMultiroomGroupState.class, "netRemote.multiroom.group.state");
            this.NodeLookup.put(NodeMultiroomGroupName.class, "netRemote.multiroom.group.name");
            this.NodeLookup.put(NodeMultiroomGroupMasterVolume.class, "netRemote.multiroom.group.masterVolume");
            this.NodeLookup.put(NodeMultiroomGroupStreamable.class, "netRemote.multiroom.group.streamable");
            this.NodeLookup.put(NodeMultiroomGroupAttachedClients.class, "netRemote.multiroom.group.attachedClients");
            this.NodeLookup.put(NodeMultiroomGroupBecomeServer.class, "netRemote.multiroom.group.becomeServer");
            this.NodeLookup.put(NodeMultiroomDeviceListAllVersion.class, "netRemote.multiroom.device.listAllVersion");
            this.NodeLookup.put(NodeMultiroomDeviceListAll.class, "netRemote.multiroom.device.listAll");
            this.NodeLookup.put(NodeMultiroomDeviceClientStatus.class, "netRemote.multiroom.device.clientStatus");
            this.NodeLookup.put(NodeMultiroomDeviceServerStatus.class, "netRemote.multiroom.device.serverStatus");
            this.NodeLookup.put(NodeMultiroomDeviceClientIndex.class, "netRemote.multiroom.device.clientIndex");
            this.NodeLookup.put(NodeMultiroomDeviceTransportOptimisation.class, "netRemote.multiroom.device.transportOptimisation");
            this.NodeLookup.put(NodeMultiroomCapsMaxClients.class, "netRemote.multiroom.caps.maxClients");
            this.NodeLookup.put(NodeMultiroomCapsProtocolVersion.class, "netRemote.multiroom.caps.protocolVersion");
            this.NodeLookup.put(NodeMultiroomClientStatus0.class, "netRemote.multiroom.client.status0");
            this.NodeLookup.put(NodeMultiroomClientStatus1.class, "netRemote.multiroom.client.status1");
            this.NodeLookup.put(NodeMultiroomClientStatus2.class, "netRemote.multiroom.client.status2");
            this.NodeLookup.put(NodeMultiroomClientStatus3.class, "netRemote.multiroom.client.status3");
            this.NodeLookup.put(NodeMultiroomClientVolume0.class, "netRemote.multiroom.client.volume0");
            this.NodeLookup.put(NodeMultiroomClientVolume1.class, "netRemote.multiroom.client.volume1");
            this.NodeLookup.put(NodeMultiroomClientVolume2.class, "netRemote.multiroom.client.volume2");
            this.NodeLookup.put(NodeMultiroomClientVolume3.class, "netRemote.multiroom.client.volume3");
            this.NodeLookup.put(NodeMultiroomClientMute0.class, "netRemote.multiroom.client.mute0");
            this.NodeLookup.put(NodeMultiroomClientMute1.class, "netRemote.multiroom.client.mute1");
            this.NodeLookup.put(NodeMultiroomClientMute2.class, "netRemote.multiroom.client.mute2");
            this.NodeLookup.put(NodeMultiroomClientMute3.class, "netRemote.multiroom.client.mute3");
            this.NodeLookup.put(NodeMultiroomSinglegroupState.class, "netRemote.multiroom.singlegroup.state");
            this.NodeLookup.put(NodeAirplaySetPassword.class, "netRemote.airplay.setPassword");
            this.NodeLookup.put(NodeAirplayClearPassword.class, "netRemote.airplay.clearPassword");
            this.NodeLookup.put(NodeCastTos.class, "netRemote.cast.tos");
            this.NodeLookup.put(NodeCastUsageReport.class, "netRemote.cast.usageReport");
            this.NodeLookup.put(NodeCastAppName.class, "netRemote.cast.appName");
            this.NodeLookup.put(NodeCastVersion.class, "netRemote.cast.version");
            this.NodeLookup.put(NodeNavFormData.class, "netRemote.nav.formData");
            this.NodeLookup.put(NodeNavFormItem.class, "netRemote.nav.form.item");
            this.NodeLookup.put(NodeNavFormOption.class, "netRemote.nav.form.option");
            this.NodeLookup.put(NodeNavFormButton.class, "netRemote.nav.form.button");
            this.NodeLookup.put(NodeSysInfoRadiotest.class, "netRemote.sys.info.radiotest");
            this.NodeLookup.put(NodeNavActionContext.class, "netRemote.nav.action.context");
            this.NodeLookup.put(NodeNavRefresh.class, "netRemote.nav.refresh");
            this.NodeLookup.put(NodeNavContextStatus.class, "netRemote.nav.context.status");
            this.NodeLookup.put(NodeNavContextErrorStr.class, "netRemote.nav.context.errorStr");
            this.NodeLookup.put(NodeNavContextNumItems.class, "netRemote.nav.context.numItems");
            this.NodeLookup.put(NodeNavContextList.class, "netRemote.nav.context.list");
            this.NodeLookup.put(NodeNavContextNavigate.class, "netRemote.nav.context.navigate");
            this.NodeLookup.put(NodeNavContextFormData.class, "netRemote.nav.context.formData");
            this.NodeLookup.put(NodeNavContextDepth.class, "netRemote.nav.context.depth");
            this.NodeLookup.put(NodeNavContextFormItem.class, "netRemote.nav.context.form.item");
            this.NodeLookup.put(NodeNavContextFormOption.class, "netRemote.nav.context.form.option");
            this.NodeLookup.put(NodeSysAudioAirableQuality.class, "netRemote.sys.audio.airableQuality");
            this.NodeLookup.put(NodePlayInfoDescription.class, "netRemote.play.info.description");
            this.NodeLookup.put(NodeNavDescription.class, "netRemote.nav.description");
            this.NodeLookup.put(NodeNavReleaseDate.class, "netRemote.nav.releaseDate");
            this.NodeLookup.put(NodeNavAmazonMpLoginUrl.class, "netRemote.nav.amazonMpLoginUrl");
            this.NodeLookup.put(NodeNavAmazonMpLoginComplete.class, "netRemote.nav.amazonMpLoginComplete");
            this.NodeLookup.put(NodeNavAmazonMpGetRating.class, "netRemote.nav.amazonMpGetRating");
            this.NodeLookup.put(NodeNavAmazonMpSetRating.class, "netRemote.nav.amazonMpSetRating");
            this.NodeLookup.put(NodePlayInfoArtistDescription.class, "netRemote.play.info.artistDescription");
			this.NodeLookup.put(NodePlayInfoAlbumDescription.class, "netRemote.play.info.albumDescription");
            this.NodeLookup.put(NodeSysInfoNetRemoteVendorId.class, "netRemote.sys.info.netRemoteVendorId");
            this.NodeLookup.put(NodeSysClockTimeZone.class, "netRemote.sys.clock.timeZone");
            this.NodeLookup.put(NodeSpotifyPlaylistName.class, "netRemote.spotify.playlist.name");
            this.NodeLookup.put(NodeSpotifyPlaylistUri.class, "netRemote.spotify.playlist.uri");
			this.NodeLookup.put(NodeSysInfoBuildVersion.class, "netRemote.sys.info.buildVersion");
			this.NodeLookup.put(NodeMiscNvsData.class, "netRemote.misc.nvs.data");
			this.NodeLookup.put(NodePlatformOEMLedIntensitySteps.class, "netRemote.platform.OEM.ledIntensitySteps");
			this.NodeLookup.put(NodePlatformOEMLedIntensity.class, "netRemote.platform.OEM.ledIntensity");
            this.NodeLookup.put(NodeMultichannelSystemCreate.class, "netRemote.multichannel.system.create");
            this.NodeLookup.put(NodeMultichannelSystemUnpair.class, "netRemote.multichannel.system.unpair");
            this.NodeLookup.put(NodeMultichannelSystemAddsecondary.class, "netRemote.multichannel.system.addsecondary");
            this.NodeLookup.put(NodeMultichannelSystemRemovesecondary.class, "netRemote.multichannel.system.removesecondary");
            this.NodeLookup.put(NodeMultichannelSystemState.class, "netRemote.multichannel.system.state");
            this.NodeLookup.put(NodeMultichannelSystemName.class, "netRemote.multichannel.system.name");
            this.NodeLookup.put(NodeMultichannelSystemCompatibilityid.class, "netRemote.multichannel.system.compatibilityid");
            this.NodeLookup.put(NodeMultichannelSystemId.class, "netRemote.multichannel.system.id");
            this.NodeLookup.put(NodeMultichannelPrimaryChannelmask.class, "netRemote.multichannel.primary.channelmask");
            this.NodeLookup.put(NodeMultichannelSecondary0Channelmask.class, "netRemote.multichannel.secondary0.channelmask");
            this.NodeLookup.put(NodeMultichannelSecondary0Status.class, "netRemote.multichannel.secondary0.status");
            this.NodeLookup.put(NodeAvsMetadata.class, "netRemote.avs.metadata");
            this.NodeLookup.put(NodeAvsHastoken.class, "netRemote.avs.hastoken");
            this.NodeLookup.put(NodeAvsToken.class, "netRemote.avs.token");
			/* End of auto-generated code. */

            int sizeOfNodeMap = NodeLookup.size();
			/* Derive a reverse lookup from String to Class. */
			this.ClassLookup = new HashMap<>(sizeOfNodeMap);

			for (String name : this.NodeLookup.values()) {
				/* Build up the Class name that we would expect for this name. */
				String nodeName = "Node";
		        Boolean upcaseNextChar = true;
		        
		        for(int loop = "NetRemote.".length(); loop < name.length(); loop += 1) {
		        	char c = name.charAt(loop);
		        	
		        	if (c == '.') {
		        		upcaseNextChar = true;
		        	} else {
		                if (upcaseNextChar) {
		                    nodeName += Character.toUpperCase(c);
		                } else {
		                    nodeName += c;
		                }                    

		                upcaseNextChar = false;
		        	}
		        }
				
		        Class result = null;
		        try {
					result = Class.forName(NodeDefs.NodeNamespace + nodeName);
				} catch (ClassNotFoundException e) {
				}
		        
		        if (result != null) {
		        	this.ClassLookup.put(name, result);
		        	this.ClassLookup.put(name.toLowerCase(), result);
		        }
			}

			/* Add base node definitions. */
			Map<Class, String> toBeAdded = new HashMap<>(this.NodeLookup.size());
			for (Class nodeClass : this.NodeLookup.keySet()) {
				Class baseClass = nodeClass.getSuperclass();
				toBeAdded.put(baseClass, this.NodeLookup.get(nodeClass));
			}
			this.NodeLookup.putAll(toBeAdded);
			
			/* Build a lookup from address to class. */
			//this.NodeAddressLookup = new HashMap<>(350);
			
			/* Start of auto-generated code. */
//            this.NodeAddressLookup.put(0x100c0100L, NodeBluetoothDiscoverableState.class);
//            this.NodeAddressLookup.put(0x100c0200L, NodeBluetoothConnectedDevices.class);
//            this.NodeAddressLookup.put(0x100c0300L, NodeBluetoothConnectedDevicesListVersion.class);
//            this.NodeAddressLookup.put(0x10101010L, NodeSysInfoFriendlyName.class);
//            this.NodeAddressLookup.put(0x10101020L, NodeSysInfoVersion.class);
//            this.NodeAddressLookup.put(0x10101030L, NodeSysInfoRadioId.class);
//            this.NodeAddressLookup.put(0x10101040L, NodeSysInfoRadioPin.class);


			/* End of auto-generated code. */
			
			/* Build reverse lookup. */
//			this.NodeAddressReverseLookup = new HashMap<>(this.NodeAddressLookup.size());
//			for (Long key : this.NodeAddressLookup.keySet()) {
//				this.NodeAddressReverseLookup.put(this.NodeAddressLookup.get(key), key);
//			}
			
			/* Constants for parsing NetRemote serial protocol. */
			FSStatusCodeLookup = new HashMap<>(10);
			FSStatusCodeLookup.put(0x00L, FSStatusCode.FS_OK);
			FSStatusCodeLookup.put(0x85L, FSStatusCode.FS_FAIL);
			FSStatusCodeLookup.put(0x01L, FSStatusCode.FS_LIST_END);
			FSStatusCodeLookup.put(0x80L, FSStatusCode.FS_PACKET_BAD);
			FSStatusCodeLookup.put(0x81L, FSStatusCode.FS_NODE_DOES_NOT_EXIST);
			FSStatusCodeLookup.put(0x82L, FSStatusCode.FS_ITEM_DOES_NOT_EXIST);
			FSStatusCodeLookup.put(0x83L, FSStatusCode.FS_NODE_BLOCKED);

            IgnoredNodesInCaseOfNodeDoesNotExist = new HashSet<>(30);
            IgnoredNodesInCaseOfNodeDoesNotExist.add(NodeMultiroomGroupName.class);
            IgnoredNodesInCaseOfNodeDoesNotExist.add(NodeMultiroomDeviceServerStatus.class);
		}
	}
	
	/**
	 * Is this type a Node or Base Type, ie. does it start Node... or Base...?
	 * 
	 * @param type		The type to be tested
	 * @return			True if it is a Base type
	 */
	public static boolean IsBaseType(Class type) {
		/* Check it's actually a node type. */
		if (!(NodeInfo.class.isAssignableFrom(type))) {
			return false;
		}
		
		return type.getSimpleName().startsWith("Base");
	}
	
	/**
	 * Get the textual name of the node.
	 * 
	 * @param nodeType		The type of node
	 * @return				The textual name
	 */
	public String GetNodeName(Class nodeType) {
		this.InitAllNodes();

        return this.NodeLookup.get(nodeType);
	}
	
	/**
	 * Get the class/type of the node specified by the given textual name.
	 * 
	 * @param name		The textual name of the node
	 * @return			The type of the node
	 */
	public Class GetNodeClass(String name) {
		this.InitAllNodes();

        return this.ClassLookup.get(name);
	}

	/**
	 * Convert some NetRemote XML into a type-safe class.
	 * 
	 * @param nodeType		The class/type that the XML represents
	 * @param value			The XML
	 * @return				An instance of the class/type initialised from the XML
	 */
	public static NodeInfo GetNodeFromXml(Class nodeType, Node value) {
		/* Now try to parse the value and turn it in to a Java object. */
		Object valueAsObject = NodeDefs.ParseValue(value);
		
		/* Now see if we can find a constructor which takes the type of the object. */
		try {
			/* Find the constructor. */
			Class[] prototype;
			if (Node.class.isAssignableFrom(valueAsObject.getClass())) {
				prototype = new Class[] {Node.class};
			} else {
				prototype = new Class[] {valueAsObject.getClass()};
			}
			Constructor con = nodeType.getConstructor(prototype);
			
			/* Create an instance. */
			Object[] args = new Object[] {valueAsObject};
			return (NodeInfo)(con.newInstance(args));
		} catch (IllegalArgumentException e) {
		} catch (SecurityException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		} catch (NoSuchMethodException e) {
			/* There wasn't a constructor able to take this kind of message. */
		}
		
		return null;
	}
		
	static Object ParseValue(Node value) {
		/* Expect to be passed something like:
		 		<value><u32>12</u32></value>
		   or sometime without the value, so just:
		 		<u32>12</u32>		 		
		 */
		
		/* See if it has the 'value' wrapper. */
		Node toBeParsed = null;
		if (value.getNodeName().equals("value")) {
			Node firstChild = NodeDefs.GetFirstChildOfInterest(value);
			if (firstChild != null) {
				toBeParsed = firstChild;
			} else {
				toBeParsed = null;
			}
		} else {
			toBeParsed = value;
		}
		
		if (toBeParsed != null) {
			/* Get the data type, eg. 'u32'. */
			String dataType = toBeParsed.getNodeName();
			
			/* The following block of code gets the string value of the node. We used to be able to just go
			   toBeParsed.getFirstChild().getNodeValue() but that goes wrong on early versions of Android
			   when the data contains a quote ("). */
			String dataContent = null;
			for (int loop = 0; loop < toBeParsed.getChildNodes().getLength(); loop += 1) {
				Node child = toBeParsed.getChildNodes().item(loop);
				if (dataContent == null) {
					dataContent = "";
				}
				String childValue = child.getNodeValue();
				if (childValue == null) { break; }
				dataContent += childValue;
			}
			
			/* See if it's a string (c8_array) because they sometimes are empty (the empty string) and in this case the node has no children. */
			if (dataType.equals("c8_array") || dataType.equals("array")) {
				if (dataContent == null) {
					return "";
				} else {
					return dataContent;
				}
			} else {
				/* Get the value as a string, eg. "12". */
				if (dataContent != null) {
					/* Now parse according to data type. */
					if (dataContent != null) {
						/* Is it a U8, U16, U32, S8, S16, or S32? */
						if (dataType.equals("u8") || dataType.equals("u16") || dataType.equals("u32") || dataType.equals("s8") || dataType.equals("s16") || dataType.equals("s32")) {
							try {
								return Long.parseLong(dataContent);
							} catch (NumberFormatException e) {}
						}
					}
				}
			}
		}
		
		/* Couldn't be parsed, so just pass back the raw XML object. */
		return value;
	}
	
	/**
	 * This exception is raised when a node cannot be parsed.
	 */
	public static class NodeParseErrorException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3640710162959495785L;
	}
	
	/**
	 * Determine whether or not a class/type represents a list node.
	 * 
	 * @param nodeType		The class/type to be examined
	 * @return				True is the class/type represents a list node
	 */
	public static Boolean IsListNode(Class nodeType) {
		return com.frontier_silicon.NetRemoteLib.Node.NodeList.class.isAssignableFrom(nodeType);
	}
	
	static Node GetFirstChildOfInterest(Node node) {
		org.w3c.dom.NodeList children = node.getChildNodes();
		for (int loop = 0; loop < children.getLength(); loop += 1) {
			Node child = children.item(loop);
			String name = child.getNodeName();
			if ((name != null) && (!name.equals("#text"))) {
				return child;
			}
		}
		
		return null;
	}
	
	/**
	 * Convert from a Node... node type to its corresponding Base... type.
	 * 
	 * @param node		The Node... node
	 * @return			The Base... node
	 */
	public NodeInfo ConvertNodeToBase(NodeInfo node) {
		NodeInfo result = null;
		
		/* First determine the base type. */
		Class baseType = NodeDefs.Instance().GetBaseTypeFromNodeType(node.getClass());
		NetRemote.assertTrue(baseType != null);
		
		/* First find the type of its value. */
		try {
			if (node.getClass().getMethod("getValue") != null) {
				Method method = node.getClass().getMethod("getValue");
				Object value = method.invoke(node);
				if (value != null) {
					Class valueType = value.getClass();
					
					/* We've got the type, so find a constructor for the base node that takes this type. */
					Class[] prototype = new Class[] {valueType};
					Constructor con = baseType.getConstructor(prototype);
					
					/* Create an instance. */
					Object[] args = new Object[] {value};
					result = (NodeInfo)(con.newInstance(args));	
				}
			}
		} catch (SecurityException e) {
		} catch (IllegalArgumentException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		} catch (InstantiationException e) {
		}
		
		return result;
	}
	
	/**
	 * Convert from a Base... node type to its corresponding Node... type.
	 * 
	 * @param node		The Base... node
	 * @return			The Node... node
	 */
	public NodeInfo ConvertBaseToNode(NodeInfo node) {
		NodeInfo result = null;
		
		/* First determine the node type. */
		Class nodeType = NodeDefs.Instance().GetNodeTypeFromBaseType(node.getClass());
		NetRemote.assertTrue(nodeType != null);
		
		/* First find the type of its value. */
		try {
			if (node.getClass().getMethod("getValue") != null) {
				Method method = node.getClass().getMethod("getValue");
				Object value = method.invoke(node);
				if (value != null) {
					Class valueType = value.getClass();
					
					/* We've got the type, so find a constructor for the base node that takes this type. */
					Class[] prototype = new Class[] {valueType};
					Constructor con = nodeType.getConstructor(prototype);
					
					/* Create an instance. */
					Object[] args = new Object[] {value};
					result = (NodeInfo)(con.newInstance(args));	
				}
			}
		} catch (SecurityException e) {
		} catch (IllegalArgumentException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		} catch (InstantiationException e) {
		}
		
		return result;		
	}
	
	/**
	 * Get the Node... type from its corresponding Base... type.
	 * 
	 * @param nodeType		The Base... type
	 * @return				The corresponding Node... type
	 */
	public Class GetNodeTypeFromBaseType(Class nodeType) {
		/* Check it's actually a node type. */
		if (!(NodeInfo.class.isAssignableFrom(nodeType))) {
			return null;
		}
		
		/* See if it's a base node. */
		Class result = null;
		String name = nodeType.getSimpleName();
		if (name.startsWith("Base")) {
			/* Get the name of the node type. */
			String nodeName = ("Node" + name.substring(4));
			
			/* Return the class for node type. */
			try {
				result = Class.forName(NodeDefs.NodeNamespace + nodeName);
			} catch (ClassNotFoundException e) {
			}
		} else if (name.startsWith("Node")) {
			/* It's already the correct type. */
			result = nodeType;
		}
		
		return result;
	}
	
	/**
	 * Get the BAse... type from its corresponding Node... type.
	 * 
	 * @param nodeType		The Node... type
	 * @return				The corresponding Base... type
	 */
	public Class GetBaseTypeFromNodeType(Class nodeType) {
		/* Check it's actually a node type. */
		if (!(NodeInfo.class.isAssignableFrom(nodeType))) {
			return null;
		}
		
		/* See if it's a node node. */
		Class result = null;
		String name = nodeType.getSimpleName();
		if (name.startsWith("Node")) {
			/* Get the name of the node type. */
			String nodeName = ("Base" + name.substring(4));
			
			/* Return the class for node type. */
			try {
				result = Class.forName(NodeDefs.NodeNamespace + nodeName);
			} catch (ClassNotFoundException e) {
			}
		} else if (name.startsWith("Base")) {
			/* It's already the correct type. */
			result = nodeType;
		}
		
		return result;
	}
	
	/**
	 * Get the prototype of a node.
	 * 
	 * @param nodeType		The type of node for which the prototype is required
	 * @return				The prototype or null if no such node
	 */
	public NodePrototype GetPrototype(Class nodeType) {
		try {
			/* Create a new node of the specified type, but make sure it is a base type as they are the only ones with a suitable constructor. */
			Class base = this.GetBaseTypeFromNodeType(nodeType);
			if (base != null) {
				NodeInfo node = (NodeInfo)(base.newInstance());
			
				/* Return it's prototype. */
				return node.getPrototype();
			}
		} catch (IllegalAccessException e1) {
		} catch (InstantiationException e1) {
		}
		
		return null;
	}

	/**
	 * Determine whether or not a type of node is notifying.
	 * 
	 * @param nodeType		The type of node which is to be interrogated
	 * @return				True if it is notifying
	 */
	public boolean IsNotifying(Class nodeType) {
		try {
			/* Create a new node of the specified type, but make sure it is a base type as they are the only ones with a suitable constructor. */
			Class base = this.GetBaseTypeFromNodeType(nodeType);
			if (base != null) {
				NodeInfo node = (NodeInfo)(base.newInstance());
			
				/* Return whether it notifies. */
				return node.IsNotifying();
			}
		} catch (IllegalAccessException e1) {
		} catch (InstantiationException e1) {
		}
		
		return false;
	}

	/**
	 * Get an FSStatusCode value from its numeric representation.
	 * 
	 * @param value			The numeric value
	 * @return				The corresponding FSStatusCode value
	 */
	public FSStatusCode GetFSStatus(Long value) {
		return FSStatusCodeLookup.get(value);
	}

    /**
     * Some nodes can return FS_NODE_DOES_NOT_EXIST but in other situations they will return valid data.
     *
     * @return  boolean that tells if the node should not be treated as a permanent missing node
     */
	public boolean NodeThatNotExistShouldBeIgnored(Class nodeType) {
        return IgnoredNodesInCaseOfNodeDoesNotExist.contains(nodeType);
    }
}
