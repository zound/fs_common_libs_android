package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.serialNumber node.
 * 
 * Node to return the serial number
 */
public class NodeSysInfoSerialNumber extends BaseSysInfoSerialNumber {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoSerialNumber(String value) {
		super(value);
	}
}
