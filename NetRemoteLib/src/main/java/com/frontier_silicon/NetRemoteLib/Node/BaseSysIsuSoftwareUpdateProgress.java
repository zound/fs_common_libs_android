package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.isu.softwareUpdateProgress node.
 *
 * netRemote node to monitor the progress during firmware update.             The
 * node is returning an integer value between 0 and 100, according to the updating
 * state. When not performing a firmware update it returns 0.
 */
public class BaseSysIsuSoftwareUpdateProgress extends NodeU8 implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.isu.softwareUpdateProgress"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10108500; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return true; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.U8));
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public BaseSysIsuSoftwareUpdateProgress(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysIsuSoftwareUpdateProgress() {
		super();
	}
}
