package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.net.wlan.performWPS node.
 *
 * Connect the device by PIN/PBC based WPS method
 */
public class BaseSysNetWlanPerformWPS extends NodeE8<BaseSysNetWlanPerformWPS.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.net.wlan.performWPS"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x1010620e; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return true; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseSysNetWlanPerformWPS(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysNetWlanPerformWPS() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseSysNetWlanPerformWPS(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value WPS_IDLE.
         */
        WPS_IDLE, 
        /**
         * Enumeration value WPS_PBC.
         */
        WPS_PBC, 
        /**
         * Enumeration value WPS_PIN.
         */
        WPS_PIN, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case WPS_IDLE: return (long)0;
			case WPS_PBC: return (long)1;
			case WPS_PIN: return (long)2;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.WPS_IDLE;
			case 1: return Ord.WPS_PBC;
			case 2: return Ord.WPS_PIN;
		}
		return null;
	}
}
