package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.bluetooth.discoverableState node.
 *
 * Bluetooth discoverable state
 */

public class NodeBluetoothDiscoverableState extends BaseBluetoothDiscoverableState {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeBluetoothDiscoverableState(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeBluetoothDiscoverableState(Ord value) {
		super(value);
	}
}
