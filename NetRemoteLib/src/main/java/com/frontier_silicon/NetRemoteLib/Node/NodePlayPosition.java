package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.position node.
 * 
 * Get or set position in current track, if known and appropriate
 */
public class NodePlayPosition extends BasePlayPosition {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayPosition(Long value) {
		super(value);
	}
}
