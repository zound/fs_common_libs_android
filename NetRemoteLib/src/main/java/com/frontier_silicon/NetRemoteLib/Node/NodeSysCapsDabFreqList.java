package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.caps.dabFreqList node.
 * 
 * Return frequencies for channels in a band
 */
public class NodeSysCapsDabFreqList extends BaseSysCapsDabFreqList {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysCapsDabFreqList(Node value) throws NodeParseErrorException {
		super(value);
	}
}
