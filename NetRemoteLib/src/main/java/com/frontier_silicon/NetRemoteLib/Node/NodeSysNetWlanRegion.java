package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.region node.
 *
 * Region of the wlan device
 */

public class NodeSysNetWlanRegion extends BaseSysNetWlanRegion {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanRegion(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanRegion(Ord value) {
		super(value);
	}
}
