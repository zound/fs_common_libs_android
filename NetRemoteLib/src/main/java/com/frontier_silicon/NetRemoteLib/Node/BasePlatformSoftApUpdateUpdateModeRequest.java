package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.platform.softApUpdate.updateModeRequest node.
 *
 * Request of the update mode
 */
public class BasePlatformSoftApUpdateUpdateModeRequest extends NodeE8<BasePlatformSoftApUpdateUpdateModeRequest.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.platform.softApUpdate.updateModeRequest"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10510200; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BasePlatformSoftApUpdateUpdateModeRequest(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BasePlatformSoftApUpdateUpdateModeRequest() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BasePlatformSoftApUpdateUpdateModeRequest(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value IDLE.
         */
        IDLE, 
        /**
         * Enumeration value START.
         */
        START, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case IDLE: return (long)0;
			case START: return (long)1;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.IDLE;
			case 1: return Ord.START;
		}
		return null;
	}
}
