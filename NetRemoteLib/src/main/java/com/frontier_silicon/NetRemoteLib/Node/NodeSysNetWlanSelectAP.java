package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.selectAP node.
 * 
 * Key (Obtained from scanList node)
 */
public class NodeSysNetWlanSelectAP extends BaseSysNetWlanSelectAP {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanSelectAP(Long value) {
		super(value);
	}
}
