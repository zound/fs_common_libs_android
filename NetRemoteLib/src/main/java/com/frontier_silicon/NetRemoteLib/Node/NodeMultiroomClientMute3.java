package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.client.mute3 node.
 *
 * Handle client 3 mute
 */

public class NodeMultiroomClientMute3 extends BaseMultiroomClientMute3 {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientMute3(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultiroomClientMute3(Ord value) {
		super(value);
	}
}
