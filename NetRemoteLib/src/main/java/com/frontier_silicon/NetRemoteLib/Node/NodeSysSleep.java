package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.sleep node.
 * 
 * Sleep timer duration in seconds.
 */
public class NodeSysSleep extends BaseSysSleep {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysSleep(Long value) {
		super(value);
	}
}
