package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.delete node.
 * 
 * Delete a preset entry from the netRemote.nav.presets list
 */
public class NodeNavPresetDelete extends BaseNavPresetDelete {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetDelete(Long value) {
		super(value);
	}
}
