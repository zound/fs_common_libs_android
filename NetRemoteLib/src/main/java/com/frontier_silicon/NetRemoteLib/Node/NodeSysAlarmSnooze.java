package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.alarm.snooze node.
 * 
 * Set the alarm snooze time in minutes (1~30 minutes) or cancel the alarm (0
 * minute).
 */
public class NodeSysAlarmSnooze extends BaseSysAlarmSnooze {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAlarmSnooze(Long value) {
		super(value);
	}
}
