package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setPassphrase node.
 * 
 * Set the passphrase/key (this must be set before selecting the SSID).
 * 
 * WPA - a null terminated passphrase between 8 and 63 characters excluding the
 * null.
 * 
 * WEP/WEP64/WEP128 - a 32 byte key represented by a null terminated string where
 * the hex value of each nibble is given by it's ASCII value.
 * 
 * The passphrase must be encrypted using RSA PKCS1 v1.5 (RSAES-PKCS1-v1_5). The
 * RSA public key can be obtained from the netRemote.sys.rsa nodes
 */
public class NodeSysNetWlanSetPassphrase extends BaseSysNetWlanSetPassphrase {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanSetPassphrase(String value) {
		super(value);
	}
}
