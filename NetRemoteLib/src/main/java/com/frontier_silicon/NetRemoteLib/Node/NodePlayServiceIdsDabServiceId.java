package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.serviceIds.dabServiceId node.
 * 
 * DAB service ID
 */
public class NodePlayServiceIdsDabServiceId extends BasePlayServiceIdsDabServiceId {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayServiceIdsDabServiceId(Long value) {
		super(value);
	}
}
