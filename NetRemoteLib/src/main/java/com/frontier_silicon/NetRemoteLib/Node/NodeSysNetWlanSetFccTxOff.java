package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setFccTxOff node.
 *
 * Sets the Tx Off (equivalent to setting channel 0 and performing Fcc Test)
 */

public class NodeSysNetWlanSetFccTxOff extends BaseSysNetWlanSetFccTxOff {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanSetFccTxOff(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetWlanSetFccTxOff(Ord value) {
		super(value);
	}
}
