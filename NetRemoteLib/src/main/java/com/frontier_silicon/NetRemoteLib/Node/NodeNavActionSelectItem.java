package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.action.selectItem node.
 * 
 * Select an item of type PLAYABLE_ITEM for playback.  The value written must be
 * the index of an item of type PLAYABLE_ITEM
 */
public class NodeNavActionSelectItem extends BaseNavActionSelectItem {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavActionSelectItem(Long value) {
		super(value);
	}
}
