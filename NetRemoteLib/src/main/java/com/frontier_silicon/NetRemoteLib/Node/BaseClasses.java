/**
 * 
 */
package com.frontier_silicon.NetRemoteLib.Node;

import java.util.ArrayList;
import java.util.List;

/*********************************************************************************
 * Base classes and interfaces that form the foundation of all node definitions. *
 *********************************************************************************/

/**
 * This is an internal helper class for all enumeration based types (eg. E8, B32).
 * 
 * @param <T>		Enumeration type for the node
 */
@SuppressWarnings("rawtypes")
abstract class NodeToken<T extends Enum> {
	protected long ValueWidth = 0;
	
	/**
	 * Constructs a NodeToken with the given width.
	 * 
	 * @param width		The width of the container (eg. 8 for a E8, or 32 for a B32).
	 */
	protected NodeToken(long width) {
		this.ValueWidth = width;
	}
	
	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	NodeToken() {
	}
	
	/**
	 * Helper function to convert from an enum to a number.
	 * 
	 * @param ord		The enumeration value
	 * @return			The numeric value
	 */
	protected abstract Long GetValueFromEnum(T ord);

	/**
	 * Helper function to convert from a number to an enum.
	 * 
	 * @param value		The numeric value
	 * @return			The enumeration value
	 */
	protected abstract T GetEnumFromValue(Long value);
	
	/**
	 * Internal helper function to get a list of enum values corresponding to a numeric value.
	 * 
	 * @param values			An array of possible enum values (from Type.values())
	 * @param numericValue		The value to be converted
	 * @return					A list of enum values
	 */
	protected List<T> getEnumListFromNumericValue(T[] values, Long numericValue)
	{
		List<T> result = new ArrayList<T>();
		
		/* Clear all bits that we know about. */
		for (T bit : values) {
			Long bitValue = this.GetValueFromEnum(bit);
			
			/* Is it set? */
			if ((numericValue & bitValue) != 0) {
				result.add(bit);
				
				/* Optimisation to see if we've finished early. */
				numericValue &= ~bitValue;
				if (numericValue == 0) {
					break;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Internal helper function to determine if a bit field has any bits set that are unknown to the enumeration.
	 * 
	 * @param values			An array of possible enum values (from Type.values())
	 * @param numericValue		The value of the bit field to be checked
	 * @return					True if any unknown bits are set
	 */
	protected boolean areAnyUnknownBitsSet(T[] values, long numericValue)
	{
		/* Clear all bits that we know about. */
		for (T value : values) {
			numericValue &= ~(this.GetValueFromEnum(value));
		}
				
		return (numericValue != 0);
	}
}
