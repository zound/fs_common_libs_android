package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.clock.mode node.
 *
 * Set MMI time display mode 12 or 24 hour. Does not change the format of time
 * returned by localTime.
 */

public class NodeSysClockMode extends BaseSysClockMode {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysClockMode(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysClockMode(Ord value) {
		super(value);
	}
}
