package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.description node.
 * 
 * Text metadata (up to 4kB)
 */
public class NodeNavDescription extends BaseNavDescription {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
    public NodeNavDescription(String value) {
        super(value);
    }
}
