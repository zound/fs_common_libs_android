package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.avs.hastoken node.
 *
 * Check wether the system has an avs refresh token
 */

public class NodeAvsHastoken extends BaseAvsHastoken {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeAvsHastoken(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeAvsHastoken(Ord value) {
		super(value);
	}
}
