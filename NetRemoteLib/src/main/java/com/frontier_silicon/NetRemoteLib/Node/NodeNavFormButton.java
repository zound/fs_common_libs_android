package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.nav.form.button node.
 * 
 * List of buttons used by forms
 */
public class NodeNavFormButton extends BaseNavFormButton {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeNavFormButton(Node value) throws NodeParseErrorException {
		super(value);
	}
}
