package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.spotify.status node.
 * 
 * Read only node that returns the current status of the Spotify connection.
 */
public class NodeSpotifyStatus extends BaseSpotifyStatus {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSpotifyStatus(Long value) {
		super(value);
	}
}
