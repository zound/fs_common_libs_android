package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.secondary0.status node.
 *
 * Secondary's status
 */

public class NodeMultichannelSecondary0Status extends BaseMultichannelSecondary0Status {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelSecondary0Status(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelSecondary0Status(Ord value) {
		super(value);
	}
}
