package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.serviceIds.fmRdsPi node.
 * 
 * FM RDS PI code, when available. Reads as 0 when not in FM mode, or not RDS
 * information received.  Notifies whenever it changes.
 */
public class NodePlayServiceIdsFmRdsPi extends BasePlayServiceIdsFmRdsPi {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayServiceIdsFmRdsPi(Long value) {
		super(value);
	}
}
