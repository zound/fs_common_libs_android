package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.caps.fmFreqRange.lower node.
 * 
 * lower frequency (in KHz) range upto which the FM tuner support
 */
public class NodeSysCapsFmFreqRangeLower extends BaseSysCapsFmFreqRangeLower {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysCapsFmFreqRangeLower(Long value) {
		super(value);
	}
}
