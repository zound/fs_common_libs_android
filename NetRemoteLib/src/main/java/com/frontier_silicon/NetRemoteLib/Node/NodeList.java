package com.frontier_silicon.NetRemoteLib.Node;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;

/**
 * This is the base class for a List Node.
 * 
 */
public abstract class NodeList {
	protected List<NodeListItem> Items = new ArrayList<NodeListItem>();
	protected boolean isEndOfList = false;
	
	/**
	 * Constructs a node list from the given XML.
	 * 
	 * @param xml		The XML specifying all entries of the node list
	 */
	@SuppressWarnings("rawtypes")
	protected NodeList(Class listItemType, Node value) throws NodeDefs.NodeParseErrorException {
		/* Expect to get something like:
		   		<item key="0">
		   			<field name="id"><c8_array>DAB</c8_array></field>
		   			<field name="selectable"><u8>1</u8></field>
		   			<field name="label"><c8_array>DAB</c8_array></field>
		   		</item>
		*/
		
		/* Iterate over all keys. */
		org.w3c.dom.NodeList items = value.getChildNodes();
		for (int loop = 0; loop < items.getLength(); loop += 1) { 
			Node item = items.item(loop);
			String name = item.getNodeName(); 
			if (name.equals("item")) {
				try {
					Class[] prototype = new Class[] {Node.class};
					Constructor con = listItemType.getConstructor(prototype);
					Object[] args = new Object[] {item};
					this.Items.add((NodeListItem)(con.newInstance(args)));
					continue;
				} catch (SecurityException e) {
				} catch (NoSuchMethodException e) {
				} catch (IllegalArgumentException e) {
				} catch (InstantiationException e) {
				} catch (IllegalAccessException e) {
				} catch (InvocationTargetException e) {
				} catch (Exception e) {
				}
				
				throw new NodeDefs.NodeParseErrorException();
			} else if (name.equals("listend")) {
				this.isEndOfList = true;
			}
		}
	}
	
	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	NodeList() {
	}
	
	/**
	 * Gets the number of items in the list.
	 * 
	 * @return The number of items
	 */
	public int Size() {
		return this.Items.size();
	}
	
	/**
	 * Returns whether or not the end of the list has been reached.
	 * 
	 * @return		True if end of list has been reached
	 */
	public boolean IsEndOfList() {
		return this.isEndOfList;
	}
	
	/**
	 * Not typically used by application code, this method clears the end of list flag such that future
	 * calls to {@link NodeList#isEndOfList} will return false.
	 */
	@SuppressWarnings("javadoc")
	public void ClearEndOfList() {
		this.isEndOfList = false;
	}
	
	/**
	 * Get the last item in the list.
	 * 
	 * @return		The last item
	 */
	public abstract Object getLastItem();
	
	/**
	 * Test the equality of two nodes.
	 * 
	 * @param otherNode		The other node to test
	 * @return				True if the value of the two nodes are equal
	 */	
	public boolean equals(NodeInfo otherNode) {
		/* This is quite a complex / meaningless test. So for now always return false. */
		return false;
	}

	/**
	 * Get the current list items for this list node
	 *
	 * @return the list items
	 */
	public List<NodeListItem> getItems() {
		return this.Items;
	}
}
