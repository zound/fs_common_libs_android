package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.secondary0.channelmask node.
 *
 * Secondary's channel mask
 */

public class NodeMultichannelSecondary0Channelmask extends BaseMultichannelSecondary0Channelmask {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelSecondary0Channelmask(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelSecondary0Channelmask(Ord value) {
		super(value);
	}
}
