package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.system.unpair node.
 *
 * Destroy the current system (primary)/ Reset the device system membership
 * (secondary)
 */

public class NodeMultichannelSystemUnpair extends BaseMultichannelSystemUnpair {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelSystemUnpair(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeMultichannelSystemUnpair(Ord value) {
		super(value);
	}
}
