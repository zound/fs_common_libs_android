package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.amazonMpLoginComplete node.
 *
 * The response of the Amazon login procedure
 */

public class NodeNavAmazonMpLoginComplete extends BaseNavAmazonMpLoginComplete {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavAmazonMpLoginComplete(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeNavAmazonMpLoginComplete(Ord value) {
		super(value);
	}
}
