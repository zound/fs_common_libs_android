package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multichannel.system.name node.
 * 
 * The name of the system
 */
public class NodeMultichannelSystemName extends BaseMultichannelSystemName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultichannelSystemName(String value) {
		super(value);
	}
}
