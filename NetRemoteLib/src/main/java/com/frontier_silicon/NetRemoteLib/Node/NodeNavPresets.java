package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.nav.presets node.
 * 
 * List of available presets (if appropriate) for current mode.
 * 
 * List is empty if not appropriate. The list also incorporates the preset source
 * and a generated hash for each entry (empty if not applicable).
 */
public class NodeNavPresets extends BaseNavPresets {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeNavPresets(Node value) throws NodeParseErrorException {
		super(value);
	}
}
