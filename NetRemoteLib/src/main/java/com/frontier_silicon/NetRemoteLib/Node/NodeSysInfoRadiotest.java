package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.radiotest node.
 * 
 * JVI test code
 */
public class NodeSysInfoRadiotest extends BaseSysInfoRadiotest {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoRadiotest(String value) {
		super(value);
	}
}
