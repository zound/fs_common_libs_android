package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.cfg.irAutoPlayFlag node.
 *
 * Flag that indicates if switching to IR mode should autoplay or not.
 */

public class NodeSysCfgIrAutoPlayFlag extends BaseSysCfgIrAutoPlayFlag {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysCfgIrAutoPlayFlag(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysCfgIrAutoPlayFlag(Ord value) {
		super(value);
	}
}
