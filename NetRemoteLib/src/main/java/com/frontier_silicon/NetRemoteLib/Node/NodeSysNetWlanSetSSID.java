package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setSSID node.
 * 
 * Manual selection of the AP with SSID (Set this after enc type and auth type is
 * set)
 */
public class NodeSysNetWlanSetSSID extends BaseSysNetWlanSetSSID {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanSetSSID(String value) {
		super(value);
	}
}
