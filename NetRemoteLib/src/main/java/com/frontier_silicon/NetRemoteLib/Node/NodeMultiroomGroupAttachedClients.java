package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.multiroom.group.attachedClients node.
 * 
 * The number of clients connected to server
 */
public class NodeMultiroomGroupAttachedClients extends BaseMultiroomGroupAttachedClients {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMultiroomGroupAttachedClients(Long value) {
		super(value);
	}
}
