package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.radioPin node.
 * 
 * Radio PIN 4 digit code
 */
public class NodeSysInfoRadioPin extends BaseSysInfoRadioPin {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoRadioPin(String value) {
		super(value);
	}
}
