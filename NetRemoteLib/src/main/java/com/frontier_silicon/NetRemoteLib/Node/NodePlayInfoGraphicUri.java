package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.info.graphicUri node.
 * 
 * A URI from which a graphic representing the current audio can be downloaded -
 * e.g. station logo, or album artwork
 */
public class NodePlayInfoGraphicUri extends BasePlayInfoGraphicUri {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayInfoGraphicUri(String value) {
		super(value);
	}
}
