package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.control node.
 *
 * Control playback of current audio, if appropriate
 */

public class NodePlayControl extends BasePlayControl {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayControl(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodePlayControl(Ord value) {
		super(value);
	}
}
