package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.avs.metadata node.
 * 
 * Get the device's metadata
 */
public class NodeAvsMetadata extends BaseAvsMetadata {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeAvsMetadata(String value) {
		super(value);
	}
}
