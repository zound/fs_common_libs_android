package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.play.caps node.
 * 
 * Indicates the control that that remote can have over the currently playing
 * audio.  This node returns a bit-field with the following meanings, where the
 * presence of a bit means that functionality is supported for the current audio:
 * 
 * !======================================== ! Bit ! Meaning
 * 
 * ! 0 ! Pause
 * 
 * ! 1 ! Stop
 * 
 * ! 2 ! Skip next track
 * 
 * ! 3 ! Skip previous track
 * 
 * ! 4 ! Fast-forward
 * 
 * ! 5 ! Rewind
 * 
 * ! 6 ! Shuffle
 * 
 * ! 7 ! Repeat
 * 
 * ! 8 ! Seek to an arbitrary position
 * 
 * ! 9 ! Apply positive / negative feedback
 * 
 * ! 10 ! Scrobbling supported
 * 
 * ! 11 ! Add preset !========================================
 */
public class NodePlayCaps extends BasePlayCaps {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodePlayCaps(Long value) {
		super(value);
	}
	
	@SuppressWarnings("javadoc")
	/**
	 * Types of operations that can be conveyed.
	 *
	 */
	public enum Operation {
		Pause,
		Stop,
		SkipNext,
		SkipPrevious,
		FastForward,
		Rewind,
		Shuffle,
		Repeat,
		Seek,
		ApplyFeedback,
		Scrobbling,
		AddPreset
	}
	
	/**
	 * Test if certain operations are supported in the current state.
	 * 
	 * @param op		The operation to be tested
	 * @return			True if supported 
	 */
	public boolean DoesSupport(Operation op) {
		return ((this.getValue() & (1 << op.ordinal())) != 0);
	}
}
