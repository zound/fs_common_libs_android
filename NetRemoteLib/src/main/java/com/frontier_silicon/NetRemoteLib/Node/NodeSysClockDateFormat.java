package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.clock.dateFormat node.
 *
 * Set MMI date format: DD-MM-YYYY or MM-DD-YYYY. Does not change the format of
 * date returned by localDate.
 */

public class NodeSysClockDateFormat extends BaseSysClockDateFormat {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysClockDateFormat(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysClockDateFormat(Ord value) {
		super(value);
	}
}
