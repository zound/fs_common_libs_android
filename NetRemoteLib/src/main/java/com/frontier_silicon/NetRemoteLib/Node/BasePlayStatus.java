package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.play.status node.
 *
 * Current playback status.  Values mean:
 * 
 * !======================================== ! Value ! Meaning
 * 
 * ! IDLE ! no playback is in progress
 * 
 * ! BUFFERING ! a track is starting, but no audio has yet played for the current
 * track
 * 
 * ! PLAYING ! a track is being played - audio is being played out
 * 
 * ! PAUSED ! the current track is paused.
 * 
 * ! REBUFFERING ! a track as started, but the audio has underrun so it is
 * rebuffering
 * 
 * ! ERROR ! an error has occurred playing the track.  The play.errorStr node is
 * updated with a user displayable string
 * 
 * ! STOPPED ! the playback has stopped. !========================================
 * 
 * As a special case, when in iPod mode without and iPod docked, the status will
 * become ERROR
 */
public class BasePlayStatus extends NodeE8<BasePlayStatus.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.play.status"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10301000; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return true; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return true; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return true; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BasePlayStatus(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BasePlayStatus() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BasePlayStatus(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value IDLE.
         */
        IDLE, 
        /**
         * Enumeration value BUFFERING.
         */
        BUFFERING, 
        /**
         * Enumeration value PLAYING.
         */
        PLAYING, 
        /**
         * Enumeration value PAUSED.
         */
        PAUSED, 
        /**
         * Enumeration value REBUFFERING.
         */
        REBUFFERING, 
        /**
         * Enumeration value ERROR.
         */
        ERROR, 
        /**
         * Enumeration value STOPPED.
         */
        STOPPED,
		/**
		 * Enumeration value ERROR_POPUP.
		 */
		ERROR_POPUP,

	}

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case IDLE: return (long)0;
			case BUFFERING: return (long)1;
			case PLAYING: return (long)2;
			case PAUSED: return (long)3;
			case REBUFFERING: return (long)4;
			case ERROR: return (long)5;
			case STOPPED: return (long)6;
			case ERROR_POPUP: return (long)7;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.IDLE;
			case 1: return Ord.BUFFERING;
			case 2: return Ord.PLAYING;
			case 3: return Ord.PAUSED;
			case 4: return Ord.REBUFFERING;
			case 5: return Ord.ERROR;
			case 6: return Ord.STOPPED;
			case 7: return Ord.ERROR_POPUP;
		}
		return null;
	}
}
