package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.ipConfig.dhcp node.
 *
 * Connection state DHCP Yes/No
 */

public class NodeSysNetIpConfigDhcp extends BaseSysNetIpConfigDhcp {
	/**
	 * Construct a new node using an integer value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetIpConfigDhcp(Long value) {
		super(value);
	}
	
	/**
	 * Construct a new node using an enumeration value.
	 *
	 * @param value      The value of the node
	 */
	public NodeSysNetIpConfigDhcp(Ord value) {
		super(value);
	}
}
