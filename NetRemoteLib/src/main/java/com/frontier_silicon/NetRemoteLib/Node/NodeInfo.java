package com.frontier_silicon.NetRemoteLib.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a fundamental interface that all nodes implement to allow basic information to be obtained
 * about the node. 
 * 
 */
public interface NodeInfo {
	/**
	 * Gets the name of the node (as used by the HTTP access protocol).
	 * @return		The node name
	 */
	public String getName();
	
	/**
	 * Gets the address of the node (as used by the dock protocol).
	 * @return		The address of the node
	 */
	public long getAddress();
	
	/**
	 * Gets whether or not the node can be cached, or whether it is volatile and should be fetched each time.
	 * @return		True if the node can be cached
	 */
	public boolean IsCacheable();
	
	/**
	 * Gets whether or not the node will notify if there is a change in its value.
	 * @return		True if the node can notify
	 */
	public boolean IsNotifying();
	
	/**
	 * Gets whether or not the node is read-only.
	 * @return		True if read-only
	 */
	public boolean IsReadOnly();
	
	/**
	 * Gets the value of the node as a string. 
	 * @return		A nicely formatted string
	 */
	public String toString();
	
	/**
	 * Test the equality of two nodes.
	 * 
	 * @param otherNode		The other node to test
	 * @return				True if the value of the two nodes are equal
	 */	
	public boolean equals(NodeInfo otherNode);
	
	/**
	 * Get the prototype of the node, ie. a description of the data elements within it.
	 * 
	 * @return				The prototype
	 */
	public NodePrototype getPrototype();
	
	/**
	 * This class defines the prototype of a node, ie. describes the data stored in a node.
	 */
	public class NodePrototype {
		/**
		 * This class defines the prototype for a single data element in the node.
		 */
		public static class Arg {
			@SuppressWarnings("javadoc")
			/**
			 * This enumeration defines all possible types of data.
			 */
			public enum ArgDataType {
				C8,
				E8,
				U8,
				U16,
				U32,
				S8,
				S16,
				S32,
			}

			private String Name = null;
			
			/**
			 * Get the name of the parameter.
			 * 
			 * @return		The name
			 */
			public String GetName() {
				return this.Name;
			}
			
			private ArgDataType Type = null;
			
			/**
			 * Get the type of the parameter.
			 * 
			 * @return		The type
			 */
			public ArgDataType GetType() {
				return this.Type;
			}
			
			private int Length = 0;
			
			/**
			 * Get the length of the parameter.
			 * 
			 * @return		The length
			 */
			public int GetLength() {
				return this.Length;
			}
			
			Arg(String name, ArgDataType type, int length) {
				this.Name = name;
				this.Type = type;
				this.Length = length;
			}
			
			Arg(ArgDataType type, int length) {
				this.Name = null;
				this.Type = type;
				this.Length = length;
			}
			
			Arg(ArgDataType type) {
				this.Name = null;
				this.Type = type;
				this.Length = 1;
			}
		}
		
		private boolean ListNode = false;
		
		/**
		 * Is this node a list node?
		 * 
		 * @return		True if it is a list node
		 */
		public boolean IsListNode() {
			return this.ListNode;
		}
		
		private List<Arg> Args = new ArrayList<Arg>();

		/**
		 * Create a prototype for a normal node (ie. not a list node).
		 * 
		 * @param arg		The parameter
		 */
		NodePrototype(Arg arg) {
			this.ListNode = false;
			this.Args.add(arg);
		}
		
		/**
		 * Create a prototype for a list node.
		 * 
		 * @param args		A list of parameters
		 */
		NodePrototype(List<Arg> args) {
			this.ListNode = true;
			this.Args = args;
		}

		/**
		 * Get the list of arguments.
		 * 
		 * @return		The list
		 */
		public List<Arg> getArgs() {
			return this.Args;
		}
	}
}
