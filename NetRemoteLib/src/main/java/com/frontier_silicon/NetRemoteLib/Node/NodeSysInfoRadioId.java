package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.info.radioId node.
 * 
 * radio ID of device
 */
public class NodeSysInfoRadioId extends BaseSysInfoRadioId {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysInfoRadioId(String value) {
		super(value);
	}
}
