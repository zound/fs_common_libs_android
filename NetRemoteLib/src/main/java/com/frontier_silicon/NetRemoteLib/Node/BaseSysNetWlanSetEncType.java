package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.sys.net.wlan.setEncType node.
 *
 * Set encryption type of the AP (used with net.wlan.setSSID)
 */
public class BaseSysNetWlanSetEncType extends NodeE8<BaseSysNetWlanSetEncType.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.sys.net.wlan.setEncType"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x1010620a; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseSysNetWlanSetEncType(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseSysNetWlanSetEncType() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseSysNetWlanSetEncType(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value NONE.
         */
        NONE, 
        /**
         * Enumeration value WEP.
         */
        WEP, 
        /**
         * Enumeration value TKIP.
         */
        TKIP, 
        /**
         * Enumeration value AES.
         */
        AES, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case NONE: return (long)0;
			case WEP: return (long)1;
			case TKIP: return (long)2;
			case AES: return (long)3;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.NONE;
			case 1: return Ord.WEP;
			case 2: return Ord.TKIP;
			case 3: return Ord.AES;
		}
		return null;
	}
}
