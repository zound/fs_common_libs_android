package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wired.macAddress node.
 * 
 * MAC address of the wired interface
 */
public class NodeSysNetWiredMacAddress extends BaseSysNetWiredMacAddress {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWiredMacAddress(String value) {
		super(value);
	}
}
