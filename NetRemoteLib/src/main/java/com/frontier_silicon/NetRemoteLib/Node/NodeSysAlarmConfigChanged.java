package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.alarm.configChanged node.
 * 
 * Alarm config changed. Any alarm configuration change will notify this node with
 * the alarm index instead of the corresponding changed node. Always read as -1.
 */
public class NodeSysAlarmConfigChanged extends BaseSysAlarmConfigChanged {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAlarmConfigChanged(Long value) {
		super(value);
	}
}
