package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.upload.artworkUrl node.
 * 
 * 
 */
public class NodeNavPresetUploadArtworkUrl extends BaseNavPresetUploadArtworkUrl {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetUploadArtworkUrl(String value) {
		super(value);
	}
}
