package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.mode node.
 * 
 * Operating mode of device.  When the mode is changed, the device will start
 * playing the last-listened station in the new mode, if there is an appropriate
 * station.
 * 
 * The value is an index into the netRemote.sys.caps.validModes list - indicating
 * the current mode, with the special case that the invalid key means that no mode
 * is currently selected.
 * 
 * Note that DMR mode cannot be entered by the remote, but the radio may enter it.
 */
public class NodeSysMode extends BaseSysMode {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysMode(Long value) {
		super(value);
	}
}
