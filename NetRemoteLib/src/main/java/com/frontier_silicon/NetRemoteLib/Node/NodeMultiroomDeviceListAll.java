package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.multiroom.device.listAll node.
 * 
 * List version
 */
public class NodeMultiroomDeviceListAll extends BaseMultiroomDeviceListAll {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeMultiroomDeviceListAll(Node value) throws NodeParseErrorException {
		super(value);
	}
}
