package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.upload.upload node.
 * 
 * 
 */
public class NodeNavPresetUploadUpload extends BaseNavPresetUploadUpload {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetUploadUpload(Long value) {
		super(value);
	}
}
