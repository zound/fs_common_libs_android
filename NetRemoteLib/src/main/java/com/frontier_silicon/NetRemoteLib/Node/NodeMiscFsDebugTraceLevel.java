package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.misc.fsDebug.traceLevel node.
 * 
 * FsDebug Trace Level
 */
public class NodeMiscFsDebugTraceLevel extends BaseMiscFsDebugTraceLevel {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeMiscFsDebugTraceLevel(Long value) {
		super(value);
	}
}
