package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.isu.version node.
 * 
 * Version of the update software (if the CHECK_FOR_UPDATE succeeds with
 * UPDATE_AVAILABLE)
 */
public class NodeSysIsuVersion extends BaseSysIsuVersion {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysIsuVersion(String value) {
		super(value);
	}
}
