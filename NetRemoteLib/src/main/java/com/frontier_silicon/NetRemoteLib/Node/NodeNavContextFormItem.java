package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.nav.context.form.item node.
 * 
 * List of items a form is created of
 */
public class NodeNavContextFormItem extends BaseNavContextFormItem {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeNavContextFormItem(Node value) throws NodeParseErrorException {
		super(value);
	}
}
