package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.preset.upload.name node.
 * 
 * 
 */
public class NodeNavPresetUploadName extends BaseNavPresetUploadName {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavPresetUploadName(String value) {
		super(value);
	}
}
