package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.nav.depth node.
 * 
 * Node used to inform about the state of the navigation depth for IR/MP browsing.
 */
public class NodeNavDepth extends BaseNavDepth {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeNavDepth(Long value) {
		super(value);
	}
}
