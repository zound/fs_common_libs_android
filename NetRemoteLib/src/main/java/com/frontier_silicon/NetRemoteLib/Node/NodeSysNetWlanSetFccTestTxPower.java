package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setFccTestTxPower node.
 * 
 * Set the Tx power for FCC test
 */
public class NodeSysNetWlanSetFccTestTxPower extends BaseSysNetWlanSetFccTestTxPower {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanSetFccTestTxPower(Long value) {
		super(value);
	}
}
