package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.alarm.snoozing node.
 * 
 * Current snoozing time in seconds.
 */
public class NodeSysAlarmSnoozing extends BaseSysAlarmSnoozing {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysAlarmSnoozing(Long value) {
		super(value);
	}
}
