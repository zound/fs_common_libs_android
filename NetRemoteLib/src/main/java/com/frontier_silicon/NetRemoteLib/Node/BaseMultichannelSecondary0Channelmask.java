package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the base class for the netRemote.multichannel.secondary0.channelmask node.
 *
 * Secondary's channel mask
 */
public class BaseMultichannelSecondary0Channelmask extends NodeE8<BaseMultichannelSecondary0Channelmask.Ord> implements NodeInfo {
        /**
         * Get the name of the node.
         *
         * @return     The name of the node.
         */
	public String getName() { return "netRemote.multichannel.secondary0.channelmask"; }
        /**
         * Get the address of the node which is used in the dock protocol.
         *
         * @return     The 32-bit address.
         */
	public long getAddress() { return 0x10d30100; }
        /**
         * Get whether or not the node can be cached.
         *
         * @return     True if the node can be cached.
         */
	public boolean IsCacheable() { return false; }
        /**
         * Get whether or not the node generates notifications.
         *
         * @return     True if the node generates notifications.
         */
	public boolean IsNotifying() { return false; }
        /**
         * Get whether or not the node is read-only.
         *
         * @return      True if the node is read-only.
         */
	public boolean IsReadOnly() { return false; }

	private final static NodePrototype Prototype = new NodePrototype(new NodePrototype.Arg(NodePrototype.Arg.ArgDataType.E8));
	
        /**
         * Get a description of the data elements within the node.
         *
         * @return      The node prototype
         */
	public NodePrototype getPrototype() {
		return Prototype;
	}

    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     */
	public BaseMultichannelSecondary0Channelmask(Long value) {
		super(value);
	}

	/**
	 * Construct a new node with no value. This is normally a no-no, but the constructor is only
	 * allowed to be used inside the package. It is needed so that an instance of a node can be
	 * created to examine the properties above.
	 */
	BaseMultichannelSecondary0Channelmask() {
		super();
	}

    /**
     * Construct a new node using an enumeration value.
     *
     * @param value      The value of the node
     */
	public BaseMultichannelSecondary0Channelmask(Ord value) {
		super(value);
	}

    /**
     * An enumeration expressing the possible values of this type of node.
     */
	public enum Ord { 
        /**
         * Enumeration value NONE.
         */
        NONE, 
        /**
         * Enumeration value LEFT.
         */
        LEFT, 
        /**
         * Enumeration value RIGHT.
         */
        RIGHT, 
        /**
         * Enumeration value STEREO.
         */
        STEREO, }

	protected Long GetValueFromEnum(Ord e) {
		switch (e) {
			case NONE: return (long)0;
			case LEFT: return (long)1;
			case RIGHT: return (long)2;
			case STEREO: return (long)3;
		}
		return null;
	}

	protected Ord GetEnumFromValue(Long value) {
		switch (value.intValue()) {
			case 0: return Ord.NONE;
			case 1: return Ord.LEFT;
			case 2: return Ord.RIGHT;
			case 3: return Ord.STEREO;
		}
		return null;
	}
}
