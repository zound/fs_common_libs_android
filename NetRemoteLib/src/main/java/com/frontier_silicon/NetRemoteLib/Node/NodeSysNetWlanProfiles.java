package com.frontier_silicon.NetRemoteLib.Node;

import org.w3c.dom.Node;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs.NodeParseErrorException;

/**
 * The definition for the netRemote.sys.net.wlan.profiles node.
 * 
 * List of saved profiles available
 */
public class NodeSysNetWlanProfiles extends BaseSysNetWlanProfiles {
    /**
     * Construct a new node using an integer value.
     *
     * @param value      The value of the node
     * @throws NodeParseErrorException 
     */
	public NodeSysNetWlanProfiles(Node value) throws NodeParseErrorException {
		super(value);
	}
}
