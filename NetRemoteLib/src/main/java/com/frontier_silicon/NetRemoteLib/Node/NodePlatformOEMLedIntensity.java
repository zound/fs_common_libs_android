package com.frontier_silicon.NetRemoteLib.Node;

/**
 * Created by cvladu on 15/06/2017.
 */

public class NodePlatformOEMLedIntensity extends BasePlatformOEMLedIntensity {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
    public NodePlatformOEMLedIntensity(Long value) {
        super(value);
    }
}
