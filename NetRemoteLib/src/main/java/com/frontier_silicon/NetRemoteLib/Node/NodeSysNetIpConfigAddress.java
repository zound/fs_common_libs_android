package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.ipConfig.address node.
 * 
 * IP address for the interface
 */
public class NodeSysNetIpConfigAddress extends BaseSysNetIpConfigAddress {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetIpConfigAddress(Long value) {
		super(value);
	}
}
