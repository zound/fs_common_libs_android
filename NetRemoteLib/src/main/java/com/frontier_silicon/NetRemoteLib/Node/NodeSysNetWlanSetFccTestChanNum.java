package com.frontier_silicon.NetRemoteLib.Node;

/**
 * The definition for the netRemote.sys.net.wlan.setFccTestChanNum node.
 * 
 * Set the channel number for FCC test
 */
public class NodeSysNetWlanSetFccTestChanNum extends BaseSysNetWlanSetFccTestChanNum {
    /**
     * Construct a new node using the specified value.
     *
     * @param value      The value of the node
     */
	public NodeSysNetWlanSetFccTestChanNum(Long value) {
		super(value);
	}
}
