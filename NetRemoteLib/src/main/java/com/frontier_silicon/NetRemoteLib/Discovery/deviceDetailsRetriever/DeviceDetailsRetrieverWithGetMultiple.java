package com.frontier_silicon.NetRemoteLib.Discovery.deviceDetailsRetriever;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.Node.BaseMultiroomDeviceTransportOptimisation;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultichannelSystemCompatibilityid;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomCapsProtocolVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceTransportOptimisation;
import com.frontier_silicon.NetRemoteLib.Node.NodePlatformOEMColorProduct;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoFriendlyName;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoNetRemoteVendorId;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanMacAddress;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodesCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeResponse;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttp;
import com.frontier_silicon.NetRemoteLib.utils.VendorIDsUtil;

import java.util.Map;

/**
 * Created by lsuhov on 10/11/2016.
 */

public class DeviceDetailsRetrieverWithGetMultiple {

    public static final Long ZoundAudsyncVersion = 128L;
    public static final Long ZoundStereoAudsyncVersion = 129L;

    DeviceRecord mDeviceRecord;
    private String[] mVendorIds;
    private String[] mFirmwareBlacklist;
    IDeviceDetailsListener mDeviceDetailsListener;
    RadioHttp mRadio;

    void retrieveInfo(DeviceRecord scanDeviceRecord, String[] vendorIds, String[] firmwareBlacklist,
                                          IDeviceDetailsListener deviceDetailsListener) {
        mDeviceRecord = scanDeviceRecord;
        mVendorIds = vendorIds;
        mFirmwareBlacklist = firmwareBlacklist;
        mDeviceDetailsListener = deviceDetailsListener;

        mRadio = new RadioHttp("GetMultiple Info Retriever " + scanDeviceRecord.fsApiLocation,
                scanDeviceRecord.ID, scanDeviceRecord.fsApiLocation);
        mRadio.setIsCheckingAvailability(false);
        mRadio.setIsAvailable(true);
        mRadio.setDDXMLLocation(scanDeviceRecord.deviceInfoLocation);

        makeRequestToSpeaker();
    }

    private void makeRequestToSpeaker() {
        Class[] nodesToFetch = new Class[] {NodeSysInfoFriendlyName.class, NodeSysNetWlanMacAddress.class,
                NodePlatformOEMColorProduct.class, NodeMultiroomDeviceTransportOptimisation.class,
                NodeSysInfoNetRemoteVendorId.class, NodeMultiroomCapsProtocolVersion.class,
                NodeMultichannelSystemCompatibilityid.class, NodeSysInfoVersion.class};

        mRadio.getNodesMultiple(nodesToFetch, true, new IGetNodesCallback() {
            @Override
            public void onResult(Map<Class, NodeResponse> nodeResponses) {

                NodeSysInfoFriendlyName friendlyNameNode = (NodeSysInfoFriendlyName)nodeResponses.get(NodeSysInfoFriendlyName.class).mNodeInfo;
                NodeSysNetWlanMacAddress macAddressNode = (NodeSysNetWlanMacAddress)nodeResponses.get(NodeSysNetWlanMacAddress.class).mNodeInfo;
                NodePlatformOEMColorProduct colorProductNode = (NodePlatformOEMColorProduct)nodeResponses.get(NodePlatformOEMColorProduct.class).mNodeInfo;
                NodeMultiroomDeviceTransportOptimisation transportOptimisationNode = (NodeMultiroomDeviceTransportOptimisation)nodeResponses.
                        get(NodeMultiroomDeviceTransportOptimisation.class).mNodeInfo;
                NodeSysInfoNetRemoteVendorId vendorIdNode = (NodeSysInfoNetRemoteVendorId)nodeResponses.get(NodeSysInfoNetRemoteVendorId.class).mNodeInfo;
                NodeMultiroomCapsProtocolVersion multiroomVersionNode = (NodeMultiroomCapsProtocolVersion)nodeResponses.get(NodeMultiroomCapsProtocolVersion.class).mNodeInfo;
                NodeMultichannelSystemCompatibilityid compatibilityidNode = (NodeMultichannelSystemCompatibilityid)nodeResponses.get(NodeMultichannelSystemCompatibilityid.class).mNodeInfo;
                NodeSysInfoVersion currentVersionNode = (NodeSysInfoVersion)nodeResponses.get(NodeSysInfoVersion.class).mNodeInfo;

                if ((!TextUtils.isEmpty(mDeviceRecord.serialNumber) && !mDeviceRecord.serialNumber.contentEquals(macAddressNode.getValue())) ||
                        !VendorIDsUtil.isInVendorIDsList(vendorIdNode.getValue(), mVendorIds)) {
                    if (mDeviceDetailsListener != null) {
                        mDeviceDetailsListener.onDeviceDetailsError(mDeviceRecord, IDeviceDetailsListener.EDeviceDetailsResult.XML_PARSE_ERROR);
                    }
                    dispose();
                    return;
                }

                mDeviceRecord.isMinuet = true;
                mDeviceRecord.isMultiroomCapable = true;
                mDeviceRecord.isSG = true;
                mDeviceRecord.multiroomVersion = multiroomVersionNode.getValue().toString();
                if (compatibilityidNode != null) {
                    mDeviceRecord.isStereoCapable = true;
                }
                mDeviceRecord.friendlyName = friendlyNameNode.getValue();
                mDeviceRecord.serialNumber = macAddressNode.getValue();
                mDeviceRecord.UDN = macAddressNode.getValue();
                mDeviceRecord.modelName = colorProductNode.getValue();
                mDeviceRecord.transportOptimized = (transportOptimisationNode.getValueEnum() == BaseMultiroomDeviceTransportOptimisation.Ord.ENABLED);
                mDeviceRecord.hasGoogleCast = true;
                mDeviceRecord.firmwareVersion = currentVersionNode.getValue();

                if (mDeviceDetailsListener != null) {
                    mDeviceDetailsListener.onDeviceDetailsRetrieved(mDeviceRecord);
                }
                dispose();
            }
        });
    }

    void dispose() {
        mDeviceRecord = null;
        mVendorIds = null;
        mFirmwareBlacklist = null;
        mDeviceDetailsListener = null;
        mRadio.close();
    }
}
