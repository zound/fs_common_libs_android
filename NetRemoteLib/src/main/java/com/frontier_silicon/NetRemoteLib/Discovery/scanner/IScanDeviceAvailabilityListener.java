package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;

public interface IScanDeviceAvailabilityListener {

	void onScanDeviceAvailable(DeviceRecord record);
	void onScanDeviceDisappeared(DeviceRecord record);
}
