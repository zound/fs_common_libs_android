package com.frontier_silicon.NetRemoteLib.Discovery.ping;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;

/**
 * Created by lsuhov on 04/11/2016.
 */

public class PingDeviceRecordRunnable implements Runnable {

    private DeviceRecord mDeviceRecord;
    private IPingDeviceRecordListener mListener;

    public PingDeviceRecordRunnable(DeviceRecord deviceRecord, IPingDeviceRecordListener listener) {
        mDeviceRecord = deviceRecord;
        mListener = listener;
    }

    @Override
    public void run() {
        PingUtil pingUtil = new PingUtil();
        boolean isAvailable = pingUtil.isRadioAvailable(mDeviceRecord);

        mListener.onPingResult(mDeviceRecord, isAvailable);

        dispose();
    }

    private void dispose() {
        mDeviceRecord = null;
        mListener = null;
    }
}
