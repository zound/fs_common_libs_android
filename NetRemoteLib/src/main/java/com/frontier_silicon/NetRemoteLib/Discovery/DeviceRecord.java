package com.frontier_silicon.NetRemoteLib.Discovery;

import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IDiscoveryScanner;

public class DeviceRecord {
	public String ID = null;
	public String serialNumber = null;
    public String IP = null;
	public String deviceInfoLocation = null;
	public String fsApiLocation = null;
	public String friendlyName = null;
	public String UDN = null;
	public String multiroomVersion = null;
	public boolean isMultiroomCapable = false;
	public String vendorID = "";
	public String modelName = null;
	public String modelNumber = null;
	public String firmwareVersion = null;
	public String imageURL = null;
    public volatile boolean invalidated = true;
	public boolean isMinuet = false;
	public boolean isVenice = false;
    public boolean isSG = false;
    public boolean isStereoCapable = false;
    public boolean hasGoogleCast = false;
    public boolean hasAVS = false;

    // Do not save. It's designed to be used only with latest data
    public Boolean transportOptimized = null;

    private long timeStamp = System.currentTimeMillis();

    public boolean isReadyToLookupName() {
        return ((this.ID != null) && (this.deviceInfoLocation != null) && (this.fsApiLocation != null));
    }

    public boolean isComplete() {
		return isReadyToLookupName() && (this.friendlyName != null) && (this.serialNumber != null) &&
                (this.UDN != null);
	}

	public boolean hasTimedOut() {

		return ((System.currentTimeMillis() - this.timeStamp) > (IDiscoveryScanner.DEVICE_DISAPPEARED_TIME_OUT * 1000));
	}

    public void refreshTimeStamp() {
        timeStamp = System.currentTimeMillis();
    }

	public String toString() {
		return "DeviceRecord for: " + friendlyName + " " + fsApiLocation;
	}

}