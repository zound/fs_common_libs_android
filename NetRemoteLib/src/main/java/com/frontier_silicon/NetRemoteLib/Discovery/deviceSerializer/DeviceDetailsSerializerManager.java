package com.frontier_silicon.NetRemoteLib.Discovery.deviceSerializer;

import android.content.Context;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by lsuhov on 08/11/2016.
 */

public class DeviceDetailsSerializerManager {
    private static DeviceDetailsSerializerManager mInstance;

    private BlockingQueue<Runnable> mSerializerRunnableQueue = new LinkedBlockingQueue<>(30);
    private ExecutorService mSerializerThreadPoolExecutor;

    public static DeviceDetailsSerializerManager getInstance() {
        if (mInstance == null) {
            mInstance = new DeviceDetailsSerializerManager();
        }

        return mInstance;
    }

    private DeviceDetailsSerializerManager() {

        mSerializerThreadPoolExecutor = new ThreadPoolExecutor(0/*corePoolSize*/, 1/*maxPoolSize*/,
                1/*KEEP_ALIVE_TIME*/, TimeUnit.SECONDS/*KEEP_ALIVE_TIME_UNIT*/, mSerializerRunnableQueue, new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                NetRemote.log(LogLevel.Error, "mSerializerThreadPoolExecutor: thread execution was rejected");
            }
        });
    }

    public void loadDeviceRecords(Context context, String wifiSSID, IDeviceDetailsDeserializerListener listener) {
        DeviceDetailsDeserializerRunnable runnable = new DeviceDetailsDeserializerRunnable(context, wifiSSID, listener);

        mSerializerThreadPoolExecutor.submit(runnable);
    }

    public void saveAllRadios(Context context, String wifiSSID, List<Radio> radios, IDeviceDetailsSerializerListener listener) {
        DeviceDetailsCollectionSerializerRunnable runnable = new DeviceDetailsCollectionSerializerRunnable(context,
                wifiSSID, radios, listener);

        mSerializerThreadPoolExecutor.submit(runnable);
    }
}
