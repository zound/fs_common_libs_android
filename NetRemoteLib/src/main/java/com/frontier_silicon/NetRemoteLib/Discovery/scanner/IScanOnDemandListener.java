package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

import java.util.List;

/**
 *  Listener for on-demand SSDP scan
 */
public interface IScanOnDemandListener {
    /**
     *  Listener for on-demand SSDP scan. gets called when scan is completed
     *
     * @param allRadios			A list of the radios found.
     */
    void onScanResults(List<Radio> allRadios);
}
