package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.LogLevel;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by lsuhov on 28/02/2017.
 */
class SSDPSearcher implements Runnable {

    private static final int SEARCH_PACKET_INTERVAL = 6; /* seconds */
    private static final int HEARTBEAT_FREQUENCY_SECONDS = 120; /* seconds */

    private static final long WAIT_BETWEEN_VENDORID_MSEARCH_MS = 500; /* milisec */

    private static final int MX_INTERVAL_SECONDS = 5; /* seconds */

    private static final String SSDP_ALL_MSEARCH_QUERY = "ssdp:all";
    private static final String UPNP_ROOT_SSDP_MSEARCH_QUERY = "upnp:rootdevice"; // query to get upnp:root response

    private boolean HasBeenAborted = false; // Kill off this instance.
    private final Object mLock = new Object();
    private Thread WorkerThread = null;

    private SSDPScanner mSsdpScanner = null;
    private String[] mSearchQueries = null;
    private boolean mUseUpnpRootForSearch;

    void Abort() {
        synchronized (mLock) {
            this.HasBeenAborted = true;
            if (this.WorkerThread != null) {
                this.WorkerThread.interrupt();
            }

            mSsdpScanner = null;
        }
    }

    SSDPSearcher(SSDPScanner ssdpScanner, String[] searchQueries, boolean useUpnpRootForSearch) {
        mSsdpScanner = ssdpScanner;
        mSearchQueries = searchQueries;
        mUseUpnpRootForSearch = useUpnpRootForSearch;

        synchronized (mLock) {
            this.WorkerThread = new Thread(this, "SSDP Searcher");
            this.WorkerThread.start();
        }
    }

    public void run() {
        NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "Start SSDP Searcher Thread");

        // initial packet search
        sendInitialSearchPackets();

        // heartbeat
        heartbeatSearchPackets();

        NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "close SSDP Searcher Thread");

        synchronized (mLock) {
            this.WorkerThread = null;
        }
    }

    private void sendInitialSearchPackets() {
        sendMSEARCHBurst();
    }


    private void heartbeatSearchPackets() {
        if (HEARTBEAT_FREQUENCY_SECONDS > 0) {

            while (!(this.HasBeenAborted)) {
                try {
                    Thread.sleep(HEARTBEAT_FREQUENCY_SECONDS * 1000);
                } catch (InterruptedException e) {
                }

                if (this.HasBeenAborted) {
                    break;
                }

                NetRemote.log(LogLevel.Info, "SSDP: Heartbeat search");

                sendMSEARCHBurst();

                try {
                    Thread.sleep(MX_INTERVAL_SECONDS * 1000);
                } catch (InterruptedException e) {
                }

                if (this.HasBeenAborted) {
                    break;
                }
            }
        }
    }

    void sendMSEARCHBurst() {

        /* Issue a search. */
        IssueSearch(SSDP_ALL_MSEARCH_QUERY);
        waitForResponses();

        if (this.HasBeenAborted) {
            return;
        }

        if (mUseUpnpRootForSearch) {
            IssueSearch(UPNP_ROOT_SSDP_MSEARCH_QUERY);
            waitForResponses();

            if (this.HasBeenAborted) {
                return;
            }
        }

//        IssueSearch(SSDP_ALL_MSEARCH_QUERY);
//        waitForResponses();
    }

    private void waitForResponses() {
        /* Wait for the results to be collected. */
        try {
            Thread.sleep(SEARCH_PACKET_INTERVAL * 1000);
        } catch (InterruptedException e) {
        }
    }

    void IssueSearchForAllVendorIds() {

        for (String searchQuery : mSearchQueries) {
            IssueSearch(searchQuery);

            try {
                Thread.sleep(WAIT_BETWEEN_VENDORID_MSEARCH_MS);
            } catch (InterruptedException e) {
            }

            if (this.HasBeenAborted) {
                break;
            }
        }
    }

    void IssueSearch(String query) {
        String msg = "";
        msg += "M-SEARCH * HTTP/1.1\r\n";
        msg += "ST: " + query + "\r\n";
        msg += "MX: " + MX_INTERVAL_SECONDS + "\r\n";
        msg += "MAN: \"ssdp:discover\"\r\n";
        msg += "HOST: " + SSDPScanner.BROADCAST_ADDRESS + ":" + SSDPScanner.PORT_BROADCAST + "\r\n";
        msg += "\r\n";

        synchronized (mLock) {
            if (mSsdpScanner == null) {
                return;
            }

            synchronized (mSsdpScanner.AllSockets) {
                for (DatagramSocket socket : mSsdpScanner.AllSockets.keySet()) {
                    InetAddress addr = mSsdpScanner.AllSockets.get(socket);
                    String addrString = addr.toString();
                    if (!addrString.contains(SSDPScanner.BROADCAST_ADDRESS)) {
                        //We need to send the MSEARCH only on broadcast address
                        continue;
                    }

                    try {
                        DatagramPacket dgmPacket = new DatagramPacket(msg.getBytes(), msg.length(), InetAddress.getByName(SSDPScanner.BROADCAST_ADDRESS), SSDPScanner.PORT_BROADCAST);
                        socket.send(dgmPacket);
                        NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP: Issued search packet on interface address " + addr.toString() + " with query " + query + ".");
                        NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP Issued MSEARCH:\n" + msg);
                    } catch (Exception e) {
                        NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error, "SSDP: Failed to TX search packet on interface address " + addr.toString() + ". " + e.getMessage());
                    }
                }
            }
        }
    }

}
