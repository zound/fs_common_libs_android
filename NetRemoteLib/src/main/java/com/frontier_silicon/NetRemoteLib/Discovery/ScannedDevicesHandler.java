package com.frontier_silicon.NetRemoteLib.Discovery;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Discovery.deviceDetailsRetriever.DeviceDetailsRetrieverRunnable;
import com.frontier_silicon.NetRemoteLib.Discovery.ping.IPingDeviceRecordListener;
import com.frontier_silicon.NetRemoteLib.Discovery.ping.PingDeviceRecordRunnable;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanDeviceAvailabilityListener;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.frontier_silicon.NetRemoteLib.Discovery.deviceDetailsRetriever.IDeviceDetailsListener.EDeviceDetailsResult.TIMEOUT_ERROR;

/**
 * Created by lsuhov on 03/11/2016.
 */

public class ScannedDevicesHandler implements IScanDeviceAvailabilityListener, IPingDeviceRecordListener {

    RadioListKeeper mRadioListKeeper;
    private String[] mVendorIDList = new String[] {};
    private String[] mFirmwareBlacklist = new String[] {};

    private List<IPingDeviceRecordListener> mListeners = new CopyOnWriteArrayList<>();

    private BlockingQueue<Runnable> mRunnableQueue = new LinkedBlockingQueue<>(30);
    ThreadPoolExecutor mThreadPoolExecutor;
    private boolean mUseGetMultipleForDeviceDetailsRetrieval = false;

    private final int PING_DEVICES_INTERVAL = 60_000;
    private Timer mPingMissingDevicesTimer;
    private PingMissingDevicesTimerTask mPingMissingDevicesTimerTask;
    private boolean mPingOnByeByeNotification = true;

    public ScannedDevicesHandler(RadioListKeeper radioListKeeper) {
        mRadioListKeeper = radioListKeeper;

        mThreadPoolExecutor = new ThreadPoolExecutor(4/*corePoolSize*/, 4/*maxPoolSize*/,
                2, TimeUnit.SECONDS/*KEEP_ALIVE_TIME_UNIT*/, mRunnableQueue,
                new RejectedExecutionHandler() {
                    @Override
                    public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                        NetRemote.log(LogLevel.Error, "ScannedDevicesHandler mThreadPoolExecutor: thread execution was rejected");
                    }
                });
        mThreadPoolExecutor.allowCoreThreadTimeOut(true);
    }

    public void start() {
        startPingMissingDevicesTimer();
    }

    public void stop() {
        stopPingMissingDevicesTimer();

        mRunnableQueue.clear();
    }

    public void addListener(IPingDeviceRecordListener listener) {
        mListeners.add(listener);
    }

    @Override
    public void onScanDeviceAvailable(DeviceRecord scanRecord) {

        DeviceDetailsRetrieverRunnable retrieverRunnable = new DeviceDetailsRetrieverRunnable(scanRecord,
                mVendorIDList, mFirmwareBlacklist, mUseGetMultipleForDeviceDetailsRetrieval, mRadioListKeeper);

        //NetRemote.log(LogLevel.Info, "onScanDeviceAvailable " + scanRecord.deviceInfoLocation);

        mThreadPoolExecutor.submit(retrieverRunnable);
    }

    @Override
    public void onScanDeviceDisappeared(DeviceRecord scanDeviceRecord) {

        DeviceRecord deviceRecord = getDeviceRecord(scanDeviceRecord);

        if (deviceRecord != null) {
            if (mPingOnByeByeNotification) {
                mThreadPoolExecutor.submit(new PingDeviceRecordRunnable(deviceRecord, this));
            } else {
                onPingResult(deviceRecord, false);
            }
        }
    }

    public void setVendorIDsList(String[] vendorIDList) {
        mVendorIDList = vendorIDList;
    }

    public void setFirmwareBlacklist(String[] firmwareBlacklist) {
        mFirmwareBlacklist = firmwareBlacklist;
    }

    void setUseGetMultipleForDeviceDetailsRetrieval(boolean useGetMultiple) {
        mUseGetMultipleForDeviceDetailsRetrieval = useGetMultiple;
    }

    public void setPingDeviceOnByeByeNotification(boolean pingOnByeBye) {
        mPingOnByeByeNotification = pingOnByeBye;
    }

    @Override
    public void onPingResult(DeviceRecord deviceRecord, boolean pingOk) {
        if (pingOk) {
            deviceRecord.refreshTimeStamp();
        } else {
            mRadioListKeeper.onDeviceDetailsError(deviceRecord, TIMEOUT_ERROR);
        }

        for (IPingDeviceRecordListener listener : mListeners) {
            listener.onPingResult(deviceRecord, pingOk);
        }
    }

    private DeviceRecord getDeviceRecord(DeviceRecord scanDeviceRecord) {

        List<Radio> radios = mRadioListKeeper.getRadios();
        int size = radios.size();
        for (int i = 0; i < size; i++) {
            DeviceRecord deviceRecord = radios.get(i).getDeviceRecord();
            if (deviceRecord != null && TextUtils.equals(deviceRecord.ID, scanDeviceRecord.ID)) {
                return deviceRecord;
            }
        }
        return null;
    }

    private void startPingMissingDevicesTimer() {
        stopPingMissingDevicesTimer();

        mPingMissingDevicesTimerTask = new PingMissingDevicesTimerTask();
        mPingMissingDevicesTimer = new Timer();
        mPingMissingDevicesTimer.schedule(mPingMissingDevicesTimerTask, PING_DEVICES_INTERVAL, PING_DEVICES_INTERVAL);
    }

    private void stopPingMissingDevicesTimer() {
        if (mPingMissingDevicesTimer != null) {
            mPingMissingDevicesTimer.cancel();
            mPingMissingDevicesTimer.purge();
            mPingMissingDevicesTimer = null;
        }
        if (mPingMissingDevicesTimerTask != null) {
            mPingMissingDevicesTimerTask.dispose();
            mPingMissingDevicesTimerTask = null;
        }
    }

    private class PingMissingDevicesTimerTask extends TimerTask {
        private boolean isDisposed = false;

        PingMissingDevicesTimerTask() {}

        @Override
        public void run() {
            if (isDisposed) {
                NetRemote.log(LogLevel.Info, "PingMissingDevicesTimerTask was canceled");
                cancel();
                return;
            }

            NetRemote.log(LogLevel.Info, "PingMissingDevicesTimerTask started");

            List<Radio> radios = mRadioListKeeper.getRadios();

            int size = radios.size();
            for (int i = 0; i < size; i++) {
                DeviceRecord deviceRecord = radios.get(i).getDeviceRecord();

                if (isDisposed) {
                    cancel();
                    return;
                }

                if (deviceRecord.hasTimedOut()) {
                    mThreadPoolExecutor.submit(new PingDeviceRecordRunnable(deviceRecord, ScannedDevicesHandler.this));
                }
            }
        }

        public void dispose() {
            isDisposed = true;
        }
    }
}
