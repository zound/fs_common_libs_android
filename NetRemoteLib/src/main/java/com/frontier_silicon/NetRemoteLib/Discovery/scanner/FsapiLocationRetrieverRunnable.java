package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttpUtil;
import com.frontier_silicon.loggerlib.LogLevel;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.SocketTimeoutException;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by lsuhov on 12/05/2017.
 */

class FsapiLocationRetrieverRunnable implements Runnable {

    private String mDeviceUrl;
    private DeviceRecord mDeviceRecord;
    private IFsapiLocationRetrieverListener mListener;

    FsapiLocationRetrieverRunnable(String deviceUrl, DeviceRecord deviceRecord, IFsapiLocationRetrieverListener listener) {
        mDeviceUrl = deviceUrl;
        mDeviceRecord = deviceRecord;
        mListener = listener;
    }

    @Override
    public void run() {
        if (mDeviceRecord.fsApiLocation != null) {
            dispose();
            return;
        }

        Document document = getResponse();

        if (mDeviceRecord.fsApiLocation != null) {
            dispose();
            return;
        }

        parse(document);

        dispose();
    }

    private void parse(Document document) {
        if (document == null) {
            dispose();
            return;
        }

        NodeList nodes = document.getElementsByTagName("webfsapi");
        if (nodes.getLength() > 0) {
            String fsapiLocation = nodes.item(0).getTextContent();
            mDeviceRecord.fsApiLocation = fsapiLocation;

            if (mListener != null) {
                mListener.onFsapiLocationRetrieved(mDeviceRecord);
            }
        }
    }

    private Document getResponse() {
        Document document = null;

        NetRemote.log(LogLevel.Info, "Requesting: " + mDeviceUrl);

        Request request = new Request.Builder().url(mDeviceUrl).build();

        try {
            Response response = NetRemote.getOkHttpClient().newCall(request).execute();
            if (response.isSuccessful()) {

                NetRemote.log(LogLevel.Info, "Response from " + mDeviceUrl);

                document = RadioHttpUtil.getDocument(response.body().byteStream());
                response.close();
            } else {
                NetRemote.log(LogLevel.Error, "Request error at: " + mDeviceUrl + ", code :" + response.code());

                //TODO check if possible
                //response.close();
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            NetRemote.log(LogLevel.Error, "Request error: " + e + " " + mDeviceUrl);
        } catch (IOException e) {
            e.printStackTrace();

            NetRemote.log(LogLevel.Error, "Request error: " + e + " " + mDeviceUrl);
        }

        return document;
    }

    void dispose() {
        mDeviceUrl = null;
        mDeviceRecord = null;
        mListener = null;
    }
}
