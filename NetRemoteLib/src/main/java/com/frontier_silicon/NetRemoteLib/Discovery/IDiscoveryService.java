package com.frontier_silicon.NetRemoteLib.Discovery;

import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanListener;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanOnDemandListener;
import com.frontier_silicon.NetRemoteLib.Node.BaseMultiroomDeviceListAll;

public interface IDiscoveryService {

    long SINGLE_SCAN_FOR_DEVICES_TIME_MS = 20_000;

    void start(boolean clearExistingRadios);

    void stop();

    void rescan();

    void cleanup();

    void reInit();

    void close();

    void singleScanOnDemand(boolean callbackOnMainThread, IScanOnDemandListener callback);

    void setVendorIDsList(String[] vendorIDs);

    void setBonjourServiceTypeList(String[] bonjourServiceTypeList);

    void setUseGetMultipleForDeviceDetailsRetrieval(boolean useGetMultiple);

    boolean isRunning();

    boolean addListener(IScanListener listener);

    boolean removeListener(IScanListener listener);

    void allowSpeakerToBeAddedFromDeviceListAll(boolean allow);

    void tryToAddSpeakerFromDeviceListAll(BaseMultiroomDeviceListAll.ListItem item);

    void setUseUpnpRootDeviceQueryInSsdp(boolean useUpnpRoot);
}
