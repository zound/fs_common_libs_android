package com.frontier_silicon.NetRemoteLib.Discovery.deviceDetailsRetriever;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;

public interface IDeviceDetailsListener {

    enum EDeviceDetailsResult {
        SUCCESS,
        DDXML_NOT_FOUND_ERROR,
        TIMEOUT_ERROR,
        XML_PARSE_ERROR,
        WRONG_VENDOR_ID,
        CONNECTION_REFUSED,
        OTHER_ERROR
    }

    void onDeviceDetailsRetrieved(DeviceRecord deviceRecord);

    void onDeviceDetailsError(DeviceRecord deviceRecord, EDeviceDetailsResult error);

}
