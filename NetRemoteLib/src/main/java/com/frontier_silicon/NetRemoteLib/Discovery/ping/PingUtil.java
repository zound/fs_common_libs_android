package com.frontier_silicon.NetRemoteLib.Discovery.ping;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.loggerlib.LogLevel;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeDefs;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanMacAddress;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttp;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttpUtil;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;

public class PingUtil {

    private static final int RADIO_PING_TIMEOUT_MS = 4000;
    private static final int RADIO_SECOND_PING_TIMEOUT_MS = 10000;
    private static final int RADIO_PING_RETRIES = 2;

    public boolean isRadioAvailable(Radio radio) {
        return isLocationAvailable(radio.getDDXMLLocation());
    }

    public boolean isRadioAvailable(DeviceRecord record) {
        return isLocationAvailable(record.deviceInfoLocation);
    }

    public boolean isLocationAvailable(String url) {
        boolean isAvailable = false;

        for (int i=0; i < RADIO_PING_RETRIES; i++) {
            if (i == 0 && pingSocket(url, RADIO_PING_TIMEOUT_MS)) {
                isAvailable = true;
                break;
            }
            if (i == 1 && pingSocket(url, RADIO_SECOND_PING_TIMEOUT_MS)) {
                isAvailable = true;
                break;
            }
        }

        return isAvailable;
    }

    /**
     * Open a socket to a host:port extracted from a url
     * @param url
     * @param timeout
     * @return true if socket is opened, false if failed
     */

    private boolean pingSocket(String url, int timeout) {
        URL urlToPing = null;
        try {
            urlToPing = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        int port = (urlToPing != null && urlToPing.getPort() != -1) ? urlToPing.getPort() : 8080;

        try {

            NetRemote.log(LogLevel.Info, "PingUtil: pinging " + urlToPing.getHost() + ":" + port);

            Socket socket = new Socket();
            InetSocketAddress socketAddress = new InetSocketAddress(urlToPing.getHost(), port);

            socket.connect(socketAddress, timeout);

            socket.close();
            return true;

        } catch (IOException e) {
            e.printStackTrace();
        }

        NetRemote.log(LogLevel.Info, "PingUtil: pinging " + urlToPing.getHost() + ":" + port + " not reachable");
        return false;
    }

    public boolean ping(String url) {
        boolean responseOk = false;

        NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, LogLevel.Info, "PingUtil: pinging at " + url);

        try {
            URL urlToPing = new URL(url);
            HttpURLConnection httpConn = (HttpURLConnection) urlToPing.openConnection();
            try {
                httpConn.setConnectTimeout(RADIO_PING_TIMEOUT_MS);
                httpConn.setReadTimeout(RADIO_PING_TIMEOUT_MS);
                httpConn.setRequestProperty("Connection", "close");
                httpConn.setRequestMethod("HEAD");
                httpConn.connect();


                int respCode = httpConn.getResponseCode();
                responseOk = true;

                NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, LogLevel.Info, "PingUtil: Response for " + url + " " + respCode);
            } catch (SocketTimeoutException | ConnectException e) {
                NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, LogLevel.Warning, "PingUtil: Failed PING at " + url + " - " + e.toString());

                responseOk = false;
            } catch (IOException e) {
                NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, LogLevel.Warning, "PingUtil: Expected error when pinging IR speaker at " + url + " - " + e.toString());

                responseOk = true;
            } catch (Exception e) {
                NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, LogLevel.Warning, "PingUtil: Failed to retrieve dd.xml for " + url + " - " + e.toString());
                responseOk = false;
            } finally {
                httpConn.disconnect();
            }
        } catch (Exception e) {
            NetRemote.log(NetRemote.TRACE_NETWORK_PACKETS_BIT, LogLevel.Warning, "PingUtil: Failed to open connection to dd.xml for " + url + " - " + e.toString());
            responseOk = false;
        }

        return responseOk;
    }

    private String getNodeAdressForPing(Radio radio) {
        String url = RadioHttpUtil.formAccessUrl(((RadioHttp)radio).getApiBase(), radio.getPIN(),
                "GET/" + NodeDefs.Instance().GetNodeName(NodeSysNetWlanMacAddress.class), null);

        return url;
    }
}
