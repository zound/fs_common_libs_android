package com.frontier_silicon.NetRemoteLib.Discovery.deviceSerializer;

import android.content.Context;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;

import java.util.List;

/**
 * Created by lsuhov on 08/11/2016.
 */

public class DeviceDetailsDeserializerRunnable implements Runnable {

    private Context mContext;
    private String mWifiSSID;
    private IDeviceDetailsDeserializerListener mListener;

    public DeviceDetailsDeserializerRunnable(Context context, String wifiSSID, IDeviceDetailsDeserializerListener listener) {
        mContext = context;
        mWifiSSID = wifiSSID;
        mListener = listener;
    }

    @Override
    public void run() {
        DeviceDetailsSerializer deviceDetailsSerializer = new DeviceDetailsSerializer(mContext, mWifiSSID);
        List<DeviceRecord> deviceRecordList = deviceDetailsSerializer.loadDeviceRecords();

        mListener.onDeviceDetailsDeserialized(deviceRecordList);

        dispose();
    }

    public void dispose() {
        mContext = null;
        mWifiSSID = null;
        mListener = null;
    }
}
