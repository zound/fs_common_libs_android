package com.frontier_silicon.NetRemoteLib.Discovery.deviceDetailsRetriever;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttpUtil;
import com.frontier_silicon.NetRemoteLib.utils.VendorIDsUtil;
import com.frontier_silicon.loggerlib.LogLevel;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import okhttp3.Request;
import okhttp3.Response;

public class DDXMLParser implements Runnable {

    private static final String MINUS_DIVIDER = "-";

    private IDeviceDetailsListener.EDeviceDetailsResult mResult = IDeviceDetailsListener.EDeviceDetailsResult.OTHER_ERROR;
    private DeviceRecord mDeviceRecord;
    private String[] mVendorIds;
    private String[] mFirmwareBlacklist;
    private IDeviceDetailsListener mDeviceDetailsListener;

    public DDXMLParser(DeviceRecord deviceRecord, String[] vendorIds, String[] firmwareBlacklist, IDeviceDetailsListener deviceDetailsListener) {
        mDeviceRecord = deviceRecord;
        mVendorIds = vendorIds;
        mFirmwareBlacklist = firmwareBlacklist;
        mDeviceDetailsListener = deviceDetailsListener;
    }

    @Override
    public void run() {

        /* get dd.xml */
        Document document = getResponse(mDeviceRecord.deviceInfoLocation);

        // retrying once
        if (mResult == IDeviceDetailsListener.EDeviceDetailsResult.TIMEOUT_ERROR ||
                mResult == IDeviceDetailsListener.EDeviceDetailsResult.CONNECTION_REFUSED) {
            document = getResponse(mDeviceRecord.deviceInfoLocation);
        }

        IDeviceDetailsListener.EDeviceDetailsResult result = parse(document, mDeviceRecord);

        if (result == IDeviceDetailsListener.EDeviceDetailsResult.SUCCESS) {
            notifyListenerOnDeviceDetailsRetrieved(mDeviceRecord);
        } else {
            notifyListenerOnDeviceDetailsError(mDeviceRecord, result);
        }

        dispose();
    }

    private void dispose() {
        mDeviceRecord = null;
        mVendorIds = null;
        mFirmwareBlacklist = null;
        mDeviceDetailsListener = null;
    }

    private Document getResponse(String urlAddress) {
        Document document = null;

        NetRemote.log(LogLevel.Info, "DDXMLParser Requesting: " + urlAddress);

        Request request = new Request.Builder().url(urlAddress).build();

        try {
            Response response = NetRemote.getOkHttpClient().newCall(request).execute();
            if (response.isSuccessful()) {

                NetRemote.log(LogLevel.Info, "DDXMLParser Response from " + urlAddress);

                document = RadioHttpUtil.getDocument(response.body().byteStream());
                response.close();

                mResult = IDeviceDetailsListener.EDeviceDetailsResult.SUCCESS;
            } else {
                NetRemote.log(LogLevel.Error, "DDXMLParser Request error at: " + urlAddress + ", code :" + response.code());

                //TODO check if possible
                //response.close();
                mResult = IDeviceDetailsListener.EDeviceDetailsResult.DDXML_NOT_FOUND_ERROR;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            NetRemote.log(LogLevel.Error, "DDXMLParser Request error: " + e + " " + urlAddress);

            mResult = IDeviceDetailsListener.EDeviceDetailsResult.TIMEOUT_ERROR;
        } catch (ConnectException e) {
            e.printStackTrace();

            NetRemote.log(LogLevel.Error, "DDXMLParser Request error: " + e + " " + urlAddress);
            String detailedMessage = e.getCause().getMessage();
            if (detailedMessage != null && detailedMessage.contains("ECONNREFUSED")) {
                mResult = IDeviceDetailsListener.EDeviceDetailsResult.CONNECTION_REFUSED;
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            } else {
                mResult = IDeviceDetailsListener.EDeviceDetailsResult.OTHER_ERROR;
            }
        } catch (IOException e) {
            e.printStackTrace();

            NetRemote.log(LogLevel.Error, "DDXMLParser Request error: " + e + " " + urlAddress);

            mResult = IDeviceDetailsListener.EDeviceDetailsResult.OTHER_ERROR;
        }

        return document;
    }

    public IDeviceDetailsListener.EDeviceDetailsResult parse(Document document, DeviceRecord record) {

        if (mResult == IDeviceDetailsListener.EDeviceDetailsResult.SUCCESS) {

            mResult = IDeviceDetailsListener.EDeviceDetailsResult.XML_PARSE_ERROR;

            String friendlyName = null;
            String serialNumber = null;
            String udn = null;
            String multiroomVersion = null;
            boolean isMultiroomCapable = false;
            String modelName = null;
            String modelNumber = null;
            String modelDescription = null;
            String imageURL = null;
            boolean vendorIdIsValid = true;
            boolean isInBlacklist = false;
            boolean isMinuet = false;
            boolean isVenice = false;
            boolean isSG = false;
            boolean isStereoCapable = false;
            boolean hasGoogleCast = false;
            boolean hasAVS = false;

            if (document != null) {
                /* Check blacklist */
                NodeList nodes = document.getElementsByTagName("pns:X_firmwareVersion");
                if (nodes.getLength() > 0) {
                    String firmwareVersion = nodes.item(0).getTextContent();
                    isInBlacklist = isFirmwareInBlacklist(firmwareVersion, mFirmwareBlacklist);
                }

                nodes = document.getElementsByTagName("netRemoteVendorId");
                if (nodes.getLength() > 0) {
                    String vendorId = nodes.item(0).getTextContent();
                    vendorIdIsValid = VendorIDsUtil.isInVendorIDsList(vendorId, mVendorIds);

                    if (!vendorIdIsValid) {
                        mResult = IDeviceDetailsListener.EDeviceDetailsResult.WRONG_VENDOR_ID;
                    }
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find vendorId in XML for " + mDeviceRecord.ID);
                }

                nodes = document.getElementsByTagName("modelDescription");
                if (nodes.getLength() > 0) {
                    modelDescription = nodes.item(0).getTextContent();
                    if (modelDescription.contains("ns")) {
                        isMinuet = true;
                    } else if (modelDescription.contains("ir")) {
                        isVenice = true;
                    } else {
                        isVenice = true;
                    }
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find modelDescription in XML for " + mDeviceRecord.ID);
                    isVenice = true;
                }

                /* Find the friendly name. */
                nodes = document.getElementsByTagName("friendlyName");
                if (nodes.getLength() > 0) {
                    friendlyName = nodes.item(0).getTextContent();
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find Friendly Name in XML for " +
                            mDeviceRecord.ID);
                }

                /* Find the serial number. */
                nodes = document.getElementsByTagName("serialNumber");
                if (nodes.getLength() > 0) {
                    serialNumber = nodes.item(0).getTextContent();
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find Serial Number in XML for " + mDeviceRecord.ID);
                }

                /* Find the UDN */
                nodes = document.getElementsByTagName("UDN");
                if (nodes.getLength() > 0) {
                    udn = nodes.item(0).getTextContent();
                    udn = sanitizeUDN(udn);
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find UDN in XML for " + mDeviceRecord.ID);
                }

                /* Find the multiroom ver. */
                nodes = document.getElementsByTagName("fsns:X_audSyncProtocolID");
                if (nodes.getLength() > 0) {
                    multiroomVersion = nodes.item(0).getTextContent();
                    multiroomVersion = sanitizeMultiroomVersion(multiroomVersion);

                    try {
                        int decimalMultiroomVersion = Integer.parseInt(multiroomVersion);
                        if (decimalMultiroomVersion < 128) {
                            isVenice = true;
                            isMinuet = false;
                        } else {
                            isVenice = false;
                            isMinuet = true;
                        }
                    } catch (NumberFormatException e) {
                        NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't parse fsns:AudioSyncProtocolID in XML for " + mDeviceRecord.ID);
                    }
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find fsns:AudioSyncProtocolID in XML for " + mDeviceRecord.ID);
                }

                /* Find if multiroom supported */
                nodes = document.getElementsByTagName("fsns:X_Features");
                if (nodes.getLength() > 0) {
                    String features = nodes.item(0).getTextContent();

                    if (!TextUtils.isEmpty(features)) {
                        String lowerCaseFeatures = features.toLowerCase();

                        isMultiroomCapable = hasMultiroomCapability(lowerCaseFeatures);
                        isSG = lowerCaseFeatures.contains("sg");
                        isStereoCapable = lowerCaseFeatures.contains("wsa");
                        hasGoogleCast = lowerCaseFeatures.contains("google cast");
                        hasAVS = lowerCaseFeatures.contains("avs");
                    }
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find fsns:X_Features in XML for " + mDeviceRecord.ID);
                }

                /* Find model name */
                nodes = document.getElementsByTagName("modelName");
                if (nodes.getLength() > 0) {
                    modelName = nodes.item(0).getTextContent();
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find modelName in XML for " + mDeviceRecord.ID);
                }

                /* Find model number */
                nodes = document.getElementsByTagName("modelNumber");
                if (nodes.getLength() > 0) {
                    modelNumber = nodes.item(0).getTextContent();
                } else {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find modelNumber in XML for " + mDeviceRecord.ID);
                }

                /* Find largest device image URL */
                imageURL = retrieveLargestIconURL("image/png", "icon", document);
                if (imageURL == null) {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "DDXMLParser Couldn't find iconList in XML for " + mDeviceRecord.ID);
                }
            }

            if (vendorIdIsValid && !isInBlacklist && (friendlyName != null) && (serialNumber != null) && (udn != null)) {

                if (!TextUtils.isEmpty(record.serialNumber) && !record.serialNumber.contentEquals(serialNumber)) {
                    return mResult;
                }

                /* Process what we have retrieved. */
                record.friendlyName = friendlyName;
                record.serialNumber = serialNumber;
                record.UDN = udn;
                record.isMultiroomCapable = isMultiroomCapable;
                record.multiroomVersion = multiroomVersion;
                record.modelName = modelName;
                record.modelNumber = modelNumber;
                record.firmwareVersion = modelDescription;
                record.imageURL = imageURL;
                record.isMinuet = isMinuet;
                record.isVenice = isVenice;
                record.isSG = isSG;
                record.isStereoCapable = isStereoCapable;
                record.hasGoogleCast = hasGoogleCast;
                record.hasAVS = hasAVS;
                record.refreshTimeStamp();

                if (TextUtils.isEmpty(record.ID)) {
                    record.ID = getIDFromUDN(udn);
                }

                NetRemote.assertTrue(record.isComplete());

                mResult = IDeviceDetailsListener.EDeviceDetailsResult.SUCCESS;
            }
        }

        return mResult;
    }

    private static final String UDN_PREFIX_TO_REMOVE = "UUID:";
    private static final String MULTIROOM_CAPABLE_FEATURE_STR = "audiosync";
    private static final String MULTIROOM_CAPABLE_FEATURE_ALT_STR = "audsync";

    public String retrieveLargestIconURL(String mimetype, String iconName, Document doc) {
        String retUrl = null;
        int width = 0;

        if (doc != null) {
            // get al <icon> nodes
            NodeList iconNodes = doc.getElementsByTagName(iconName);

            for (int idx = 0; idx < iconNodes.getLength(); idx++) {

                Node currIconNode = iconNodes.item(idx);
                NodeList iconChildNodes = currIconNode.getChildNodes();

                boolean hasMimetype = false;
                int currWidth = 0;
                String currUrl = null;

                // get all child nodes for <icon> node (<mimetype>, <width>, <url>...)
                for (int childIdx = 0; childIdx < iconChildNodes.getLength(); childIdx++) {
                    Node currChildNode = iconChildNodes.item(childIdx);

                    if (currChildNode.getNodeName().equalsIgnoreCase("mimetype") && mimetype.equalsIgnoreCase(currChildNode.getTextContent())) {
                        hasMimetype = true;
                        continue;
                    }

                    if (currChildNode.getNodeName().equalsIgnoreCase("width")) {
                        currWidth = Integer.parseInt(currChildNode.getTextContent());
                        continue;
                    }

                    if (currChildNode.getNodeName().equalsIgnoreCase("url")) {
                        currUrl = currChildNode.getTextContent();
                        continue;
                    }
                }

                if (hasMimetype && currWidth > width){
                    //update the retIcon node
                    retUrl = currUrl;
                }

            }
        }

        return retUrl;
    }


    public String sanitizeUDN(String udn) {
        if (!TextUtils.isEmpty(udn)) {
            udn = udn.toUpperCase().replace(UDN_PREFIX_TO_REMOVE, "");
        }

        return udn;
    }

    public String sanitizeMultiroomVersion(String multiroomVersion) {
        if (!TextUtils.isEmpty(multiroomVersion)) {
            multiroomVersion = multiroomVersion.trim();
        }

        return multiroomVersion;
    }

    public boolean hasMultiroomCapability(String features) {
        boolean isMultiroomCap = false;
        if (!TextUtils.isEmpty(features)) {
            String lowerCaseFeatures = features.toLowerCase();
            isMultiroomCap = lowerCaseFeatures.contains(MULTIROOM_CAPABLE_FEATURE_STR) ||
                lowerCaseFeatures.contains(MULTIROOM_CAPABLE_FEATURE_ALT_STR);
        }
        return isMultiroomCap;
    }

    public static boolean isFirmwareInBlacklist(String firmware, String[] firmwareBlacklist) {
        if (firmware == null) {
            return false;
        }

        for (String blockedFirmwarePart : firmwareBlacklist) {
            if (firmware.contains(blockedFirmwarePart)) {
                return true;
            }
        }
        return false;
    }

    private String getIDFromUDN(String udn) {
        String id = "";

        // use the last segment of the UUID from UDN
        if (!TextUtils.isEmpty(udn)) {
            String[] udnSegments = udn.split(MINUS_DIVIDER);
            if (udnSegments.length > 0) {
                id = udnSegments[udnSegments.length-1];
                id = id.toUpperCase();
            }
        }

        return id;
    }

    private void notifyListenerOnDeviceDetailsRetrieved(DeviceRecord deviceRecord) {
        if (mDeviceDetailsListener != null) {
            mDeviceDetailsListener.onDeviceDetailsRetrieved(deviceRecord);
        }
    }

    private void notifyListenerOnDeviceDetailsError(DeviceRecord deviceRecord, IDeviceDetailsListener.EDeviceDetailsResult error) {
        if (mDeviceDetailsListener != null) {
            mDeviceDetailsListener.onDeviceDetailsError(deviceRecord, error);
        }
    }
}
