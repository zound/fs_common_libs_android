package com.frontier_silicon.NetRemoteLib.Discovery.deviceSerializer;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;

import java.util.List;

/**
 * Created by lsuhov on 08/11/2016.
 */

public interface IDeviceDetailsDeserializerListener {
    void onDeviceDetailsDeserialized(List<DeviceRecord> deviceRecordList);
}
