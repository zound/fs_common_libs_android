package com.frontier_silicon.NetRemoteLib.Discovery;

import com.frontier_silicon.NetRemoteLib.Discovery.deviceDetailsRetriever.DeviceDetailsRetrieverWithGetMultiple;
import com.frontier_silicon.NetRemoteLib.Node.BaseMultiroomDeviceListAll;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lsuhov on 14/11/2016.
 */

public class DevicesFromMultiroomDeviceListAllHelper {

    private Map<String, Boolean> mDeviceListAllItems = new HashMap<>(10);
    private boolean mUseGetMultipleForDeviceDetailsRetrieval = false;
    private boolean mAllowSpeakerToBeAddedFromDeviceListAll = true;

    void clear() {
        mDeviceListAllItems.clear();
    }

    void setUseGetMultipleForDeviceDetailsRetrieval(boolean useGetMultiple) {
        mUseGetMultipleForDeviceDetailsRetrieval = useGetMultiple;
    }

    boolean deviceWasAlreadyHandled(BaseMultiroomDeviceListAll.ListItem item) {
        return mDeviceListAllItems.get(item.getIPAddress()) != null;
    }

    boolean itemFromDeviceListAllIsValid(BaseMultiroomDeviceListAll.ListItem item) {
        if (!mUseGetMultipleForDeviceDetailsRetrieval) {
            return true;
        } else {
            return item.getAudioSyncVersion().equals(DeviceDetailsRetrieverWithGetMultiple.ZoundAudsyncVersion.toString()) ||
                    item.getAudioSyncVersion().equals(DeviceDetailsRetrieverWithGetMultiple.ZoundStereoAudsyncVersion.toString());
        }
    }

    DeviceRecord getDeviceRecord(BaseMultiroomDeviceListAll.ListItem item) {
        DeviceRecord scanDeviceRecord = new DeviceRecord();
        scanDeviceRecord.ID = item.getUDN();
        scanDeviceRecord.IP = item.getIPAddress();
        scanDeviceRecord.deviceInfoLocation = "http://" + item.getIPAddress() + ":8080/dd.xml";
        scanDeviceRecord.fsApiLocation = "http://" + item.getIPAddress() + ":80/fsapi";

        return scanDeviceRecord;
    }

    void registerItemForDeviceListAll(BaseMultiroomDeviceListAll.ListItem item) {
        mDeviceListAllItems.put(item.getIPAddress(), true);
    }

    public void allowSpeakerToBeAddedFromDeviceListAll(boolean allow) {
        mAllowSpeakerToBeAddedFromDeviceListAll = allow;
    }

    public boolean speakerIsAllowedToBeAddedFromDeviceListAll() {
        return mAllowSpeakerToBeAddedFromDeviceListAll;
    }
}
