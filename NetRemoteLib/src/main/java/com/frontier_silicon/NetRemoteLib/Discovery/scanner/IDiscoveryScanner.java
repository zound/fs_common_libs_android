package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

import com.frontier_silicon.NetRemoteLib.Discovery.ping.IPingDeviceRecordListener;
import com.frontier_silicon.NetRemoteLib.EventHelper;

/**
 * Created by viftime on 02/03/16.
 */
public interface IDiscoveryScanner extends IPingDeviceRecordListener{

    enum ScanError {
        NO_ERROR,
        NETWORK_ERROR,
        UNKNOWN_ERROR
    }

    interface IDiscoveryScannerListener {
        void onScannerStarted(ScanError error);
    }

    int DEVICE_DISAPPEARED_TIME_OUT = 40; /* seconds */

    void setEventHelper(EventHelper<Long, IScanDeviceAvailabilityListener> eventHelper, long index);

    void unregisterDeviceAvailabilityCallback(IScanDeviceAvailabilityListener callback);

    void registerDeviceAvailabilityCallback(boolean callbackOnMainThread, IScanDeviceAvailabilityListener callback);

    void stop();

    void start(IDiscoveryScannerListener listener, boolean singleScan);

    void close();

    void setVendorIDList(String[] vendorIDs);

    void clean();

    boolean speakerIsHandledByScanner(String ipAddress);
}
