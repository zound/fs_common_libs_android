package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

/**
 * Callback interface for scan state.
 *
 */
public interface IScanListener {

    /**
     *  Called when the discovery service scan failed
     */
    void onScanError(IDiscoveryScanner.ScanError error);

    /**
     *  Called when scan/restart started
     */
    void onScanStart();

}

