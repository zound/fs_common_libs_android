package com.frontier_silicon.NetRemoteLib.Discovery.deviceDetailsRetriever;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.LogLevel;

/**
 * Created by lsuhov on 10/11/2016.
 */

public class DeviceDetailsRetrieverRunnable implements Runnable {

    private DeviceRecord mDeviceRecord;
    private String[] mVendorIds;
    private String[] mFirmwareBlacklist;
    private boolean mUseGetMultipleForDeviceDetailsRetrieval;
    IDeviceDetailsListener mDeviceDetailsListener;

    public DeviceDetailsRetrieverRunnable(DeviceRecord deviceRecord, String[] vendorIds, String[] firmwareBlacklist,
                                          boolean useGetMultipleForDeviceDetailsRetrieval, IDeviceDetailsListener deviceDetailsListener) {
        mDeviceRecord = deviceRecord;
        mVendorIds = vendorIds;
        mFirmwareBlacklist = firmwareBlacklist;
        mUseGetMultipleForDeviceDetailsRetrieval = useGetMultipleForDeviceDetailsRetrieval;
        mDeviceDetailsListener = deviceDetailsListener;
    }

    @Override
    public void run() {
        NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "DeviceDetailsRetrieverRunnable start " + mDeviceRecord.deviceInfoLocation);

        if (mUseGetMultipleForDeviceDetailsRetrieval) {
            retrieveWithGetMultiple();
        } else {
            retrieveInfoFromDDXML();
        }
    }

    private void retrieveWithGetMultiple() {
        DeviceDetailsRetrieverWithGetMultiple detailsRetriever = new DeviceDetailsRetrieverWithGetMultiple();

        detailsRetriever.retrieveInfo(mDeviceRecord, mVendorIds, mFirmwareBlacklist, new IDeviceDetailsListener() {
            @Override
            public void onDeviceDetailsRetrieved(DeviceRecord deviceRecord) {
                if (mDeviceDetailsListener != null) {
                    mDeviceDetailsListener.onDeviceDetailsRetrieved(deviceRecord);
                }
                dispose();
            }

            @Override
            public void onDeviceDetailsError(DeviceRecord deviceRecord, EDeviceDetailsResult error) {
                if (mDeviceDetailsListener != null) {
                    mDeviceDetailsListener.onDeviceDetailsError(deviceRecord, error);
                }
                dispose();
            }
        });
    }

    void retrieveInfoFromDDXML() {
        DDXMLParser ddxmlRetriever = new DDXMLParser(mDeviceRecord, mVendorIds, mFirmwareBlacklist, new IDeviceDetailsListener() {
            @Override
            public void onDeviceDetailsRetrieved(DeviceRecord deviceRecord) {
                if (mDeviceDetailsListener != null) {
                    mDeviceDetailsListener.onDeviceDetailsRetrieved(deviceRecord);
                }
                dispose();
            }

            @Override
            public void onDeviceDetailsError(DeviceRecord deviceRecord, EDeviceDetailsResult error) {
                if (mDeviceDetailsListener != null) {
                    mDeviceDetailsListener.onDeviceDetailsError(deviceRecord, error);
                }
                dispose();
            }
        });

        ddxmlRetriever.run();
    }

    void dispose() {
        mDeviceRecord = null;
        mVendorIds = null;
        mFirmwareBlacklist = null;
        mDeviceDetailsListener = null;
    }
}
