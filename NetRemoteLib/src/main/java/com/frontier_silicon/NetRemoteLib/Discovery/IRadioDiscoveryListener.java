package com.frontier_silicon.NetRemoteLib.Discovery;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by lsuhov on 01/11/2016.
 */

public interface IRadioDiscoveryListener {

    /**
     * Called when a new radio is discovered.
     *
     * @param radio			The radio
     */
    void onRadioFound(Radio radio);

    /**
     * Called when previously visible radio is no longer visible on the network. Note that this is different
     * to when a radio is still visible, but the session ID has become invalidated (in that situation the
     * Radio.IConnectionCallback.onDisconnected callback is used).
     *
     * @param radio			The radio
     */
    void onRadioLost(Radio radio);


    /**
     * Called when an existing radio has updated information, for example a friendly name change.
     *
     * @param radio			The radio
     */
    void onRadioUpdated(Radio radio);
}
