package com.frontier_silicon.NetRemoteLib.Discovery;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

import java.util.List;

/**
 * Created by lsuhov on 01/11/2016.
 */

public interface IRadioListKeeper {
    List<Radio> getRadios();

    void cacheRadiosForFutureUse(boolean cacheRadios);

    boolean removeDeviceManually(Radio radio);

    boolean addListener(IRadioDiscoveryListener listener);

    boolean removeListener(IRadioDiscoveryListener listener);
}
