package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;

/**
 * Created by lsuhov on 12/05/2017.
 */

interface IFsapiLocationRetrieverListener {
    void onFsapiLocationRetrieved(DeviceRecord record);
}
