package com.frontier_silicon.NetRemoteLib.Discovery.ping;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by lsuhov on 09/11/2016.
 */

public interface IPingRadioListener {
    void onPingResult(Radio radio, boolean pingOk);
}
