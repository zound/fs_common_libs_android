package com.frontier_silicon.NetRemoteLib.Discovery.deviceSerializer;

/**
 * Created by lsuhov on 08/11/2016.
 */

public interface IDeviceDetailsSerializerListener {
    void onDeviceDetailsSaved();
}
