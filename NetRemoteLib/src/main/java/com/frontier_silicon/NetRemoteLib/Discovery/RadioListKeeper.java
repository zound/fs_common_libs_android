package com.frontier_silicon.NetRemoteLib.Discovery;

import android.content.Context;
import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.NetRemoteLib.Discovery.deviceDetailsRetriever.IDeviceDetailsListener;
import com.frontier_silicon.NetRemoteLib.Discovery.deviceSerializer.DeviceDetailsSerializerManager;
import com.frontier_silicon.NetRemoteLib.Discovery.deviceSerializer.IDeviceDetailsDeserializerListener;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttp;
import com.frontier_silicon.loggerlib.LogLevel;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by lsuhov on 31/10/2016.
 */
public class RadioListKeeper implements IRadioListKeeper, IDeviceDetailsListener {

    private Context mContext;
    final Map<String /* Serial Number */, Radio> mAllRadiosMap = new HashMap<>();
    private boolean mCacheRadios = false;
    private IDiscoveryService mDiscoveryService;
    private final List<IRadioDiscoveryListener> mListeners = new CopyOnWriteArrayList<>();
    private ScannedDevicesHandler mScannedDevicesHandler;
    private boolean mShowAlsoMissingSpeakers = false;

    public RadioListKeeper(Context context) {
        mContext = context;
    }

    public void setDiscoveryService(IDiscoveryService discoveryService) {
        mDiscoveryService = discoveryService;
    }

    public void setScannedDevicesHandler(ScannedDevicesHandler scannedDevicesHandler) {
        mScannedDevicesHandler = scannedDevicesHandler;
    }

    private boolean isDeviceInfoChanged(DeviceRecord newDeviceRecord, Radio oldRadio) {
        if (newDeviceRecord == null) {
            return false;
        }
        boolean isSame;

        isSame = (newDeviceRecord.friendlyName.equals(oldRadio.getFriendlyName()) &&
                !isIpAddressChanged(newDeviceRecord, oldRadio) &&
                newDeviceRecord.isMultiroomCapable == oldRadio.isMultiroomCapable());

        if (!TextUtils.isEmpty(newDeviceRecord.multiroomVersion)) {
            isSame &= newDeviceRecord.multiroomVersion.equals(oldRadio.getMultiroomVersion());
        }

        isSame &= newDeviceRecord.isVenice == oldRadio.getIsVenice();
        isSame &= newDeviceRecord.isMinuet == oldRadio.getIsMinuet();
        isSame &= newDeviceRecord.isStereoCapable == oldRadio.isStereoCapable();
        isSame &= newDeviceRecord.isSG == oldRadio.isSG();
        isSame &= newDeviceRecord.hasGoogleCast == oldRadio.hasGoogleCast();
        isSame &= newDeviceRecord.hasAVS == oldRadio.hasAVS();

        if (!TextUtils.isEmpty(newDeviceRecord.firmwareVersion)) {
            isSame &= newDeviceRecord.firmwareVersion.equals(oldRadio.getFirmwareVersion());
        }

        return !isSame;
    }

    private boolean isIpAddressChanged(DeviceRecord newDeviceRecord, Radio oldRadio) {
        boolean isSame = true;

        try {
            URI apiUri = new URI(newDeviceRecord.fsApiLocation);
            String newIP = apiUri.getHost();

            isSame = newIP.equals(oldRadio.getIpAddress()) &&
                    newDeviceRecord.deviceInfoLocation.equals(oldRadio.getDDXMLLocation());

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return !isSame;
    }

    private Radio createRadioObject(DeviceRecord record) {
        Radio radio = new RadioHttp(record);

        radio.setIsCheckingAvailability(false);
        radio.setIsAvailable(true);

        return radio;
    }

    public void cleanup() {

        for (Radio radio : getRadios()) {
            NetRemote.log(LogLevel.Info, "RadioListKeeper cleanup: onRadioLost for radio " + radio.getFriendlyName());

            synchronized (mAllRadiosMap) {
                mAllRadiosMap.remove(radio.getSN());
            }

            notifyListenersOnRadioLost(radio);
        }
    }


    @Override
    public List<Radio> getRadios() {
        synchronized (mAllRadiosMap) {
            return new ArrayList<>(mAllRadiosMap.values());
        }
    }

    @Override
    public void cacheRadiosForFutureUse(boolean cacheRadios) {
        mCacheRadios = cacheRadios;
    }

    public void setShowAlsoMissingSpeakers(boolean showAlsoMissingSpeakers) {
        mShowAlsoMissingSpeakers = showAlsoMissingSpeakers;
    }

    public void addDeviceManually(DeviceRecord record) {

        Radio foundRadio;
        synchronized (mAllRadiosMap) {
            foundRadio = mAllRadiosMap.get(record.serialNumber);
        }

        if (foundRadio == null) {

            Radio radio = new RadioHttp(record);

            radio.setIsCheckingAvailability(true);
            radio.setIsAvailable(false);

            synchronized (mAllRadiosMap) {
                mAllRadiosMap.put(record.serialNumber, radio);
            }

            notifyListenersOnRadioFound(radio);

            mScannedDevicesHandler.onScanDeviceAvailable(record);
        }
    }

    @Override
    public boolean removeDeviceManually(Radio radio) {
        boolean removed = false;

        Radio foundRadio;
        synchronized (mAllRadiosMap) {
			/* Find the radio object. */
            foundRadio = mAllRadiosMap.get(radio.getSN());
        }

        if (foundRadio != null) {
            // Remove from radios list
            synchronized (mAllRadiosMap) {
                mAllRadiosMap.remove(radio.getSN());
            }

            // save radios list to JSON file
            saveAllRadios();

            // notify listeners that radio is lost
            if (mDiscoveryService.isRunning()) {
                notifyListenersOnRadioLost(radio);
            }

            removed = true;
        } else {
            NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error, "RadioListKeeper: removeDeviceManually no radio record for " + radio);
        }

        return removed;
    }

    @Override
    public boolean addListener(IRadioDiscoveryListener listener) {
        return !mListeners.contains(listener) && mListeners.add(listener);
    }

    @Override
    public boolean removeListener(IRadioDiscoveryListener listener) {
        return mListeners.remove(listener);
    }

    public void saveAllRadios() {
        if (!mCacheRadios)
            return;

        DeviceDetailsSerializerManager.getInstance().saveAllRadios(mContext, AccessPointUtil.getSSIDOfCurrentConnection(), getRadios(), null);
    }

    public void restoreAllRadios(Context context, String wifiSSID) {
        if (!mCacheRadios)
            return;

        DeviceDetailsSerializerManager.getInstance().loadDeviceRecords(context, wifiSSID, new IDeviceDetailsDeserializerListener() {
            @Override
            public void onDeviceDetailsDeserialized(List<DeviceRecord> deviceRecordList) {

                for (DeviceRecord record : deviceRecordList) {
                    if (record.serialNumber != null && record.fsApiLocation != null) {

                        addDeviceManually(record);
                    }
                }
            }
        });
    }

    public List<Radio> getAvailableRadios() {
        List<Radio> availableRadios = new ArrayList<>();

        for (Radio radio : getRadios()) {
            if (radio.isAvailable()) {
                availableRadios.add(radio);
            }
        }

        return availableRadios;
    }

    public void notifyListenersAboutExistingRadios() {
        // See if there are any callbacks wanting to know about existing devices?
        List<Radio> radios = getRadios();
        for (Radio radio : radios) {
            notifyListenersOnRadioFound(radio);
        }
    }

    void notifyListenersOnRadioLost(Radio radio) {
        for (IRadioDiscoveryListener listener : mListeners) {
            listener.onRadioLost(radio);
        }
    }

    void notifyListenersOnRadioFound(Radio radio) {
        for (IRadioDiscoveryListener listener : mListeners) {
            listener.onRadioFound(radio);
        }
    }

    void notifyListenersOnRadioUpdated(Radio radio) {
        for (IRadioDiscoveryListener listener : mListeners) {
            listener.onRadioUpdated(radio);
        }
    }

    @Override
    public void onDeviceDetailsRetrieved(DeviceRecord record) {
        Radio radio;
        synchronized (mAllRadiosMap) {
			/* See if we already have a radio for this device? */
            radio = mAllRadiosMap.get(record.serialNumber);
        }

        if (radio == null) {
            /* No, so need to create one. */
            radio = createRadioObject(record);

            /* Add to all radios. */
            if (radio != null) {
                synchronized (mAllRadiosMap) {
                    mAllRadiosMap.put(radio.getSN(), radio);
                }

                //https://jira.toumazgroup.com/browse/UNDC-742
                AccessPointUtil.setTdlsEnabled(radio.getIpAddress(), false);
            }

            /* Call callbacks. */
            if (mDiscoveryService.isRunning()) {
                notifyListenersOnRadioFound(radio);
            }

            saveAllRadios();

        } else {
            boolean isDeviceChanged = isDeviceInfoChanged(record, radio);
            boolean shouldUpdate = (!radio.isAvailable() || radio.isCheckingAvailability() || isDeviceChanged);

            radio.storeDeviceRecord(record);

            radio.setIsAvailable(true);
            radio.setIsCheckingAvailability(false);

            if (shouldUpdate) {
                NetRemote.log(LogLevel.Warning, "RadioListKeeper: onScanDeviceAvailable UPDATE " + radio);

                if (mDiscoveryService.isRunning()) {
                    radio.applyDataFromDeviceRecord(record);

                    //https://jira.toumazgroup.com/browse/UNDC-742
                    AccessPointUtil.setTdlsEnabled(radio.getIpAddress(), false);

                    notifyListenersOnRadioUpdated(radio);

                    if (isDeviceChanged) {
                        saveAllRadios();
                    }
                } else {
                    NetRemote.log(LogLevel.Error, "RadioListKeeper: radio is " +
                            "changed but we are not saving it because scan is not running. " + radio);
                }
            }
        }
    }

    @Override
    public void onDeviceDetailsError(DeviceRecord record, EDeviceDetailsResult error) {
        Radio radio;

        synchronized (mAllRadiosMap) {
			/* Find the radio object. */
            radio = this.mAllRadiosMap.get(record.serialNumber);
        }

        if (radio != null) {

            /* Remove from all radios. */
            if (!mShowAlsoMissingSpeakers) {
                synchronized (mAllRadiosMap) {
                    mAllRadiosMap.remove(record.serialNumber);
                }
                saveAllRadios();
            }

            radio.setIsAvailable(false);
            radio.setIsCheckingAvailability(false);

            /* Call callbacks. */
            if (mDiscoveryService.isRunning()) {
                notifyListenersOnRadioLost(radio);
            }
        } else {
            NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error, "Got onScanDeviceDisappeared event, but no radio record for " + record.friendlyName);
        }
    }
}
