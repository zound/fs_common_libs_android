package com.frontier_silicon.NetRemoteLib.Discovery.ping;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;

/**
 * Created by lsuhov on 04/11/2016.
 */

public interface IPingDeviceRecordListener {
    void onPingResult(DeviceRecord deviceRecord, boolean pingOk);
}
