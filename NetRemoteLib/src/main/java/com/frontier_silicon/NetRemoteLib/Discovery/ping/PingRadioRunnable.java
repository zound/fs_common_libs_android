package com.frontier_silicon.NetRemoteLib.Discovery.ping;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by lsuhov on 09/11/2016.
 */

public class PingRadioRunnable implements Runnable {

    private Radio mRadio;
    private IPingRadioListener mListener;

    public PingRadioRunnable(Radio radio, IPingRadioListener listener) {
        mRadio = radio;
        mListener = listener;
    }

    @Override
    public void run() {
        PingUtil pingUtil = new PingUtil();
        boolean isAvailable = pingUtil.isRadioAvailable(mRadio);

        mListener.onPingResult(mRadio, isAvailable);

        dispose();
    }

    private void dispose() {
        mRadio = null;
        mListener = null;
    }
}
