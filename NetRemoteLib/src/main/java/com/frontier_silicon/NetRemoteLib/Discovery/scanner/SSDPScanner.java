/******************************************************************************
 * Copyright 2015 by Frontier Silicon Ltd. All rights reserved.
 *
 * No part of this software, either material or conceptual may be copied or
 * distributed, transmitted, transcribed, stored in a retrieval system or
 * translated into any human or computer language in any form by any means,
 * electronic, mechanical, manual or otherwise, or disclosed to third parties
 * without the express written permission of Frontier Silicon Ltd.
 * 137 Euston Road, London, NW1 2AA.
 ******************************************************************************/
package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.EventHelper;
import com.frontier_silicon.NetRemoteLib.EventHelper.CallCallbackCodeFragment;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.LogLevel;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.DatagramChannel;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *  New SSDP scanner: issue a single M_SEARCH request and then listen for alive and byebye notifications
 * @author viftime
 *
 */
public class SSDPScanner implements IDiscoveryScanner {

	//private final static boolean DETAILED_TRACING_ENABLED = false;
	static final String BROADCAST_ADDRESS = "239.255.255.250";
	static final int PORT = 1900;
	static final int PORT_BROADCAST = 1900;
	private static final int RECV_MESSAGE_BUFSIZE = 1024;

	private static final String M_SEARCH_PACKET_IGNORE = "M-SEARCH"; /* ignore m-search packets from other devices */

	private String[] VENDOR_IDS = {
			"undok",
			/*"fs_reference"*/
	};

	private String RequiredServiceTemplatePrefix = "urn:schemas-frontier-silicon-com:";
	private String RequiredServiceTemplateSuffix = ":fsapi:1";
	private String RequiredServiceTemplate = RequiredServiceTemplatePrefix + "%s" + RequiredServiceTemplateSuffix;
	private String[] SearchQueries;

	EventHelper<Long, IScanDeviceAvailabilityListener> Callbacks = null;
    long EventIndex = 0;

	final Map<String /* USN */, DeviceRecord> AllScanDeviceRecords = new HashMap<>(20);
    final Map<String /* IP*/, DeviceRecord> ScanDeviceRecordsByIP = new HashMap<>(20);

    private ThreadPoolExecutor mScanStarterThreadPoolExecutor;
    private ThreadPoolExecutor mFsapiRetrieverFromUpnpRootThreadPoolExecutor;

    final List<InetAddress> AllInterfaceAddresses = new ArrayList<>();
    final List<Listener> AllListeners = new ArrayList<>();
    final Map<DatagramSocket, InetAddress> AllSockets = new HashMap<>();

    SSDPSearcher CurrentSearcher = null;
    private boolean mUseUpnpRootForSearch = true;

    public SSDPScanner() {
		initSearchQueries();
        initThreadPool();
	}

    private void initThreadPool() {
        mScanStarterThreadPoolExecutor = new ThreadPoolExecutor(0/*corePoolSize*/, 1/*maxPoolSize*/, 1/*KEEP_ALIVE_TIME*/,
                TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(50), new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error,
                        "SSDPScanner mScanStarterThreadPoolExecutor: thread execution was rejected");
            }
        });

        mFsapiRetrieverFromUpnpRootThreadPoolExecutor = new ThreadPoolExecutor(0/*corePoolSize*/, 1/*maxPoolSize*/, 1/*KEEP_ALIVE_TIME*/,
                TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(50), new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error,
                        "SSDPScanner mFsapiRetrieverFromUpnpRootThreadPoolExecutor: thread execution was rejected");
            }
        });
    }

    private void initSearchQueries() {
		SearchQueries = new String[VENDOR_IDS.length];

		for (int i = 0; i < VENDOR_IDS.length; i++) {
			String vendorId = VENDOR_IDS[i];
			String searchQuery = String.format(RequiredServiceTemplate, vendorId);

			SearchQueries[i] = searchQuery;
		}
	}

    @Override
    public void setEventHelper(EventHelper<Long, IScanDeviceAvailabilityListener> eventHelper, long index) {
        Callbacks = eventHelper;
        EventIndex = index;
    }

	@Override
	public void unregisterDeviceAvailabilityCallback(IScanDeviceAvailabilityListener callback) {
		this.Callbacks.deregisterCallback(EventIndex, callback);
	}

	@Override
	public void registerDeviceAvailabilityCallback(boolean callbackOnMainThread, IScanDeviceAvailabilityListener callback) {
		this.Callbacks.registerCallback(EventIndex, callbackOnMainThread, callback);
	}

	@Override
	public void start(IDiscoveryScannerListener callback, boolean singleScan) {
		NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP: start");

		/* Out with the old. */
		PauseHelper();


        mScanStarterThreadPoolExecutor.submit(new StartSSDPRunnable(callback));
	}

    public void setUseUpnpRootDeviceQueryInSsdp(boolean useUpnpRoot) {
        mUseUpnpRootForSearch = useUpnpRoot;
    }

    @Override
    public void onPingResult(DeviceRecord deviceRecord, boolean pingOk) {
        if (!pingOk && deviceRecord != null) {
            AllScanDeviceRecords.remove(deviceRecord.ID);
            ScanDeviceRecordsByIP.remove(deviceRecord.IP);
        }
    }

    private class StartSSDPRunnable implements Runnable {

        private IDiscoveryScannerListener mCallback;

        StartSSDPRunnable(IDiscoveryScannerListener callback) {
            mCallback = callback;
        }

        @Override
        public void run() {
            NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "Start SSDP");

            /* Enumerate network interfaces. */
            synchronized (AllInterfaceAddresses) {
                AllInterfaceAddresses.clear();
                try {
                    /* Get all interfaces. */
                    Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
                    while (nis.hasMoreElements()){
                        NetworkInterface ni = nis.nextElement();

                        /* Get all addresses. */
                        Enumeration<InetAddress> addrs = ni.getInetAddresses();
                        while (addrs.hasMoreElements()) {
                            InetAddress addr = addrs.nextElement();

                            /* Is this an address we can use? */
                            if (isAddressValid(addr)) {
                                AllInterfaceAddresses.add(addr);
                                NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP: Found interface address: " + addr.toString());
                            }
                        }
                    }
                } catch (Exception e) {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error, "SSDP: Could not enumerate interfaces - " + e.toString());
                    PauseHelper();

                    if (mCallback != null) {
                        mCallback.onScannerStarted(ScanError.NETWORK_ERROR);
                        mCallback = null;
                        return;
                    }
                }

                /* Create a socket on each address, and start a listener. */
                synchronized (AllListeners) {
                    synchronized (AllSockets) {
                        int count = 0;
                        for (InetAddress addr : AllInterfaceAddresses) {
                            try {
                                DatagramChannel channel = DatagramChannel.open();
                                DatagramSocket socket = channel.socket();
                                socket.setReuseAddress(true);
                                InetSocketAddress saddr = new InetSocketAddress(addr, SSDPScanner.PORT);
                                socket.bind(saddr);

                                Listener listener = new Listener(count++, socket);
                                AllListeners.add(listener);
                                AllSockets.put(socket, addr);
                                NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, "SSDP " + (count-1) + " : Socket opened for " + addr + ":" + socket.getLocalPort() + ".");
                            } catch (Exception e) {
                                NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error, "SSDP: Could not create listening socket for " + addr + ":" + SSDPScanner.PORT + " - " + e.toString());
                                PauseHelper();

                                if (mCallback != null) {
                                    mCallback.onScannerStarted(ScanError.NETWORK_ERROR);
                                    mCallback = null;
                                    return;
                                }
                            }
                        }

                        /* open multicast socket for SSDP alive/byebye notif. */
                        try {
                            MulticastSocket socket = new MulticastSocket(SSDPScanner.PORT_BROADCAST);
                            socket.joinGroup(InetAddress.getByName(SSDPScanner.BROADCAST_ADDRESS));

                            Listener listener = new Listener(count++, socket);
                            AllListeners.add(listener);
                            AllSockets.put(socket, InetAddress.getByName(SSDPScanner.BROADCAST_ADDRESS));

                            NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, "SSDP " + (count-1) + " : MulticastSocket opened for " + SSDPScanner.BROADCAST_ADDRESS + ":" + SSDPScanner.PORT);

                        } catch (IOException e) {
                            NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error, "SSDP: Could not create multicast socket " + e.toString());
                        }

                    }
                }
            }

            synchronized (SSDPScanner.this) {
                NetRemote.assertTrue(CurrentSearcher == null);
                CurrentSearcher = new SSDPSearcher(SSDPScanner.this, SearchQueries, mUseUpnpRootForSearch);
            }

            if (mCallback != null) {
                mCallback.onScannerStarted(ScanError.NO_ERROR);
                mCallback = null;
            }
        }
    }

    private class PauseSSDPRunnable implements Runnable {

        PauseSSDPRunnable() {
        }

        @Override
        public void run() {
            NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "pause SSDP");

            synchronized (AllInterfaceAddresses) {
                synchronized (AllListeners) {
                    synchronized (AllSockets) {
                        /* Stop the searcher. */
                        synchronized (SSDPScanner.this) {
                            if (CurrentSearcher != null) {
                                CurrentSearcher.Abort();
                                CurrentSearcher = null;
                            }
                        }

                        /* Close all sockets (under the feet of the listeners. */
                        for (DatagramSocket socket : AllSockets.keySet()) {
                            if (!socket.isClosed()) {
                                try {
                                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, "SSDP: Socket for " + socket.getLocalAddress() + ":" + socket.getLocalPort() + " closing.");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                socket.close();
                            }
                        }

                        /* Abort all listeners, which will also abort their name finders. */
                        for (Listener listener : AllListeners) {
                            listener.Abort();
                        }

                        AllListeners.clear();
                        AllInterfaceAddresses.clear();
                        AllSockets.clear();
                    }
                }
            }

            clearAllDeviceRecords();
        }
    }

    boolean isAddressValid(InetAddress iNetAddress) {
        if (!(iNetAddress instanceof Inet4Address)) {
            return false;
        }

        String addressString = iNetAddress.getHostAddress();
        if (addressString.contentEquals("127.0.0.1")) {
            return false;
        }

        return true;
    }

	public boolean IsSearching() {
		return (this.CurrentSearcher != null);
	}


    boolean stCorrespondsWithOneSearchedVendorIds(String st) {
		if (!TextUtils.isEmpty(st)) {

            for (String searchQuery : SearchQueries) {
				if (st.equals(searchQuery))
					return true;
			}
		}

		return false;
	}

	public void stop() {
		NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP: pause()");
		this.PauseHelper();
	}

	void PauseHelper() {
        mScanStarterThreadPoolExecutor.submit(new PauseSSDPRunnable());
	}

	void clearAllDeviceRecords() {
		synchronized (this.AllScanDeviceRecords) {
			this.AllScanDeviceRecords.clear();
            this.ScanDeviceRecordsByIP.clear();
		}
	}

	@Override
	public void close() {
		this.Callbacks.clearAllCallbacks();

		this.PauseHelper();

		clearAllDeviceRecords();
	}

	private class Listener implements Runnable {
		private static final String HTTP_HEADER_KEY = "HTTP";
		private static final String OK_HEADER_VALUE = " OK";
		private static final String NOTIFY_PACKET_KEY = "NOTIFY";
		private static final String NTS_KEY = "NTS";
		private static final String NT_KEY = "NT";
		private static final String USN_KEY = "USN";
		private static final String ST_KEY = "ST";
		private static final String LOCATION_KEY = "LOCATION";
		private static final String SPEAKER_NAME_KEY = "SPEAKER-NAME";
		private static final String UUID_TOKEN = "uuid";
		private static final String SSDP_BYEBYE_TOKEN = "ssdp:byebye";
		private static final String SSDP_ALIVE_TOKEN = "ssdp:alive";
		private static final String UPNP_TOKEN = "upnp";
		private static final String ROOTDEVICE_TOKEN = "rootdevice";
		private static final String COLON_DIVIDER = ":";
		private static final String MINUS_DIVIDER = "-";
		private static final String NEWLINE_DIVIDER = "\n";

		private boolean HasBeenAborted = false; // Kill off this instance.

		private Long InstanceNumber;
		private Thread WorkerThread;
		private DatagramSocket Socket;

		public void Abort() {
			/* Stop the worker thread. */
			this.HasBeenAborted = true;
			this.WorkerThread.interrupt();
            this.Socket.close();
		}

		public Listener(int instanceCount, DatagramSocket socket) {
			this.InstanceNumber = (long) instanceCount;
			this.Socket = socket;
			this.WorkerThread = new Thread(this, ("SSDP Listener #" + this.InstanceNumber.toString()));
			this.WorkerThread.start();

			NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): Listener created.");
		}

		public void run() {
			NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): Listener Starting");

			byte recvBuf[] = new byte[SSDPScanner.RECV_MESSAGE_BUFSIZE];
			DatagramPacket pack = new DatagramPacket(recvBuf, recvBuf.length);

			while (!(this.HasBeenAborted)) {
				try {
					/* Block waiting for next packet. */
					this.Socket.receive(pack);
				} catch (Exception e) {
					NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, (e.toString().contains("Socket closed") || e.toString().contains("Socket is closed"))?LogLevel.Warning:LogLevel.Error,
								         "SSDP(" + this.InstanceNumber + "): Aborting listener: receive failed - " + e.toString());
					break;
				}

				if (this.HasBeenAborted) {
					break;
				}

				InetAddress remoteAddr = pack.getAddress();
				NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info,  "SSDP(" + this.InstanceNumber + "): Received search response from " + remoteAddr.toString() + ".");

				/* Don't do any processing if the record is already complete. */
				if (pack.getLength() > 0) {
					String packetData = new String(pack.getData(), 0, pack.getLength());
					/* We should get a response packet which looks something like this:
						 		HTTP/1.1 200 OK
								CACHE-CONTROL: max-age=1800
								EXT:
								LOCATION: http://192.168.1.99:80/device
								POSIX, UPnP/1.0, Intel MicroStack/1.0.2777
								ST: urn:schemas-frontier-silicon-com:fs_reference:fsapi:1
								USN: uuid:46c19e4a-472b-11e1-9f67-002261000258::urn:schemas-frontier-silicon-com:fs_reference:fsapi:1
					 */

					if (!packetData.startsWith(M_SEARCH_PACKET_IGNORE)) {

                        NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): " + packetData);

						/* Split into lines. */
						String[] lines = packetData.split(NEWLINE_DIVIDER);
						Map<String, String> tokens = this.Tokenize(lines, true);

						/* Basic sanity check on reply. */
						if (lines.length < 1) {
							NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "SSDP(" + this.InstanceNumber + "): Packet was empty.");
							continue;
						}

						String http = lines[0];

						if (!http.startsWith(HTTP_HEADER_KEY) || !http.endsWith(OK_HEADER_VALUE)) {
							if (http.startsWith(NOTIFY_PACKET_KEY)) {
                                NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + ") parsing notify packet");
								parseNotifyPacket(packetData, http, tokens);
							} else {
								NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Warning, "SSDP(" + this.InstanceNumber + "): HTTP status was not OK: " + http);
							}
						} else {
							NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + ") Search REPLY ST:" +  tokens.get(ST_KEY));
                            NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + ") parsing search packet");

							parseSearchPacket(tokens);
						}
					} else {
                        NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): Packet is MSEARCH");
						NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): " + packetData);
					}

				} else {
					NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "SSDP(" + this.InstanceNumber + "): Packet was zero length.");
				}
			}

			NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): Listener Ending.");
			if (this.Socket.isClosed()) {
				NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): No need to close socket, it was done for us.");
			} else {
                try {
                    NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): Listener is having to close socket for " + this.Socket.getLocalAddress() + ":" + this.Socket.getLocalPort() + ".");
                } catch (Exception e) {
                    e.printStackTrace();
                }
				this.Socket.close();
			}
			this.Socket = null;
		}

		private String getIDFromUSN(String usn) {
			String id = "";

			// use the last segment of the UUID from USN
			if (!TextUtils.isEmpty(usn)) {
				String[] usnTokens = usn.split(COLON_DIVIDER);
				if (usnTokens.length >= 2) {
					if (usnTokens[0].equalsIgnoreCase(UUID_TOKEN)){
						String uuid = usnTokens[1];
						if (!TextUtils.isEmpty(uuid)) {
							String[] uuidParts = uuid.split(MINUS_DIVIDER);
							id = uuidParts[uuidParts.length-1];
							id = id.toUpperCase();
						}
					}
				}
			}

			return id;
		}

		private void parseNotifyPacket(String packetData, String firstLine, Map<String, String> tokens) {
			if (firstLine.startsWith(NOTIFY_PACKET_KEY)) {
				String notificationType = tokens.get(NTS_KEY);

				if (notificationType != null) {
					if (notificationType.equalsIgnoreCase(SSDP_BYEBYE_TOKEN)) {

						String nt = tokens.get(NT_KEY);

						if (stCorrespondsWithOneSearchedVendorIds(nt)) {
							String usn = tokens.get(USN_KEY);

							NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP: Notify BYE BYE: " + usn);

							//NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP BYEBYE:\n" + packetData);

                            handleByeBye(tokens);
						} else {
							//NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "Ignore SSDP BYEBYE:\n" + packetData);
						}

					} else if (notificationType.equalsIgnoreCase(SSDP_ALIVE_TOKEN)) {
						NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP: Notify ALIVE: " + tokens.get(USN_KEY));

						//NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP ALIVE:\n" + packetData);

						tokens.put(ST_KEY, tokens.get(NT_KEY));
						parseSearchPacket(tokens);
					}
				}
			}
		}

		private boolean parseSearchPacket(Map<String, String> tokens) {
			/* Try to read parameters of interest. */
			String st = tokens.get(ST_KEY);
			String location = tokens.get(LOCATION_KEY);

			/* Use UUID fromUSN as ID */
			String id = getIDFromUSN(tokens.get(USN_KEY));

			if (st == null) {
				NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "SSDP(" + this.InstanceNumber + "): Packet did not contain ST field.");
				return false;
			}

			if (location == null) {
				NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Warning, "SSDP(" + this.InstanceNumber + "): Packet did not contain LOCATION field.");
				return false;
			}

            URI locationUri;
            try {
                locationUri = new URI(location);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Error, "SSDP(" + this.InstanceNumber + "): Packet did not contain valid LOCATION: " + location);

                return false;
            }

			synchronized (AllScanDeviceRecords) {
				/* Do we have an existing device record for this device? */
				DeviceRecord record = AllScanDeviceRecords.get(id);

                if (record == null) {
                    record = ScanDeviceRecordsByIP.get(locationUri.getHost());
                }

				if (record == null) {
					record = new DeviceRecord();
				}
                record.IP = locationUri.getHost();

				/* Begin new, or update, record. */
				synchronized (record) {
					/* See if it's a reply to the "all" search. */
					String[] stTokens = st.split(COLON_DIVIDER);
					if ((stTokens.length == 2) && (stTokens[0].equalsIgnoreCase(UPNP_TOKEN)) && (stTokens[1].equalsIgnoreCase(ROOTDEVICE_TOKEN))) {
						/* Add info. */
						record.ID = id;

                        if (location.endsWith("dd.xml")) {
                            record.deviceInfoLocation = location;

                        } else if (location.endsWith("/device") && TextUtils.isEmpty(record.fsApiLocation)) {
                            mFsapiRetrieverFromUpnpRootThreadPoolExecutor.submit(
                                    new FsapiLocationRetrieverRunnable(location, record, new IFsapiLocationRetrieverListener() {
                                        @Override
                                        public void onFsapiLocationRetrieved(DeviceRecord record) {
                                            callDeviceAvailableIfNeeded(record);
                                        }
                                    }));
                        }

                        /* See if it's a reply to the radio specific search. */
					} else if (SSDPScanner.this.stCorrespondsWithOneSearchedVendorIds(st)) {
						/* Add info. */
						record.ID = id;
						record.vendorID = extractVendorID(st);

                        try {
                            URI apiUri = new URI(location);
                            apiUri = new URI("http://" + apiUri.getAuthority() + "/fsapi");
                            record.fsApiLocation = apiUri.toString();
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }

						// mark as incomplete when packet is ssdp:alive => trigger dd.xml read (maybe smth changed)
						String notifType = tokens.get(NTS_KEY);
						if (SSDP_ALIVE_TOKEN.equalsIgnoreCase(notifType)) {
							record.invalidated = true;
						}

						// This command was responsible for Roberts 83i not shown
						if (record.deviceInfoLocation == null) {

                            //We assume that if the FSAPI port is 80, then the port of dd.xml must be 8080
                            int port = locationUri.getPort();
                            if (port == 80) {
                                record.deviceInfoLocation = getDefaultDDXMLLocation(location);
                            }
						}
					} else {
						// not an "upnp:root" or ":fsapi" packet => ignore
                        NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Warning, "SSDP(" + this.InstanceNumber + "): returning because the packet is not compatible");
						return false;
					}

					callDeviceAvailableIfNeeded(record);

					/* Save the new or updated device record. */
					SSDPScanner.this.AllScanDeviceRecords.put(id, record);
                    SSDPScanner.this.ScanDeviceRecordsByIP.put(record.IP, record);
					NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Warning, "SSDP(" + this.InstanceNumber + "): Now have " + SSDPScanner.this.AllScanDeviceRecords.size() + " device records.");
				}
			}

			return true;
		}

		void callDeviceAvailableIfNeeded(DeviceRecord record) {
            /* Have we had both the root and fsapi response, ie. we're now ready to look up the name. */
            if (record.isReadyToLookupName() && record.invalidated) {
                /* Go off and find out the friendly name. */
                final DeviceRecord finalRecord = record;
                Callbacks.callCallbacks(EventIndex, new EventHelper.CallCallbackCodeFragment() {
                    public void run(Object callbackObject, Object tag) {
                        IScanDeviceAvailabilityListener callback = (IScanDeviceAvailabilityListener)callbackObject;
                        callback.onScanDeviceAvailable(finalRecord);
                    }
                });
                record.invalidated = false;
                NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): Record ID: " + record.ID + ", with IP: " + record.IP + " passed to upper layer");
            } else {
                NetRemote.log(NetRemote.TRACE_DISCOVERY_DEBUG_BIT, LogLevel.Info, "SSDP(" + this.InstanceNumber + "): Record ID: " + record.ID + ", with IP: " + record.IP + " not passed to upper layer");
            }
        }

        void handleByeBye(Map<String, String> tokens) {

			/* Use UUID fromUSN as ID */
            String id = getIDFromUSN(tokens.get(USN_KEY));

            final DeviceRecord record;
            synchronized (SSDPScanner.this.AllScanDeviceRecords) {

                record = SSDPScanner.this.AllScanDeviceRecords.get(id);

                if (record != null) {
                    SSDPScanner.this.AllScanDeviceRecords.remove(record.ID);
                    SSDPScanner.this.ScanDeviceRecordsByIP.remove(record.IP);
                }
            }

            if (record != null) {
                SSDPScanner.this.Callbacks.callCallbacks(EventIndex, new CallCallbackCodeFragment() {
                    public void run(Object callbackObject, Object tag) {
                        IScanDeviceAvailabilityListener callback = (IScanDeviceAvailabilityListener) callbackObject;
                        callback.onScanDeviceDisappeared(record);
                    }
                });

                NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "SSDP Device disapperead with BYE BYE " + record.ID);
            }
        }

		private String getDefaultDDXMLLocation(String deviceLocation) {
			String ddXMLLocation = null;

			try {
				URI apiUri = new URI(deviceLocation);
				apiUri = new URI("http://" + apiUri.getHost() + ":8080/dd.xml");
				ddXMLLocation = apiUri.toString();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}

			return ddXMLLocation;
		}

		private Map<String, String> Tokenize(String[] lines, boolean sanitise) {
			Map<String, String> result = new HashMap<>();

			for (int i = 0; i < lines.length; i++) {
                String line = lines[i];
				String tidy = line.trim();
                lines[i] = tidy;

				if (tidy.length() > 0) {
					int colonPos = tidy.indexOf(COLON_DIVIDER);
					String key;
					String value = "";
					if (colonPos < 1) {
						key = tidy;
					} else {
						key = tidy.substring(0, colonPos);
						value = tidy.substring(colonPos + 1);
					}

					if (sanitise) {
						//key = key.toLowerCase();
						key = key.trim();
						value = value.trim();
					}

					if (!(result.containsKey(key))) {
						result.put(key, value);
					}
				}
			}

			return result;
		}
	}

	String extractVendorID(String st) {
		String vendorID = "";

		if (!TextUtils.isEmpty(st)) {
			if (st.startsWith(RequiredServiceTemplatePrefix) && st.endsWith(RequiredServiceTemplateSuffix)) {
				int suffixIDX = st.indexOf(RequiredServiceTemplateSuffix);
				vendorID = st.substring(RequiredServiceTemplatePrefix.length(), suffixIDX);
			}
		}

		return vendorID;
	}

    @Override
	public void setVendorIDList(String[] vendorIDs) {
		VENDOR_IDS = vendorIDs;
		initSearchQueries();
	}

    @Override
    public void clean() {
        clearAllDeviceRecords();
    }

    @Override
    public boolean speakerIsHandledByScanner(String ipAddress) {
        DeviceRecord scanRecord = ScanDeviceRecordsByIP.get(ipAddress);

        return scanRecord != null;
    }
}
