package com.frontier_silicon.NetRemoteLib.Discovery;

import android.content.Context;

import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IDiscoveryScanner;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanDeviceAvailabilityListener;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanListener;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanOnDemandListener;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.JMDNSScanner;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.SSDPScanner;
import com.frontier_silicon.NetRemoteLib.EventHelper;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.BaseMultiroomDeviceListAll;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by lsuhov on 23/03/16.
 */
public class UnifiedDiscoveryService implements IDiscoveryService {

    private Context mContext;
    RadioListKeeper mRadioListKeeper;
    private ScannedDevicesHandler mScannedDevicesHandler = null;
    List<IDiscoveryScanner> mScanners = new ArrayList<>();
    private EventHelper<Long, IScanDeviceAvailabilityListener> mScannerCallbacks = new EventHelper<>();
    boolean mIsScanRunning = false;

    private List<IScanListener> mListeners = new CopyOnWriteArrayList<>();
    private DevicesFromMultiroomDeviceListAllHelper mDevicesFromMultiroomDeviceListAllHelper = new DevicesFromMultiroomDeviceListAllHelper();

    public UnifiedDiscoveryService(Context context, RadioListKeeper radioListKeeper,
                                   ScannedDevicesHandler scannedDevicesHandler,
                                   int scannersFlag) {
        mContext = context;
        mRadioListKeeper = radioListKeeper;
        mScannedDevicesHandler = scannedDevicesHandler;

        initScanners(scannersFlag);
    }

    private void initScanners(int scannersFlag) {
        IDiscoveryScanner scanner;

        if ((scannersFlag & NetRemote.SCANNER_SSDP) > 0) {
            scanner = new SSDPScanner();
            mScanners.add(scanner);
        }

        if ((scannersFlag & NetRemote.SCANNER_BONJOUR) > 0) {
            scanner = new JMDNSScanner(mContext);
            mScanners.add(scanner);
        }

        registerDeviceAvailabilityCallbackToScanners();
    }

    @Override
    public void start(boolean clearExistingRadios) {
        startDiscoveryScanners(clearExistingRadios, false);

        mRadioListKeeper.restoreAllRadios(mContext, AccessPointUtil.getSSIDOfCurrentConnection());
    }

    @Override
    public void stop() {
        stopDiscoveryScanners();
    }

    @Override
    public void rescan() {
        NetRemote.log(LogLevel.Warning, "UnifiedDiscoveryService: rescan. All radios will be cleared!");

        List<Radio> availableRadios = mRadioListKeeper.getAvailableRadios();

        cleanup();

        mRadioListKeeper.saveAllRadios();

        checkRadiosIfAvailable(availableRadios);

        startDiscoveryScanners(true, false);
    }

    private void checkRadiosIfAvailable(List<Radio> availableRadios) {
        for (Radio radio : availableRadios) {

            mScannedDevicesHandler.onScanDeviceAvailable(radio.getDeviceRecord());
        }
    }

    @Override
    public void cleanup() {
        mRadioListKeeper.cleanup();
        mDevicesFromMultiroomDeviceListAllHelper.clear();

        for (IDiscoveryScanner discoveryScanner : mScanners) {
            discoveryScanner.clean();
        }
    }

    @Override
    public void reInit() {
        boolean wasRunning = mIsScanRunning;

        stopDiscoveryScanners();
        cleanup();

        if (wasRunning) {
            start(true);
        }
    }

    @Override
    synchronized public void close() {
        mIsScanRunning = false;

        for (IDiscoveryScanner discoveryScanner : mScanners) {
            discoveryScanner.close();
        }
        mScanners.clear();
    }

    @Override
    public void singleScanOnDemand(final boolean callbackOnMainThread, final IScanOnDemandListener callback) {
        startDiscoveryScanners(true, true);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                final List<Radio> results = mRadioListKeeper.getRadios();
                stopDiscoveryScanners();

                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onScanResults(results);
                    }
                }, callbackOnMainThread);

            }
        }, SINGLE_SCAN_FOR_DEVICES_TIME_MS);
    }

    @Override
    public boolean isRunning() {
        return mIsScanRunning;
    }

    private synchronized void registerDeviceAvailabilityCallbackToScanners() {
        long callbackIndex = 0;

        for (IDiscoveryScanner discoveryScanner : mScanners) {
            discoveryScanner.setEventHelper(mScannerCallbacks, callbackIndex++);
            discoveryScanner.registerDeviceAvailabilityCallback(false, mScannedDevicesHandler);
            mScannedDevicesHandler.addListener(discoveryScanner);
        }
    }

    @Override
    public boolean addListener(IScanListener listener) {
        return !mListeners.contains(listener) && mListeners.add(listener);
    }

    @Override
    public boolean removeListener(IScanListener listener) {
        return mListeners.remove(listener);
    }

    @Override
    public void allowSpeakerToBeAddedFromDeviceListAll(boolean allow) {
        mDevicesFromMultiroomDeviceListAllHelper.allowSpeakerToBeAddedFromDeviceListAll(allow);
    }

    @Override
    public void tryToAddSpeakerFromDeviceListAll(BaseMultiroomDeviceListAll.ListItem item) {
        if (!mDevicesFromMultiroomDeviceListAllHelper.speakerIsAllowedToBeAddedFromDeviceListAll() ||
                mDevicesFromMultiroomDeviceListAllHelper.deviceWasAlreadyHandled(item)) {
            return;
        }

        boolean isHandledByScanner = false;
        for (IDiscoveryScanner discoveryScanner : mScanners) {
            isHandledByScanner |= discoveryScanner.speakerIsHandledByScanner(item.getIPAddress());
        }

        if (!isHandledByScanner && mDevicesFromMultiroomDeviceListAllHelper.itemFromDeviceListAllIsValid(item)) {
            DeviceRecord deviceRecord = mDevicesFromMultiroomDeviceListAllHelper.getDeviceRecord(item);

            mScannedDevicesHandler.onScanDeviceAvailable(deviceRecord);
        }

        mDevicesFromMultiroomDeviceListAllHelper.registerItemForDeviceListAll(item);
    }

    @Override
    public void setUseUpnpRootDeviceQueryInSsdp(boolean useUpnpRoot) {
        for (IDiscoveryScanner discoveryScanner : mScanners) {
            if (discoveryScanner instanceof SSDPScanner) {
                ((SSDPScanner)discoveryScanner).setUseUpnpRootDeviceQueryInSsdp(useUpnpRoot);
            }
        }
    }


    @Override
    public synchronized void setVendorIDsList(String[] vendorIDs) {
        for (IDiscoveryScanner discoveryScanner : mScanners) {
            discoveryScanner.setVendorIDList(vendorIDs);
        }
    }

    @Override
    public void setBonjourServiceTypeList(String[] bonjourServiceTypeList) {
        for (IDiscoveryScanner discoveryScanner : mScanners) {
            if (discoveryScanner instanceof JMDNSScanner) {
                ((JMDNSScanner)discoveryScanner).setServiceTypeList(bonjourServiceTypeList);
            }
        }
    }

    @Override
    public void setUseGetMultipleForDeviceDetailsRetrieval(boolean useGetMultiple) {
        mScannedDevicesHandler.setUseGetMultipleForDeviceDetailsRetrieval(useGetMultiple);
        mDevicesFromMultiroomDeviceListAllHelper.setUseGetMultipleForDeviceDetailsRetrieval(useGetMultiple);
    }

    private synchronized void startDiscoveryScanners(boolean clearExistingRadios, boolean singleScan) {
        stopDiscoveryScanners();

        if (clearExistingRadios) {
            mRadioListKeeper.cleanup();
        }

        mIsScanRunning = true;

        notifyListenersOnScanStart();
        mRadioListKeeper.notifyListenersAboutExistingRadios();

        IDiscoveryScanner.IDiscoveryScannerListener discoveryScannerListener = new IDiscoveryScanner.IDiscoveryScannerListener() {
            private long numberOfAnswersFromScanners = 0;
            private boolean unifiedIsScanning = false;

            @Override
            public void onScannerStarted(IDiscoveryScanner.ScanError error) {
                numberOfAnswersFromScanners++;
                unifiedIsScanning |= (error == IDiscoveryScanner.ScanError.NO_ERROR);

                if (numberOfAnswersFromScanners == mScanners.size()) {
                    mIsScanRunning = unifiedIsScanning;

                    if (!mIsScanRunning && error != IDiscoveryScanner.ScanError.NO_ERROR) {
                        notifyListenersOnScanError(error);
                    }
                }
            }
        };

        for (IDiscoveryScanner discoveryScanner : mScanners) {
            discoveryScanner.start(discoveryScannerListener, singleScan);
        }

        mScannedDevicesHandler.start();
    }

    synchronized void stopDiscoveryScanners() {
        mIsScanRunning = false;

        for (IDiscoveryScanner discoveryScanner : mScanners) {
            discoveryScanner.stop();
        }

        mScannedDevicesHandler.stop();
    }

    void notifyListenersOnScanError(IDiscoveryScanner.ScanError error) {
        for (IScanListener listener : mListeners) {
            listener.onScanError(error);
        }
    }

    private void notifyListenersOnScanStart() {
        for (IScanListener listener : mListeners) {
            listener.onScanStart();
        }
    }
}
