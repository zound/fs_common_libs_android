package com.frontier_silicon.NetRemoteLib.Discovery.deviceSerializer;

import android.content.Context;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

import java.util.List;

/**
 * Created by lsuhov on 08/11/2016.
 */

public class DeviceDetailsCollectionSerializerRunnable implements Runnable {

    private Context mContext;
    private String mWiFiSSID;
    private List<Radio> mRadios;
    private IDeviceDetailsSerializerListener mListener;

    public DeviceDetailsCollectionSerializerRunnable(Context context, String wifiSSID,
                                                     List<Radio> radios, IDeviceDetailsSerializerListener listener) {
        mContext = context;
        mWiFiSSID = wifiSSID;
        mRadios = radios;
        mListener = listener;
    }

    @Override
    public void run() {
        DeviceDetailsSerializer deviceDetailsSerializer = new DeviceDetailsSerializer(mContext, mWiFiSSID);
        deviceDetailsSerializer.saveRadios(mRadios);

        if (mListener != null) {
            mListener.onDeviceDetailsSaved();
        }

        dispose();
    }

    private void dispose() {
        mContext = null;
        mWiFiSSID = null;
        mRadios = null;
        mListener = null;
    }
}
