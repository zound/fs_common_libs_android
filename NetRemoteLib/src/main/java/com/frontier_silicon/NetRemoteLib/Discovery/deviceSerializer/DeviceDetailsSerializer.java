package com.frontier_silicon.NetRemoteLib.Discovery.deviceSerializer;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.loggerlib.LogLevel;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class DeviceDetailsSerializer {

    private static final String JSON_FILE_SUFFIX = ".json";

    private static final String RADIO_ICON_PORT = ":8080";
    
    private static final String KEY_ALL_DEVICES = "ALL_DEVICES";    
    private static final String KEY_RADIO_SN = "RADIO_SN";
    private static final String KEY_RADIO_FRIENDLY_NAME = "RADIO_FRIENDLY_NAME";
    private static final String KEY_RADIO_UDN = "RADIO_UDN";
    private static final String KEY_RADIO_IS_MR = "RADIO_IS_MR";
    private static final String KEY_RADIO_MR_VER = "RADIO_MR_VER";
    private static final String KEY_RADIO_VENDOR_ID = "RADIO_VENDOR_ID";
    private static final String KEY_RADIO_BASE_API = "RADIO_BASE_API";
    private static final String KEY_RADIO_MODEL_NAME = "RADIO_MODEL_NAME";
    private static final String KEY_RADIO_MODEL_NUMBER = "RADIO_MODEL_NUMBER";
    private static final String KEY_RADIO_ICON_URL = "RADIO_ICON_URL";
    private static final String KEY_RADIO_DDXML_URL = "RADIO_DDXML_URL";
    private static final String KEY_RADIO_MINUET = "RADIO_IS_MINUET";
    private static final String KEY_RADIO_VENICE = "RADIO_IS_VENICE";
    private static final String KEY_RADIO_IS_SG = "RADIO_IS_SG";
    private static final String KEY_RADIO_IS_STEREO_CAPABLE = "RADIO_IS_STEREO_CAPABLE";
    private static final String KEY_RADIO_HAS_GOOGLE_CAST = "RADIO_HAS_GOOGLE_CAST";
    private static final String KEY_RADIO_FIRMWARE_VERSION = "RADIO_FIRMWARE_VERSION";
    private static final String KEY_RADIO_HAS_AVS = "RADIO_HAS_AVS";
    private static final int FILE_READ_BUFF_SIZE = 1024;


    private final String mFileName;
    private String mFileFullPath = null;

    public DeviceDetailsSerializer(Context context, String wifiSSID) {
        mFileName = getFileNameUID(wifiSSID) + JSON_FILE_SUFFIX;
        File externalFileDir = null;

        try {
            externalFileDir = context.getExternalFilesDir(null);
        } catch (Exception e) {
            NetRemote.log(LogLevel.Error, e.toString());
        }

        if (externalFileDir != null) {
            mFileFullPath = externalFileDir.getAbsolutePath() +"/" + mFileName;
        }
    }

    public List<DeviceRecord> loadDeviceRecords() {
        List<DeviceRecord> deviceRecords = new ArrayList<>();

        String serializedJSON = "";

        NetRemote.log(LogLevel.Info, "DeviceDetailsSerializer: loadDeviceRecords from " + mFileFullPath);

        if (TextUtils.isEmpty(mFileFullPath)) {
            NetRemote.log(LogLevel.Info, "DeviceDetailsSerializer: loadDeviceRecords cannot run because mFileFullPath is empty");
            return deviceRecords;
        }

        FileReader fileReader = null;
        try {
            File file = new File(mFileFullPath);
            if (!file.exists()) {
                return deviceRecords;
            }

            fileReader = new FileReader(mFileFullPath);

            StringBuilder result = new StringBuilder();
            char[] buffer = new char[FILE_READ_BUFF_SIZE];
            while (fileReader.read(buffer) >= 0) {
                result.append(new String(buffer));
            }

            serializedJSON = result.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        deviceRecords = deserializeDeviceDetailsJSON(serializedJSON);

        return deviceRecords;
    }

    public boolean saveRadios(List<Radio> radios) {
        boolean savedOk = false;

        if (TextUtils.isEmpty(mFileFullPath)) {
            NetRemote.log(LogLevel.Info, "DeviceDetailsSerializer: saveRadios cannot run because mFileFullPath is empty");
            return false;
        }

        NetRemote.log(LogLevel.Info, "DeviceDetailsSerializer: saveRadios to " + mFileFullPath);

        String serializedJSON = getDeviceDetailsFromRadiosJSON(radios);

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(mFileFullPath);
            fileWriter.write(serializedJSON);
            fileWriter.flush();
            savedOk = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return savedOk;
    }

    private String getDeviceDetailsFromRadiosJSON(List<Radio> radios) {
        String serializedJSON = "";

        try {
            JSONObject allDevicesDetailsJSON = new JSONObject();

            JSONArray devicesListJSONArray = new JSONArray();

            for (Radio radio : radios) {
                NetRemote.log(LogLevel.Info, "DeviceDetailsSerializer: serializing radio " + radio);

                JSONObject deviceJSONObj = getDeviceJSONObject(radio);
                if (deviceJSONObj != null) {
                    devicesListJSONArray.put(deviceJSONObj);
                } else {
                    NetRemote.log(LogLevel.Error, "DeviceDetailsSerializer: ERROR serializing radio " + radio);
                }
            }

            allDevicesDetailsJSON.put(KEY_ALL_DEVICES, devicesListJSONArray);

            serializedJSON = allDevicesDetailsJSON.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return serializedJSON;
    }

    private String getFileNameUID(String wifiSSID) {
        return Base64.encodeToString(wifiSSID.getBytes(), Base64.NO_WRAP);
    }

    private List<DeviceRecord> deserializeDeviceDetailsJSON(String serializedJSON) {
        List<DeviceRecord> deviceRecords = new ArrayList<>();
        if (TextUtils.isEmpty(serializedJSON)) {
            return deviceRecords;
        }

        try {
            JSONObject allDevicesJSON = new JSONObject(serializedJSON);

            JSONArray devicesArrayJSON = allDevicesJSON.getJSONArray(KEY_ALL_DEVICES);
            for (int idx = 0; idx < devicesArrayJSON.length(); idx++) {
                JSONObject deviceDetailsJSON = (JSONObject) devicesArrayJSON.get(idx);

                DeviceRecord deviceRecord = getDeviceRecord(deviceDetailsJSON);
                if (deviceRecord != null) {
                    NetRemote.log(LogLevel.Info, "DeviceDetailsSerializer: deserialized device record " + deviceRecord);
                    deviceRecords.add(deviceRecord);
                } else {
                    NetRemote.log(LogLevel.Error, "DeviceDetailsSerializer: error deserializing json object at idx= " + idx);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return deviceRecords;
    }

    private JSONObject getDeviceJSONObject(Radio radio) {
        String sn = radio.getSN();
        String friendlyName = radio.getFriendlyName();
        String udn = radio.getUDN();
        boolean isMRCapable = radio.isMultiroomCapable();
        String mrVer = radio.getMultiroomVersion();
        String vendorID = radio.getVendorID();
        String baseAPI = ((RadioHttp) radio).getApiBase();
        String modelName = radio.getModelName();
        String modelNumber = radio.getModelNumber();
        String deviceInfoLocation = radio.getDDXMLLocation();
        boolean isMinuet = radio.getIsMinuet();
        boolean isVenice = radio.getIsVenice();
        boolean isSG = radio.isSG();
        boolean isStereoCapable = radio.isStereoCapable();
        boolean hasGoogleCast = radio.hasGoogleCast();
        String firmwareVersion = radio.getFirmwareVersion();
        boolean hasAVS = radio.hasAVS();

        URI imageUri = radio.getImageURL();
        String iconUrlFull = imageUri != null ? imageUri.toString() : null;
        String iconUrl = null;

        if (!TextUtils.isEmpty(iconUrlFull)) {
            int idx = iconUrlFull.lastIndexOf(RADIO_ICON_PORT);
            if (idx >= 0) {
                int iconUrlStartIdx = idx + RADIO_ICON_PORT.length();
                iconUrl = iconUrlFull.substring(iconUrlStartIdx);
            }
        }

        JSONObject jsonObject = null;

        if (isAllDataValid(sn, friendlyName, udn, baseAPI, deviceInfoLocation)) {
            try {
                jsonObject = new JSONObject();

                jsonObject.put(KEY_RADIO_SN, sn);
                jsonObject.put(KEY_RADIO_FRIENDLY_NAME, friendlyName);
                jsonObject.put(KEY_RADIO_UDN, udn);
                jsonObject.put(KEY_RADIO_IS_MR, isMRCapable);
                jsonObject.put(KEY_RADIO_MR_VER, mrVer);
                jsonObject.put(KEY_RADIO_VENDOR_ID, vendorID);
                jsonObject.put(KEY_RADIO_BASE_API, baseAPI);
                jsonObject.put(KEY_RADIO_MODEL_NAME, modelName);
                jsonObject.put(KEY_RADIO_MODEL_NUMBER, modelNumber);
                jsonObject.put(KEY_RADIO_ICON_URL, iconUrl);
                jsonObject.put(KEY_RADIO_DDXML_URL, deviceInfoLocation);
                jsonObject.put(KEY_RADIO_MINUET, isMinuet);
                jsonObject.put(KEY_RADIO_VENICE, isVenice);
                jsonObject.put(KEY_RADIO_IS_SG, isSG);
                jsonObject.put(KEY_RADIO_IS_STEREO_CAPABLE, isStereoCapable);
                jsonObject.put(KEY_RADIO_HAS_GOOGLE_CAST, hasGoogleCast);
                jsonObject.put(KEY_RADIO_FIRMWARE_VERSION, firmwareVersion);
                jsonObject.put(KEY_RADIO_HAS_AVS, hasAVS);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return jsonObject;
    }

    private DeviceRecord getDeviceRecord(JSONObject deviceDetailsJSON) {
        DeviceRecord deviceRecord = new DeviceRecord();

        try {
            deviceRecord.serialNumber = deviceDetailsJSON.getString(KEY_RADIO_SN);
            deviceRecord.friendlyName = deviceDetailsJSON.getString(KEY_RADIO_FRIENDLY_NAME);
            deviceRecord.UDN = deviceDetailsJSON.getString(KEY_RADIO_UDN);
            deviceRecord.isMultiroomCapable = deviceDetailsJSON.getBoolean(KEY_RADIO_IS_MR);
            deviceRecord.multiroomVersion = deviceDetailsJSON.optString(KEY_RADIO_MR_VER);
            deviceRecord.vendorID = deviceDetailsJSON.optString(KEY_RADIO_VENDOR_ID);
            deviceRecord.fsApiLocation = deviceDetailsJSON.getString(KEY_RADIO_BASE_API);
            deviceRecord.modelName = deviceDetailsJSON.getString(KEY_RADIO_MODEL_NAME);
            deviceRecord.modelNumber = deviceDetailsJSON.optString(KEY_RADIO_MODEL_NUMBER);
            deviceRecord.imageURL = deviceDetailsJSON.optString(KEY_RADIO_ICON_URL);
            deviceRecord.deviceInfoLocation = deviceDetailsJSON.getString(KEY_RADIO_DDXML_URL);
            deviceRecord.isMinuet = deviceDetailsJSON.optBoolean(KEY_RADIO_MINUET);
            deviceRecord.isVenice = deviceDetailsJSON.optBoolean(KEY_RADIO_VENICE, true);
            deviceRecord.isSG = deviceDetailsJSON.optBoolean(KEY_RADIO_IS_SG);
            deviceRecord.isStereoCapable = deviceDetailsJSON.optBoolean(KEY_RADIO_IS_STEREO_CAPABLE);
            deviceRecord.hasGoogleCast = deviceDetailsJSON.optBoolean(KEY_RADIO_HAS_GOOGLE_CAST);
            deviceRecord.firmwareVersion = deviceDetailsJSON.optString(KEY_RADIO_FIRMWARE_VERSION);
            deviceRecord.hasAVS = deviceDetailsJSON.optBoolean(KEY_RADIO_HAS_AVS);

            // !!! All new properties have to be retrieved with opt functions. Otherwise we risk exceptions.
            // !!! Also function RadioListKeeper->isDeviceInfoChanged() should be updated

            //public String ID = null;
        } catch (JSONException e) {
            NetRemote.log(LogLevel.Error, e.getMessage());
            e.printStackTrace();
        }

        return deviceRecord;
    }

    private boolean isAllDataValid(String sn, String friendlyName, String udn, String fsAPIURL, String ddXMLURL) {
        return (sn != null && friendlyName != null && udn != null && fsAPIURL != null && ddXMLURL != null);
    }

}
