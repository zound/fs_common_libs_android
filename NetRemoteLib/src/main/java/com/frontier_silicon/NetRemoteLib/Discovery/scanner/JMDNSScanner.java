package com.frontier_silicon.NetRemoteLib.Discovery.scanner;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.EventHelper;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.utils.VendorIDsUtil;
import com.frontier_silicon.loggerlib.LogLevel;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;


/**
 * Created by viftime on 09/03/16.
 */
public class JMDNSScanner implements IDiscoveryScanner {

    //private static final String SERVICE_TYPE = "_undok._tcp.local.";
    //private static final String SERVICE_TYPE = "_spotify-connect._tcp.local.";

    private final int HEARTBEAT_INTERVAL = 120 * 1000;
    private final int INITIAL_HEARTBEAT = 5 * 1000;

    private Timer mHeartbeatTimer;
    private HeartbeatTimerTask mHeartbeatTimerTask;

    private static final String DNS_RECORD_VENDOR_ID = "VendorID";
    private static final String DNS_RECORD_DEVICE_INFO_LOCATION = "DeviceInfoLocation";
    private static final String DNS_RECORD_FSAPI_LOCATION = "FsAPILocation";

    Context mContext;
    JmDNS mJmDNS;

    private ServiceListener mServiceListener;

    private String[] mVendorsIdList;

    private EventHelper<Long, IScanDeviceAvailabilityListener> mCallbacks = null;
    private Long mEventIndex = 0L;

    private final Map<String, DeviceRecord> mAllScanDeviceRecords = new HashMap<>(10);
    private final Map<String, DeviceRecord> mScanDeviceRecordsByIP = new HashMap<>(10);
    private WifiManager.MulticastLock mMulticastLock;

    private BlockingQueue<Runnable> mStartScanRunnablesQueue = new LinkedBlockingQueue<>(5);
    ExecutorService mStartScanThreadPoolExecutor;

    final ReentrantLock mLock = new ReentrantLock();
    private String[] mServiceTypeList;

    public JMDNSScanner(Context context) {
        mContext = context;

        mStartScanThreadPoolExecutor = new ThreadPoolExecutor(0, 1, 1, TimeUnit.SECONDS, mStartScanRunnablesQueue, new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                NetRemote.log(LogLevel.Error, "JMDNSScanner mStartScanThreadPoolExecutor: thread execution was rejected");
            }
        });
    }

    @Override
    public void setEventHelper(EventHelper<Long, IScanDeviceAvailabilityListener> eventHelper, long index) {
        mCallbacks = eventHelper;
        mEventIndex = index;
    }

    @Override
    public void unregisterDeviceAvailabilityCallback(IScanDeviceAvailabilityListener callback) {
        mCallbacks.deregisterCallback(mEventIndex, callback);
    }

    @Override
    public void registerDeviceAvailabilityCallback(boolean callbackOnMainThread, IScanDeviceAvailabilityListener callback) {
        mCallbacks.registerCallback(mEventIndex, callbackOnMainThread, callback);
    }

    @Override
    synchronized public void start(final IDiscoveryScannerListener listener, final boolean singleScan) {
        NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "JMDNSScanner: start method");

        mStartScanThreadPoolExecutor.submit(new Runnable() {
            @Override
            public void run() {
                boolean jmDNSInitialized = true;

                NetRemote.log(LogLevel.Info, "JMDNSScanner: Start thread");

                acquireMulticastLock(mContext);

                NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "JMDNSScanner: acquired multicast lock");

                mLock.lock();
                try {
                    if (mJmDNS == null) {
                        jmDNSInitialized = initJMDNS();
                    }
                } finally {
                    mLock.unlock();
                }

                if (jmDNSInitialized) {
                    mStartScanThreadPoolExecutor.submit(new Runnable() {
                        @Override
                        public void run() {

                            addServiceListener(listener);

                            if (!singleScan) {
                                startHeartbeatTimer();
                            }
                        }
                    });
                } else {
                    NetRemote.log(LogLevel.Error, "JMDNSScanner: Init error");
                }
            }
        });
    }

    @Override
    synchronized public void stop() {
        NetRemote.log(LogLevel.Info, "JMDNSScanner: Stop");

        if (mLock.tryLock()) {
            try {
                removeServiceListeners();
            } finally {
                mLock.unlock();
            }
        }

        stopHeartbeatTimer();
        releaseMulticastLock();

        closeJMDNS();
    }


    void runHeartbeatScan() {
        if (mJmDNS != null) {
            NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "JMDNSScanner: heartbeat scan");

            if (mServiceListener != null) {
                if (mLock.tryLock()) {
                    try {
                        removeServiceListeners();
                    } finally {
                        mLock.unlock();
                    }
                }
            }

            addServiceListener(null);
        }
    }

    void removeServiceListeners() {
        if (mJmDNS != null && mServiceListener != null) {
            for (String serviceType : mServiceTypeList) {
                mJmDNS.removeServiceListener(serviceType, mServiceListener);
            }

            mServiceListener = null;
        }
    }

    @Override
    synchronized public void close() {
        stop();

        mCallbacks.clearAllCallbacks();

        clearAllDeviceRecords();
    }

    @Override
    public void setVendorIDList(String[] vendorIDs) {
        mVendorsIdList = vendorIDs;
    }

    @Override
    public void clean() {
        clearAllDeviceRecords();
    }

    @Override
    public boolean speakerIsHandledByScanner(String ipAddress) {
        DeviceRecord scanDeviceRecord = mScanDeviceRecordsByIP.get(ipAddress);
        return scanDeviceRecord != null;
    }

    public void setServiceTypeList(String[] serviceTypeList) {
        mServiceTypeList = serviceTypeList;
    }

    boolean initJMDNS() {
        boolean initOk = false;
        NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "JMDNSScanner: trying to init jmdns");

        try {
            InetAddress addr = getLocalIpAddress(mContext);

            // I don't think we really need the hostName. The IP address should suffice.
            // Also if there are issues with the internet connection getHostName takes 10s
            String hostname = addr.getHostAddress();

            mJmDNS = JmDNS.create(addr, hostname);

            NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "JMDNSScanner: initialized jmdns");

            initOk = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return initOk;
    }

    private void closeJMDNS() {
        NetRemote.log(NetRemote.TRACE_DISCOVERY_BIT, LogLevel.Info, "JMDNSScanner: starting closeJMDNS");

        mStartScanThreadPoolExecutor.submit(new Runnable() {
            @Override
            public void run() {

                mLock.lock();
                try {
                    NetRemote.log(LogLevel.Info, "JMDNSScanner: started closeJMDNS");

                        if (mJmDNS != null) {
                            mJmDNS.close();
                        }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {

                    mJmDNS = null;
                    mLock.unlock();
                }

                NetRemote.log(LogLevel.Info, "JMDNSScanner: finished closeJMDNS");
            }
        });
    }

    void addServiceListener(IDiscoveryScannerListener listener) {
        NetRemote.log(LogLevel.Warning, "JMDNSScanner: addServiceListener");

        mServiceListener = new ServiceListener() {
            public void serviceResolved(ServiceEvent serviceEvent) {
                NetRemote.log(LogLevel.Info, "JMDNSScanner: Service resolved " + serviceEvent.getName());

                ServiceInfo serviceInfo = serviceEvent.getInfo();
                if (isInVendorIDsList(serviceInfo)) {
                    sendScannedDeviceToHandler(serviceInfo);
                }
            }

            public void serviceRemoved(ServiceEvent serviceEvent) {
                NetRemote.log(LogLevel.Error, "JMDNSScanner: Service lost " + serviceEvent.getName());

                handleServiceLost(serviceEvent);
            }

            public void serviceAdded(ServiceEvent serviceEvent) {

                NetRemote.log(LogLevel.Info, "JMDNSScanner: Service found " + serviceEvent.getName());

                // Required to force serviceResolved to be called again
                // (after the first search)
                if (mLock.tryLock()) {
                    try {
                        if (mJmDNS != null) {
                            mJmDNS.requestServiceInfo(serviceEvent.getType(), serviceEvent.getName());
                        } else {
                            NetRemote.log(LogLevel.Error, "JMDNSScanner - serviceAdded: mJmDNS is null");
                        }
                    } finally {
                        mLock.unlock();
                    }
                }
            }
        };

        mLock.lock();
        try {
            if (mJmDNS != null) {
                for (String serviceType : mServiceTypeList) {
                    mJmDNS.addServiceListener(serviceType, mServiceListener);
                }

                if (listener != null) {
                    listener.onScannerStarted(ScanError.NO_ERROR);
                }

            }

        } catch (NullPointerException nullException) {
            //this has been seen while the mJmDns was closing.
            if (listener != null) {
                listener.onScannerStarted(ScanError.UNKNOWN_ERROR);
            }
        } finally {
            mLock.unlock();
        }
    }

    boolean isInVendorIDsList(ServiceInfo serviceInfo) {
        String vendorID = serviceInfo.getPropertyString(DNS_RECORD_VENDOR_ID);

        return VendorIDsUtil.isInVendorIDsList(vendorID, mVendorsIdList);
    }

    private void clearAllDeviceRecords() {
        synchronized (mAllScanDeviceRecords) {
            mAllScanDeviceRecords.clear();
            mScanDeviceRecordsByIP.clear();
        }
    }

    void sendScannedDeviceToHandler(ServiceInfo serviceInfo) {
        final DeviceRecord deviceRecord = new DeviceRecord();

        String ipAddress = getIpAddress(serviceInfo);

        if (ipAddress != null) {
            deviceRecord.ID = serviceInfo.getName();
            deviceRecord.IP = ipAddress;
            deviceRecord.deviceInfoLocation = generateDDXMLLocation(serviceInfo, ipAddress);
            deviceRecord.fsApiLocation = generateFSAPILocation(serviceInfo, ipAddress);
            deviceRecord.vendorID = serviceInfo.getPropertyString(DNS_RECORD_VENDOR_ID);

            mCallbacks.callCallbacks(mEventIndex, new EventHelper.CallCallbackCodeFragment() {
                public void run(Object callbackObject, Object tag) {
                    IScanDeviceAvailabilityListener callback = (IScanDeviceAvailabilityListener)callbackObject;
                    callback.onScanDeviceAvailable(deviceRecord);
                }
            });

            synchronized (mAllScanDeviceRecords) {
                mAllScanDeviceRecords.put(serviceInfo.getName(), deviceRecord);
                mScanDeviceRecordsByIP.put(ipAddress, deviceRecord);
            }

        } else {
            NetRemote.log(LogLevel.Error, "JMDNSScanner: sendScannedDeviceToHandler: null IPv4 address for " + serviceInfo.getName());
        }
    }

    void handleServiceLost(ServiceEvent serviceEvent) {
        DeviceRecord scanDeviceRecord;

        synchronized (mAllScanDeviceRecords) {
            scanDeviceRecord = mAllScanDeviceRecords.get(serviceEvent.getInfo().getName());
            if (scanDeviceRecord != null) {
                mScanDeviceRecordsByIP.remove(scanDeviceRecord.IP);
            }
        }

        final DeviceRecord finalScanDeviceRecord = scanDeviceRecord;
        JMDNSScanner.this.mCallbacks.callCallbacks(mEventIndex, new EventHelper.CallCallbackCodeFragment() {
            public void run(Object callbackObject, Object tag) {
                IScanDeviceAvailabilityListener callback = (IScanDeviceAvailabilityListener) callbackObject;
                callback.onScanDeviceDisappeared(finalScanDeviceRecord);
            }
        });

        NetRemote.log(LogLevel.Warning, "JMDNSScanner: Device disappeared with BYE BYE " + scanDeviceRecord.ID);
    }

    private String getIpAddress(ServiceInfo serviceInfo) {
        String ipAddr = null;
        Inet4Address[] ipv4AddrList = serviceInfo.getInet4Addresses();
        for (Inet4Address anIpv4Addr : ipv4AddrList) {
            ipAddr = anIpv4Addr.getHostAddress();
            if (ipAddr.contentEquals("172.24.0.1")) {
                // This address is from SETUP mode
                continue;
            } else {
                break;
            }
        }

        return ipAddr;
    }

    private String generateDDXMLLocation(ServiceInfo serviceInfo, String ipAddress) {
        String ddxmlLocation = serviceInfo.getPropertyString(DNS_RECORD_DEVICE_INFO_LOCATION);

        return "http://" + ipAddress + ":" + serviceInfo.getPort() + ddxmlLocation;
    }

    private String generateFSAPILocation(ServiceInfo serviceInfo, String ipAddress) {
        String fsAPILocation = serviceInfo.getPropertyString(DNS_RECORD_FSAPI_LOCATION);


        return "http://" + ipAddress + ":" + serviceInfo.getPort() + fsAPILocation;
    }


    void acquireMulticastLock(Context context) {
        releaseMulticastLock();

        NetRemote.log(LogLevel.Info, "JMDNSScanner: Starting Multicast Lock...");

        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        mMulticastLock = wifi.createMulticastLock("JMDNSMulticastLock");
        mMulticastLock.setReferenceCounted(true);
        mMulticastLock.acquire();
    }

    private void releaseMulticastLock() {
        if (mMulticastLock != null && mMulticastLock.isHeld()) {
            NetRemote.log(LogLevel.Info, "JMDNSScanner: Releasing Multicast Lock...");

            mMulticastLock.release();
            mMulticastLock = null;
        }
    }

    private InetAddress getLocalIpAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        int ipAddress = wifiInfo.getIpAddress();
        InetAddress address = null;
        try {
            address = InetAddress.getByName(String.format(Locale.ENGLISH,
                    "%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff)));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return address;
    }

    void startHeartbeatTimer() {
        stopHeartbeatTimer();

        mHeartbeatTimerTask = new HeartbeatTimerTask();
        mHeartbeatTimer = new Timer();
        mHeartbeatTimer.schedule(mHeartbeatTimerTask, INITIAL_HEARTBEAT, HEARTBEAT_INTERVAL);
    }

    private void stopHeartbeatTimer() {
        if (mHeartbeatTimer != null) {
            mHeartbeatTimer.cancel();
            mHeartbeatTimer.purge();
            mHeartbeatTimer = null;
        }
        if (mHeartbeatTimerTask != null) {
            mHeartbeatTimerTask.dispose();
            mHeartbeatTimerTask = null;
        }
    }

    @Override
    public void onPingResult(DeviceRecord deviceRecord, boolean pingOk) {
        if (!pingOk && deviceRecord != null) {
            mAllScanDeviceRecords.remove(deviceRecord.ID);
            mScanDeviceRecordsByIP.remove(deviceRecord.IP);
        }
    }

    private class HeartbeatTimerTask extends TimerTask {
        private boolean isDisposed = false;

        HeartbeatTimerTask() {
        }

        @Override
        public void run() {
            if (isDisposed) {
                NetRemote.log(LogLevel.Info, "JMDNSScanner: Heartbeat timer was canceled");
                cancel();
                return;
            }

            NetRemote.log(LogLevel.Info, "JMDNSScanner: Heartbeat started");

            runHeartbeatScan();
        }

        public void dispose() {
            isDisposed = true;
        }
    }
}
