package com.frontier_silicon.NetRemoteLib;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import com.frontier_silicon.loggerlib.LogLevel;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class AccessPointUtil {
	
	private static Context mContext;
	
	public static void init(Context context) {
		mContext = context;
	}
	
	public static WifiManager getWifiManager() {
		return (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
	}

	public static List<ScanResult> getScanResults() {
		WifiManager wifiManager = getWifiManager();

        List<ScanResult> scanResults;
        try {
            scanResults = wifiManager.getScanResults();

        } catch (SecurityException e) {
            NetRemote.log(LogLevel.Error, e.getMessage());
            scanResults = new ArrayList<>();
        }

        return scanResults;
	}
	
	public static boolean startAccesPointsScan() {
		WifiManager wifiManager = getWifiManager();
		return wifiManager.startScan();
	}
	
	public static void connectToAccessPoint(ScanResult accessPoint) {
		WifiManager wifiManager = getWifiManager();
		
		WifiConfiguration wc = new WifiConfiguration();
		wc.SSID = String.format("\"%s\"", accessPoint.SSID);
		wc.hiddenSSID = true;
		wc.allowedKeyManagement.set(KeyMgmt.NONE);
		wc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);

		wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
		wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

		wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
		wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);

		wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
		wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

		connectToAccessPointImpl(wifiManager, wc);
	}
	
	public static void connectToAccessPoint(String ssid, String wiFiPassword) {
		WifiManager wifiManager = getWifiManager();
		
		WifiConfiguration wifiConfig = new WifiConfiguration();
		wifiConfig.SSID = String.format("\"%s\"", ssid);
		
		if (wiFiPassword.length() == 0) {
			wifiConfig.allowedKeyManagement.set(KeyMgmt.NONE);
		}
		else {
			wifiConfig.preSharedKey = String.format("\"%s\"", wiFiPassword);
		}
		
		connectToAccessPointImpl(wifiManager, wifiConfig);
	}
	
	public static void connectToAccessPointImpl(WifiManager wifiManager, WifiConfiguration wc) {
        unbindWiFiNetworkToProcess();

		// check captive portal
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
			// from Android 5.1 device will not reconnect to a network that did not in the past had internet connection

			boolean mIsCaptivePortalCheckEnabled = Settings.Global.getInt(mContext.getContentResolver(),
					"captive_portal_detection_enabled", 1) == 1;
			String server = Settings.Global.getString(mContext.getContentResolver(), "captive_portal_server");
			NetRemote.log(LogLevel.Info, "Phone: captivePortal = " + mIsCaptivePortalCheckEnabled + " - server: " + server);
		}

		//searching and removing identical configurations
		boolean found = false;

		List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
		if (list != null) {
			for (WifiConfiguration wifiConfig : list) {
				if (wifiConfig.SSID != null && wifiConfig.SSID.equals(wc.SSID)) {
					// don't remove if already configured
					//wifiManager.removeNetwork(wifiConfig.networkId);

					found = true;
					wc = wifiConfig;
					break;
				}

			}
		}

		// bind connection
		// Bind moved in NetRemote on onConnected listener

		if (!found) {

			wc.status = WifiConfiguration.Status.ENABLED;
			wc.priority = getMaxPriority(wifiManager) + 10;

			wifiManager.disconnect();

			int netId = wifiManager.addNetwork(wc);
			wifiManager.enableNetwork(netId, true);
            //wifiManager.saveConfiguration();

			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
				wifiManager.reconnect();
			}
		} else {
			wifiManager.disconnect();
			wifiManager.enableNetwork(wc.networkId, true);

			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
				// from Android 5.1 device will not reconnect to a network that did not in the past had internet connection

				wifiManager.reconnect();
			}
		}		
	}

    private static int getMaxPriority(WifiManager wifiManager) {
        int maxPriority = 0;
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        if (list != null) {
            for (WifiConfiguration wifiConfiguration : list) {
                if (wifiConfiguration.priority > maxPriority) {
                    maxPriority = wifiConfiguration.priority;
                }
            }
        }

        return maxPriority;
    }
	
	public static boolean isConnectedToWiFi() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return isConnectedToWiFiOrEthernetAndroid4(true);
        } else {
            return isConnectedToWiFiOrEthernetAndroid5(true);
        }
	}

    public static boolean isConnectedToWiFiOrEthernet() {
        String product = Build.PRODUCT;
        if (!TextUtils.isEmpty(product) && product.contains("sdk_google_phone")) {
            //is emulator, assuming that it is connected to a network with speakers already setup
            return true;
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return isConnectedToWiFiOrEthernetAndroid4(true);
        } else {
            return isConnectedToWiFiOrEthernetAndroid5(true);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static boolean isConnectedToWiFiOrEthernetAndroid5(boolean wifiOnly) {
        boolean isConnected = false;

        ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network[] networks = connManager.getAllNetworks();
        if (networks == null) {
            return isConnected;
        }

        for (Network network : networks) {

            NetworkInfo networkInfo = null;
            if (network != null) {
                try {
                    networkInfo = connManager.getNetworkInfo(network);
                } catch (NullPointerException e) {
                    NetRemote.log(LogLevel.Error, "getNetworkInfoForWiFi: " + e.toString());
                }
            }

            if (networkInfo == null || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_ETHERNET)) {
                continue;
            }

            if (wifiOnly && networkInfo.getType() == ConnectivityManager.TYPE_ETHERNET) {
                continue;
            }

            if (networkInfo.isConnected()) {
                isConnected = true;
                break;
            }
        }

        return isConnected;
    }

    @SuppressWarnings("deprecation")
    private static boolean isConnectedToWiFiOrEthernetAndroid4(boolean wifiOnly) {
        ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        boolean isConnected = (networkInfo != null) && networkInfo.isConnected();
        NetRemote.log(LogLevel.Info, "AccessPointUtil wifi connected = " + (isConnected));

        if (isConnected || wifiOnly) {
            return isConnected;
        }

        networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
        isConnected = (networkInfo != null) && networkInfo.isConnected();
        NetRemote.log(LogLevel.Info, "AccessPointUtil ethernet connected = " + (isConnected));

        return isConnected;
    }

    public static boolean isConnectedToGivenAP(ScanResult scanResult) {
		if (scanResult == null)
			return false;
		
		if (!isConnectedToWiFi())
			return false;
		
		WifiInfo wifiInfo = getWiFiConnectionInfo();
		if (wifiInfo == null)
			return false;
		
		String bssidOfCurrentAP = wifiInfo.getBSSID();
		if (bssidOfCurrentAP != null && bssidOfCurrentAP.contentEquals(scanResult.BSSID))
			return true;
		
		String ssidOfCurrentAP = wifiInfo.getSSID();
		if (bssidOfCurrentAP == null && ssidOfCurrentAP != null && ssidOfCurrentAP.contains(scanResult.SSID))
			return true;
		
		return false;
	}
	
	public static boolean isConnectedToGivenAP(String ssid) {
		if (!isConnectedToWiFi())
			return false;
		
		WifiInfo wifiInfo = getWiFiConnectionInfo();
		if (wifiInfo == null)
			return false;
		
		String ssidOfCurrentAP = wifiInfo.getSSID();
		return ssidOfCurrentAP != null && ssidOfCurrentAP.contains(ssid);
	}

	public static boolean isConnectedToGivenAP(int networkId) {
        return networkId == getCurrentWiFiNetworkId();
    }

	public static WifiInfo getWiFiConnectionInfo() {
		WifiManager wifiMgr = getWifiManager();
        WifiInfo wifiInfo = null;
        try {
        	wifiInfo = wifiMgr.getConnectionInfo(); 
        } catch (NullPointerException exception) { }

        return wifiInfo;
	}
	
	public static String getSSIDOfCurrentConnection() {
		WifiInfo wifiInfo = getWiFiConnectionInfo();
		if (wifiInfo == null)
			return "";
		
		String rawSSID = wifiInfo.getSSID();

		return sanitizeSSID(rawSSID);
	}

    public static String getBSSIDOfCurrentConnection() {
        WifiInfo wifiInfo = getWiFiConnectionInfo();
        if (wifiInfo == null)
            return "";

        return wifiInfo.getBSSID();
    }

	public static int getCurrentWiFiNetworkId() {
		WifiInfo wifiInfo = getWiFiConnectionInfo();
		if (wifiInfo == null)
			return -1;

		return wifiInfo.getNetworkId();
	}
	
	public static boolean connectToNetworkId(int networkId) {
        unbindWiFiNetworkToProcess();

		WifiManager wifiMgr = getWifiManager();
		
		return wifiMgr.enableNetwork(networkId, true);
	}
	
	public static boolean isWifiEnabled() {
		WifiManager wifiMgr = getWifiManager();

		return wifiMgr.isWifiEnabled();
	}
	
	public static boolean isWiFiEnabledOrEnabling() {
		WifiManager wifiMgr = getWifiManager();
		
		int wifiState = wifiMgr.getWifiState(); 
		return (wifiState == WifiManager.WIFI_STATE_ENABLED || 
				wifiState == WifiManager.WIFI_STATE_ENABLING);
	}

    public static boolean checkLocationIfAndroid6OrMore(Context context) {
        int locationMode = 0;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            return true;
        }
    }

    //Android sometimes returns the SSID in the form of "AccessPoint". We remove the quotes.
	public static String sanitizeSSID(String ssid) {
		if (TextUtils.isEmpty(ssid))
			return "";
		else if (ssid.length() >= 2 && ssid.startsWith("\"") )
			return ssid.substring(1, ssid.length()-1);
		else
			return ssid;
	}
	
	public static boolean isOpenAP(ScanResult scanResult) {
		final String cap = scanResult.capabilities;
        final String[] securityModes = { "WEP", "PSK", "EAP" };
        for (int i = securityModes.length - 1; i >= 0; i--) {
            if (cap.contains(securityModes[i])) {
                return false;
            }
        }
        
        return true;
	}

	public static boolean isOpenAP(WifiInfo wifiInfo){
        String ssid = wifiInfo.getSSID();
        if (ssid == null) {
            return false;
        }

		WifiManager wifi = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		List<ScanResult> networkList = null;

        try {
            networkList = wifi.getScanResults();
        } catch (SecurityException e) {
            NetRemote.log(LogLevel.Error, e.getMessage());
        }

		if (networkList != null) {
			for (ScanResult network : networkList) {
				//check if current connected SSID
				if (ssid.equals(network.SSID)) {
					return isOpenAP(network);
				}
			}
		}

		return true;
	}
	
	public static void disconnectCurrentWifi() {

        unbindWiFiNetworkToProcess();

		WifiManager wifiMgr = getWifiManager();
		wifiMgr.disconnect();
	}

	public static void removeCurrentWiFiConnection() {

        int id = getCurrentWiFiNetworkId();

        unbindWiFiNetworkToProcess();
        WifiManager wifiMgr = getWifiManager();
        wifiMgr.disconnect();

        if (id != -1) {
            wifiMgr.removeNetwork(id);
        }
    }

    public static void bindWifiNetworkToProcess() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		Network[] networks = connManager.getAllNetworks();
        if (networks == null) {
            return;
        }

        for (Network network : networks) {
            NetworkInfo networkInfo = null;
            try {
                networkInfo = connManager.getNetworkInfo(network);
            } catch (NullPointerException e) {
                NetRemote.log(LogLevel.Error, "bindWifiNetworkToProcess: " + e.toString());
            }

            if (networkInfo == null) {
                continue;
            }

            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                Network boundNetwork = null;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    boundNetwork = connManager.getBoundNetworkForProcess();
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    boundNetwork = ConnectivityManager.getProcessDefaultNetwork();
                }

                if (boundNetwork != null && boundNetwork.equals(network)) {
                    NetRemote.log(LogLevel.Info, "bindWifiNetworkToProcess: network already bound");
                    return;
                }

                boolean success = false;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    success = connManager.bindProcessToNetwork(network);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    success = ConnectivityManager.setProcessDefaultNetwork(network);
                }

                NetRemote.log(LogLevel.Info, "BindToProcessNetwork " + success);
                break;
            }
        }
    }

    public static void unbindWiFiNetworkToProcess() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            connManager.bindProcessToNetwork(null);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ConnectivityManager.setProcessDefaultNetwork(null);
        }
    }

	public static boolean isConnectedToHeadlessAP(String[] namePartsForSuggestedDevices) {
		boolean isConnectedToHeadlessAP = false;
		if (isConnectedToWiFi()) {
			String currentSSID = AccessPointUtil.getSSIDOfCurrentConnection();
            NetRemote.log(LogLevel.Info, "SETUP isConnectedToHeadlessAP  - " + currentSSID);

			isConnectedToHeadlessAP = findStringInArray(currentSSID, namePartsForSuggestedDevices);
		}

		return isConnectedToHeadlessAP;
	}

	private static boolean findStringInArray(String name, String[] namePartsForSuggestedDevices) {
		if (name != null) {
			name = name.toLowerCase();

			for (String huiName : namePartsForSuggestedDevices) {
				huiName = huiName.toLowerCase();
				if (name.contains(huiName))
					return true;
			}
		}

		return false;
	}

	public static String integerIPv4ToHumanReadableIPv4(int ipv4) {
		return ( ipv4 & 0xFF) + "." +
				((ipv4 >> 8 ) & 0xFF) + "." +
				((ipv4 >> 16 ) & 0xFF) + "." +
				((ipv4 >> 24 ) & 0xFF );
	}

    public static long getDecimalFromMACAddress(String macAddress) {
        macAddress = macAddress.replace(":", "");

        Long decimalBssid = Long.parseLong(macAddress, 16);

        return decimalBssid;
    }

    public static boolean isWifiConnected(Intent intent) {
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        boolean isConnected = networkInfo.isConnected();

        String currSSID = "";
        WifiInfo wifiInfo = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
        if (wifiInfo != null) {
            currSSID = wifiInfo.getSSID();

            isConnected = (isConnected && wifiInfo.getNetworkId() >= 0);
        }

        NetRemote.log(LogLevel.Info, "isWifiConnected: " + currSSID + " " + isConnected);

        return isConnected;
    }

    public static void setTdlsEnabled(String ipAddress, boolean enabled) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return;
        }

        try {
            InetAddress inetAddress = InetAddress.getByName(ipAddress);

            WifiManager wifiManager = getWifiManager();
            wifiManager.setTdlsEnabled(inetAddress, enabled);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

	public static boolean tryConnectToNetworkId(int netWorkId) {
		if (netWorkId == getCurrentWiFiNetworkId()) {
			return true;
		}

		return connectToNetworkId(netWorkId);
	}
}
