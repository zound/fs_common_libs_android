/******************************************************************************
 * Copyright 2012 by Frontier Silicon Ltd. All rights reserved.
 *
 * No part of this software, either material or conceptual may be copied or
 * distributed, transmitted, transcribed, stored in a retrieval system or
 * translated into any human or computer language in any form by any means,
 * electronic, mechanical, manual or otherwise, or disclosed to third parties
 * without the express written permission of Frontier Silicon Ltd.
 * 137 Euston Road, London, NW1 2AA.
 ******************************************************************************/
package com.frontier_silicon.NetRemoteLib;

import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.frontier_silicon.NetRemoteLib.Discovery.IDiscoveryService;
import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.Discovery.IRadioListKeeper;
import com.frontier_silicon.NetRemoteLib.Discovery.RadioListKeeper;
import com.frontier_silicon.NetRemoteLib.Discovery.ScannedDevicesHandler;
import com.frontier_silicon.NetRemoteLib.Discovery.UnifiedDiscoveryService;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttp;
import com.frontier_silicon.loggerlib.FileLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * This is the main high-level class for accessing and managing radios. You should create a single
 * instance of it to be shared by all parts of your application.
 *
 */
public class NetRemote {
	static final String LIB_NAME = "NetRemote";

	/**
	 * TRACE_NETWORK_PACKETS flag value
	 */
	static final public int TRACE_NETWORK_PACKETS_BIT  = (1 << 0);
	/**
	 * TRACE_NODE_REQUESTS flag value
	 */
	static final public int TRACE_NODE_REQUESTS_BIT    = (1 << 1);
	/**
	 * TRACE_NOTIFICATIONS flag value
	 */
	static final public int TRACE_NOTIFICATIONS_BIT    = (1 << 3);
	/**
	 * TRACE_RADIOVIS flag value
	 */
	static final public int TRACE_RADIOVIS_BIT         = (1 << 4);
	/**
	 * TRACE_STOMP flag value
	 */
	static final public int TRACE_STOMP_BIT            = (1 << 5);
	/**
	 * TRACE_SSDP flag value
	 */
	static final public int TRACE_DISCOVERY_BIT        = (1 << 6);
	/**
	 * TRACE_SSDP_DEBUG flag value
	 */
	static final public int TRACE_DISCOVERY_DEBUG_BIT  = (1 << 7);
	/**
	 * TRACE_EVENTHELPER flag value
	 */
	static final public int TRACE_EVENTHELPER_BIT      = (1 << 8);

	/**
	 * TRACE_ALL_ERRORS flag value
	 */
	static final public int TRACE_ALL_ERRORS_BIT       = (1 << 12);
	/**
	 * TRACE_DEFAULT flag value
	 */
	static final public int TRACE_DEFAULT          	   = (TRACE_NETWORK_PACKETS_BIT | TRACE_NOTIFICATIONS_BIT);



    /**
     * Flag for choosing SSDP scanner
     */
    static final public int SCANNER_SSDP = 1;
    /**
     * Flag for choosing Bonjour scanner
     */
    static final public int SCANNER_BONJOUR = (1 << 1);

    private static final long WAIT_BEFORE_NETREMOTE_REINIT_MS = 1_000;
    private static final int DEFAULT_NETWORK_TIMEOUT_MS = 10_000;
    private static final int NOTIFICATION_REQUEST_TIMEOUT_MS = 40_000;
    private static final int MAX_REQUESTS_PER_HOST = 2;

	static private int LogTraceFlag = TRACE_DEFAULT;

	static private FileLogger mFileLogger = null;
    private boolean mShowAlsoMissingSpeakers = false;

	/* Here you can defined static radios, which is useful on the Android emulator which can't do IP multicasts.
	   Use port 2244 for Jupiter and 80 for Mars. */
	final static boolean FORCE_USE_OF_STATIC_RADIO = false;
	final static String[] StaticallyDefinedRadios = new String[]{/*"http://192.168.150.164"*/};

	private IDiscoveryService mDiscoveryService;
    private ScannedDevicesHandler mScannedDevicesHandler;
    private RadioListKeeper mRadioListKeeper;

	/* radios added manually OR Bluetooth radios */
	private final Map<String /* Serial Number */, Radio> mAllStaticRadios = new HashMap<>();

	/* WiFi connection listener  */
	private NetworkStateReceiver mWiFiConnectedReceiver = null;
	private List<IWifiConnection> mWifiConnection = new ArrayList<>();

    private static OkHttpClient mOkHttpClient = null;
    private static OkHttpClient mOkHttpClientForNotifications = null;

	private static MainThreadExecutor mMainThreadExecutor = null;

    private Radio mCurrentRadio = null;

	/**
	 * Construct a new NetRemote object. This is the main high-level class for accessing and managing
	 * radios. You should create a single instance of it to be shared by all parts of your application.
	 * @param parent    The parent context which will remain valid for the lifetime of this instance
	 *
	 */
	public NetRemote(Context parent){
		this.Init(parent, SCANNER_SSDP | SCANNER_BONJOUR);
	}

    /**
     * Construct a new NetRemote object. This is the main high-level class for accessing and managing
     * radios. You should create a single instance of it to be shared by all parts of your application.
     * @param parent    The parent context which will remain valid for the lifetime of this instance
     * @param scannersFlag Select what scanners to load
     *
     */
    public NetRemote(Context parent, int scannersFlag){
        this.Init(parent, scannersFlag);
    }

	private void Init(Context context, int scannersFlag) {

        AccessPointUtil.init(context);

		initStaticRadios();

		initMainThreadExecutor();

        initOkHttpClient();

        initDiscoveryServices(context, scannersFlag);

		registerWifiConnectionListener(context);
	}

    private void initOkHttpClient() {
        mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(DEFAULT_NETWORK_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .readTimeout(DEFAULT_NETWORK_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .writeTimeout(DEFAULT_NETWORK_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .retryOnConnectionFailure(false)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        //We need to close the connection because the socket is forcibly closed by the speaker
                        //I'm aware that this client is used for internet requests also, but this is
                        // the best solution at the moment

                        //An improvement would be to filter this header based on IP. if it's not local LAN
                        // then don't add the "close" header
                        Request request = chain.request()
                                .newBuilder().header("Connection", "close").build();

                        return chain.proceed(request);
                    }
                })
                .build();

        mOkHttpClient.dispatcher().setMaxRequestsPerHost(MAX_REQUESTS_PER_HOST);

        //This client will have the same connection pool
        mOkHttpClientForNotifications = mOkHttpClient.newBuilder()
                .connectTimeout(NOTIFICATION_REQUEST_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .readTimeout(NOTIFICATION_REQUEST_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .writeTimeout(NOTIFICATION_REQUEST_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .build();
    }

    private void initDiscoveryServices(Context parent, int scannersFlag) {
        mRadioListKeeper = new RadioListKeeper(parent);

        mScannedDevicesHandler = new ScannedDevicesHandler(mRadioListKeeper);

        mDiscoveryService = new UnifiedDiscoveryService(parent, mRadioListKeeper, mScannedDevicesHandler,
                scannersFlag);

        mRadioListKeeper.setDiscoveryService(mDiscoveryService);
        mRadioListKeeper.setScannedDevicesHandler(mScannedDevicesHandler);
    }

    public void setVendorIDsList(String[] vendorIDList) {
        mDiscoveryService.setVendorIDsList(vendorIDList);
        mScannedDevicesHandler.setVendorIDsList(vendorIDList);
    }

    public void setFirmwareBlacklist(String[] firmwareBlacklist) {
        mScannedDevicesHandler.setFirmwareBlacklist(firmwareBlacklist);
    }

    public void setBonjourServiceTypeList(String[] bonjourServiceTypeList) {
        mDiscoveryService.setBonjourServiceTypeList(bonjourServiceTypeList);
    }

    public static OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    public static OkHttpClient getOkHttpClientForNotifications() {
        return mOkHttpClientForNotifications;
    }

	private void initMainThreadExecutor() {
		mMainThreadExecutor = new MainThreadExecutor(new Handler(Looper.getMainLooper()));
	}

	public static MainThreadExecutor getMainThreadExecutor() {
		return mMainThreadExecutor;
	}

	private void initStaticRadios() {
    	/* Add any static radios (only on the Emulator). */
		if (NetRemote.isRunningOnEmulator() || NetRemote.FORCE_USE_OF_STATIC_RADIO) {
			synchronized (this.mAllStaticRadios) {
				Long count = 1L;
				for (String address : NetRemote.StaticallyDefinedRadios) {
					try {
						String apiUrl = "http://" + address + "/fsapi";

						Radio radio = new RadioHttp(("Static" + count.toString()), ("Static radio " + address), apiUrl);
						count += 1;
						this.mAllStaticRadios.put(radio.getSN(), radio);
					} catch (Exception e) {
						log(LogLevel.Error, "Error trying to create static radio at " + address);
					}
				}
			}
		}
	}

	/**
	 * This function allows the creation of a radio / registering of a radio with the NetRemote library. It
	 * allows radios to be used that were not discovered via SSDP.
	 * @param address		The IP address or host name of the radio, eg. "192.168.1.4"
	 * @param friendlyName	The friendly name of the radio
	 * @return				The radio object, or null on error
	 */
	public Radio createStaticRadioHTTP(String address, String friendlyName, String udn) {
        return createStaticRadioHTTP(address, friendlyName, udn, false);
	}

	public Radio createStaticRadioHTTP(String address, String friendlyName, String udn, boolean hiddenRadio) {
        Radio result = null;
        synchronized (this.mAllStaticRadios) {
            try {
                String apiUrl = "http://" + address + "/fsapi";

                Radio radio = new RadioHttp(("Static radio " + friendlyName), friendlyName, udn, apiUrl);
                radio.setIsCheckingAvailability(false);
                radio.setIsAvailable(true);

                if (!hiddenRadio) {
                    this.mAllStaticRadios.put(radio.getSN(), radio);
                }
                result = radio;
            } catch (Exception e) {
                log(LogLevel.Error, "Error trying to create static radio at " + address);
            }
        }

        return result;
    }

	/**
	 * This function should be called if networking has changed (eg. joined new WiFi network).
	 */
	public void networkReInit() {
		if (mDiscoveryService != null) {
			mDiscoveryService.reInit();
		}
	}

	/**
	 * log the message, using the {@link LogLevel#Info} log level.
	 *
	 * @param message		The message to be logged.
	 */
	static public void log(String message) {
		log(true, LogLevel.Info, message);
	}

	/**
	 * log the message, unconditionally, using the log level specified.
	 *
	 * @param level			The log level.
	 * @param message		The message to be logged.
	 */
	static public void log(LogLevel level, String message) {
		log(true, level, message);
	}

	/**
	 * log the message, if the the condition is true, using the log level specified.
	 *
	 * @param condition		The condition which must be true for the logging to be output.
	 * @param level			The log level.
	 * @param message		The message to be logged.
	 */
	static public void log(Boolean condition, LogLevel level, String message) {
		if (condition || (isLogTraceFlagSet(NetRemote.TRACE_ALL_ERRORS_BIT) && (level == LogLevel.Error))) {
			switch (level) {
				case Warning: {
					Log.w(LIB_NAME, message);
					break;
				}
				case Error: {
					Log.e(LIB_NAME, message);
					break;
				}
				default: {
					Log.i(LIB_NAME, message);
					break;
				}
			}

			if (mFileLogger != null) {
				mFileLogger.logToFile(LIB_NAME, message, level);
			}
		}
	}

	/**
	 * log the message, if the trace flag was set using the {@link LogLevel#Info} log level.
	 *
	 * @param traceFlag		The trace flag to be checked
	 * @param message		The message to be logged.
	 */
	static public void log(int traceFlag, String message) {
		log(traceFlag, LogLevel.Info, message);
	}

	/**
	 * log the message if the trace flag was set (using SetLogTrace)
	 *
	 * @param traceFlag		The trace flag to be checked
	 * @param level			The log level.
	 * @param message		The message to be logged.
	 */
	static public void log(int traceFlag, LogLevel level, String message) {
		boolean showLog = isLogTraceFlagSet(traceFlag);

		log(showLog, level, message);
	}

	/**
	 * Set the log trace flag
	 *
	 * @param logTraceFlag		The log trace flag as a bit mask
	 */
	static public void setLogTraceFlag(int logTraceFlag) {
		LogTraceFlag = logTraceFlag;
	}

	/**
	 * Check if a trace flag was set
	 *
	 * @param traceFlag		The log trace flag to check
	 * @return	if the flag was set
	 */
	static public boolean isLogTraceFlagSet(int traceFlag) {
		return ((traceFlag & LogTraceFlag) > 0);
	}

	/**
	 * Enable/disable writing logs to a file
	 * @param fileLogger The file logger to use
	 */
	public static void setFileLogging(FileLogger fileLogger) {
		mFileLogger = fileLogger;
	}

	/**
	 * close all radio connections. Used when handling WiFi network changes
	 *
	 */
	public void closeAllRadios() {
		/* Free all radios. */
		synchronized (this.mAllStaticRadios) {
			for (Radio radio : this.getAllStaticRadios()) {
				radio.close();
			}
			this.mAllStaticRadios.clear();
		}
	}

	/**
	 * close this instance of the class, and free all its resources. After calling close() you should
	 * not use any other functionality or radio objects.
	 */
	public void close() {
		closeAllRadios();

		mDiscoveryService.close();
	}

	/**
	 * pause all radios, including all worker threads.
	 */
	public void pause() {
        log("Pausing all radios");
        
		synchronized (this.mAllStaticRadios) {
			for (Radio radio : this.getAllStaticRadios()) {
				radio.pause();
			}
		}

		for (Radio radio : mRadioListKeeper.getRadios()) {
			radio.pause();
		}
	}

	/**
	 * resume all radios, and un-pause all worker threads.
	 */
	public void resume() {
        log("Resuming all radios");

		synchronized (this.mAllStaticRadios) {
			for (Radio radio : this.getAllStaticRadios()) {
				radio.resume();
			}
		}

		for (Radio radio : mRadioListKeeper.getRadios()) {
			radio.resume();
		}
	}

	public void setShowAlsoMissingSpeakers(boolean showAlsoMissingSpeakers) {
		mShowAlsoMissingSpeakers = showAlsoMissingSpeakers;
        mRadioListKeeper.setShowAlsoMissingSpeakers(showAlsoMissingSpeakers);
	}

	public void setPingDeviceOnByeByeNotification(boolean pingOnByeBye) {
        mScannedDevicesHandler.setPingDeviceOnByeByeNotification(pingOnByeBye);
    }

    /**
     * Retrieves a list of the available radios or in case setShowAlsoMissingSpeakers() was set to true,
     * the function will return also the missing radios.
     *
     * @return radios
     */
    public List<Radio> getRadios() {
		if (mShowAlsoMissingSpeakers) {
			return getAllRadios();
		} else {
			return 	getNotMissingRadios();
		}
	}

	/**
	 * Retrieves a list of all radios, including the missing ones.
	 *
	 * @return		List of all Radios
	 */
	public List<Radio> getAllRadios()
	{
		ArrayList<Radio> allRadiosList = new ArrayList<>();
		
		/* Code for a real device. */
		synchronized (this.mAllStaticRadios) {
			allRadiosList.addAll(this.mAllStaticRadios.values());
		}

		allRadiosList.addAll(mRadioListKeeper.getRadios());

		return allRadiosList;
	}

    /**
     * Retrieves a list of currently radios that are available or in the process of checking availability.
     *
     * @return		List of Radios
     */
    public List<Radio> getNotMissingRadios() {
        ArrayList<Radio> radios = new ArrayList<>();

        /* Code for a real device. */
        synchronized (this.mAllStaticRadios) {
            radios.addAll(this.mAllStaticRadios.values());
        }

        List<Radio> allRadios = mRadioListKeeper.getRadios();
        for (Radio radio : allRadios) {
            if (!radio.isMissing()) {
                radios.add(radio);
            }
        }

        return radios;
    }

	private List<Radio> getAllStaticRadios()
	{
		/* Code for a real device. */
		synchronized (this.mAllStaticRadios) {
			return new ArrayList<>(this.mAllStaticRadios.values());
		}
	}

	/**
	 * Report an exception, and record it to the log.
	 *
	 * @param e			The exception
	 */
	public static void reportProblem(Exception e) {
		if (e != null) {
			log(LogLevel.Error, "Fatal problem: " + e.toString());
		} else {
			log(LogLevel.Error, "Fatal problem - no specific exception to report.");
		}
		e.printStackTrace();
		assert(false);
	}


	public IDiscoveryService getDiscoveryService() {
		return mDiscoveryService;
	}

    public IRadioListKeeper getRadioListKeeper() {
        return mRadioListKeeper;
    }

    public boolean addRadioDiscoveryListener(IRadioDiscoveryListener listener) {
        return mRadioListKeeper.addListener(listener);
    }

    public boolean removeRadioDiscoveryListener(IRadioDiscoveryListener listener) {
        return mRadioListKeeper.removeListener(listener);
    }

	public void addWifiConnectionListener(IWifiConnection callback){
		this.mWifiConnection.add(callback);
	}

	/**
	 *  Register a broadcast listener to be notified on WiFi connection changes
	 * @param context an Android Context
	 */
	private void registerWifiConnectionListener(Context context) {
		unregisterWifiConnectionListener(context);

		mWiFiConnectedReceiver = new NetworkStateReceiver();
		mWiFiConnectedReceiver.setConnectionListener(new IWifiConnection() {
		    private boolean firstFire = true;

			@Override
			public void onConnected() {
				AccessPointUtil.bindWifiNetworkToProcess();

                if (firstFire) {
                    firstFire = false;
                    return;
                }

                closeCurrentRadio();
				closeAllRadios();

				// ??? delay
				(new Handler()).postDelayed(new Runnable() {
					@Override
					public void run() {
						NetRemote.log(LogLevel.Info, "NetworkStateReceiver: starting netRemote reinit");

						networkReInit();
					}
				}, WAIT_BEFORE_NETREMOTE_REINIT_MS);

				// pass notification
				for(IWifiConnection listener: mWifiConnection) {
					listener.onConnected();
				}
			}

			@Override
			public void onDisconnected() {
				AccessPointUtil.unbindWiFiNetworkToProcess();

                closeCurrentRadio();
				closeAllRadios();
				getDiscoveryService().cleanup();

				// pass notification
				for(IWifiConnection listener: mWifiConnection) {
					listener.onDisconnected();
				}
			}
		});

		//context.registerReceiver(mWiFiConnectedReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)); //handle Ethernet connection
		context.registerReceiver(mWiFiConnectedReceiver, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
	}

	/**
	 * Unregisters the broadcast listener frome WiFi connection notifications
	 * @param context an Android Context
	 */
	private void unregisterWifiConnectionListener(Context context) {
		if (mWiFiConnectedReceiver != null) {
			context.unregisterReceiver(mWiFiConnectedReceiver);
			mWiFiConnectedReceiver = null;
		}
	}

	/**
	 * Determine whether or not we are running on the Android Emulator.
	 *
	 * @return		True if running on the emulator
	 */
	static public boolean isRunningOnEmulator() {
		return (Build.PRODUCT.equals("google_sdk") || Build.PRODUCT.equals("sdk") || Build.PRODUCT.equals("sdk_x86"));
	}

	static public String getClassName(Class type) {
		String name = type.toString();

		int endPos = name.indexOf("$");
		if (endPos == -1) {
			endPos = name.length();
		}

		return name.substring(0, endPos);
	}

	/**
	 * Assert a condition to be true. If it fails, raise an exception to report the failure.
	 *
	 * @param condition			The condition which should be true
	 */
	public static void assertTrue(Boolean condition) {
		if (!condition) {
			NetRemote.reportProblem(new Exception());
			assert(false);
		}
	}

	public static String getVersion() {
		return "2.0-0";
	}

	public Radio getCurrentRadio() {
        return mCurrentRadio;
    }

    public void setCurrentRadio(Radio radio) {
        mCurrentRadio = radio;
    }

    public void closeCurrentRadio() {
        if (mCurrentRadio != null) {
            mCurrentRadio.close();
            mCurrentRadio = null;
        }
    }
}
