package com.frontier_silicon.NetRemoteLib;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;

public class MainThreadExecutor {
    private final Executor mResponsePoster;

    public MainThreadExecutor(final Handler handler) {
        // Make an Executor that just wraps the handler.
        mResponsePoster = new Executor() {
            @Override
            public void execute(Runnable command) {
                handler.post(command);
            }
        };
    }

    public void executeOnMainThread(Runnable runnable, boolean forceOnMainThread) {
        if (forceOnMainThread) {
            mResponsePoster.execute(runnable);
        } else {
            runnable.run();
        }
    }

    public void executeOnMainThread(Runnable runnable) {
        executeOnMainThread(runnable, true);
    }

    public boolean isOnMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
