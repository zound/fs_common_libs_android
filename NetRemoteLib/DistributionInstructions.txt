Release:
  * Export -> JAR file.

  * Ensure NetRemoteLib is the only project selected.
  * Ensure "Export all output folders for checked projects" is
    checked.

  * Ensure that "Export generated class files and resources", "Export
    refactorings for check projects", and "Export Java source files
    and resources" are NOT checked.

  * Click Finish.

Debug:
  * Export -> JAR file.

  * Ensure NetRemoteLib is the only project selected.
  * Ensure "Export all output folders for checked projects" and
    "Export Java source files and resources" are checked.

  * Uncheck everything for NetRemoteLib, ie. ".classpath", ".project",
    ... "project.properties".

  * Ensure that "Export generated class files and resources" and
    "Export refactorings for check projects" are NOT checked.

  * Under NetRemoteLib UNcheck everything, except leave "src" checked.

  * Click Finish.

Instructions for testing what you've exported:

  Test both the release and debug builds can be successfully used in a
  project.

  2) Modify FSIRC to load NetRemoteLib from JAR rather than from
  source:

    * Remove reference to existing NetRemoteLib source:
        - Go to the Properties of FSIRCActivity.
        - Then click "Android" and remove NetRemoteLib from the Libraries.
    * Add dnsjava-2.1.3.jar:
        - Copy dnsjava-2.1.3.jar to FSIRC/libs.
    * Add the NetRemoteLib.jar:
        - Right click on FSIRC, and go to Properties.
 	- Go to "Java Build Path" -> "Libraries".
	- "Add External JARs..."
	- Locate the NetRemoteLib.jar file.
	- Make sure that under "Order and Export" that
          NetRemoteLib.jar is checked.

    * Refresh the FSIRC project in Eclipse.
    * Build and run.

  Tests for a release build:

    * Load the JAR file in to emacs and verify that it only contains
      the manifest.mf file and .class files in
      com/frontier_silicon... and org/apache...

    * Check the JAR file size is about 748KB, and contains 562 files.

    * Check RadioVIS slides appear (this tests dnsjava-2.1.3.jar was
      found in FSIRC/libs).

    * Restart Eclipse to force it to refresh what it knowns about the
      JAR file. Check that no JavaDoc is present by hovering over a
      NetRemoteLib function in the source code for FSIRC, and seeing
      the warning about no JavaDoc information being available.

  Tests for a debug build:

    * Load the JAR file in to emacs and verify that it only contains
      the manifest.mf file, and .class|.java files in
      com/frontier_silicon... and org/apache...

    * Check the JAR file size is about 1.4MB, and contains 894 files.

    * Check RadioVIS slides appear (this tests dnsjava-2.1.3.jar was
      found in FSIRC/libs).

    * Restart Eclipse to force it to refresh what it knowns about the
      JAR file. Check that JavaDoc IS present by hovering over a
      NetRemoteLib function in the source code for FSIRC, and seeing
      that details are available.
