package com.frontier_silicon.components.common;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class PinSanitizer extends BaseSanitizer {
	private IPinSanitizerListener mSanitizerListener;
	
	public interface IPinSanitizerListener {
		void onNewPIN(boolean goodPin);
		void onPinTooBig();
	}
	
	public void appendSanitizerToEditText(EditText editText, Context context, IPinSanitizerListener sanitizerListener) {
		mEditText = editText;
		mContext = context;
		mSanitizerListener = sanitizerListener;
		
		mEditText.addTextChangedListener(new TextWatcher() {
			int startIndexOfChange = 0;
			int numberOfAddedChars = 0;
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				startIndexOfChange = start;
				numberOfAddedChars = count;
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				hideError();
			}
			
			@Override
			public void afterTextChanged(Editable editableText) {

				if (limitLengthOfPin(editableText, startIndexOfChange, numberOfAddedChars)) {
					mSanitizerListener.onPinTooBig();
				}
				
				if (mSanitizerListener == null)
					return;
				
				boolean goodPin = editableText.length() == FsComponentsConfig.LENGTH_OF_PIN; 
				mSanitizerListener.onNewPIN(goodPin);
			}
		});
	}

	private boolean limitLengthOfPin(Editable editableText, int startIndexOfChange, int numberOfAddedChars) {
		return super.limitLengthOfText(editableText, startIndexOfChange, numberOfAddedChars, FsComponentsConfig.LENGTH_OF_PIN);
	}
}
