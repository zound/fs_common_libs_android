package com.frontier_silicon.components.common;

import java.util.HashMap;
import java.util.Set;

import android.app.Activity;
import android.app.Dialog;

public class DialogsHolder {

	private static HashMap<String, Dialog> mHashDialogs = new HashMap<>();

	public static void addDialogToCollection(String dialogName, Dialog dialog) {
		mHashDialogs.put(dialogName, dialog);
	}

	public static void dismissAllRegisteredDialogs() {
		Set<String> dialogKeys = mHashDialogs.keySet();
		
		for (String dialogName : dialogKeys) {
			Dialog dialog = mHashDialogs.get(dialogName);
			DialogsHolder.dismissDialog(dialog);
		}
		mHashDialogs.clear();
	}

	public static void dismissDialog(String dialogKey) {
		Dialog dialog = mHashDialogs.get(dialogKey);
		DialogsHolder.dismissDialog(dialog);
	}

	public static void dismissDialog(Dialog dialog) {
		if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (java.lang.IllegalArgumentException e) {}
		}
	}

    public static void dismissDialog(Dialog dialog, Activity activity) {
        if (dialog != null && dialog.isShowing()) {
            try {
                if (activity != null && !activity.isFinishing()) {
                    dialog.dismiss();
                }
            } catch (Exception e) {
            }
        }
    }

	public static Dialog getDialog(String dialogKey) {
		return mHashDialogs.get(dialogKey);
	}

	public static boolean isShowing(String dialogKey) {
		Dialog dialog = getDialog(dialogKey);
		return DialogsHolder.isShowing(dialog);
	}

	public static boolean isShowing(Dialog dialog) {
		return dialog != null && dialog.isShowing();
	}

	public static boolean isShowingOrActivityIsFinishing(String dialogKey, Activity activity) {
		if (isShowing(dialogKey))
			return true;
		
		return activityIsFinishing(activity);
	}
	
	public static boolean activityIsFinishing(Activity activity) {
		if (activity != null && activity.isFinishing())
			return true;
		return false;
	}
}
