package com.frontier_silicon.components.common;

import com.frontier_silicon.NetRemoteLib.Node.BaseNavContextFormItem;
import com.frontier_silicon.NetRemoteLib.Node.BaseNavFormItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;

/**
 * Created by adanaila on 16/11/2016.
 */

public class UnifiedFormListItem{
    private NodeListItem.FormItemType itemType;
    private String id;
    private String label;
    private String description;
    private long optionsCnt;
    private long key;

    public UnifiedFormListItem(BaseNavFormItem.ListItem listItm){
        itemType = listItm.getFormItemType();
        id = listItm.getId();
        label = listItm.getLabel();
        description = listItm.getDescription();
        optionsCnt = listItm.getOptionsCount();
        key = listItm.getKey();
    }

    public UnifiedFormListItem(BaseNavContextFormItem.ListItem listItm){
        itemType = listItm.getFormItemType();
        id = listItm.getId();
        label = listItm.getLabel();
        description = listItm.getDescription();
        optionsCnt = listItm.getOptionsCount();
        key = listItm.getKey();
    }

    public NodeListItem.FormItemType getFormItemType() { return itemType; }
    public String getId() { return id; }
    public String getLabel() { return label; }
    public String getDescription() { return description; }
    public Long getOptionsCount() { return optionsCnt; }
    public Long getKey() { return key; }
}
