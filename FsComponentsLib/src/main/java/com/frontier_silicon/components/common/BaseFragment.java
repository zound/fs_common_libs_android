package com.frontier_silicon.components.common;

import android.support.v4.app.Fragment;

/**
 * Created by lsuhov on 25/08/15.
 */
public abstract class BaseFragment extends Fragment {
    protected boolean mIsRestoreInstance = false;
    protected boolean mIsFragmentActive = false;
    protected boolean forceResumeOnActivated = true;

    public boolean onFragmentActivatedBase() {
        if (getView() == null) {
            mIsRestoreInstance = true;
            return false;
        }

        if (mIsFragmentActive) {
            return false;
        }

        mIsFragmentActive = true;

        if (forceResumeOnActivated) {
            onResume();
        }

        return true;
    }

    public boolean onFragmentDeactivatedBase() {
        mIsFragmentActive = false;

        return getView() != null;
    }
}
