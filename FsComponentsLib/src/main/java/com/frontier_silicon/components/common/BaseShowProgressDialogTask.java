package com.frontier_silicon.components.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.NetRemote;

import java.util.Timer;
import java.util.TimerTask;

public abstract class BaseShowProgressDialogTask<ResultT> extends AsyncTask<Integer, Integer, ResultT> {

	protected Context mContext;
	protected ProgressDialog mProgressDlg;
	protected long TASK_TIMEOUT_MS = 20_000;
	protected Timer mTimeoutTimer;
	protected TimeoutTask mTimeoutTimerTask;
	protected String mDialogKey = "generic_key";
    protected String mLoadingMessage;
    protected boolean mDialogCancelable = true;

    @Override
    protected void onPreExecute() {
        if (mContext != null && !DialogsHolder.isShowingOrActivityIsFinishing(mDialogKey, (Activity)mContext)) {
            showProgressDialog();

            initTimeoutTimer();
        }
    }

    protected  void showProgressDialog() {
        mProgressDlg = new ProgressDialog(mContext);
        mProgressDlg.setMessage(mLoadingMessage);
        mProgressDlg.setIndeterminate(true);
        mProgressDlg.setCancelable(mDialogCancelable);
        mProgressDlg.show();

        DialogsHolder.addDialogToCollection(mDialogKey, mProgressDlg);
    }

	protected void initTimeoutTimer() {
		initTimeoutTimer(TASK_TIMEOUT_MS);
	}

    protected void initTimeoutTimer(long timeoutMs) {
        mTimeoutTimerTask = new TimeoutTask();
        mTimeoutTimer = new Timer();
        mTimeoutTimer.schedule(mTimeoutTimerTask, timeoutMs);
    }
	
	private void stopTimeoutTimer() {
		if (mTimeoutTimer != null) {
			mTimeoutTimer.cancel();
			mTimeoutTimer.purge();
			mTimeoutTimer = null;
		}

        if (mTimeoutTimerTask != null) {
            mTimeoutTimerTask.cancel();
            mTimeoutTimerTask.dispose();
        }
	}

	private class TimeoutTask extends TimerTask {
        private boolean isDisposed = false;

		@Override
		public void run() {
            if (!isDisposed) {
                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        onPostExecute(null);
                    }
                }, true);
            }
		}

        public void dispose() {
            isDisposed = true;
        }
	}
	
	@Override
	protected void onPostExecute(ResultT result) {
		if (result != null) {
			stopTimeoutTimer();
		}

		DialogsHolder.dismissDialog(mProgressDlg, (Activity) mContext);
        mProgressDlg = null;
	}

    protected void dispose() {
        mContext = null;
        mTimeoutTimerTask = null;
    }
}
