package com.frontier_silicon.components.common;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;

public class LayoutDecider {
	
	public static boolean isTablet(Activity activity) {
		if (activity == null)
			return false;

        DisplayMetrics displayMetrics = new DisplayMetrics();
		Display display = activity.getWindowManager().getDefaultDisplay();
        display.getMetrics(displayMetrics);

        int heightPixels = displayMetrics.heightPixels;
        int widthPixels = displayMetrics.widthPixels;

        // includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17) {
            try {
                widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (Build.VERSION.SDK_INT >= 17) {
            try {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(display, realSize);
                widthPixels = realSize.x;
                heightPixels = realSize.y;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        float dpHeight =  heightPixels / displayMetrics.density;
        float dpWidth = widthPixels / displayMetrics.density;
        
        float smallestWidth = dpHeight;
        if (dpWidth < dpHeight)
        	smallestWidth = dpWidth;
        
        if (smallestWidth >= 600)
        	return true;
        
        return false;
	}

	public static boolean isTabletAndLandscape(Activity activity) {
		
		return isTablet(activity) && isLandscape(activity);
	}
	
	public static boolean isLanscapeButNotTablet(Activity activity) {
		return !isTablet(activity) && isLandscape(activity);
	}
	
	public static boolean isLandscape(Activity activity) {
		return getOrientation(activity) == Configuration.ORIENTATION_LANDSCAPE;
	}
	
	public static int getOrientation(Activity activity) {
		if (activity == null)
			return Configuration.ORIENTATION_UNDEFINED;
		
		return activity.getResources().getConfiguration().orientation;
	}
}
