package com.frontier_silicon.components.common;

/**
 * Created by cvladu on 03/02/17.
 */

public enum ErrorSanitizerMessageType {
    EmptyName,
    BiggerThanSupportedLimit,
    CharacterNotAllowed,
    WhiteSpaceAtBeginning,
    MultipleWhiteSpaces
}
