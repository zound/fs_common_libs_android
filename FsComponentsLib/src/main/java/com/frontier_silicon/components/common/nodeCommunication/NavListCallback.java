package com.frontier_silicon.components.common.nodeCommunication;

import com.frontier_silicon.NetRemoteLib.Node.BaseNavList;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavList;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.RadioNavigationUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 18/04/2017.
 */

public class NavListCallback implements IGetNodeCallback {

    private static final int MAX_NAV_STACK_DEPTH = 20;
    private Radio mRadio;
    private String mStartKey;
    private boolean mIsDisposed = false;
    private Object mLock = new Object();
    private int mCurrentStackDepth = 0;
    private boolean mFirstPageOnly = false;
    private List<NodeNavList.ListItem> mNavListItems = new ArrayList<>();
    private NodeNavList mNavLastListNode;
    private boolean mNavListComplete = false;

    public NavListCallback(Radio radio, String startKey, boolean firstPageOnly) {
        mRadio = radio;
        mStartKey = startKey;
        mFirstPageOnly = firstPageOnly;
    }

    public List<NodeNavList.ListItem> getNavList() {
        mRadio.getListNode(NodeNavList.class, mStartKey, FsComponentsConfig.MAX_NAV_LIST_ITEMS,
                true, this);

        while (!mFirstPageOnly && !mNavListComplete) {
            // get next set of items

            //mRadio can become null any time if dispose() is called
            Radio radio;
            synchronized (mLock) {
               radio = mRadio;
            }

            if (radio == null) {
                return mNavListItems;
            }

            RadioNavigationUtil.waitForNavStatusReady(radio);
            radio.getListNodeNext(mNavLastListNode, FsComponentsConfig.MAX_NAV_LIST_ITEMS, true, this);
        }

        return mNavListItems;
    }

    @Override
    public void getNodeResult(NodeInfo node) {
        synchronized (mLock) {
            if (mIsDisposed)
                return;

            mNavLastListNode = (NodeNavList) node;

            addItemsFromNodeList(mNavLastListNode);

            mCurrentStackDepth++;

            if (mNavLastListNode == null || mNavLastListNode.IsEndOfList() ||
                    mNavLastListNode.Size() == 0 ||
                    mCurrentStackDepth >= MAX_NAV_STACK_DEPTH ||
                    mFirstPageOnly) {
                // end of list reached or max items retrieved in one go or stack depth dangerously high

                mNavListComplete = (mNavLastListNode == null || mNavLastListNode.IsEndOfList() || mNavLastListNode.Size() == 0);
            }
        }
    }

    private void addItemsFromNodeList(NodeNavList nodeNavList) {
        if (nodeNavList == null) {
            return;
        }

        List<BaseNavList.ListItem> items = nodeNavList.getEntries();
        for (BaseNavList.ListItem item : items) {
            mNavListItems.add(item);

            FsLogger.log(">> NAV: " + item.getName() + " key " + item.getKey(), LogLevel.Warning);
        }
    }

    @Override
    public void getNodeError(Class nodeType, NodeErrorResponse error) {
        FsLogger.log("NavListCallback: getNodeError " + error, LogLevel.Error);

        ErrorResponse.FSStatusCode code = error.getFSStatus();
        if (code == ErrorResponse.FSStatusCode.FS_LIST_END) {
            //end of list reached (for IR 2.6.*)
        } else {
            if (mIsDisposed)
                return;

            mNavListItems = null;
        }

        mNavListComplete = true;
    }

    public void dispose() {
        synchronized (mLock) {
            mRadio = null;
            mIsDisposed = true;
        }
    }
}
