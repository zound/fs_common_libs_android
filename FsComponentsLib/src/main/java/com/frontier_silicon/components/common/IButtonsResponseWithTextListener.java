package com.frontier_silicon.components.common;

public interface IButtonsResponseWithTextListener {
	public void onOkClicked(String response);
	public void onCancelClicked();
}
