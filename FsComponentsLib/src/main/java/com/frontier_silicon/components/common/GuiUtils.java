package com.frontier_silicon.components.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.frontier_silicon.FsComponentsLib.R;

import java.util.List;

public class GuiUtils {

    //Used in fragments of ViewPager. We couldn't swipe in some cases.
	public static void ignoreTouchEvents(View view) {
		view.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
	}
	
	public static void replaceContentsOfList(List destinationList, List newList) {
		if (newList == null) 
			return;
		
    	destinationList.clear();
    	destinationList.addAll(newList);
    }

	public static void showKeyboardForGivenEditText(final EditText editText, final FragmentActivity activity) {
        if (editText == null || activity == null) {
            return;
        }
		editText.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(editText, 0);
            }
        }, 100);
    }

    public static void showKeyboardForGivenEditText(final FragmentActivity activity) {
        InputMethodManager keyboard = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void hideKeyboard(final View view, final Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager == null)
            return;

        IBinder windowToken = view.getWindowToken();
        if (windowToken == null)
            return;

        inputManager.hideSoftInputFromWindow(windowToken, 0);
    }

	public static void hideKeyboard(final Activity activity) {
		if (activity != null) {
			View focusedView = activity.getCurrentFocus();
			if (focusedView == null)
				return;

			hideKeyboard(focusedView, activity);
		}
	}

	public static Point getSizeOfUsableScreen(Context context) {
		
		Point size = new Point();
		WindowManager w = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);

        w.getDefaultDisplay().getSize(size);

		return size;
	}
	
	public static void hideBackButtonFromActionBar(AppCompatActivity activity) {
		ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        //actionBar.setHomeAsUpIndicator(R.drawable.ic_back_hidden);
	}

	public static void updateActionBarTitle(Activity mActivity, String title) {
		AppCompatActivity activity = (AppCompatActivity)mActivity;
		if (activity == null)
			return;
		
		ActionBar actionBar = activity.getSupportActionBar();
		if (actionBar == null)
			return;
		
		actionBar.setTitle(title);
		
		TextView titleView = (TextView) actionBar.getCustomView().findViewById(R.id.text);
		if (titleView == null)
			return;
		
		titleView.setText(title);
	}

    public static int getColorFromAttribute(Context context, int attribute) {
        if (context == null)
            return Color.TRANSPARENT;

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        boolean result = theme.resolveAttribute(attribute, typedValue, true);
        int color = typedValue.data;

        return color;
    }

    public static ColorStateList getColorStateListFromAttribute(Context context, int attribute) {

        Resources.Theme theme = context.getTheme();
        TypedArray typedArray = theme.obtainStyledAttributes(new int[]{ attribute });
        ColorStateList colorStateList = typedArray.getColorStateList(0);

        return colorStateList;
    }

    public static void setTintToDrawable(Drawable drawable, Context context, int attribute) {
        if (drawable == null)
            return;

        drawable.setColorFilter(getColorFromAttribute(context, attribute), PorterDuff.Mode.MULTIPLY);
    }
    
    public static void setTintToToggleButton(ToggleButton toggleButton, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toggleButton.setBackgroundTintList(ColorStateList.valueOf(color));
        } else {
            toggleButton.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        }
    }

    public static void setTintToToggleButton(ToggleButton toggleButton, Context context, int attribute) {
        int color = getColorFromAttribute(context, attribute);
        setTintToToggleButton(toggleButton, color);
    }

    public static void setTintListToDrawable(Drawable drawable, Context context, int attr) {
        ColorStateList colorStateList = getColorStateListFromAttribute(context, attr);
        if (colorStateList != null) {
            DrawableCompat.setTintList(drawable, colorStateList);
        }
    }

    public static String getMilisecAsTimeString(long ms) {
        float totalSeconds = (ms / 1000.0f);

        long hours = (long) (totalSeconds / 3600);
        totalSeconds = totalSeconds - (hours * 3600);

        long minutes = (long) (totalSeconds / 60);
        totalSeconds = totalSeconds - (minutes * 60);

        int seconds = Math.round(totalSeconds);

        String timeString;
        if (hours > 0) {
            timeString = String.format("%d:%02d:%02d", hours, minutes, seconds);
        } else {
            timeString = String.format("%d:%02d", minutes, seconds);
        }

        return timeString;
    }

    public static void showToast(CharSequence message, Context context) {
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    public static void removeViewTreeObserverListener(View view, ViewTreeObserver.OnGlobalLayoutListener listener) {
        ViewTreeObserver treeObserver = view.getViewTreeObserver();
        if (treeObserver.isAlive()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                treeObserver.removeOnGlobalLayoutListener(listener);
            } else {
                treeObserver.removeGlobalOnLayoutListener(listener);
            }
        }
    }

    public static void openLink(String url, Activity activity) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(browserIntent);
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static boolean isClosed(Fragment fragment) {
        return fragment == null || fragment.isDetached() || fragment.getActivity() == null;
    }
}
