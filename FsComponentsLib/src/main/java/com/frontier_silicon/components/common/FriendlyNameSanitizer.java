package com.frontier_silicon.components.common;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.frontier_silicon.FsComponentsLib.R;

public class FriendlyNameSanitizer extends BaseSanitizer {
	public boolean mIsManualChange = false;
	private IFriendlyNameSanitizerListener mSanitizerListener;
    private TextWatcher mTextWatcher;
    private String mAllowedChars;
    private int mMaxLengthOfFriendlyName = -1;

    public interface IFriendlyNameSanitizerListener {
		void onNewFriendlyName(String friendlyName);
		void onSubmitFriendlyName(String friendlyName);
		void onBadFriendlyName(ErrorSanitizerMessageType errorType);
	}

	public FriendlyNameSanitizer(int maxLengthOfFriendlyName) {
        mMaxLengthOfFriendlyName = maxLengthOfFriendlyName;
    }

	public void appendSanitizerToEditText(EditText editText, Context context, IFriendlyNameSanitizerListener sanitizerListener) {
		
		mEditText = editText;
		mContext = context;
		mSanitizerListener = sanitizerListener;
        mAllowedChars = context.getString(R.string.allowed_friendly_name_chars);
        mAllowedChars += "<";

        mTextWatcher = new TextWatcher() {
            int startIndexOfChange = 0;
            int numberOfAddedChars = 0;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                startIndexOfChange = start;
                numberOfAddedChars = count;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                hideError();
            }

            @Override
            public void afterTextChanged(Editable editableText) {
                if (mIsManualChange) {
                    mIsManualChange = false;
                    return;
                }

                if (limitLengthOfFriendlyName(editableText, startIndexOfChange, numberOfAddedChars)) {
                    mSanitizerListener.onBadFriendlyName(ErrorSanitizerMessageType.BiggerThanSupportedLimit);
					return;
				}

                if (removeMultipleWhiteSpacesFromBeginningOfString(editableText)) {
                    // return at this point to not overlap error messages
                    return;
                }



                if (mSanitizerListener != null) {
                   if(editableText.length() == 0) {
                       mSanitizerListener.onBadFriendlyName(ErrorSanitizerMessageType.EmptyName);
                   } else {
                       mSanitizerListener.onNewFriendlyName(editableText.toString());
					   sanitizeCharactersOfFriendlyName(editableText);
                   }
            }

            }
        };
		
		mEditText.addTextChangedListener(mTextWatcher);
		
		mEditText.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					String friendlyName = mEditText.getText().toString().trim();
					
					if (!TextUtils.isEmpty(friendlyName)) {
						if (mSanitizerListener != null) {
                            hideError();
                            mSanitizerListener.onSubmitFriendlyName(friendlyName);
                        }
						return false;
					}
					else {
						if (mSanitizerListener != null)
							mSanitizerListener.onBadFriendlyName(ErrorSanitizerMessageType.EmptyName);
					}
				}
					
				return false;
			}
		});
	}
	
	private boolean limitLengthOfFriendlyName(Editable editableText, int startIndexOfChange, int numberOfAddedChars) {
		return super.limitLengthOfText(editableText, startIndexOfChange, numberOfAddedChars, mMaxLengthOfFriendlyName);


	}
	
	private boolean removeMultipleWhiteSpacesFromBeginningOfString(Editable editableText) {
		boolean firstWhiteSpaceWasAlreadyFound = false;
		boolean multipleWhiteSpacesWereFound = false;
		
		
		for (int i = 0, charIndex = 0; i < editableText.length(); i++) {
			Character c = editableText.charAt(charIndex);
			if (Character.isWhitespace(c)) {
				if (firstWhiteSpaceWasAlreadyFound) {
					editableText.replace(charIndex, charIndex + 1, "");
					multipleWhiteSpacesWereFound = true;
				}
				else {
					firstWhiteSpaceWasAlreadyFound = true;
					charIndex++;
				}
			}
			else
				break;
		}
		
		if (firstWhiteSpaceWasAlreadyFound && editableText.length() == 1 && mSanitizerListener != null) {
			editableText.replace(0, 1, "");
			mSanitizerListener.onBadFriendlyName(ErrorSanitizerMessageType.WhiteSpaceAtBeginning);
			return true;
		}
		
		if (multipleWhiteSpacesWereFound && mSanitizerListener != null) {
			mSanitizerListener.onBadFriendlyName(ErrorSanitizerMessageType.MultipleWhiteSpaces);
			return true;
		}
		
		return false;
	}
	
	private void sanitizeCharactersOfFriendlyName(Editable editableText) {
		boolean invalidCharWasFound = false;
		
		for (int i = 0, charIndex = 0; i < editableText.length(); i++) {
			Character c = editableText.charAt(charIndex);
			if (!characterIsValidForFriendlyName(c)) {
				invalidCharWasFound = true;
				
				editableText.replace(charIndex, charIndex + 1, "");
			}
			else
				charIndex++;
		}
		
		if (invalidCharWasFound)
			mSanitizerListener.onBadFriendlyName(ErrorSanitizerMessageType.CharacterNotAllowed);
		else {
			showWarningInCaseFriendlyNameIsEmpty();
		}
	}
	
	private void showWarningInCaseFriendlyNameIsEmpty() {
		String friendlyName = mEditText.getText().toString();
		if (TextUtils.isEmpty(friendlyName))
			mSanitizerListener.onBadFriendlyName(ErrorSanitizerMessageType.EmptyName);
	}

	private boolean characterIsValidForFriendlyName(Character c) {
		return Character.isLetterOrDigit(c) || mAllowedChars.indexOf(c) != -1;
	}

    @Override
    public void dispose() {
        if (mEditText != null) {
            mEditText.removeTextChangedListener(mTextWatcher);
            mEditText.setOnEditorActionListener(null);
        }

        mSanitizerListener = null;

        super.dispose();
    }
}
