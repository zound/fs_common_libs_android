package com.frontier_silicon.components.common;

public class FsComponentsConfig {
	public static boolean SHOW_ONLY_SUGGESTED_RADIOS = true;
	public static boolean SHOW_EXIT_HUI_SETUP_BUTTON = true;
	
	public static int MAX_SECONDS_TO_CONFIGURE_THE_DEVICE = 30;
	public static int MAX_SECONDS_TO_WAIT_FOR_CONNECTING_TO_AP = 20;
	public static int MAX_SECONDS_TO_LOOK_FOR_CONFIGURED_RADIO = 120;
	public static int MAX_SECOND_TO_CONFIG_WITH_WPS = 5;
	public static int MAX_SECONDS_TO_CONNECT_WITH_WPS = 120;
	public static int MAX_RETRIES_FOR_CHECKING_ISU = 15;
	public static int MILLISECONDS_TO_WAIT_BETWEEN_CHECKING_ISU = 3000;
	public static int MILLISECONDS_TO_WAIT_BETWEEN_NODE_SET = 1000;
	public static int MAX_LENGTH_OF_FRIENDLY_NAME = 32;  // Spotify requirement
    public static int MAX_LENGTH_OF_FRIENDLY_NAME_WHEN_GOOGLE_CAST_IS_PRESENT = 24;
	public static int LENGTH_OF_PIN = 4;
	public static int SECONDS_TO_WAIT_BEFORE_MAKING_NEW_AP_SCAN_WHEN_REQUESTED = 60;
	public static int SECONDS_TO_WAIT_UNTIL_CURRENTLY_CONNECTING_AP_SHOULD_BE_DISMISSED = 30;
	public static int MAX_SECONDS_TO_GET_NEW_LIST_OF_AP_FROM_PHONE = 24;
	/** MAX_NAV_LIST_ITEMS is decreased from 20 to 15 because in
	 * airable we receive only 17 elements (cause of 4k memory limitation )
	 */
    public static int MAX_NAV_LIST_ITEMS = 15;
    public static int MILLISECONDS_TO_WAIT_BEFORE_NAV_STATUS_CHECK = 500;
}
