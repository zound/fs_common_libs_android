package com.frontier_silicon.components.common;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.concurrent.ThreadPoolExecutor;

public class TaskHelper {
	
	public static void initRejectedExecutionHandler() {

        // change reject handler policy  to discard (avoid RejectedExecutionException)
        ((ThreadPoolExecutor) AsyncTask.THREAD_POOL_EXECUTOR).setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy() {

            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
                FsLogger.log("Rejected execution for a async task", LogLevel.Error);
            }
        });
	}
	
	public static <P, T extends AsyncTask<P, ?, ?>> void execute(T task) {
        execute(task, (P[]) null);
    }

    @SuppressLint("NewApi")
    public static <P, T extends AsyncTask<P, ?, ?>> void execute(T task, P... params) {
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
    }
}
