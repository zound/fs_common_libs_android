package com.frontier_silicon.components.common.nodeCommunication;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeList;
import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;

import java.util.ArrayList;
import java.util.List;

public class ListNodeCallback implements IGetNodeCallback {

    private static final int MAX_NAV_STACK_DEPTH = 20;
    private static final int NUM_ALL_LIST_ITEMS = -1;
    private static final int MAX_GET_LIST_NODE_ITEMS = 30;
    private List<NodeListItem> mResultList = new ArrayList<>();
    private Radio mRadio;
    private Class mNodeClass;
    private IListNodeListener mListener;
    private boolean mSynchronous;
    private int mStartKey;
    private int mMaxItems;
    private int mCurrentStackDepth = 0;
    private boolean mIsListComplete = false;
    private  NodeList listNode;

    public ListNodeCallback(Radio radio, Class nodeClass, int startKey, int maxItems, boolean synchronous, IListNodeListener listener) {
        mRadio = radio;
        mNodeClass = nodeClass;
        mStartKey = startKey;
        mMaxItems = maxItems;
        mSynchronous = synchronous;
        mListener = listener;
    }

    public void startRetrieve() {
        String startKey = Integer.toString(mStartKey);

        int maxItemsToGetPerRequest = MAX_GET_LIST_NODE_ITEMS;
        if (mMaxItems < maxItemsToGetPerRequest) {
            maxItemsToGetPerRequest = mMaxItems;
        }

        mRadio.getListNode(mNodeClass, startKey, maxItemsToGetPerRequest, mSynchronous, this);

        if (mSynchronous) {
            while (!mIsListComplete && mRadio != null) {
                mRadio.getListNodeNext(listNode, MAX_GET_LIST_NODE_ITEMS, mSynchronous, this);
            }
        }
    }

    @Override
    public void getNodeResult(NodeInfo node) {
         listNode = (NodeList)node;

        if (listNode != null) {
            mResultList.addAll(listNode.getItems());
        }

        mCurrentStackDepth++;

        if (listNode == null || listNode.IsEndOfList() ||
                listNode.Size() == 0 || (mResultList.size() >= mMaxItems && mMaxItems != NUM_ALL_LIST_ITEMS) ||
                mCurrentStackDepth >= MAX_NAV_STACK_DEPTH) {
            // end of list reached or max items retrieved in one go or stack depth dangerously high

            mIsListComplete = (listNode == null || listNode.IsEndOfList() || listNode.Size() == 0);
            notifyListenerOnComplete(true, mIsListComplete);
            dispose();

        } else {
            if (!mSynchronous) {
                mRadio.getListNodeNext(listNode, MAX_GET_LIST_NODE_ITEMS, mSynchronous, this);
            }
        }
    }

    @Override
    public void getNodeError(Class nodeType, NodeErrorResponse error) {
        boolean success = false;

        ErrorResponse.FSStatusCode code = error.getFSStatus();
        if (code == ErrorResponse.FSStatusCode.FS_LIST_END) {
            //end of list reached (for IR 2.6.*)
            success = true;
        }

        notifyListenerOnComplete(success, false);
        dispose();
    }

    private void notifyListenerOnComplete(boolean success, boolean isListComplete) {
        if (mListener != null) {
            if (success) {
                mListener.onListNodeResult(mResultList, isListComplete);
            } else {
                mListener.onListNodeResult(null, isListComplete);
            }
        }
    }

    private void dispose() {
        mRadio = null;
        mResultList = null;
        mListener = null;
    }
}

