package com.frontier_silicon.components.common;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Node.NodeDefs;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodesCallback;
import com.frontier_silicon.NetRemoteLib.Radio.ISetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.NodeResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.nodeCommunication.IListNodeListener;
import com.frontier_silicon.components.common.nodeCommunication.ListNodeCallback;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import org.xbill.DNS.utils.base16;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RadioNodeUtil {

    public interface NodeExists{
        void onNode(boolean exists);
    }

    static public void waitMs(int waitTimeMS) {
        try {
            Thread.sleep(waitTimeMS);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    static public void waitDefaultMs() {
        waitMs(FsComponentsConfig.MILLISECONDS_TO_WAIT_BETWEEN_NODE_SET);
    }

    static public String getHumanReadableSSID(String ssid) {
        if (TextUtils.isEmpty(ssid))
            return "";

        byte[] ssidBytes = base16.fromString(ssid);
        if (ssidBytes == null)
            return "";

        return new String(ssidBytes);
    }

    static public byte[] encryptPassphrase(String txtToEncrypt, String rsaPubKey) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException {
        Cipher cipher;
        byte[] key;

        cipher = Cipher.getInstance("RSA/2/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, generatePublicKey(rsaPubKey));
        key = cipher.doFinal(txtToEncrypt.getBytes());

        return key;
    }

    static public RSAPublicKey generatePublicKey(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String[] keyParts = publicKey.split(":");
        String exp = keyParts[0];
        String modul = keyParts[1];
        BigInteger modulus = new BigInteger(modul, 16);
        BigInteger pubExp = new BigInteger(exp, 16);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(modulus, pubExp);
        RSAPublicKey key = (RSAPublicKey) keyFactory.generatePublic(pubKeySpec);

        return key;
    }

    static public String getBase16(byte[] valueBytes) {
        return base16.toString(valueBytes);
    }

    static public long getIPNumericValue(String ipAddress) {
        long ipAddr = 0;
        int shiftValue = 24;
        String[] parts = ipAddress.split("\\.");
        try {
            for (String part : parts) {
                long ipGroupValue = Integer.valueOf(part);

                ipAddr = ipAddr + (ipGroupValue << shiftValue);
                shiftValue = shiftValue - 8;
            }
        } catch (Exception e) {
            ipAddr = 0;
        }

        return ipAddr;
    }

    public static int convertPercentVolumeToRadioVolume(int volume, long volSteps) {
        float vol = 0;

        vol = ((float) volume / 100.0f) * (volSteps - 1);

        return (int) (vol + 0.5f);
    }

    public static int convertValueToPercent(long volume, long volSteps) {
        float vol = 0;

        vol = ((float) volume / (float) (volSteps - 1)) * 100.0f;

        return (int) vol;
    }

    public interface INodeResultListener {
        void onNodeResult(NodeInfo node);
    }

    public interface INodeSetResultListener {
        void onNodeSetResult(boolean success);
    }

    public interface INodesResultListener {
        void onNodesResult(Map<Class, NodeInfo> nodes);
    }

    public static void getNodeFromRadioAsync(Radio radio, final Class nodeType, final INodeResultListener listener) {
        radio.getNode(nodeType, false, true, new IGetNodeCallback() {
            @Override
            public void getNodeResult(NodeInfo node) {
                if (listener != null) {
                    listener.onNodeResult(node);
                }
            }

            @Override
            public void getNodeError(Class nodeType, NodeErrorResponse error) {
                if (listener != null) {
                    listener.onNodeResult(null);
                }

                if (error.getFSStatus() == NodeErrorResponse.FSStatusCode.FS_NODE_DOES_NOT_EXIST){
                    FsLogger.log("node not exists: " + nodeType.getName());
                }

            }
        });
    }

    public static void fsNodeExists(Radio radio, Class nodeType, final NodeExists listener) {

        radio.getNode(nodeType, false, true, new IGetNodeCallback() {
            @Override
            public void getNodeResult(NodeInfo node) {
                if (listener != null) {
                    listener.onNode(true);
                }
            }

            @Override
            public void getNodeError(Class nodeType, NodeErrorResponse error) {
                if (listener != null) {

                    FsLogger.log("node not exists: " + nodeType.getName());
                    listener.onNode(error.getFSStatus() != NodeErrorResponse.FSStatusCode.FS_NODE_DOES_NOT_EXIST);
                }
            }
        });

    }

    public static void setNodeToRadioAsync(Radio radio, final NodeInfo node, final INodeSetResultListener listener) {
        radio.setNode(node, false, new ISetNodeCallback() {
            public void setNodeSuccess(NodeInfo node) {
                if (listener != null) {
                    listener.onNodeSetResult(true);
                }
            }

            public void setNodeError(NodeInfo node, NodeErrorResponse error) {
                if (listener != null) {
                    listener.onNodeSetResult(false);
                }
            }
        });
    }

    public static void getNodesFromRadioAsync(Radio radio, final Class[] nodes, boolean forceUncached, final INodesResultListener listener) {
        radio.getNodes(nodes, false, forceUncached, new IGetNodesCallback() {
            @Override
            public void onResult(Map<Class, NodeResponse> nodeResponses) {
                Map<Class, NodeInfo> nodes = new HashMap<>();
                for (Class key : nodeResponses.keySet()) {
                    NodeResponse response = nodeResponses.get(key);
                    if (response.mType == NodeResponse.NodeResponseType.NODE) {
                        nodes.put(key, response.mNodeInfo);
                    } else { //is error
                        FsLogger.log("GetNodesResult: Error " + key + " " + response.mError, LogLevel.Error);
                    }
                }

                if (listener != null) {
                    listener.onNodesResult(nodes);
                }
            }

        });
    }

    public static void getNodesFromRadio(Radio radio, final Class[] nodes, boolean sync,
                                         boolean forceUncached, final INodesResultListener listener) {
        radio.getNodes(nodes, sync, forceUncached, new IGetNodesCallback() {
            @Override
            public void onResult(Map<Class, NodeResponse> nodeResponses) {
                Map<Class, NodeInfo> nodes = new HashMap<>();
                for (Class key : nodeResponses.keySet()) {
                    NodeResponse response = nodeResponses.get(key);
                    if (response.mType == NodeResponse.NodeResponseType.NODE) {
                        nodes.put(key, response.mNodeInfo);
                    } else { //is error
                        FsLogger.log("GetNodesResult: Error " + key + " " + response.mError, LogLevel.Error);
                    }
                }

                if (listener != null) {
                    listener.onNodesResult(nodes);
                }
            }

        });
    }

    public static void getListNodeItems(Radio radio, Class nodeClass, boolean synchronous, IListNodeListener listener) {
        getListNodeItems(radio, nodeClass, NodeDefs.StartIndexForList, NodeDefs.MaxItemsForList, synchronous, listener);
    }

    public static void getListNodeItems(Radio radio, Class nodeClass, int startIdx, int maxItems, boolean synchronous, IListNodeListener listener) {

        ListNodeCallback listNodeCallback = new ListNodeCallback(radio, nodeClass, startIdx, maxItems, synchronous, listener);
        listNodeCallback.startRetrieve();
    }
}
