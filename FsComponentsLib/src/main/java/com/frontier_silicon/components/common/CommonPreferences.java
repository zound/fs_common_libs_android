package com.frontier_silicon.components.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by lsuhov on 18/11/15.
 */
public class CommonPreferences {
    private static CommonPreferences mInstance;

    private final String CURRENT_THEME_KEY = "CURRENT_THEME_KEY";
    private final String LOGGING_ENABLED_KEY = "LOGGING_ENABLED_KEY";
    private final String SPOTIFY_ACCESS_TOKEN_KEY = "SPOTIFY_ACCESS_TOKEN_KEY";
    private final String SPOTIFY_REFRESH_TOKEN_KEY = "SPOTIFY_REFRESH_TOKEN_KEY";
    private final String SPOTIFY_TOKEN_EXPIRATION_TIME_KEY = "SPOTIFY_TOKEN_EXPIRATION_TIME_KEY";
    private final String SPOTIFY_TOKEN_TYPE_KEY = "SPOTIFY_TOKEN_TYPE_KEY";
    private final String CAST_TOS_ACCEPTANCE_KEY = "CAST_TOS_ACCEPTANCE_KEY";
    private final String LAST_CONNECTED_SPEAKER_SERIAL_NUMBER_KEY = "LAST_CONNECTED_SPEAKER_SERIAL_NUMBER_KEY";
    private final String UNLOCKED_SPEAKERS_CHECKING_DATE ="UNLOCKED_SPEAKERS_CHECKIND_DATE";

    public static final long SPOTIFY_TOKEN_EXPIRATION_TIME_DEFAULT_VALUE = -1;

    private Context mContext;

    public static CommonPreferences getInstance() {
        if (mInstance == null) {
            mInstance = new CommonPreferences();
        }
        return mInstance;
    }

    public void init(Context context) {
        mContext = context;
    }

    private SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    private SharedPreferences.Editor getEditor() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.edit();
    }

    public int getCurrentThemeIndex() {
        SharedPreferences preferences = getPreferences();

        return preferences.getInt(CURRENT_THEME_KEY, 0);
    }

    public void setCurrentThemeIndex(int themeIndex) {
        SharedPreferences.Editor editor = getEditor();

        editor.putInt(CURRENT_THEME_KEY, themeIndex).commit();
    }

    public boolean getLoggingEnabled() {
        SharedPreferences preferences = getPreferences();

        return preferences.getBoolean(LOGGING_ENABLED_KEY, false);
    }

    public void setLoggingEnabled(boolean enabled) {
        SharedPreferences.Editor editor = getEditor();

        editor.putBoolean(LOGGING_ENABLED_KEY, enabled).commit();
    }

    public void setSpotifyAccessToken(String accessToken) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(SPOTIFY_ACCESS_TOKEN_KEY, accessToken).apply();
    }

    public String getSpotifyAccessToken() {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(SPOTIFY_ACCESS_TOKEN_KEY, "");
    }

    public void setSpotifyRefreshToken(String refreshToken) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(SPOTIFY_REFRESH_TOKEN_KEY, refreshToken).apply();
    }

    public String getSpotifyRefreshToken() {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(SPOTIFY_REFRESH_TOKEN_KEY, "");
    }

    public void setSpotifyTokenExpirationTime(long time) {
        SharedPreferences.Editor editor = getEditor();
        editor.putLong(SPOTIFY_TOKEN_EXPIRATION_TIME_KEY, time);
        editor.apply();
    }

    public long getSpotifyTokenExpirationTime() {
        SharedPreferences preferences = getPreferences();
        return preferences.getLong(SPOTIFY_TOKEN_EXPIRATION_TIME_KEY, SPOTIFY_TOKEN_EXPIRATION_TIME_DEFAULT_VALUE);
    }

    public void setSpotifyTokenType(String tokenType) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(SPOTIFY_TOKEN_TYPE_KEY, tokenType);
        editor.apply();
    }

    public String getSpotifyTokenType() {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(SPOTIFY_TOKEN_TYPE_KEY, "");
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(key, value).commit();
    }

    public String getString(String key) {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(key, "");
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        SharedPreferences preferences = getPreferences();
        return preferences.getBoolean(key, defaultValue);
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(key, value).commit();
    }

    public void setCastTosAcceptance(boolean enable) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(CAST_TOS_ACCEPTANCE_KEY, enable).commit();
    }

    public boolean getCastTosAcceptance() {
        SharedPreferences preferences = getPreferences();
        return preferences.getBoolean(CAST_TOS_ACCEPTANCE_KEY, false);
    }

    public void setLastConnectedDeviceSN(String serialNumber) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(LAST_CONNECTED_SPEAKER_SERIAL_NUMBER_KEY, serialNumber).apply();
    }

    public String getLastConnectedDeviceSN() {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(LAST_CONNECTED_SPEAKER_SERIAL_NUMBER_KEY, "");
    }

    public void setCheckingDateForUnlockedSpeakers(String key, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(UNLOCKED_SPEAKERS_CHECKING_DATE + key, value).apply();
    }

    public String getCheckingDateForUnlocekdSpeakers(String key) {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(UNLOCKED_SPEAKERS_CHECKING_DATE + key, "");
    }
}
