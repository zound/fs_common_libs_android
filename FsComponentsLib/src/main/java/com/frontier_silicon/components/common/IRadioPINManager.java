package com.frontier_silicon.components.common;

import android.content.Context;

public interface IRadioPINManager {

	void setPIN(String radioSerialNumber, String PIN);
	void removePIN(String radioSerialNumber);
	boolean isPINSaved(String radioSerialNumber);
	String getPIN(String radioSerialNumber);
	
	void savePINs(Context context);
	void restorePINs(Context context);
	
}
