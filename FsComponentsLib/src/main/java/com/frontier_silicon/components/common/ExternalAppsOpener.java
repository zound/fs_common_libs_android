package com.frontier_silicon.components.common;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.frontier_silicon.FsComponentsLib.R;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

public class ExternalAppsOpener {

    public static boolean openGoogleHome(Activity activity) {
        return openGoogleCast(activity);
    }

    public static boolean openGoogleHomeForSpeakerSettings(Activity activity, Radio radio) {
        if (activity != null && radio != null) {
            String packageName = activity.getString(R.string.google_cast_package_name);
            boolean isInstalled = isAppInstalled(packageName, activity);
            if (isInstalled){
                packageName = activity.getString(R.string.google_home_package_name_device_settings);
                Intent intent = new Intent(packageName);
                intent.putExtra("com.google.android.apps.chromecast.app.extra.IP_ADDRESS", radio.getIpAddress());
                activity.startActivity(intent);
            } else {
                openAppInStore(packageName, activity);
            }
        }

        return false;
    }

    public static boolean openSpotify(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.spotify_app_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }
    public static boolean openSpotify(String packageName,String marketUrl, Activity activity) {
        if (activity != null) {
            return tryOpenApp(packageName, marketUrl, activity);
        }

        return false;
    }

    public static boolean openPandora(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.pandora_app_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }

    public static void openSpotifyInStore(Activity activity) {
        String packageName = activity.getString(R.string.spotify_app_package_name);
        openAppInStore(packageName, activity);
    }

    public static boolean openDeezer(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.deezer_app_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }

    public static boolean openYoutube(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.youtube_app_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }

    public static boolean openTidal(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.tidal_app_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }

    public static boolean openGoogleCast(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.google_cast_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }

    public static boolean openGooglePlay(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.google_play_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }

    public static boolean openNprone(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.nprone_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }

    public static boolean openTunein(Activity activity) {
        if (activity != null) {
            String packageName = activity.getString(R.string.tunein_app_package_name);
            return tryOpenApp(packageName, activity);
        }

        return false;
    }

    private static boolean tryOpenApp(String packageName, Activity activity) {
        boolean appStarted = false;

        if (activity != null) {
            boolean isInstalled = isAppInstalled(packageName, activity);

            if (isInstalled) {
                appStarted = startApp(packageName, activity);
            } else {
                openAppInStore(packageName, activity);
            }
        }

        return appStarted;
    }

    private static boolean tryOpenApp(String packageName, String marketUrl, Activity activity) {
        boolean appStarted = false;

        if (activity != null) {
            boolean isInstalled = isAppInstalled(packageName, activity);

            if (isInstalled) {
                appStarted = startApp(packageName, activity);
            } else {
                openAppInStore(packageName, marketUrl, activity);
            }
        }

        return appStarted;
    }

    private static void openAppInStore(String packageName, String marketUrl, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(marketUrl + packageName));
        activity.startActivity(intent);
    }

    private static void openAppInStore(String packageName, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String playStoreBaseURL = activity.getString(R.string.google_play_market_url);
        intent.setData(Uri.parse(playStoreBaseURL + packageName));
        activity.startActivity(intent);
    }

    public static boolean startApp(String packageName, Activity activity) {
        boolean appStared = false;
        PackageManager pm = activity.getPackageManager();
        try {
            Intent i = pm.getLaunchIntentForPackage(packageName);
            if (i == null)
                throw new PackageManager.NameNotFoundException();
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            activity.startActivity(i);
            appStared = true;
        } catch (PackageManager.NameNotFoundException e) {
            FsLogger.log(packageName + " is not installed", LogLevel.Error);
        }

        return appStared;
    }

    public static boolean isAppInstalled(String uri, Activity activity) {
        boolean isInstalled;

        PackageManager pm = activity.getPackageManager();

        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            isInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            isInstalled = false;
        }
        return isInstalled;
    }
}
