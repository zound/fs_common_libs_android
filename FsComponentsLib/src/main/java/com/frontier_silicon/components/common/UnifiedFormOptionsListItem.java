package com.frontier_silicon.components.common;

import com.frontier_silicon.NetRemoteLib.Node.BaseNavContextFormOption;
import com.frontier_silicon.NetRemoteLib.Node.BaseNavFormOption;

/**
 * Created by adanaila on 16/11/2016.
 */

public class UnifiedFormOptionsListItem {
    private String name;
    private Long key;

    public UnifiedFormOptionsListItem(BaseNavFormOption.ListItem listItm){
        this.name = listItm.getName();
        this.key = listItm.getKey();
    }

    public UnifiedFormOptionsListItem(BaseNavContextFormOption.ListItem listItm){
        this.name = listItm.getName();
        this.key = listItm.getKey();
    }

    public String getName() { return this.name; }
    public Long getKey() { return this.key; }
}
