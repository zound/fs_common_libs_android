package com.frontier_silicon.components.common;

public interface IDialogButtonsResponse {

    void onPositiveClicked();
    void onNegativeClicked();
    void onNeutralClicked();

}
