package com.frontier_silicon.components.common.nodeCommunication;

import java.util.List;

public interface IListNodeListener<NodeListItem> {
    void onListNodeResult(List<NodeListItem> resultList, boolean isListComplete);
}
