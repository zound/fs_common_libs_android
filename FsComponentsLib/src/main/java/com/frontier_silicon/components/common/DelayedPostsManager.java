package com.frontier_silicon.components.common;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbalazs on 09/11/2016.
 */
public class DelayedPostsManager {

    public interface IPostCallback {
        void onPost(long callbackID);
    }

    private static DelayedPostsManager mInstance;

    private static long CURRENT_UNASSIGNED_ID = 0;

    private Handler mHandler;

    @SuppressLint("UseSparseArrays")
    private final Map<Long, DelayedPostRunnable> mTimerCallbacks = new HashMap<>();

    private final List<Long> mRegisteredTimerIDs = new ArrayList<>();

    private DelayedPostsManager() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    public static DelayedPostsManager getInstance() {
        if (mInstance == null) {
            mInstance = new DelayedPostsManager();
        }
        return mInstance;
    }

    /**
     * Generates a new callback id and registers it in the internal database
     * @return callback id
     */
    public synchronized final long registerNewCallbackID() {
        long callbackID = CURRENT_UNASSIGNED_ID++;

        mRegisteredTimerIDs.add(callbackID);

        return callbackID;
    }

    /**
     * Function for scheduling a delayed post
     */
    public synchronized final void postDelayed(IPostCallback listener, long callbackID, long delayMillis) {
        if (listener != null && mRegisteredTimerIDs.contains(callbackID)) {
            cancelCallback(callbackID);

            DelayedPostRunnable delayedPostRunnable = new DelayedPostRunnable(listener, callbackID);
            mTimerCallbacks.put(callbackID, delayedPostRunnable);
            mHandler.postDelayed(delayedPostRunnable, delayMillis);
        }
    }

    /**
     * Function for scheduling a delayed post
     */
    public synchronized final void postDelayed(Runnable runnable, long callbackID, long delayMillis) {
        if (runnable != null && mRegisteredTimerIDs.contains(callbackID)) {
            cancelCallback(callbackID);

            DelayedPostRunnable delayedPostRunnable = new DelayedPostRunnable(runnable, callbackID);
            mTimerCallbacks.put(callbackID, delayedPostRunnable);
            mHandler.postDelayed(delayedPostRunnable, delayMillis);
        }
    }

    public synchronized final void postDelayed(Runnable runnable, long delayMillis) {
        long callbackId = registerNewCallbackID();

        postDelayed(runnable, callbackId, delayMillis);
    }

    /**
     * Removes callbackId and the callback in case it's registered
     */
    public synchronized final void removeCallbackId(long callbackID){
        int index = mRegisteredTimerIDs.indexOf(callbackID);
        if (index >= 0) {
            cancelCallback(callbackID);
            mRegisteredTimerIDs.remove(index);
        }
    }

    /**
     * Cancels a registered callback associated with a callback id
     */
    public synchronized void cancelCallback(long callbackID) {
        if (mTimerCallbacks.containsKey(callbackID)){
            Runnable runnable = mTimerCallbacks.remove(callbackID);
            mHandler.removeCallbacks(runnable);
        }
    }

    private class DelayedPostRunnable implements Runnable {

        private IPostCallback mListener;
        private Runnable mRunnable;
        private long mCallbackID;

        DelayedPostRunnable(IPostCallback listener, long callbackID) {
            mListener = listener;
            mCallbackID = callbackID;
        }

        DelayedPostRunnable(Runnable runnable, long callbackID) {
            mRunnable = runnable;
            mCallbackID = callbackID;
        }

        @Override
        public void run() {
            mTimerCallbacks.remove(mCallbackID);

            if (mListener != null) {
                mListener.onPost(mCallbackID);
            }

            if (mRunnable != null) {
                mRunnable.run();
            }

            mListener = null;
            mRunnable = null;
        }
    }
}
