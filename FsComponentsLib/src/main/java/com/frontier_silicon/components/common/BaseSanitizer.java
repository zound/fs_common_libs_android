package com.frontier_silicon.components.common;

import android.content.Context;
import android.text.Editable;
import android.widget.EditText;

public class BaseSanitizer {
	
	protected EditText mEditText;
	protected Context mContext;
	
	protected boolean limitLengthOfText(Editable editableText, int startIndexOfChange, int numberOfAddedChars,
			int maxLength) {

		if (editableText.length() <= maxLength) {

			return false;
		}
		
		int exceedingSize = editableText.length() - maxLength;
		if (exceedingSize <= 0) {
			return false;
		}

		replaceLastCharacters(editableText, maxLength);

		return true;
	}

	private void replaceLastCharacters(Editable editable, int maxSize) {
		for (int i = maxSize; i < editable.length(); i++) {
			editable.replace(i, i + 1, "");
		}
	}

	public void hideError() {
        mEditText.setError(null);
    }

    public void dispose() {
        mContext = null;
        mEditText = null;
    }
}
