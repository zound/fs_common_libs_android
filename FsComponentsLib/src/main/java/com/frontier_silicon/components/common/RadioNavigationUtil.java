package com.frontier_silicon.components.common;

import com.frontier_silicon.NetRemoteLib.Node.BaseNavContextFormItem;
import com.frontier_silicon.NetRemoteLib.Node.BaseNavContextFormOption;
import com.frontier_silicon.NetRemoteLib.Node.BaseNavContextList;
import com.frontier_silicon.NetRemoteLib.Node.BaseNavFormOption;
import com.frontier_silicon.NetRemoteLib.Node.BaseNavList;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavActionContext;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavActionNavigate;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavContextFormItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavContextFormOption;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavContextList;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavContextNavigate;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavContextStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavFormItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavFormOption;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavList;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavSearchTerm;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavState;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavStatus;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.nodeCommunication.NavListCallback;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.List;

public class RadioNavigationUtil {

    private static RadioNavigationUtil mInstance;

    private static final String START_ITEM_IDX = "-1";
    public static final Long INVALID_INT32_KEY = 0xffffffffL;

    private static final long MAX_WAIT_NAV_ROOT_RESET_TIME_MS = 30_000;

    private RadioNavigationUtil() {}

    public static RadioNavigationUtil getInstance() {
        if (mInstance == null) {
            mInstance = new RadioNavigationUtil();
        }

        return mInstance;
    }

    public interface IEnableRadioRequestStatusListener{
        void onEnableFinished(NodeNavStatus nodeNavStatus);
    }

    public static NodeNavStatus resetNavigationToRootIfNeeded(Radio radio) {
        boolean forceUncached = true;
        NodeNavStatus statusNode = (NodeNavStatus) radio.getNodeSyncGetter(
                NodeNavStatus.class, forceUncached).get();

        if (statusNode != null && (statusNode.getValueEnum() == NodeNavStatus.Ord.READY || statusNode.getValueEnum() == NodeNavStatus.Ord.READY_ROOT)) {
            return statusNode;
        } else if (statusNode != null && statusNode.getValueEnum() == NodeNavStatus.Ord.WAITING) {
            return waitForNavStatusReady(radio);
        } else {
            return resetNavigationToRoot(radio);
        }
    }

    public static NodeNavStatus resetNavigationToRoot(Radio radio) {
        radio.setNode(new NodeNavState(NodeNavState.Ord.OFF), true);
        radio.setNode(new NodeNavState(NodeNavState.Ord.ON), true);

        return waitForNavStatusReady(radio);
    }

    public static NodeNavStatus waitForNavStatusReady(Radio radio) {
        boolean forceUncached = true;
        int currentNumberOfTimeouts = 0;
        NodeNavStatus statusNode = (NodeNavStatus) radio.getNodeSyncGetter(NodeNavStatus.class, forceUncached).get();

        int count = 0;
        int waitTimeMs = FsComponentsConfig.MILLISECONDS_TO_WAIT_BEFORE_NAV_STATUS_CHECK;
        int maxWaitCount = (int) (MAX_WAIT_NAV_ROOT_RESET_TIME_MS / waitTimeMs);

        while (((statusNode == null && currentNumberOfTimeouts < 2) || (statusNode != null &&
                statusNode.getValueEnum() == NodeNavStatus.Ord.WAITING)) &&
                count < maxWaitCount) {

            if (statusNode == null) {
                currentNumberOfTimeouts++;
            }

            statusNode = (NodeNavStatus) radio.getNodeSyncGetter(NodeNavStatus.class, forceUncached).get();

            RadioNodeUtil.waitMs(waitTimeMs);
            count++;
        }

        return statusNode;
    }

    static public void enableNavigationAsync(final Radio radio, final IEnableRadioRequestStatusListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                NodeNavStatus nodeNavStatus = enableNavigation(radio);
                listener.onEnableFinished(nodeNavStatus);
            }
        }).start();
    }

    static public NodeNavStatus enableNavigation(Radio radio) {
        boolean forceUncached = true;
        NodeNavStatus statusNode = (NodeNavStatus) radio.getNodeSyncGetter(
                NodeNavStatus.class, forceUncached).get();

        if (statusNode != null && (statusNode.getValueEnum() == NodeNavStatus.Ord.READY || statusNode.getValueEnum() == NodeNavStatus.Ord.READY_ROOT)) {
            return statusNode;
        } else if (statusNode != null && statusNode.getValueEnum() == NodeNavStatus.Ord.WAITING) {
            return waitForNavStatusReady(radio);
        } else {
            radio.setNode(new NodeNavState(NodeNavState.Ord.ON), true);
            return waitForNavStatusReady(radio);
        }
    }

    public boolean navigateUp(Radio radio) {
        disposeNavListRequest();

        boolean result = radio.getNodeSyncSetter(new NodeNavActionNavigate(INVALID_INT32_KEY)).set();

        waitForNavStatusReady(radio);

        return result;
    }

    public boolean navigateNavItem(Radio radio, long keyId) {
        disposeNavListRequest();

        boolean result = radio.getNodeSyncSetter(new NodeNavActionNavigate(keyId)).set();

        waitForNavStatusReady(radio);

        return result;
    }

    private void disposeNavListRequest() {
        if (mNavListCallback != null) {
            mNavListCallback.dispose();
        }
    }

    private static ArrayList<UnifiedFormOptionsListItem> mNavOptionListItems = null;
    private static NavOptionListCallback mNavOptionListCallback;

    static public synchronized ArrayList<UnifiedFormOptionsListItem> getNavOptionList(final Radio radio) {
        int listParam = -1;
        mNavOptionListItems = new ArrayList<>();

        if (radio != null) {
            mNavOptionListCallback = new NavOptionListCallback();
            radio.getListNode(NodeNavFormOption.class, ""+listParam, listParam, true, mNavOptionListCallback);
        }

        mNavOptionListCallback = null;
        return mNavOptionListItems;
    }

    static private class NavOptionListCallback implements IGetNodeCallback {
        private boolean mIsDisposed = false;
        private Object mLock = new Object();

        public NavOptionListCallback() {
        }

        @Override
        public void getNodeResult(NodeInfo node) {
            synchronized (mLock) {
                if (mIsDisposed)
                    return;

                if (mNavOptionListItems == null) {
                    mNavOptionListItems = new ArrayList<>();
                }
                if (node == null) {
                    return;
                }

                NodeNavFormOption nodeNavOptionList = (NodeNavFormOption)node;

                List<BaseNavFormOption.ListItem> items = nodeNavOptionList.getEntries();
                for (BaseNavFormOption.ListItem item : items) {
                    mNavOptionListItems.add(new UnifiedFormOptionsListItem(item));

                    FsLogger.log(">> NavForm: " + item.getName() + " key " + item.getKey(), LogLevel.Warning);
                }
            }
        }

        @Override
        public void getNodeError(Class nodeType, NodeErrorResponse error) {
            FsLogger.log("NavListCallback: getNodeError " + error, LogLevel.Error);

            ErrorResponse.FSStatusCode code = error.getFSStatus();
            if (code == ErrorResponse.FSStatusCode.FS_LIST_END) {
                //end of list reached (for IR 2.6.*)
            } else {
                if (mIsDisposed)
                    return;

                mNavOptionListItems = null;
            }

            if (code == ErrorResponse.FSStatusCode.FS_NODE_BLOCKED) {
                //some
            }
        }

        public void dispose() {
            synchronized (mLock) {
                mIsDisposed = true;
            }
        }
    }

    private static ArrayList<UnifiedFormListItem> mNavFormListItems = null;
    private static NavFormListCallback mNavFormListCallback;

    static public synchronized ArrayList<UnifiedFormListItem> getNavFormList(final Radio radio) {
        int listParam = -1;
        mNavFormListItems = new ArrayList<>();

        if (radio != null) {
            mNavFormListCallback = new NavFormListCallback();
            radio.getListNode(NodeNavFormItem.class, ""+listParam, listParam, true, mNavFormListCallback);
        }

        return mNavFormListItems;
    }

    static private class NavFormListCallback implements IGetNodeCallback {
        private boolean mIsDisposed = false;
        private Object mLock = new Object();

        public NavFormListCallback() {
        }

        @Override
        public void getNodeResult(NodeInfo node) {
            synchronized (mLock) {
                if (mIsDisposed)
                    return;

                if (mNavFormListItems == null) {
                    mNavFormListItems = new ArrayList<>();
                }
                if (node == null) {
                    return;
                }

                NodeNavFormItem nodeNavFormList = (NodeNavFormItem)node;

                List<NodeNavFormItem.ListItem> items = nodeNavFormList.getEntries();
                for (NodeNavFormItem.ListItem item : items) {
                    mNavFormListItems.add(new UnifiedFormListItem(item));

                    FsLogger.log(">> NavForm: " + item.getLabel() + " key " + item.getKey(), LogLevel.Warning);
                }
            }
        }

        @Override
        public void getNodeError(Class nodeType, NodeErrorResponse error) {
            FsLogger.log("NavListCallback: getNodeError " + error, LogLevel.Error);

            ErrorResponse.FSStatusCode code = error.getFSStatus();
            if (code == ErrorResponse.FSStatusCode.FS_LIST_END) {
                //end of list reached (for IR 2.6.*)
            } else {
                if (mIsDisposed)
                    return;

                mNavFormListItems = null;
            }
        }

        public void dispose() {
            synchronized (mLock) {
                mIsDisposed = true;
            }
        }
    }

    private NavListCallback mNavListCallback;

    public synchronized List<BaseNavList.ListItem> getNavList(final Radio radio, String startKey,
                                                                          boolean firstPageOnly) {
        List<NodeNavList.ListItem> navListItems = new ArrayList<>();

        if (radio != null) {
            mNavListCallback = new NavListCallback(radio, startKey, firstPageOnly);
            navListItems = mNavListCallback.getNavList();

            mNavListCallback.dispose();
        }

        mNavListCallback = null;
        return navListItems;
    }


    public static void searchNavItem(Radio radio, String searchString, long keyId) {
        if (radio == null) {
            FsLogger.log("Radio was null in searchNavItem");
            return;
        }
        NodeInfo[] nodesToWrite = new NodeInfo[]{
                new NodeNavSearchTerm(searchString),
                new NodeNavActionNavigate(keyId)
        };

        radio.setNodes(nodesToWrite, true);

        waitForNavStatusReady(radio);
    }

    private static void waitForContextStatusReady(Radio radio) {
        boolean forceUncached = true;
        NodeNavContextStatus statusNode = (NodeNavContextStatus) radio.
                getNodeSyncGetter(NodeNavContextStatus.class, forceUncached).get();

        int count = 0;
        int waitTimeMs = FsComponentsConfig.MILLISECONDS_TO_WAIT_BEFORE_NAV_STATUS_CHECK;
        int maxWaitCount = (int) (MAX_WAIT_NAV_ROOT_RESET_TIME_MS / waitTimeMs);
        while (statusNode != null &&
                statusNode.getValueEnum() == NodeNavContextStatus.Ord.WAITING &&
                count < maxWaitCount) {

            statusNode = (NodeNavContextStatus) radio.getNodeSyncGetter(NodeNavContextStatus.class, forceUncached).get();

            RadioNodeUtil.waitMs(waitTimeMs);
            count++;
        }
    }

    public boolean navigateContextUp(Radio radio) {
        disposeNavListRequest();

        boolean result = radio.getNodeSyncSetter(new NodeNavContextNavigate(INVALID_INT32_KEY)).set();

        waitForContextStatusReady(radio);

        return result;
    }

    public boolean navigateContextItem(Radio radio, long keyId) {
        disposeNavListRequest();

        boolean result = radio.getNodeSyncSetter(new NodeNavContextNavigate(keyId)).set();

        waitForContextStatusReady(radio);

        return result;
    }

    public boolean activateContextItem(Radio radio, long keyId) {
        disposeNavListRequest();

        boolean result = radio.getNodeSyncSetter(new NodeNavActionContext(keyId)).set();

        waitForContextStatusReady(radio);

        return result;
    }

    private static boolean mContextFormListComplete = false;
    private static ArrayList<UnifiedFormListItem> mContextFormListItems = null;
    private static ContextFormListCallback mContextFormListCallback;

    static public synchronized ArrayList<UnifiedFormListItem> getContextFormList(final Radio radio) {
        int listParam = -1;
        mContextFormListItems = new ArrayList<>();

        if (radio != null) {
            mContextFormListCallback = new ContextFormListCallback();
            radio.getListNode(NodeNavContextFormItem.class, ""+listParam, listParam, true, mContextFormListCallback);
        }

        mContextFormListCallback = null;
        return mContextFormListItems;
    }

    static private class ContextFormListCallback implements IGetNodeCallback {
        private boolean mIsDisposed = false;
        private Object mLock = new Object();

        public ContextFormListCallback() {
        }

        @Override
        public void getNodeResult(NodeInfo node) {
            synchronized (mLock) {
                if (mIsDisposed)
                    return;

                if (mContextFormListItems == null) {
                    mContextFormListItems = new ArrayList<>();
                }
                if (node == null) {
                    return;
                }

                NodeNavContextFormItem nodeNavContextFormList = (NodeNavContextFormItem)node;

                List<BaseNavContextFormItem.ListItem> items = nodeNavContextFormList.getEntries();
                for (BaseNavContextFormItem.ListItem item : items) {
                    mContextFormListItems.add(new UnifiedFormListItem(item));

                    FsLogger.log(">> NavForm: " + item.getLabel() + " key " + item.getKey(), LogLevel.Warning);
                }
            }
        }

        @Override
        public void getNodeError(Class nodeType, NodeErrorResponse error) {
            FsLogger.log("NavListCallback: getNodeError " + error, LogLevel.Error);

            ErrorResponse.FSStatusCode code = error.getFSStatus();
            if (code == ErrorResponse.FSStatusCode.FS_LIST_END) {
                //end of list reached (for IR 2.6.*)
            } else {
                if (mIsDisposed)
                    return;

                mContextFormListItems = null;
            }

            if (code == ErrorResponse.FSStatusCode.FS_NODE_BLOCKED) {
                mContextFormListComplete = false;
            }
        }

        public void dispose() {
            synchronized (mLock) {
                mIsDisposed = true;
            }
        }
    }


    private static boolean mContextListComplete = false;
    private static ArrayList<BaseNavContextList.ListItem> mContextListItems = null;
    private static ContextListCallback mContextListCallback;

    static public synchronized ArrayList<BaseNavContextList.ListItem> getContextList(final Radio radio, String startKey,
                                                                                     boolean firstPageOnly) {
        mContextListItems = new ArrayList<>();
        mContextListComplete = false;

        if (radio != null) {
            mContextListCallback = new ContextListCallback(radio, firstPageOnly);
            radio.getListNode(NodeNavContextList.class, startKey, FsComponentsConfig.MAX_NAV_LIST_ITEMS, true,
                    mContextListCallback);
        }

        return mContextListItems;
    }
    static private class ContextListCallback implements IGetNodeCallback {

        private static final int MAX_NAV_STACK_DEPTH = 20;
        private Radio mRadio;
        private boolean mIsDisposed = false;
        private Object mLock = new Object();
        private int mCurrentStackDepth = 0;
        private boolean mFirstPageOnly = false;

        public ContextListCallback(Radio radio, boolean firstPageOnly) {
            mRadio = radio;
            mFirstPageOnly = firstPageOnly;
        }

        @Override
        public void getNodeResult(NodeInfo node) {
            synchronized (mLock) {
                if (mIsDisposed)
                    return;

                if (mContextListItems == null) {
                    mContextListItems = new ArrayList<>();
                }

                NodeNavContextList navListNode = (NodeNavContextList) node;

                addItemsFromNodeList(node);

                mCurrentStackDepth++;

                if (navListNode == null || navListNode.IsEndOfList() ||
                        navListNode.Size() == 0 ||
                        mCurrentStackDepth >= MAX_NAV_STACK_DEPTH ||
                        mFirstPageOnly) {
                    // end of list reached or max items retrieved in one go or stack depth dangerously high

                    mContextListComplete = (navListNode == null || navListNode.IsEndOfList() || navListNode.Size() == 0);
                } else {
                    // get next set of items

                    FsLogger.log("== NAV GET NEXT SET ==", LogLevel.Error);

                    waitForContextStatusReady(mRadio);
                    mRadio.getListNodeNext(navListNode, FsComponentsConfig.MAX_NAV_LIST_ITEMS, true, this);
                }
            }
        }

        private void addItemsFromNodeList(NodeInfo node) {
            if (node == null) {
                return;
            }

            NodeNavContextList nodeNavList = (NodeNavContextList)node;

            List<BaseNavContextList.ListItem> items = nodeNavList.getEntries();
            for (BaseNavContextList.ListItem item : items) {
                mContextListItems.add(item);

                FsLogger.log(">> NAV: " + item.getName() + " key " + item.getKey(), LogLevel.Warning);
            }
        }

        @Override
        public void getNodeError(Class nodeType, NodeErrorResponse error) {
            FsLogger.log("NavListCallback: getNodeError " + error, LogLevel.Error);

            ErrorResponse.FSStatusCode code = error.getFSStatus();
            if (code == ErrorResponse.FSStatusCode.FS_LIST_END) {
                //end of list reached (for IR 2.6.*)
            } else {
                if (mIsDisposed)
                    return;

                mContextListItems = null;
            }

            mContextListComplete = true;

            if (code == ErrorResponse.FSStatusCode.FS_NODE_BLOCKED) {
                mContextListComplete = false;
            }
        }

        public void dispose() {
            synchronized (mLock) {
                mRadio = null;
                mIsDisposed = true;
            }
        }
    }

    static public boolean isContextListComplete() {
        return mContextListComplete;
    }


    private static ArrayList<UnifiedFormOptionsListItem> mContextOptionListItems = null;
    private static ContextOptionListCallback mContextOptionListCallback;

    static public synchronized ArrayList<UnifiedFormOptionsListItem> getContextOptionList(final Radio radio) {
        int listParam = -1;
        mContextOptionListItems = new ArrayList<>();

        if (radio != null) {
            mContextOptionListCallback = new ContextOptionListCallback();
            radio.getListNode(NodeNavContextFormOption.class, ""+listParam, listParam, true, mContextOptionListCallback);
        }

        mContextOptionListCallback = null;
        return mContextOptionListItems;
    }

    static private class ContextOptionListCallback implements IGetNodeCallback {
        private boolean mIsDisposed = false;
        private Object mLock = new Object();

        public ContextOptionListCallback() {
        }

        @Override
        public void getNodeResult(NodeInfo node) {
            synchronized (mLock) {
                if (mIsDisposed)
                    return;

                if (mContextOptionListItems == null) {
                    mContextOptionListItems = new ArrayList<>();
                }
                if (node == null) {
                    return;
                }

                NodeNavContextFormOption nodeNavOptionList = (NodeNavContextFormOption)node;

                List<BaseNavContextFormOption.ListItem> items = nodeNavOptionList.getEntries();
                for (BaseNavContextFormOption.ListItem item : items) {
                    mContextOptionListItems.add(new UnifiedFormOptionsListItem(item));

                    FsLogger.log(">> NavForm: " + item.getName() + " key " + item.getKey(), LogLevel.Warning);
                }
            }
        }

        @Override
        public void getNodeError(Class nodeType, NodeErrorResponse error) {
            FsLogger.log("NavListCallback: getNodeError " + error, LogLevel.Error);

            ErrorResponse.FSStatusCode code = error.getFSStatus();
            if (code == ErrorResponse.FSStatusCode.FS_LIST_END) {
                //end of list reached (for IR 2.6.*)
            } else {
                if (mIsDisposed)
                    return;

                mContextOptionListItems = null;
            }

            if (code == ErrorResponse.FSStatusCode.FS_NODE_BLOCKED) {
                //some
            }
        }

        public void dispose() {
            synchronized (mLock) {
                mIsDisposed = true;
            }
        }
    }
}
