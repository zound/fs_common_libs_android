package com.frontier_silicon.components.connection;

public class ConnectionStateModel {

    public boolean mRadiosSearching = false;
    public boolean mRadiosFound = false;
    public boolean mSearchingTimeElapsed = false;
    public boolean mConnectToRadioInAPMode = false;
    public boolean mRadioConnected = false;
    public boolean mRadioConnectionLost = false;
    public boolean mRadioInvalidSession = false;
    public boolean mPinError = false;
    public boolean mNotificationTimeout = false;
}
