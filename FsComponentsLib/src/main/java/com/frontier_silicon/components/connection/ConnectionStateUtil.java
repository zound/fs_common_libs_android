package com.frontier_silicon.components.connection;

import android.os.Handler;

import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IDiscoveryScanner;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanListener;
import com.frontier_silicon.NetRemoteLib.IWifiConnection;
import com.frontier_silicon.NetRemoteLib.Radio.ConnectionErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.IConnectionCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.NetRemoteLib.Radio.TransportErrorDetail;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class ConnectionStateUtil implements IConnectionCallback,
        IScanListener, IRadioDiscoveryListener {

    private static ConnectionStateUtil mInstance;

    private CopyOnWriteArrayList<IConnectionStateListener> mConnectionStateListeners;

    private ConnectionStateModel mConnectionStateModel;

    private ConnectionState mCurrentConnectionState = ConnectionState.NOT_CONNECTED_TO_RADIO;

    private static final long RADIO_SEARCH_TIMEOUT_MS = 15_000;

    private Timer mFindRadiosTimeoutTimer;


    static public ConnectionStateUtil getInstance() {
        if (mInstance == null) {
            mInstance = new ConnectionStateUtil();
        }

        return mInstance;
    }

    private ConnectionStateUtil() {
        mConnectionStateListeners = new CopyOnWriteArrayList<>();
        mConnectionStateModel = new ConnectionStateModel();
    }

    public void init() {

        registerNetworkListeners();

        NetRemoteManager.getInstance().removeConnectionListener(this);
        NetRemoteManager.getInstance().addConnectionListener(this);

        NetRemoteManager.getInstance().removeScanListener(this);
        NetRemoteManager.getInstance().addScanListener(this);

        NetRemoteManager.getInstance().removeRadioDiscoveryListener(this);
        NetRemoteManager.getInstance().addRadioDiscoveryListener(this);

        initInitialState();
    }

    public boolean addListener(IConnectionStateListener listener, boolean callUpdateState) {
        boolean addOk = false;
        if (listener != null) {
            addOk = mConnectionStateListeners.add(listener);

            if (callUpdateState) {
                listener.onStateUpdate(mCurrentConnectionState);
            }

        }
        return addOk;
    }

    public boolean removeListener(IConnectionStateListener listener) {
        return mConnectionStateListeners.remove(listener);
    }

    private void notifyConnectionStateUpdated(ConnectionState newState) {
        for (IConnectionStateListener listener : mConnectionStateListeners) {
            listener.onStateUpdate(newState);
        }
    }

    public ConnectionState getConnectionState() {
        return mCurrentConnectionState;
    }

    private void setConnectionState(ConnectionState newState, boolean checkSameState) {
        boolean notifyStateUpdated = true;

        if (checkSameState) {
            notifyStateUpdated = (mCurrentConnectionState != newState);
        }

        if (notifyStateUpdated) {
            mCurrentConnectionState = newState;
            notifyConnectionStateUpdated(newState);
        }
    }

    private ConnectionState getConnectionState(ConnectionStateModel connStateModel) {
        if (!AccessPointUtil.isConnectedToWiFiOrEthernet()) {
            FsLogger.log("not connected to WiFI or Ethernet", LogLevel.Info);
            return ConnectionState.NO_WIFI_OR_ETHERNET;
        }

        FsLogger.log("connected to WiFI or Ethernet", LogLevel.Info);
        //TODO: set on WiFi connection (check AP name if in our list of devices)
        if (connStateModel.mConnectToRadioInAPMode) {
            return ConnectionState.CONNECTED_RADIO_AP_MODE;
        }

        if (connStateModel.mRadiosSearching) {
            if (connStateModel.mRadiosFound) {
                return ConnectionState.SEARCHING_RADIOS_SPEAKER_FOUND;
            } else {
                return ConnectionState.SEARCHING_RADIOS_NO_SPEAKER_FOUND_YET;
            }
        }

        if (!connStateModel.mRadiosFound && connStateModel.mSearchingTimeElapsed) {
            return ConnectionState.NO_RADIOS_FOUND_AFTER_10_SECONDS;
        }

        if (connStateModel.mRadioInvalidSession) {
            return ConnectionState.INVALID_SESSION;
        }

        if (connStateModel.mPinError) {
            return ConnectionState.DISCONNECTED_PIN_ERROR;
        }

        if (connStateModel.mRadioConnectionLost) {
            return ConnectionState.DISCONNECTED;
        }

        if (connStateModel.mRadioConnected) {
            return ConnectionState.CONNECTED_TO_RADIO;
        }

        FsLogger.log("Not connected to radio");
        return ConnectionState.NOT_CONNECTED_TO_RADIO;
    }

    private void onRadioFound(boolean radioFound) {
        mConnectionStateModel.mRadiosSearching = false;
        mConnectionStateModel.mRadiosFound = radioFound;

        if (radioFound) {
            stopFindRadiosTimeoutTimer();
        }

        setConnectionState(getConnectionState(mConnectionStateModel), true);
    }

    @Override
    public void onConnectionSuccess(Radio radio) {
        mConnectionStateModel.mRadioConnectionLost = false;
        mConnectionStateModel.mRadioInvalidSession = false;
        mConnectionStateModel.mRadiosSearching = false;
        mConnectionStateModel.mPinError = false;
        mConnectionStateModel.mRadiosFound = true;
        mConnectionStateModel.mRadioConnected = true;
        mConnectionStateModel.mNotificationTimeout = false;

        setConnectionState(getConnectionState(mConnectionStateModel), false);
    }

    @Override
    public void onConnectionError(Radio radio, ConnectionErrorResponse error) {
        mConnectionStateModel.mRadioConnectionLost = true;
        mConnectionStateModel.mRadioInvalidSession = false;
        mConnectionStateModel.mRadioConnected = false;
        if (NetRemoteManager.isPinError(error)) {
            mConnectionStateModel.mPinError = true;
        }
        mConnectionStateModel.mNotificationTimeout = false;

        setConnectionState(getConnectionState(mConnectionStateModel), false);
    }

    @Override
    public void onDisconnected(Radio radio, ConnectionErrorResponse error) {
        mConnectionStateModel.mRadioConnectionLost = true;
        mConnectionStateModel.mRadioConnected = false;
        if (NetRemoteManager.isPinError(error)) {
            mConnectionStateModel.mPinError = true;
        }

        boolean isSessionStolen = false;
        boolean notificationTimeout = false;
        if (error != null && AccessPointUtil.isConnectedToWiFiOrEthernet()) {
            isSessionStolen = (error.getErrorCode() == ErrorResponse.ErrorCode.InvalidSession);
            notificationTimeout = error.getTransportDetail() != null &&
                    error.getTransportDetail().getErrorCode() == TransportErrorDetail.ErrorCode.NotificationTimeout;
        }
        mConnectionStateModel.mRadioInvalidSession = isSessionStolen;
        mConnectionStateModel.mNotificationTimeout = notificationTimeout;

        setConnectionState(getConnectionState(mConnectionStateModel), false);
    }

    @Override
    public void onRadioFound(Radio radio) {
        onRadioFound(true);
    }

    @Override
    public void onRadioLost(Radio radio) {
        if (NetRemoteManager.getInstance().getRadios().size() == 0 &&
                !NetRemoteManager.getInstance().checkConnection()) {
            mConnectionStateModel.mRadiosFound = false;
            mConnectionStateModel.mSearchingTimeElapsed = true;
            mConnectionStateModel.mNotificationTimeout = false;
            setConnectionState(getConnectionState(mConnectionStateModel), true);
        }
    }

    @Override
    public void onRadioUpdated(Radio radio) {
        onRadioFound(true);
    }

    @Override
    public void onScanError(IDiscoveryScanner.ScanError error) {
        onRadioFound(false);
    }

    @Override
    public void onScanStart() {
        FsLogger.log("ConnectionStateUtil: scan started");
        onStartDiscoveryScan();
    }

    public void updateState() {
        setConnectionState(getConnectionState(mConnectionStateModel), true);
    }

    private void resetStateModel() {
        mConnectionStateModel.mRadiosSearching = false;
        mConnectionStateModel.mRadiosFound = false;
        mConnectionStateModel.mRadioConnectionLost = false;
        mConnectionStateModel.mRadioInvalidSession = false;
        mConnectionStateModel.mRadioConnected = false;
        mConnectionStateModel.mSearchingTimeElapsed = false;
        mConnectionStateModel.mPinError = false;
        mConnectionStateModel.mNotificationTimeout = false;
    }

    public void resetDisconnectState() {
        mConnectionStateModel.mRadioInvalidSession = false;
        mConnectionStateModel.mNotificationTimeout = false;
        mConnectionStateModel.mRadioConnectionLost = false;
        mConnectionStateModel.mPinError = false;

        updateState();
    }

    public ConnectionStateModel getConnectionStateModel() {
        return mConnectionStateModel;
    }

    private void onWifiChanged() {
        FsLogger.log("ConnectionStateUtil: wifi changed", LogLevel.Info);

        setConnectionState(getConnectionState(mConnectionStateModel), true);
    }

    private void registerNetworkListeners() {

        NetRemoteManager.getInstance().getNetRemote().addWifiConnectionListener(new IWifiConnection() {
            @Override
            public void onConnected() {
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onWifiChanged();
                    }
                }, 1000);
            }

            @Override
            public void onDisconnected() {
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        resetStateModel();
                        onWifiChanged();
                    }
                }, 100);
            }
        });

    }

    private void initInitialState() {

        if (NetRemoteManager.getInstance().isDiscoveryScanActive()) {
            onStartDiscoveryScan();
        } else {
            onWifiChanged();
        }
    }

    private void onRadioSearchTimeout() {
        mConnectionStateModel.mSearchingTimeElapsed = true;
        mConnectionStateModel.mRadiosSearching = false;

        setConnectionState(getConnectionState(mConnectionStateModel), true);
    }

    private void onStartDiscoveryScan() {
        initFindRadiosTimeoutTimer(RADIO_SEARCH_TIMEOUT_MS);

        mConnectionStateModel.mRadiosSearching = true;
        mConnectionStateModel.mRadiosFound = false;
        mConnectionStateModel.mRadioConnectionLost = false;
        mConnectionStateModel.mRadioInvalidSession = false;
        mConnectionStateModel.mSearchingTimeElapsed = false;
        mConnectionStateModel.mPinError = false;

        setConnectionState(getConnectionState(mConnectionStateModel), true);
    }

    private synchronized void initFindRadiosTimeoutTimer(long timeoutMs) {

        stopFindRadiosTimeoutTimer();

        if (mFindRadiosTimeoutTimer == null) {
            TimerTask mFindRadiosTimeoutTimerTask = new FindRadiosTimeoutTask();
            mFindRadiosTimeoutTimer = new Timer();
            mFindRadiosTimeoutTimer.schedule(mFindRadiosTimeoutTimerTask, timeoutMs);
        }
    }

    private synchronized void stopFindRadiosTimeoutTimer() {
        if (mFindRadiosTimeoutTimer != null) {
            mFindRadiosTimeoutTimer.cancel();
            mFindRadiosTimeoutTimer.purge();
            mFindRadiosTimeoutTimer = null;
        }
    }

    private class FindRadiosTimeoutTask extends TimerTask {
        @Override
        public void run() {
            onRadioSearchTimeout();
            stopFindRadiosTimeoutTimer();
        }
    }

}
