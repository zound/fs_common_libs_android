package com.frontier_silicon.components.connection;

public interface IConnectionStateListener {

    void onStateUpdate(ConnectionState newState);

}
