package com.frontier_silicon.components.connection;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoControllerName;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.ISetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;

/**
 * Created by lsuhov on 01/06/16.
 */
public class BeforeConnectionNodesSetter {

    public void setNeededNodes(final Radio radio, final boolean synchronous, final RadioNodeUtil.INodeSetResultListener listener) {
        String nameOfCurrentPhone = getNameOfCurrentPhone();

        radio.setNode(new NodeSysInfoControllerName(nameOfCurrentPhone), synchronous, new ISetNodeCallback() {
            @Override
            public void setNodeSuccess(NodeInfo node) {
                callListener(listener, true);
            }

            @Override
            public void setNodeError(NodeInfo node, NodeErrorResponse error) {
                NodeErrorResponse.ErrorCode errorCode = error.getErrorCode();

                callListener(listener, errorCode != ErrorResponse.ErrorCode.NetworkTimeout);
            }
        });
    }

    private String getNameOfCurrentPhone() {
        String nameOfPhone = android.os.Build.MODEL;

        if (nameOfPhone != null && nameOfPhone.length() >= 128) {
            nameOfPhone = nameOfPhone.substring(0, 126);
        }

        return nameOfPhone;
    }

    void callListener(RadioNodeUtil.INodeSetResultListener listener, boolean result) {
        if (listener != null) {
            listener.onNodeSetResult(result);
        }
    }
}
