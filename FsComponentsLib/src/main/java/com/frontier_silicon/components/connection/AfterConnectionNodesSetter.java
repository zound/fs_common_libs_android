package com.frontier_silicon.components.connection;

import com.frontier_silicon.NetRemoteLib.Node.BaseCastTos;
import com.frontier_silicon.NetRemoteLib.Node.NodeCastTos;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceTransportOptimisation;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCfgIrAutoPlayFlag;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetKeepConnected;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.CommonPreferences;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;

import java.util.List;

/**
 * Created by lsuhov on 17/06/16.
 */
public class AfterConnectionNodesSetter {
    public static void setTransportOptimisationToAllClientsSync(Radio radio) {
        MultiroomGroupManager multiroomGroupManager = MultiroomGroupManager.getInstance();
        MultiroomDeviceModel serverDeviceModel = multiroomGroupManager.getDeviceByUdn(radio.getUDN());
        if (serverDeviceModel == null || serverDeviceModel.mRadio == null || !serverDeviceModel.isServer())
            return;

        Boolean isTransportOptimised = isTransportOptimisedSync(serverDeviceModel.mRadio);
        if (isTransportOptimised == null)
            return;

        List<MultiroomDeviceModel> multiroomItems = multiroomGroupManager.getDeviceModelsFromCurrentGroup();
        for (int i = 0; i < multiroomItems.size(); i++) {
            MultiroomDeviceModel multiroomDeviceModel = multiroomItems.get(i);

            if (!multiroomDeviceModel.isServer()) {
                setTransportOptimisationSync(multiroomDeviceModel.mRadio, isTransportOptimised);
            }
        }
    }

    public static Boolean isTransportOptimisedSync(Radio radio) {
        Boolean isTransportOptimised = null;

        if (radio != null) {
            NodeMultiroomDeviceTransportOptimisation nodeOptimisation =
                    (NodeMultiroomDeviceTransportOptimisation) radio.
                            getNodeSyncGetter(NodeMultiroomDeviceTransportOptimisation.class).get();

            if (nodeOptimisation != null) {
                isTransportOptimised = nodeOptimisation.getValueEnum() == NodeMultiroomDeviceTransportOptimisation.Ord.ENABLED;
            }
        }

        return isTransportOptimised;
    }

    public static void setTransportOptimisationSync(Radio radio, Boolean networkOptimisation) {
        if (radio == null) {
            return;
        }

        NodeMultiroomDeviceTransportOptimisation.Ord value = networkOptimisation ? NodeMultiroomDeviceTransportOptimisation.Ord.ENABLED :
                NodeMultiroomDeviceTransportOptimisation.Ord.DISABLED;

        radio.setNode(new NodeMultiroomDeviceTransportOptimisation(value), true);
    }

    public static void setCastAcceptanceToServerAndClientsSync(Radio radio, String groupId) {

        if (radio == null) {
            return;
        }

        MultiroomGroupManager multiroomGroupManager = MultiroomGroupManager.getInstance();
        if (multiroomGroupManager.isUngroupedDevicesGroup(groupId)) {
            setCastAcceptanceNodeSync(radio);
            return;
        }

        List<MultiroomDeviceModel> devicesFromGroup = multiroomGroupManager.getAllDevicesForGroupId(groupId);
        for (MultiroomDeviceModel multiroomDeviceModel : devicesFromGroup) {
            setCastAcceptanceNodeSync(multiroomDeviceModel.mRadio);
        }
    }

    public static void setCastAcceptanceNodeSync(Radio radio) {
        if (radio == null) {
            return;
        }

        NodeCastTos nodeCastTosStatus = (NodeCastTos) radio.getNodeSyncGetter(NodeCastTos.class).get();

        if (nodeCastTosStatus == null) {
            return;
        }

        boolean castIsAlreadyAccepted = nodeCastTosStatus.getValueEnum() == NodeCastTos.Ord.ACTIVE;
        if (castIsAlreadyAccepted) {
            return;
        }

        boolean castAcceptance = CommonPreferences.getInstance().getCastTosAcceptance();
        if ((nodeCastTosStatus.getValueEnum() == BaseCastTos.Ord.UNSET ||
                nodeCastTosStatus.getValueEnum() == BaseCastTos.Ord.INACTIVE) && castAcceptance) {
            radio.setNode(new NodeCastTos(NodeCastTos.Ord.ACTIVE), true);
        }
    }

    public static void enableNodesAfterConnection(Radio radio) {
        if (radio != null) {
            radio.setNode(new NodeSysCfgIrAutoPlayFlag(NodeSysCfgIrAutoPlayFlag.Ord.AUTOPLAY_ON), false);
            radio.setNode(new NodeSysNetKeepConnected(NodeSysNetKeepConnected.Ord.YES), false);
        }
    }
}
