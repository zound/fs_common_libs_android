package com.frontier_silicon.components;

import android.content.Context;
import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanListener;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.BaseMultiroomDeviceListAll;
import com.frontier_silicon.NetRemoteLib.Radio.ConnectionErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.IConnectionCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.NetRemoteLib.Radio.TransportErrorDetail;
import com.frontier_silicon.components.common.IRadioPINManager;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.connection.BeforeConnectionNodesSetter;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class NetRemoteManager {
	private static NetRemoteManager mInstance = null;
	
	private NetRemote mNetRemote;

    private BeforeConnectionNodesSetter mBeforeConnectionNodesSetter;

	boolean mRadioConnectionCompleted = true;

	public static final String RADIO_DEFAULT_PIN = "1234";
	public static final String RADIO_HUI_DEFAULT_ADDRESS = "172.24.0.1";

	private IRadioPINManager mRadioPINManager = null;

	private List<IConnectionCallback> mRadioConnListeners = new CopyOnWriteArrayList<>();

    public static NetRemoteManager getInstance() {
		if (mInstance == null)
			mInstance = new NetRemoteManager();
		
		return mInstance;
	}
	
	private NetRemoteManager() {
		mBeforeConnectionNodesSetter = new BeforeConnectionNodesSetter();
	}

	public boolean initNetRemote(Context context, String[] vendorIDList, String[] firmwareBlacklist,
                                 String[] bonjourServiceTypeList,
                                 int scannersFlag, boolean cacheRadios, boolean useGetMultipleForDeviceDetailsRetrieval,
								 boolean showAlsoMissingSpeakers) {

        mNetRemote = new NetRemote(context, scannersFlag);

        //setLogTraceFlag(/*NetRemote.TRACE_DEFAULT | */NetRemote.TRACE_DISCOVERY_BIT | NetRemote.TRACE_DISCOVERY_DEBUG_BIT);

        mNetRemote.setVendorIDsList(vendorIDList);
        mNetRemote.setFirmwareBlacklist(firmwareBlacklist);
        mNetRemote.setBonjourServiceTypeList(bonjourServiceTypeList);

        mNetRemote.getRadioListKeeper().cacheRadiosForFutureUse(cacheRadios);

        mNetRemote.getDiscoveryService().setUseGetMultipleForDeviceDetailsRetrieval(useGetMultipleForDeviceDetailsRetrieval);

		mNetRemote.setShowAlsoMissingSpeakers(showAlsoMissingSpeakers);

        return true;
	}

	public void setLogTraceFlag(int logFlagValue) {
		NetRemote.setLogTraceFlag(logFlagValue);
	}

	public Radio getCurrentRadio() {
		return mNetRemote.getCurrentRadio();
	}

	public NetRemote getNetRemote() {
		return mNetRemote;
	}

	public void setCurrentRadio(Radio radio) {
		mNetRemote.setCurrentRadio(radio);
	}

	synchronized public void connectToRadio(final Radio radio, final boolean synchronous,
                                            boolean setNodesBeforeConnection, final boolean failSilently) {
		if (mRadioConnectionCompleted) {
			
			if (radio != null) {
				mRadioConnectionCompleted = false;

                radio.setPIN(getPIN(radio));

                if (!setNodesBeforeConnection) {
                    connectToRadioAfterNameOfPhoneWasSet(radio, synchronous, failSilently);
                    return;
                }

                mBeforeConnectionNodesSetter.setNeededNodes(radio, synchronous, new RadioNodeUtil.INodeSetResultListener() {
                    @Override
                    public void onNodeSetResult(boolean success) {
                        if (success) {
                            connectToRadioAfterNameOfPhoneWasSet(radio, synchronous, failSilently);
                        } else {
                            mRadioConnectionCompleted = true;
                        }
                    }
                });
			}
		}
	}

    void connectToRadioAfterNameOfPhoneWasSet(Radio radio, boolean synchronous, final boolean failSilently) {

        Radio currentRadio = mNetRemote.getCurrentRadio();
        if (currentRadio != null) {
            currentRadio.close();
            setCurrentRadio(null);
        }

        FsLogger.log("connectToRadio: Try connect to radio " + radio);

        radio.connect(synchronous, new IConnectionCallback() {
            long mStartOfConnect = System.currentTimeMillis();

            @Override
            public void onConnectionSuccess(Radio radio) {
                FsLogger.log("Connected to " + radio + " connectedOk: " + radio.isConnectionOk());

                setCurrentRadio(radio);
                mRadioConnectionCompleted = true;

                notifyRadioConnected(radio);
            }

            @Override
            public void onConnectionError(Radio radio, ConnectionErrorResponse error) {
                FsLogger.log("Error connecting to " + radio + " "  + error, LogLevel.Error);

                Radio currentRadio = mNetRemote.getCurrentRadio();
                if (currentRadio != null && !currentRadio.equals(radio)) {
                    FsLogger.log("NetRemoteManager: connection error received for different speaker than current connection");
                } else {
                    setCurrentRadio(null);
                }
                mRadioConnectionCompleted = true;

                if (canFailSilently(error)) {
                    FsLogger.log("Failing silently", LogLevel.Error);
                    return;
                }

                notifyRadioConnectionError(radio, error);
            }

            @Override
            public void onDisconnected(Radio radio, ConnectionErrorResponse error) {
                FsLogger.log("Connection lost to " + radio + " " + error, LogLevel.Error);

                Radio currentRadio = mNetRemote.getCurrentRadio();
                if (currentRadio != null && !currentRadio.equals(radio)) {
                    FsLogger.log("NetRemoteManager: disconnected error received for different speaker than current connection");
                } else {
                    setCurrentRadio(null);
                }
                mRadioConnectionCompleted = true;

                if (canFailSilently(error)) {
                    FsLogger.log("Failing silently", LogLevel.Error);
                    return;
                }

                notifyRadioConnectionLost(radio, error);
            }

            private boolean canFailSilently(ConnectionErrorResponse error) {
                if (isSessionStolenError(error)) {
                    return false;
                }

                boolean failHappenedFast = (System.currentTimeMillis() - mStartOfConnect) < 60_000;

                return failSilently && failHappenedFast;
            }
        });
    }

	public static boolean isPinError(ErrorResponse error) {
		TransportErrorDetail errorTransportDetail = error.getTransportDetail();
		if (errorTransportDetail != null && errorTransportDetail.getErrorCode() == TransportErrorDetail.ErrorCode.PinError)
			return true;
		
		return false;
	}

	public static boolean isSessionStolenError(ErrorResponse error) {
        TransportErrorDetail errorTransportDetail = error.getTransportDetail();
        if (errorTransportDetail != null && errorTransportDetail.getErrorCode() == TransportErrorDetail.ErrorCode.InvalidSession)
            return true;

        return false;
    }

	public boolean addConnectionListener(IConnectionCallback listener) {
        return !mRadioConnListeners.contains(listener) && mRadioConnListeners.add(listener);
	}

	public boolean removeConnectionListener(IConnectionCallback listener) {
        return mRadioConnListeners.remove(listener);
	}
	
	void notifyRadioConnected(Radio radio) {
        for (IConnectionCallback listener : mRadioConnListeners) {
            listener.onConnectionSuccess(radio);
        }
	}
	
	public void notifyRadioConnectionError(Radio radio, ConnectionErrorResponse error) {
        for (IConnectionCallback listener : mRadioConnListeners) {
            listener.onConnectionError(radio, error);
        }
	}
	
	public void notifyRadioConnectionLost(Radio radio, ConnectionErrorResponse error) {
        for (IConnectionCallback listener : mRadioConnListeners) {
            listener.onDisconnected(radio, error);
        }
	}

	public boolean checkConnection() {
		return checkConnection(mNetRemote.getCurrentRadio());
	}

    public boolean checkConnection(Radio radio) {
        if (radio == null || !radio.isConnectionOk()) {
            FsLogger.log("NetRemoteManager checkConnection failed: No Radio connection", LogLevel.Error);
            return false;
        }
        return true;
    }



    public Radio getRadioFromSN(String serialNumber) {
        Radio device = null;

        if (!TextUtils.isEmpty(serialNumber)) {
            List<Radio> radios = mNetRemote.getAllRadios();
            for (Radio radio : radios) {
                if (serialNumber.equals(radio.getSN())) {
                    device = radio;
                    break;
                }
            }
        }

        return device;
    }

	public void setRadioPINManager(IRadioPINManager radioPINManager) {
		mRadioPINManager = radioPINManager;
	}

	public String getPIN(Radio radio) {
		String PIN = RADIO_DEFAULT_PIN;
		
		String radioSN = radio.getSN();
		if (mRadioPINManager != null) {
			if (mRadioPINManager.isPINSaved(radioSN)) {
				PIN = mRadioPINManager.getPIN(radioSN);
			}
		}
		
		return PIN;
	}
	
	public boolean addScanListener(IScanListener listener) {
        return mNetRemote.getDiscoveryService().addListener(listener);
	}
	
	public boolean removeScanListener(IScanListener listener) {
		return mNetRemote.getDiscoveryService().removeListener(listener);
	}

    public boolean addRadioDiscoveryListener(IRadioDiscoveryListener listener) {
        return mNetRemote.addRadioDiscoveryListener(listener);
    }

    public boolean removeRadioDiscoveryListener(IRadioDiscoveryListener listener) {
        return mNetRemote.removeRadioDiscoveryListener(listener);
    }

	public void startBackgroundRadioScan(boolean clearExistingRadios) {

        stopBackgroundRadioScan();

        FsLogger.log("startBackgroundRadioScan: NetRemote start background scan");

        mNetRemote.getDiscoveryService().start(clearExistingRadios);
	}
	
	public void stopBackgroundRadioScan() {		

        FsLogger.log("stopBackgroundRadioScan: NetRemote stop background scan");

        mNetRemote.getDiscoveryService().stop();
	}

	public void resumeRadios() {
        mNetRemote.resume();
    }

    public void pauseRadios() {
        mNetRemote.pause();
    }

    public void rescan() {
        mNetRemote.getDiscoveryService().rescan();
    }

	public List<Radio> getRadios() {

        return mNetRemote.getRadios();
	}

	public boolean matchUDN(String ssdpUDN, String multiroomUDN) {
        boolean matchFound = false;
        String colon = ":";

        if (!TextUtils.isEmpty(ssdpUDN) && !TextUtils.isEmpty(multiroomUDN)) {

            String[] udnParts = ssdpUDN.split("-");
            String finalPartSSDP = "";
            if (udnParts.length > 0)
                finalPartSSDP = udnParts[udnParts.length-1];
            if (finalPartSSDP.contains(colon)) {
                finalPartSSDP = finalPartSSDP.replace(colon, "");
            }

            udnParts = multiroomUDN.split("-");
            String finalPartUDN = "";
            if (udnParts.length > 0)
                finalPartUDN = udnParts[udnParts.length-1];
            if (finalPartUDN.contains(colon)) {
                finalPartUDN = finalPartUDN.replace(colon, "");
            }

            matchFound = finalPartSSDP.equalsIgnoreCase(finalPartUDN);
        }

        return matchFound;
	}

	private void resetAllConnectionData() {
		mNetRemote.closeCurrentRadio();
		mRadioConnectionCompleted = true;		
	}
	
	public void resetNetRemote() {
		resetAllConnectionData();
		mNetRemote.networkReInit();
	}
	
	public Radio getRadio(String udn) {
		Radio foundRadio = null;
		
		List<Radio> radios = mNetRemote.getAllRadios();
		for (Radio radio : radios) {
			if (matchUDN(radio.getUDN(), udn)) {
				foundRadio = radio;
				break;
			}
		}
		
		return foundRadio;
	}

	public boolean isDiscoveryScanActive() {
		return mNetRemote.getDiscoveryService().isRunning();
	}

	public void allowSpeakerToBeAddedFromDeviceListAll(boolean allow) {
        mNetRemote.getDiscoveryService().allowSpeakerToBeAddedFromDeviceListAll(allow);
    }

    public void tryToAddSpeakerFromDeviceListAll(BaseMultiroomDeviceListAll.ListItem item) {
        mNetRemote.getDiscoveryService().tryToAddSpeakerFromDeviceListAll(item);
    }
}
