package com.frontier_silicon.components.multiroom;

public interface IMultiroomGroupingListener {
	void onGroupingUpdate();
}
