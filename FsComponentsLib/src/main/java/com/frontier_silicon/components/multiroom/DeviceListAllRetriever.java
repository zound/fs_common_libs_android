package com.frontier_silicon.components.multiroom;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultichannelSystemId;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultichannelSystemName;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultichannelSystemState;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus0;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus1;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus2;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus3;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceListAll;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceServerStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupId;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupName;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.nodeCommunication.IListNodeListener;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by lsuhov on 08/08/2017.
 *
 * This class is doing multiple listAll requests in case it deals with different speaker generations.
 *
 * 1. If we have Venice and Minuet speakers we need to query each of a kind.
 * 2. If we have a Minuet speaker with stereo capability we need to query it instead of a simple Minuet
 *
 */

public class DeviceListAllRetriever {

    private List<Radio> mRadioList;
    private MultiroomGroupManager mMultiroomGroupManager;
    private NetRemoteManager mNetRemoteManager;

    private List<Radio> mVeniceSpeakers = new ArrayList<>();
    private List<Radio> mStereoAndSimpleMinuetSpeakers = new ArrayList<>();

    private List<MultiroomDeviceModel> mResults = new ArrayList<>();

    public DeviceListAllRetriever(List<Radio> radioList, MultiroomGroupManager multiroomGroupManager, NetRemoteManager netRemoteManager) {
        mRadioList = radioList;
        mMultiroomGroupManager = multiroomGroupManager;
        mNetRemoteManager = netRemoteManager;
    }

    public List<MultiroomDeviceModel> getDeviceListAllResults() {

        splitRadioList();

        retrieveDeviceListAllFromListOfSpeakers(mVeniceSpeakers);

        retrieveDeviceListAllFromListOfSpeakers(mStereoAndSimpleMinuetSpeakers);

        dispose();

        return mResults;
    }

    private void splitRadioList() {
        List<Radio> simpleMinuetSpeakers = new ArrayList<>();

        for (Radio radio : mRadioList) {
            if (radio.getIsVenice() && radio.isMultiroomCapable()) {
                mVeniceSpeakers.add(radio);
            } else if (radio.isStereoCapable()) {
                mStereoAndSimpleMinuetSpeakers.add(radio);
            } else if (radio.getIsMinuet()) {
                simpleMinuetSpeakers.add(radio);
            }
        }

        mStereoAndSimpleMinuetSpeakers.addAll(simpleMinuetSpeakers);

        Radio radio = mNetRemoteManager.getCurrentRadio();
        if (radio == null) {
            return;
        }

        if (radio.getIsVenice()) {
            moveConnectedSpeakerToFirstPosition(radio, mVeniceSpeakers);
        } else if (radio.isStereoCapable()) {
            moveConnectedSpeakerToFirstPosition(radio, mStereoAndSimpleMinuetSpeakers);
        } else if (mStereoAndSimpleMinuetSpeakers.size() == simpleMinuetSpeakers.size()) {
            // It means that we don't have stereo speakers
            moveConnectedSpeakerToFirstPosition(radio, mStereoAndSimpleMinuetSpeakers);
        }
    }

    private void moveConnectedSpeakerToFirstPosition(Radio radio, List<Radio> radioList) {

        if (radio != null && radioList.contains(radio)) {
            radioList.remove(radio);
            radioList.add(0, radio);
        }
    }

    private boolean retrieveDeviceListAllFromListOfSpeakers(List<Radio> radioList) {
        if (radioList.size() == 0) {
            return false;
        }

        List<NodeMultiroomDeviceListAll.ListItem> rawDeviceList = getDeviceListAllResults(radioList);

        if (rawDeviceList != null)
            filterDeviceListBasedOnSpeakerType(rawDeviceList, radioList);

        return true;
    }

    private List<NodeMultiroomDeviceListAll.ListItem> getDeviceListAllResults(final List<Radio> radioList) {
        List<NodeMultiroomDeviceListAll.ListItem> deviceList = new ArrayList<>();

        Iterator<Radio> radiosIterator = radioList.iterator();
        while (radiosIterator.hasNext()) {
            Radio radio = radiosIterator.next();

            if (!radio.isCheckingAvailability() && radio.isAvailable()) {

                FsLogger.log("getting device.listAll: " + radio.getFriendlyName() + " " + radio.getIpAddress());
                deviceList = getDeviceListAllFromRadio(radio);

                if (deviceList == null ||  (deviceList.size() == 0 && radiosIterator.hasNext())) {
                    continue;
                }

                // add device that provided the deviceListAll manually because it's not included in the list
                MultiroomDeviceModel deviceThatProvidedListAll = getDeviceModelForRadio(radio);

                mResults.add(deviceThatProvidedListAll);

                break;
            } else {
                FsLogger.log(">>>>> TRY ANOTHER RADIO for addAllFoundDeviceNoConnection", LogLevel.Warning);
            }

        }

        return deviceList;
    }

    private void filterDeviceListBasedOnSpeakerType(List<NodeMultiroomDeviceListAll.ListItem> rawDeviceList,
                                                    List<Radio> radioList) {

        for (NodeMultiroomDeviceListAll.ListItem listItem : rawDeviceList) {
            Radio radio = getRadio(listItem.getUDN(), radioList);

            if (radio != null) {
                buildMultiroomDeviceModelFromNodeListItem(listItem, radio);

                //checking the speaker in the whole list before deciding to put in unknown list
            } else if (getRadio(listItem.getUDN(), mRadioList) == null) {
                mNetRemoteManager.tryToAddSpeakerFromDeviceListAll(listItem);
            }
        }
    }

    public Radio getRadio(String udn, List<Radio> radios) {
        Radio foundRadio = null;

        for (Radio radio : radios) {
            if (mNetRemoteManager.matchUDN(radio.getUDN(), udn)) {
                foundRadio = radio;
                break;
            }
        }

        return foundRadio;
    }

    List<NodeMultiroomDeviceListAll.ListItem> mTempMultiroomDeviceListAllItems = null;
    private List<NodeMultiroomDeviceListAll.ListItem> getDeviceListAllFromRadio(Radio radio) {
        RadioNodeUtil.getListNodeItems(radio, NodeMultiroomDeviceListAll.class, true/*sync*/, new IListNodeListener<NodeListItem>() {
            @Override
            public void onListNodeResult(List resultList, boolean isListComplete) {
                mTempMultiroomDeviceListAllItems = resultList;
            }
        });

        if (mTempMultiroomDeviceListAllItems == null) {
            mTempMultiroomDeviceListAllItems = new ArrayList<>();
        }

        return mTempMultiroomDeviceListAllItems;
    }

    private void buildMultiroomDeviceModelFromNodeListItem(NodeMultiroomDeviceListAll.ListItem item, Radio radio) {
        String udn = item.getUDN();

        MultiroomDeviceModel device = new MultiroomDeviceModel(radio);

        device.mGroupId = item.getGroupId();
        device.mGroupName = item.getGroupName();
        device.mClientId = item.getClientNumber();

        device.mGroupRole = NodeMultiroomGroupState.Ord.values()[item.getGroupRole().ordinal()];

        if (mMultiroomGroupManager.isCurrentDeviceInGroup(device.mGroupId)) {
            device.mSyncStatus = getClientSyncStatus(item.getClientNumber());
        } else {
            device.mSyncStatus = NodeMultiroomClientStatus0.Ord.READY_TO_STREAM.ordinal();
        }

        device.mServerStatus = NodeMultiroomDeviceServerStatus.Ord.STREAM_PRESENTABLE.ordinal();
        device.mMultiroomUDN = udn;

        NodeListItem.SystemRole stereoRole = item.getSystemRole();
        if (stereoRole != null) {
            device.mStereoRole = stereoRole.ordinal();
        }
        device.mStereoSystemId = item.getSystemId();
        device.mStereoSystemName = item.getSystemName();

        if (TextUtils.isEmpty(device.mGroupId) && stereoRole == NodeListItem.SystemRole.Secondary) {
            device.mGroupId = MultiroomGroupManager.STEREO_SECONDARY_GROUP_ID;
            device.mGroupName = MultiroomGroupManager.STEREO_SECONDARY_GROUP_NAME;
        }

        mResults.add(device);
    }

    private long getClientSyncStatus(long clientId) {
        long syncStatus = -1;
        Radio radio = mNetRemoteManager.getCurrentRadio();

        if (!mNetRemoteManager.checkConnection(radio))
            return syncStatus;

        //TEMP: until notif. work
        boolean forceUncached = true;

        try {
            if (clientId == 0) {
                NodeMultiroomClientStatus0 clientStatusNode = (NodeMultiroomClientStatus0) radio.
                        getNodeSyncGetter(NodeMultiroomClientStatus0.class, forceUncached).get();
                if (clientStatusNode != null) {
                    syncStatus = clientStatusNode.getValue();
                }
            }

            if (clientId == 1) {
                NodeMultiroomClientStatus1 clientStatusNode = (NodeMultiroomClientStatus1) radio.
                        getNodeSyncGetter(NodeMultiroomClientStatus1.class, forceUncached).get();
                if (clientStatusNode != null) {
                    syncStatus = clientStatusNode.getValue();
                }
            }

            if (clientId == 2) {
                NodeMultiroomClientStatus2 clientStatusNode = (NodeMultiroomClientStatus2) radio.
                        getNodeSyncGetter(NodeMultiroomClientStatus2.class, forceUncached).get();
                if (clientStatusNode != null) {
                    syncStatus = clientStatusNode.getValue();
                }
            }

            if (clientId == 3) {
                NodeMultiroomClientStatus3 clientStatusNode = (NodeMultiroomClientStatus3) radio.
                        getNodeSyncGetter(NodeMultiroomClientStatus3.class, forceUncached).get();
                if (clientStatusNode != null) {
                    syncStatus = clientStatusNode.getValue();
                }
            }
        } catch (Exception e) {
            FsLogger.log("getClientSyncStatus " + e, LogLevel.Error);
            e.printStackTrace();
        }

        return syncStatus;
    }

    private MultiroomDeviceModel getDeviceModelForRadio(final Radio radio) {
        final MultiroomDeviceModel device = new MultiroomDeviceModel(radio);

        boolean sync = true;
        boolean forceUncached = true;

        Class[] nodeClasses = getNodeClassesForUpdatingMultiroomStatusOfRadio();

        RadioNodeUtil.getNodesFromRadio(radio, nodeClasses, sync, forceUncached,
                new RadioNodeUtil.INodesResultListener() {
                    @Override
                    public void onNodesResult(Map<Class, NodeInfo> nodes) {

                        buildMultiroomDeviceModel(device, radio, nodes);
                    }
                });

        return device;
    }

    private Class[] getNodeClassesForUpdatingMultiroomStatusOfRadio() {
        return new Class[]{NodeMultiroomGroupName.class, NodeMultiroomGroupId.class,
                NodeMultiroomGroupState.class, NodeMultiroomDeviceServerStatus.class,
                NodeMultichannelSystemState.class, NodeMultichannelSystemName.class,
                NodeMultichannelSystemId.class};
    }

    private void buildMultiroomDeviceModel(MultiroomDeviceModel device, Radio radio, Map<Class, NodeInfo> nodes) {

        NodeMultiroomGroupName groupNameNode = (NodeMultiroomGroupName) nodes.get(NodeMultiroomGroupName.class);
        NodeMultiroomGroupId groupIdNode = (NodeMultiroomGroupId) nodes.get(NodeMultiroomGroupId.class);
        NodeMultiroomGroupState groupRoleNode = (NodeMultiroomGroupState) nodes.get(NodeMultiroomGroupState.class);
        NodeMultiroomDeviceServerStatus serverStatusNode = (NodeMultiroomDeviceServerStatus) nodes.get(NodeMultiroomDeviceServerStatus.class);
        NodeMultichannelSystemState stereoRoleNode = (NodeMultichannelSystemState) nodes.get(NodeMultichannelSystemState.class);
        NodeMultichannelSystemName stereoSystemNameNode = (NodeMultichannelSystemName) nodes.get(NodeMultichannelSystemName.class);
        NodeMultichannelSystemId stereoSystemIdNode = (NodeMultichannelSystemId) nodes.get(NodeMultichannelSystemId.class);

        if (groupNameNode != null) {
            device.mGroupName = groupNameNode.getValue();
        } else {
            device.mGroupName = radio.isMultiroomCapable() ? MultiroomGroupManager.NO_GROUP_NAME : MultiroomGroupManager.NO_MR_GROUP_NAME;
        }

        if (groupIdNode != null) {
            device.mGroupId = groupIdNode.getValue();
        } else {
            device.mGroupId = radio.isMultiroomCapable() ? MultiroomGroupManager.NO_GROUP_ID : MultiroomGroupManager.NO_MR_GROUP_ID;
        }

        if (groupRoleNode != null) {
            device.mGroupRole = groupRoleNode.getValueEnum();
        }

        if (device.mGroupRole == NodeMultiroomGroupState.Ord.NO_GROUP && MultiroomGroupManager.NO_GROUP_NAME.equals(device.mGroupName)) {
            device.mGroupId = MultiroomGroupManager.NO_GROUP_ID;
        }

        if (serverStatusNode != null) {
            device.mServerStatus = serverStatusNode.getValue();
        }

        if (stereoRoleNode != null) {
            device.mStereoRole = stereoRoleNode.getValue();

            if (stereoRoleNode.getValueEnum() == NodeMultichannelSystemState.Ord.SECONDARY) {
                device.mGroupName = MultiroomGroupManager.STEREO_SECONDARY_GROUP_NAME;
                device.mGroupId = MultiroomGroupManager.STEREO_SECONDARY_GROUP_ID;
            }
        }

        if (stereoSystemNameNode != null) {
            device.mStereoSystemName = stereoSystemNameNode.getValue();
        }

        if (stereoSystemIdNode != null) {
            device.mStereoSystemId = stereoSystemIdNode.getValue();
        }

        device.mClientId = MultiroomGroupManager.SERVER_NUM_ID;
        device.mSyncStatus = NodeMultiroomClientStatus0.Ord.READY_TO_STREAM.ordinal();
    }

    private void dispose() {
        mRadioList = null;
        mMultiroomGroupManager = null;
        mNetRemoteManager = null;

        mVeniceSpeakers.clear();
        mStereoAndSimpleMinuetSpeakers.clear();
    }
}
