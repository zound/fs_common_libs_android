package com.frontier_silicon.components.multiroom;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus0;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceServerStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MultiroomListUtil {
    private final Object mLock = new Object();

    private static final String MULTIROOM_UDN_PREFIX = "46C19E4A-472B-11E1-9F67-";

    private HashMap<MultiroomGroupModel, List<MultiroomDeviceModel>> mGroupDeviceMap;

    private MultiroomGroupManager mMultiroomManager;
    private NetRemoteManager mNetRemoteManager;

    public MultiroomListUtil(MultiroomGroupManager multiroomManager, NetRemoteManager netRemoteManager) {
        mMultiroomManager = multiroomManager;
        mNetRemoteManager = netRemoteManager;

        mGroupDeviceMap = new HashMap<>();
    }

    public Set<MultiroomGroupModel> getGroupsFromDeviceMap() {
        Set<MultiroomGroupModel> groupsList = new HashSet<>();
        synchronized (mLock) {
            groupsList.addAll(mGroupDeviceMap.keySet());
        }

        return groupsList;
    }

    public List<MultiroomDeviceModel> getAllMultiroomDeviceModels() {
        List<MultiroomDeviceModel> allDeviceModels = new ArrayList<>();

        Set<MultiroomGroupModel> setOfGroupModels = getGroupsFromDeviceMap();
        for (MultiroomGroupModel groupModel : setOfGroupModels) {

            List<MultiroomDeviceModel> deviceModels = mGroupDeviceMap.get(groupModel);
            if (deviceModels == null) {
                continue;
            }

            synchronized (mLock) {
                allDeviceModels.addAll(deviceModels);
            }

        }

        return allDeviceModels;
    }

    public MultiroomDeviceModel getSelectedDevice() {
        Radio currentRadio = mNetRemoteManager.getCurrentRadio();

        if (currentRadio != null) {
            return getDeviceByUdn(currentRadio.getUDN());
        }

        return null;
    }

    public List<MultiroomItemModel> getFlatGroupDeviceList() {
        List<MultiroomItemModel> groupsAndDevicesFlatList = new ArrayList<>();

        Set<MultiroomGroupModel> groupsList = getGroupsFromDeviceMap();
        List<MultiroomGroupModel> groupsArrayList = new ArrayList<>(groupsList);
        MultiroomGroupModel secondaryStereoGroupModel = null;

        for (MultiroomGroupModel group : groupsArrayList) {

            if (mMultiroomManager.isUngroupedDevicesGroup(group.mGroupId)) {

                // add all non-grouped/standalone devices
                List<MultiroomDeviceModel> devicesList = getAllDevicesForGroup(group);

                for (MultiroomDeviceModel device : devicesList) {
                    if (device.mRadio != null) {
                        MultiroomItemModel deviceItem = new MultiroomItemModel(device);
                        groupsAndDevicesFlatList.add(deviceItem);
                    }
                }

            } else if (mMultiroomManager.isSecondaryStereoGroup(group.mGroupId)) {
                secondaryStereoGroupModel = group;

            } else {

                // add only group name for "real" groups
                List<MultiroomDeviceModel> devicesList = getOrderedDevicesForGroup(group);
                MultiroomItemModel groupItem = new MultiroomItemModel(group.mGroupName, group.mGroupId, devicesList);

                // do not add group with empty name
                if ((groupItem.getGroupName() != null) && (groupItem.getGroupId() != null) &&
                        !groupItem.getGroupId().isEmpty() && !groupItem.getGroupName().isEmpty()) {
                    groupsAndDevicesFlatList.add(groupItem);
                } else {
                    // empty name / id group, it will not be added to list
                    FsLogger.log("GroupItem removed: " + groupItem.toString());
                }
            }
        }

        markMissingStereoSpeakers(secondaryStereoGroupModel, groupsAndDevicesFlatList);

        // sort groups + ungrouped devices alphabetically
        Collections.sort(groupsAndDevicesFlatList, new Comparator<MultiroomItemModel>() {
            @Override
            public int compare(MultiroomItemModel lhs, MultiroomItemModel rhs) {
                String lhsString = lhs.toString();
                if (lhsString == null) {
                    return 1;
                }
                String rhsString = rhs.toString();
                if (rhsString == null) {
                    return -1;
                }

                if (lhs.getType() != rhs.getType()) {
                    return lhs.getType() < rhs.getType() ? 1 : -1;
                }

                return lhsString.compareToIgnoreCase(rhs.toString());
            }
        });

        return groupsAndDevicesFlatList;
    }

    // we can have missing primary or secondary stereo speakers
    private void markMissingStereoSpeakers(MultiroomGroupModel secondaryStereoGroupModel,
                                           List<MultiroomItemModel> groupsAndDevicesFlatList) {

        List<MultiroomDeviceModel> secondaryDeviceModels = getAllDevicesForGroup(secondaryStereoGroupModel);
        HashMap<String, MultiroomDeviceModel> secondaryStereoSpeakersForAdding = new HashMap<>();

        //adding all secondary speakers initially
        for (MultiroomDeviceModel secondaryDeviceModel : secondaryDeviceModels) {
            secondaryStereoSpeakersForAdding.put(secondaryDeviceModel.mStereoSystemId, secondaryDeviceModel);
        }

        //searching for incomplete systems
        for (MultiroomItemModel multiroomItemModel : groupsAndDevicesFlatList) {
            if (multiroomItemModel.getIsGroupHeader()) {
                List<MultiroomDeviceModel> groupedDeviceModels = multiroomItemModel.getFrozenMultiroomDeviceModels();
                for (MultiroomDeviceModel deviceModel : groupedDeviceModels) {
                    markPrimaryAsFaultyOrRemoveSecondary(deviceModel, secondaryStereoSpeakersForAdding);
                }
            } else {
                MultiroomDeviceModel deviceModel = multiroomItemModel.getDeviceModel();

                markPrimaryAsFaultyOrRemoveSecondary(deviceModel, secondaryStereoSpeakersForAdding);
            }
        }

        // Now all secondary speakers that have a paired primary were removed. It's time to add the
        // remaining ones to the principal list in order to show to the user that the stereo systems have a problem

        for (MultiroomDeviceModel secondaryDeviceModel : secondaryStereoSpeakersForAdding.values()) {
            secondaryDeviceModel.mIsIncompleteStereoSystem = true;

            MultiroomItemModel secondaryItemModel = new MultiroomItemModel(secondaryDeviceModel);
            groupsAndDevicesFlatList.add(secondaryItemModel);
        }
    }

    private void markPrimaryAsFaultyOrRemoveSecondary(MultiroomDeviceModel deviceModel,
                                                      HashMap<String, MultiroomDeviceModel> secondaryStereoSpeakersForAdding) {

        if (deviceModel.isPrimaryStereo()) {
            if (secondaryStereoSpeakersForAdding.containsKey(deviceModel.mStereoSystemId)) {
                // we have a complete stereo system
                secondaryStereoSpeakersForAdding.remove(deviceModel.mStereoSystemId);
                deviceModel.mIsIncompleteStereoSystem = false;
            } else {
                // the secondary stereo speaker is missing
                deviceModel.mIsIncompleteStereoSystem = true;
            }
        }
    }

    private List<MultiroomDeviceModel> getOrderedDevicesForGroup(MultiroomGroupModel multiroomGroupModel) {
        List<MultiroomDeviceModel> deviceList = getAllDevicesForGroup(multiroomGroupModel);

        Collections.sort(deviceList, new Comparator<MultiroomDeviceModel>() {
            @Override
            public int compare(MultiroomDeviceModel lhs, MultiroomDeviceModel rhs) {
                if (lhs.mGroupRole == NodeMultiroomGroupState.Ord.SERVER) {
                    return -1;
                }
                if (rhs.mGroupRole == NodeMultiroomGroupState.Ord.SERVER) {
                    return 1;
                }

                if (lhs.mRadio != null && rhs.mRadio != null) {
                    return lhs.toString().compareToIgnoreCase(rhs.toString());
                } else {
                    return 0;
                }
            }
        });

        return deviceList;
    }

    //This function gets the items from the current group.
    //In case the current device is from ungrouped category, we wrap it in array.
    public List<MultiroomItemModel> getItemModelsFromCurrentGroup() {
        List<MultiroomItemModel> array = new ArrayList<>();

        MultiroomDeviceModel currentDeviceModel = getSelectedDevice();
        if (currentDeviceModel == null) {
            return array;
        }

        String groupId = currentDeviceModel.mGroupId;
        if (MultiroomGroupManager.NO_GROUP_ID.equals(groupId)) {
            MultiroomItemModel itemModel = getItemModelOfCurrentDevice();

            if (itemModel != null)
                array.add(itemModel);
            return array;
        } else
            return getItemsFromGroup(groupId);
    }

    private MultiroomItemModel getItemModelOfCurrentDevice() {
        List<MultiroomItemModel> allItems = getFlatGroupDeviceList();
        MultiroomDeviceModel currentDeviceModel = getSelectedDevice();
        if (currentDeviceModel == null) {
            return null;
        }

        for (int i = 0; i < allItems.size(); i++) {
            MultiroomItemModel item = allItems.get(i);
            if (item.getGroupId().contentEquals(currentDeviceModel.mGroupId) &&
                    item.getRadio() != null &&
                    item.getRadio().getFriendlyName().contentEquals(currentDeviceModel.mRadio.getFriendlyName())) {

                return item;
            }
        }
        return null;
    }

    private List<MultiroomItemModel> getItemsFromGroup(String groupId) {
        List<MultiroomDeviceModel> allDevicesInGroup = getAllDevicesForGroupId(groupId);

        List<MultiroomItemModel> itemsFromGroup = new ArrayList<>();

        for (MultiroomDeviceModel device : allDevicesInGroup) {
            MultiroomItemModel item = new MultiroomItemModel(device);
            if (item.getGroupId().equals(groupId) && !item.getIsGroupHeader()) {
                itemsFromGroup.add(item);
            }
        }

        return itemsFromGroup;
    }

    public void clearDevicesList() {
        synchronized (mLock) {
            mGroupDeviceMap.clear();
        }
    }

    public void addDeviceToMap(MultiroomDeviceModel device) {
        addDeviceToMap(device, mGroupDeviceMap);
    }

    private void addDeviceToMap(MultiroomDeviceModel device, HashMap<MultiroomGroupModel, List<MultiroomDeviceModel>> groupDeviceMap) {
        if (device == null) {
            return;
        }

        MultiroomGroupModel group = new MultiroomGroupModel();
        group.mGroupName = device.mGroupName;
        group.mGroupId = device.mGroupId;

        if (device.mRadio != null && TextUtils.isEmpty(device.mMultiroomUDN)) {
            device.mMultiroomUDN = getMultiroomUDN(device.mRadio.getUDN());
        }

        List<MultiroomDeviceModel> devicesList = groupDeviceMap.get(group);
        if (devicesList == null) {
            devicesList = new ArrayList<>();
        }

        synchronized (mLock) {
            if (!isDeviceInList(device, devicesList)) {
                devicesList.add(device);
                groupDeviceMap.put(group, devicesList);
            }
        }
    }

    private String getMultiroomUDN(String ssdpUDN) {
        String multiroomUDN = "";

        if (!TextUtils.isEmpty(ssdpUDN)) {
            String[] udnParts = ssdpUDN.split("-");
            String finalPartSSDP;

            if (udnParts.length > 0) {
                finalPartSSDP = udnParts[udnParts.length - 1];
                multiroomUDN = MULTIROOM_UDN_PREFIX + finalPartSSDP;
            }
        }

        return multiroomUDN;
    }

    public void removeDeviceFromGroupMap(MultiroomDeviceModel device, MultiroomGroupModel group) {
        List<MultiroomDeviceModel> devicesList = mGroupDeviceMap.get(group);
        if (devicesList != null) {
            synchronized (mLock) {
                if (isDeviceInList(device, devicesList)) {
                    devicesList.remove(device);
                    mGroupDeviceMap.put(group, devicesList);
                }
            }
        }
    }

    protected MultiroomGroupModel findGroup(String groupId) {
        if (groupId != null) {
            Set<MultiroomGroupModel> groupsList = getGroupsFromDeviceMap();

            for (MultiroomGroupModel group : groupsList) {
                if (groupId.equalsIgnoreCase(group.mGroupId)) {
                    return group;
                }
            }
        }

        return null;
    }

    public List<MultiroomDeviceModel> getAllDevicesForGroupId(String groupId) {
        MultiroomGroupModel groupModel = findGroup(groupId);
        return getAllDevicesForGroup(groupModel);
    }

    public List<MultiroomDeviceModel> getAllDevicesForGroup(MultiroomGroupModel groupModel) {
        List<MultiroomDeviceModel> list = new ArrayList<>();
        if (groupModel == null) {
            return list;
        }

        List<MultiroomDeviceModel> devices = mGroupDeviceMap.get(groupModel);
        if (devices != null) {
            synchronized (mLock) {
                list.addAll(devices);
            }

            return list;
        } else {
            return list;
        }
    }

    public List<MultiroomDeviceModel> getDeviceModelsFromCurrentGroup() {
        List<MultiroomDeviceModel> list = new ArrayList<>();

        MultiroomDeviceModel currentDeviceModel = getSelectedDevice();
        if (currentDeviceModel == null) {
            return list;
        }

        String groupId = currentDeviceModel.mGroupId;
        if (MultiroomGroupManager.NO_GROUP_ID.equals(groupId)) {
            list.add(currentDeviceModel);

        } else {
            list.addAll(getAllDevicesForGroupId(groupId));
        }

        return list;
    }

    private boolean isDeviceInList(MultiroomDeviceModel device, List<MultiroomDeviceModel> deviceList) {
        boolean isInList = false;

        for (MultiroomDeviceModel dev : deviceList) {
            if (dev.mRadio.getUDN().equalsIgnoreCase(device.mRadio.getUDN())) {
                isInList = true;
                break;
            }
        }

        return isInList;
    }

    public void addNonMultiroomDevicesToMap(List<Radio> radios,
                                            HashMap<MultiroomGroupModel, List<MultiroomDeviceModel>> groupDeviceMap) {
        for (Radio radio : radios) {
            if (!radio.isMultiroomCapable()) {
                MultiroomDeviceModel device = getNonMultiroomDeviceItemFromRadio(radio);
                addDeviceToMap(device, groupDeviceMap);
            }
        }
    }

    private MultiroomDeviceModel getNonMultiroomDeviceItemFromRadio(Radio radio) {
        MultiroomDeviceModel device = new MultiroomDeviceModel(radio);

        device.mGroupName = MultiroomGroupManager.NO_MR_GROUP_NAME;
        device.mGroupId = MultiroomGroupManager.NO_MR_GROUP_ID;
        device.mGroupRole = NodeMultiroomGroupState.Ord.NO_GROUP;
        device.mSyncStatus = NodeMultiroomClientStatus0.Ord.READY_TO_STREAM.ordinal();
        device.mServerStatus = NodeMultiroomDeviceServerStatus.Ord.STREAM_PRESENTABLE.ordinal();

        return device;
    }

    public void addMultiroomDevicesFromDiscoveryToMap(List<Radio> radios,
                                                      HashMap<MultiroomGroupModel, List<MultiroomDeviceModel>> groupDeviceMap) {
        for (Radio radio : radios) {
            if (radio.isMultiroomCapable() && getDeviceByUdn(radio.getUDN(), groupDeviceMap) == null) {
                MultiroomDeviceModel device = getMultiroomDeviceItemFromRadio(radio);
                addDeviceToMap(device, groupDeviceMap);
            }
        }
    }

    private MultiroomDeviceModel getMultiroomDeviceItemFromRadio(Radio radio) {
        MultiroomDeviceModel device = new MultiroomDeviceModel(radio);

        device.mGroupName = MultiroomGroupManager.NO_GROUP_NAME;
        device.mGroupId = MultiroomGroupManager.NO_GROUP_ID;
        device.mGroupRole = NodeMultiroomGroupState.Ord.NO_GROUP;
        device.mSyncStatus = NodeMultiroomClientStatus0.Ord.READY_TO_STREAM.ordinal();
        device.mServerStatus = NodeMultiroomDeviceServerStatus.Ord.STREAM_PRESENTABLE.ordinal();

        return device;
    }

    public boolean isGroupNameDuplicated(String groupName) {
        Set<MultiroomGroupModel> groups = getGroupsFromDeviceMap();
        for (MultiroomGroupModel group : groups) {
            if (group != null && group.mGroupName != null && group.mGroupName.equalsIgnoreCase(groupName)) {
                return true;
            }
        }

        return false;
    }

    public boolean listIsEmpty() {
        return mGroupDeviceMap.size() == 0;
    }

    public boolean isStillCheckingRadios(List<Radio> radios) {
        boolean isChecking = true;

        for (Radio radio : radios) {
            if (!radio.isCheckingAvailability() && radio.isAvailable()) {
                isChecking = false;
                break;
            }
        }

        return isChecking;
    }

    public void updateAllRadiosAndGroups() {
        List<Radio> radios = mNetRemoteManager.getRadios();

        showRadioListFromNetRemote_Debug(radios);

        HashMap<MultiroomGroupModel, List<MultiroomDeviceModel>> groupDeviceMap = new HashMap<>();

        if (isStillCheckingRadios(radios)) {
            FsLogger.log("updateAllRadiosAndGroups: still checking radios", LogLevel.Error);

            addNonMultiroomDevicesToMap(radios, groupDeviceMap);
            addMultiroomDevicesFromDiscoveryToMap(radios, groupDeviceMap);

        } else {
            FsLogger.log("updateAllRadiosAndGroups: all radios are available");
            addAllFoundDevices(radios, groupDeviceMap);

            addNonMultiroomDevicesToMap(radios, groupDeviceMap);
            addMultiroomDevicesFromDiscoveryToMap(radios, groupDeviceMap);
        }

        copyMultiroomDevicesToFinalContainer(groupDeviceMap);
    }

    private void copyMultiroomDevicesToFinalContainer(HashMap<MultiroomGroupModel, List<MultiroomDeviceModel>> groupDeviceMap) {
        synchronized (mLock) {
            mGroupDeviceMap.clear();
            mGroupDeviceMap.putAll(groupDeviceMap);
        }
    }

    private void addAllFoundDevices(final List<Radio> radiosList,
                                       HashMap<MultiroomGroupModel, List<MultiroomDeviceModel>> groupDeviceMap) {

        DeviceListAllRetriever deviceListAllRetriever = new DeviceListAllRetriever(radiosList, mMultiroomManager, mNetRemoteManager);
        List<MultiroomDeviceModel> deviceListAllResults = deviceListAllRetriever.getDeviceListAllResults();

        FsLogger.log(">>>>>> RADIOS from NodeMultiroomDeviceListAll", LogLevel.Warning);

        for (MultiroomDeviceModel deviceModel : deviceListAllResults) {
            FsLogger.log("   " + deviceModel.mFrozenIsAvailable + " group: " + deviceModel.mGroupName +
                    " role: " + deviceModel.mGroupRole + " clientNum: " + deviceModel.mClientId + " " + deviceModel.mMultiroomUDN);

            addDeviceToMap(deviceModel, groupDeviceMap);
        }
    }

    private void showRadioListFromNetRemote_Debug(List<Radio> radios) {
        FsLogger.log(">>>>>> RADIOS", LogLevel.Warning);
        for (Radio radio : radios) {
            FsLogger.log("   " + radio + " mr: " + radio.isMultiroomCapable() + " " + radio.getDetailDescription() + " " + radio.getUDN());
        }
    }

    public MultiroomDeviceModel getDeviceByUdn(String udn) {
        return getDeviceByUdn(udn, mGroupDeviceMap);
    }

    private MultiroomDeviceModel getDeviceByUdn(String udn, HashMap<MultiroomGroupModel, List<MultiroomDeviceModel>> groupDeviceMap) {

        synchronized (mLock) {
            Set<MultiroomGroupModel> groupsList = groupDeviceMap.keySet();

            for (MultiroomGroupModel group : groupsList) {

                List<MultiroomDeviceModel> devicesList = groupDeviceMap.get(group);
                for (MultiroomDeviceModel device : devicesList) {
                    if (device.mRadio != null) {
                        if (mNetRemoteManager.matchUDN(udn, device.mRadio.getUDN())) {
                            return device;
                        }
                    }
                }
            }

            return null;
        }
    }
}
