package com.frontier_silicon.components.multiroom;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Node.BaseMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultichannelSystemState;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;

public class MultiroomDeviceModel {

	public Radio mRadio;

    // These fields will not change in case the data changes in Radio object. This is intended for RecyclerView
    // presentation, which needs stable data, otherwise it cannot detect changes and update the UI.
    public String mFrozenFriendlyName;
    public boolean mFrozenIsCheckingAvailability;
    public boolean mFrozenIsAvailable;

    public String mGroupId;
	public NodeMultiroomGroupState.Ord mGroupRole = BaseMultiroomGroupState.Ord.NO_GROUP;

	public String mGroupName;
	public String mOldGroupId = "";
	public String mMultiroomUDN = "";

	public long mClientId = 255;
	public long mSyncStatus = -1;
	public long mServerStatus = 1;

    public long mStereoRole = -1;
    public String mStereoSystemId = "";
    public String mStereoSystemName = "";
    public boolean mIsIncompleteStereoSystem = false;

    public MultiroomDeviceModel(Radio radio) {
        mRadio = radio;
        mFrozenFriendlyName = radio.getFriendlyName();
        mFrozenIsCheckingAvailability = radio.isCheckingAvailability();
        mFrozenIsAvailable = radio.isAvailable();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((mGroupId == null) ? 0 : mGroupId.hashCode());
		result = prime * result
				+ ((mGroupName == null) ? 0 : mGroupName.hashCode());
		result = prime * result + (int) (mGroupRole.ordinal() ^ (mGroupRole.ordinal() >>> 32));
		result = prime * result + ((mRadio == null) ? 0 : mRadio.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MultiroomDeviceModel other = (MultiroomDeviceModel) obj;
		if (mGroupId == null) {
			if (other.mGroupId != null)
				return false;
		} else if (!mGroupId.equals(other.mGroupId))
			return false;
		if (mGroupName == null) {
			if (other.mGroupName != null)
				return false;
		} else if (!mGroupName.equals(other.mGroupName))
			return false;
		if (mGroupRole != other.mGroupRole)
			return false;

        if (mStereoSystemId == null) {
            if (other.mStereoSystemId != null)
                return false;
        } else if (!mStereoSystemId.equals(other.mStereoSystemId))
            return false;

		if (mRadio == null) {
			if (other.mRadio != null)
				return false;
		} else if (!mRadio.equals(other.mRadio))
			return false;
		return true;
	}	
	
	public final boolean isServer() {
		return mGroupRole == NodeMultiroomGroupState.Ord.SERVER;
	}

	public final boolean isClient() {
		return mGroupRole == NodeMultiroomGroupState.Ord.CLIENT;
	}

	public final boolean isLonely() { return mGroupRole == NodeMultiroomGroupState.Ord.NO_GROUP; }

    public boolean isStereoCapable() {
        return mRadio != null && mRadio.isStereoCapable();
    }

    public boolean isSecondaryStereo() {
        return mStereoRole == NodeMultichannelSystemState.Ord.SECONDARY.ordinal();
    }

    public boolean isPrimaryStereo() {
        return mStereoRole == NodeMultichannelSystemState.Ord.PRIMARY.ordinal();
    }

    public boolean isStereoSystem() {
        return mStereoRole == NodeMultichannelSystemState.Ord.SECONDARY.ordinal() ||
                mStereoRole == NodeMultichannelSystemState.Ord.PRIMARY.ordinal();
    }

    public String toString() {
        if (isStereoCapable() && !TextUtils.isEmpty(mStereoSystemName)) {
            // return the name of Stereo system
            return mStereoSystemName;
        } else {
            return mRadio.getFriendlyName();
        }
    }
}
