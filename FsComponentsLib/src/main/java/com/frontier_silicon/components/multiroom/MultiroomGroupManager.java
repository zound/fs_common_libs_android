package com.frontier_silicon.components.multiroom;

import com.frontier_silicon.NetRemoteLib.Node.NodeE8;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus0;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus1;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus2;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientStatus3;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceListAllVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceServerStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupAttachedClients;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupId;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class MultiroomGroupManager {

	public static final String TEMP_GROUP_ID = "TEMP_ID";
	
	public static final String NO_GROUP_ID = "";
	public static final String NO_GROUP_NAME = "";
	
	public static final String NO_MR_GROUP_ID = "NON_MR_GROUP_ID";
	public static final String NO_MR_GROUP_NAME = "STANDALONE_DEVICES";

    public static final String STEREO_SECONDARY_GROUP_ID = "STEREO_SECONDARY_GROUP_ID";
    public static final String STEREO_SECONDARY_GROUP_NAME = "STEREO_SECONDARY_GROUP";
	
	public static final long WAIT_TIME_MS = 500;
	
	private static final long MAX_WAIT_TIME_DEVICE_LIST_ALL_NOTIF_MS = 5000;
	
	public static final long SERVER_NUM_ID = 255;
	
	private NetRemoteManager mNetRemoteManager = null;
	
	private long mDeviceListAllVersion = -1;

	private List<IMultiroomGroupingListener> mListeners = null;
	
	private boolean mOperationInProgressBlockRescan = false;
	
	private static MultiroomGroupManager mInstance = null;
	
	private MultiroomOperationsUtil mMultiroomOpUtil = null;
	private MultiroomListUtil mMultiroomListUtil = null;
	private MultiroomVolumeUtil mMultiroomVolUtil = null;

	private INodeNotificationCallback mMultiroomNodesNotifListener;

	public static MultiroomGroupManager getInstance() {
		if (mInstance == null)
			mInstance = new MultiroomGroupManager();
		
		return mInstance;
	}
 
	private MultiroomGroupManager() {
		mListeners = new CopyOnWriteArrayList<>();
		
		mOperationInProgressBlockRescan = false;
	}

	public void init(NetRemoteManager netRemoteManager) {

        mNetRemoteManager = netRemoteManager;
        mMultiroomOpUtil = new MultiroomOperationsUtil(this, mNetRemoteManager);
        mMultiroomListUtil = new MultiroomListUtil(this, mNetRemoteManager);
        mMultiroomVolUtil = new MultiroomVolumeUtil(this, mNetRemoteManager);
	}

    public boolean listIsEmpty() {
        return mMultiroomListUtil.listIsEmpty();
    }
	
	public boolean addListener(IMultiroomGroupingListener listener) {
        return !mListeners.contains(listener) && mListeners.add(listener);
	}
	
	public boolean removeListener(IMultiroomGroupingListener listener) {
        return mListeners.remove(listener);
	}

    private void notifyListenerOnUpdate() {
        for (IMultiroomGroupingListener listener : mListeners) {
			listener.onGroupingUpdate();
		}
    }

	public boolean rescanMultiroomGroupsAndDevices(boolean forceRescan) {

		// check if a rescan is already in progress
		if (mOperationInProgressBlockRescan) {
			return false;
		}

		mOperationInProgressBlockRescan = true;

        try {
            if (forceRescan) {
                mNetRemoteManager.getNetRemote().getDiscoveryService().rescan();
                clearDevicesList();
            }

            updateAllRadiosAndGroups();

            notifyListenerOnUpdate();
        } finally {
            mOperationInProgressBlockRescan = false;
        }

        return true;
	}

    public void recreateGroupsAndDevicesSync() {
        FsLogger.log("recreateGroupsAndDevicesSync", LogLevel.Info);

        updateAllRadiosAndGroups();

        notifyListenerOnUpdate();
    }

	public MultiroomDeviceModel findServerForGroup(MultiroomGroupModel group) {

		List<MultiroomDeviceModel> devicesList = mMultiroomListUtil.getAllDevicesForGroup(group);

        for (MultiroomDeviceModel device : devicesList) {
            if (device.isServer()) {
				return device;
			}
        }
		
		return null;
	}

	public boolean isCurrentDeviceInGroup(String groupId) {
        String currentGroupId = getCurrentGroupId();
		if (currentGroupId == null) {
			return false;
		}
		
		return currentGroupId.equalsIgnoreCase(groupId);
	}

    public boolean connectToRadio(Radio radio, boolean failSilently) {
        boolean connectedOk = false;

        if (radio != null) {
            FsLogger.log("try connect to radio " + radio);

            mNetRemoteManager.connectToRadio(radio, /*synchronous*/ true, /*setNodesBeforeConnection*/ true, failSilently);

            Radio currentRadio = mNetRemoteManager.getCurrentRadio();
            connectedOk = (currentRadio != null && currentRadio.getUDN().equals(radio.getUDN()));

            if (connectedOk) {
                FsLogger.log("Connected to another radio: " + mNetRemoteManager.getCurrentRadio(), LogLevel.Info);

                rescanMultiroomGroupsAndDevices(false);
                registerNotifCallback();
            } else {
                FsLogger.log("Connected to another radio error", LogLevel.Error);
                connectedOk = false;
            }
        }

        return connectedOk;
    }

	public long getMaxVolumeSteps() {
		return mMultiroomVolUtil.getVolumeSteps();
	}

	public synchronized void updateAllRadiosAndGroups() {
        mMultiroomListUtil.updateAllRadiosAndGroups();
	}

	public long getNumClientsForCurrentGroup() {
		long numClients = 0;
		
		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (!mNetRemoteManager.checkConnection(radio)) {
			return numClients;
		}

		NodeMultiroomGroupAttachedClients groupNumClientsNode = (NodeMultiroomGroupAttachedClients) radio.
                getNodeSyncGetter(NodeMultiroomGroupAttachedClients.class).get();
		
		if (groupNumClientsNode != null) {
			numClients = groupNumClientsNode.getValue();
		} else {
			numClients = 0;
		}
		
		return numClients;		
	}

	public List<MultiroomDeviceModel> getGroupOfRadio(Radio radio){

		// find radio group Id
		NodeMultiroomGroupId groupIdNode = (NodeMultiroomGroupId) radio.
                getNodeSyncGetter(NodeMultiroomGroupId.class).get();

		if (groupIdNode != null) {
			return mMultiroomListUtil.getAllDevicesForGroupId(groupIdNode.getValue());
		}

		return new ArrayList<>();
	}

	public String getCurrentGroupName() {
		String groupName = NO_GROUP_NAME;
		MultiroomDeviceModel currentDeviceModel = getCurrentSelectedDevice();
		
		if (currentDeviceModel == null || !mNetRemoteManager.checkConnection(currentDeviceModel.mRadio)) {
			return groupName;
		}

        groupName = currentDeviceModel.mGroupName;
		
		return groupName;
	}

	public String getCurrentGroupId() {
        MultiroomDeviceModel currentDeviceModel = getCurrentSelectedDevice();

        if (currentDeviceModel != null) {
            return currentDeviceModel.mGroupId;
        } else {
            return NO_GROUP_ID;
        }
    }
	
	public MultiroomDeviceModel getServerDeviceFromGroupId(String groupId) {
		MultiroomGroupModel group = findGroup(groupId);

		return findServerForGroup(group);
	}

	public String getClientFriendlyName(long clientId) {
        MultiroomDeviceModel clientDeviceModel = findClient(clientId);

        if (clientDeviceModel != null && clientDeviceModel.mRadio != null) {
            return clientDeviceModel.mRadio.getFriendlyName();
        }

        return "";
	}
	
	public MultiroomDeviceModel findClient(long clientId) {
		String groupId = getCurrentGroupId();
		MultiroomGroupModel group = findGroup(groupId);
		
		if (group != null) {
			List<MultiroomDeviceModel> devicesList = mMultiroomListUtil.getAllDevicesForGroup(group);
			for (MultiroomDeviceModel device : devicesList) {
				if (device.mClientId == clientId) {
					return device;
				}
			}
		}
		
		return null;
	}

	public boolean isUngroupedDevicesGroup(String groupId) {
		return MultiroomGroupManager.NO_GROUP_ID.equals(groupId) ||
				MultiroomGroupManager.NO_MR_GROUP_ID.equals(groupId);
	}

	public boolean isSecondaryStereoGroup(String groupId) {
        return MultiroomGroupManager.STEREO_SECONDARY_GROUP_ID.equals(groupId);
    }
	
	public boolean isCurrentDeviceInAnyMultiroomGroup() {
        MultiroomDeviceModel currentDeviceModel = getCurrentSelectedDevice();
		if (currentDeviceModel == null) {
			return false;
		}
		
		return (currentDeviceModel.mGroupRole == NodeMultiroomGroupState.Ord.SERVER ||
				currentDeviceModel.mGroupRole == NodeMultiroomGroupState.Ord.CLIENT) &&
                !isUngroupedDevicesGroup(currentDeviceModel.mGroupId) &&
                !isSecondaryStereoGroup(currentDeviceModel.mGroupId);
	}

	public NodeMultiroomGroupState.Ord getCurrentDeviceGroupRole() {
        MultiroomDeviceModel currentRadioDevice = getCurrentSelectedDevice();
		if (currentRadioDevice == null) {
			return NodeMultiroomGroupState.Ord.NO_GROUP;
		}

		return currentRadioDevice.mGroupRole;
	}
	
	public MultiroomOperationResult destroyGroup(String groupId) {

		FsLogger.log("Destroy group id: " + groupId);
		
		MultiroomGroupModel group = findGroup(groupId);
		boolean destroyedOk = mMultiroomOpUtil.destroyGroup(group);

		updateAllRadiosAndGroups();

        MultiroomOperationResult opResult = new MultiroomOperationResult();
		opResult.mGroup = group;
		if (destroyedOk) {
			opResult.mOperationResult = MultiroomOperationResult.OperationResult.SUCCESS;
		} else {
			opResult.mOperationResult = MultiroomOperationResult.OperationResult.DESTROY_GROUP_ERR;			
		}	
		
		return opResult;
	}

	public List<MultiroomItemModel> getItemModelsFromCurrentGroup() {
		return mMultiroomListUtil.getItemModelsFromCurrentGroup();
	}

    public List<MultiroomDeviceModel> getDeviceModelsFromCurrentGroup() {
        return mMultiroomListUtil.getDeviceModelsFromCurrentGroup();
    }

	public boolean isGroupNameDuplicated(String groupName) {
		return mMultiroomListUtil.isGroupNameDuplicated(groupName);
	}

    public MultiroomGroupModel findGroup(String groupId) {
        return mMultiroomListUtil.findGroup(groupId);
    }

    public List<MultiroomDeviceModel> getAllDevicesForGroupId(String groupId) {
        return mMultiroomListUtil.getAllDevicesForGroupId(groupId);
    }

    public List<MultiroomDeviceModel> getAllDevicesForGroup(MultiroomGroupModel groupModel) {
        return mMultiroomListUtil.getAllDevicesForGroup(groupModel);
    }

    public int getNumDevicesInGroup(String groupId) {
        List<MultiroomDeviceModel> devicesInGroup = getAllDevicesForGroupId(groupId);

        return devicesInGroup.size();
    }

	public List<MultiroomItemModel> getFlatGroupDeviceList() {
		return mMultiroomListUtil.getFlatGroupDeviceList();
	}

	public MultiroomDeviceModel getCurrentSelectedDevice() {
		return mMultiroomListUtil.getSelectedDevice();
	}

	public void setVolume(int volume, long clientId, boolean forceUpdate) {
		mMultiroomVolUtil.setVolume(volume, clientId, forceUpdate);
	}

	private void setVolumeInSteps(int volume, int clientId, boolean forceUpdate) {
		mMultiroomVolUtil.setVolumeInSteps(volume, clientId, forceUpdate);
	}

	public void initVolumeSteps() {
		mMultiroomVolUtil.updateVolumeSteps();
	}

	public int getVolume(long clientId) {
		return mMultiroomVolUtil.getVolume(clientId);
	}

	private int getVolumeInSteps(long clientId) {
		return mMultiroomVolUtil.getVolumeInSteps(clientId);
	}

	public int getMainVolume() {
		if (isCurrentDeviceInAnyMultiroomGroup()) {
			return getVolume(MultiroomVolumeUtil.MASTER_VOLUME_ID);
		} else {
			return getVolume(MultiroomVolumeUtil.SINGLE_DEVICE_VOLUME_ID);
		}
	}

	public void setMainVolume(int volume, boolean forceUpdate) {
		if (isCurrentDeviceInAnyMultiroomGroup()) {
			setVolume(volume, MultiroomVolumeUtil.MASTER_VOLUME_ID, forceUpdate);
		} else {
			setVolume(volume, MultiroomVolumeUtil.SINGLE_DEVICE_VOLUME_ID, forceUpdate);
		}
	}

	public int getMainVolumeInSteps() {
		if (isCurrentDeviceInAnyMultiroomGroup()) {
			return getVolumeInSteps(MultiroomVolumeUtil.MASTER_VOLUME_ID);
		} else {
			return getVolumeInSteps(MultiroomVolumeUtil.SINGLE_DEVICE_VOLUME_ID);
		}
	}

	public void setMainVolumeInSteps(int volume, boolean forceUpdate) {
		if (isCurrentDeviceInAnyMultiroomGroup()) {
			setVolumeInSteps(volume, MultiroomVolumeUtil.MASTER_VOLUME_ID, forceUpdate);
		} else {
			setVolumeInSteps(volume, MultiroomVolumeUtil.SINGLE_DEVICE_VOLUME_ID, forceUpdate);
		}
	}

	public boolean getMute(long clientId){
        return mMultiroomVolUtil.getMute(clientId);
	}

	public void setMute(boolean mute, long clientId) {
		mMultiroomVolUtil.setMute(mute, clientId);
	}

	public boolean getMainMute() {
		if (isCurrentDeviceInAnyMultiroomGroup()) {
			return getMute(MultiroomVolumeUtil.MASTER_VOLUME_ID);
		} else {
			return getMute(MultiroomGroupManager.SERVER_NUM_ID);
		}
	}

	public long getMaxNumClients() {
		return mMultiroomOpUtil.getMaxNumClients();
	}

	private void deregisterNodesNotificationListener() {
		if (mMultiroomNodesNotifListener != null) {

			Radio radio = mNetRemoteManager.getCurrentRadio();
			if (mNetRemoteManager.checkConnection(radio)) {
				radio.removeNotificationListener(mMultiroomNodesNotifListener);
			}
		}
	}

	public void registerNotifCallback() {
		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (mNetRemoteManager.checkConnection(radio)) {

			deregisterNodesNotificationListener();

			mMultiroomNodesNotifListener = new INodeNotificationCallback() {
				@Override
				public void onNodeUpdated(final NodeInfo node) {
					if (node instanceof NodeMultiroomDeviceListAllVersion) {
                        if (mOperationInProgressBlockRescan) {
                            return;
                        }

						// get MR devices list info on thread (don't block notification callback for other listeners)_
						Thread updateDeviceListAllThread = new Thread(new Runnable() {
							@Override
							public void run() {
								onDeviceListAllVersionChanged((NodeMultiroomDeviceListAllVersion) node);
							}
						}, "UpdateDeviceListAllThread");

						updateDeviceListAllThread.start();
					} else if (node instanceof NodeMultiroomClientStatus0) {
						onClientStatusChanged(node, 0);
					} else if (node instanceof NodeMultiroomClientStatus1) {
						onClientStatusChanged(node, 1);
					} else if (node instanceof NodeMultiroomClientStatus2) {
						onClientStatusChanged(node, 2);
					} else if (node instanceof NodeMultiroomClientStatus3) {
						onClientStatusChanged(node, 3);
					} else if (node instanceof NodeMultiroomDeviceServerStatus) {
						onServerStatusChanged((NodeMultiroomDeviceServerStatus)node);
					} else if (node instanceof NodeMultiroomGroupState) {

						Thread updateDeviceListAllThread = new Thread(new Runnable() {
							@Override
							public void run() {
								onGroupStateChanged((NodeMultiroomGroupState)node);
							}
						}, "UpdateDeviceListAllThread");

						updateDeviceListAllThread.start();
					}
				}
			};

			radio.addNotificationListener(mMultiroomNodesNotifListener);
		}
	}

	private void onGroupStateChanged(NodeMultiroomGroupState groupStateNode) {
        MultiroomDeviceModel currentDeviceModel = getCurrentSelectedDevice();
		if (currentDeviceModel != null) {
			if (currentDeviceModel.mServerStatus != groupStateNode.getValue()) {
				currentDeviceModel.mServerStatus = groupStateNode.getValue();
			}
		}

        refreshAll();

		FsLogger.log("NodeMultiroomGroupState changed " + groupStateNode.toString(), LogLevel.Info);
	}

	private void onServerStatusChanged(NodeMultiroomDeviceServerStatus serverStatusNode) {
        MultiroomDeviceModel currentDeviceModel = getCurrentSelectedDevice();
		if (currentDeviceModel != null) {
			if (currentDeviceModel.mServerStatus != serverStatusNode.getValue()) {
				currentDeviceModel.mServerStatus = serverStatusNode.getValue();
				notifyListenerOnUpdate();
			}
		}
	}

	private void onDeviceListAllVersionChanged(NodeMultiroomDeviceListAllVersion versionNode) {
		if (mDeviceListAllVersion != versionNode.getValue()) {
			mDeviceListAllVersion = versionNode.getValue();

            refreshAll();
		}

		FsLogger.log("NodeMultiroomDeviceListAllVersion changed " + versionNode.toString(), LogLevel.Warning);
	}

	private void refreshAll() {
        // wait for scan op. in progress to finish
        int count = 0;
        long maxWaitCnt = (MAX_WAIT_TIME_DEVICE_LIST_ALL_NOTIF_MS / WAIT_TIME_MS);
        while (mOperationInProgressBlockRescan && count < maxWaitCnt) {
            try {
                Thread.sleep(WAIT_TIME_MS);
                count++;
            } catch (InterruptedException e) {
            }
        }

        if (!mOperationInProgressBlockRescan) {
            mOperationInProgressBlockRescan = true;

            try {
                updateAllRadiosAndGroups();
                notifyListenerOnUpdate();

            } finally {
                mOperationInProgressBlockRescan = false;
            }
        }
    }

	private void onClientStatusChanged(final NodeInfo node, final int clientId) {
		NodeE8<?> nodeEnum = (NodeE8<?>)node;
		FsLogger.log("NodeMultiroomClientStatus" + clientId + " changed " + node.toString(), LogLevel.Warning);

		MultiroomDeviceModel clientDevice = findClient(clientId);
		if (clientDevice != null) {
			clientDevice.mSyncStatus = nodeEnum.getValue();
			notifyListenerOnUpdate();
		}
	}

	public boolean isTempGroup(String groupId) {
		return TEMP_GROUP_ID.equals(groupId);
	}

	public boolean isOperationInProgress() {
		return mOperationInProgressBlockRescan;
	}

    public void setOperationInProgress(boolean inProgress) {
        mOperationInProgressBlockRescan = inProgress;
    }

    public MultiroomOperationResult addDeviceToGroup(MultiroomDeviceModel device, MultiroomGroupModel group) {

		// save prev group to remove device asap after MR op (don't wait for ListAll)
		MultiroomGroupModel oldGroup = new MultiroomGroupModel();
		oldGroup.mGroupName = device.mGroupName;
		oldGroup.mGroupId = device.mOldGroupId;

		updateDeviceListBeforeRemovingServer(device, group);

		MultiroomOperationResult opResult = mMultiroomOpUtil.performMultiroomOperation(device, group);

		// update device/grouping info as soon as possible after MR op.
		if (opResult.mOperationResult == MultiroomOperationResult.OperationResult.SUCCESS) {
			boolean isAddOperation = !MultiroomGroupManager.NO_GROUP_ID.equals(group.mGroupId);

			if (isAddOperation) {
				mMultiroomListUtil.addDeviceToMap(device);
			} else {
				mMultiroomListUtil.removeDeviceFromGroupMap(device, oldGroup);
			}
		}

        return opResult;
    }

	private void updateDeviceListBeforeRemovingServer(MultiroomDeviceModel device, MultiroomGroupModel group) {
		// update devices list before removing a server (needed if MR_OP_CHECK = false)
		boolean isRemoveOp = (MultiroomGroupManager.NO_GROUP_ID.equals(group.mGroupId)) ||
							 (MultiroomGroupManager.NO_MR_GROUP_ID.equals(group.mGroupId));

		boolean isServer = device.isServer();

		if (isServer && isRemoveOp) {
			try {
				Thread.sleep(WAIT_TIME_MS);
			} catch (InterruptedException e) {
			}

			updateAllRadiosAndGroups();
		}
	}

    private void clearDevicesList() {
        mMultiroomListUtil.clearDevicesList();
    }

    public MultiroomDeviceModel getDeviceByUdn(String udn) {
        return mMultiroomListUtil.getDeviceByUdn(udn);
    }

    public MultiroomDeviceModel getAnyDeviceFromGroupId(String groupId) {
        List<MultiroomDeviceModel> multiroomDeviceModels = mMultiroomListUtil.getAllDevicesForGroupId(groupId);
        if (multiroomDeviceModels.size() > 0) {
            return multiroomDeviceModels.get(0);
        } else {
            return null;
        }
    }

    public Radio getServerOrUngroupedRadio(MultiroomDeviceModel deviceModel) {
        if (deviceModel == null) {
            FsLogger.log("Received deviceModel was null in getServerOrUngroupedRadio");
            return null;
        }

        if (isUngroupedDevicesGroup(deviceModel.mGroupId) ||
                deviceModel.mGroupRole == NodeMultiroomGroupState.Ord.SERVER) {
            return deviceModel.mRadio;
        } else {
            MultiroomDeviceModel serverDeviceModel = getServerDeviceFromGroupId(deviceModel.mGroupId);
            return serverDeviceModel != null ? serverDeviceModel.mRadio : null;
        }
    }

    public Set<MultiroomGroupModel> getGroupsFromDeviceMap() {
	    return mMultiroomListUtil.getGroupsFromDeviceMap();
    }

    public void sendSingleGroupCommand(Radio radio, boolean checked) {
        mMultiroomOpUtil.sendSingleGroupCommand(radio, checked);
    }

    public List<MultiroomDeviceModel> getAllMultiroomDeviceModels() {
        return mMultiroomListUtil.getAllMultiroomDeviceModels();
    }
}
