package com.frontier_silicon.components.multiroom;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientMute0;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientMute1;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientMute2;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientMute3;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientVolume0;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientVolume1;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientVolume2;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientVolume3;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupMasterVolume;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioMute;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioVolume;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsVolumeSteps;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.RadioNodeUtil;

import java.util.Map;

public class MultiroomVolumeUtil {

	public static final int SINGLE_DEVICE_VOLUME_ID = 255;
	public static final int MASTER_VOLUME_ID = -1;	
	final private static int MIN_TIME_BETWEEN_VOLUME_SET_MS = 100;
	private long mLastTimeVolumeSet = 0;
	private boolean mServerRequestInProgress = false;
	private boolean mNewVolumeShouldBeSent = false;
	private NodeInfo mVolumeNode;
	
	private MultiroomGroupManager mMultiroomManager;
    private NetRemoteManager mNetRemoteManager;
	
	private long mVolSteps = 0;
	
	public MultiroomVolumeUtil(MultiroomGroupManager multiroomManager, NetRemoteManager netRemoteManager){
		mMultiroomManager = multiroomManager;
        mNetRemoteManager = netRemoteManager;
	}
	
	public long updateVolumeSteps() {
		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (mNetRemoteManager.checkConnection(radio)) {
			NodeSysCapsVolumeSteps volStepNode = (NodeSysCapsVolumeSteps) radio.
                    getNodeSyncGetter(NodeSysCapsVolumeSteps.class).get();

			if (volStepNode != null) {
				mVolSteps = volStepNode.getValue();
			}
		}
		
		return mVolSteps;
	}
	
	public long getVolumeSteps() {
		return mVolSteps;
	}

	private boolean canSetVolume(long clientId) {
		return ( (System.currentTimeMillis() - mLastTimeVolumeSet > MIN_TIME_BETWEEN_VOLUME_SET_MS) || 
				 clientId == MultiroomGroupManager.SERVER_NUM_ID);
	}

	public void setVolumeInSteps(int volume, long clientId, boolean forceUpdate) {
		if (canSetVolume(clientId) || forceUpdate) {

			if (clientId == MASTER_VOLUME_ID) {
				setVolumeForMaster(volume, false);
			} else if (clientId == MultiroomGroupManager.SERVER_NUM_ID) {
				setVolumeForServer(volume, false);
			} else {
				setVolumeForClient(volume, clientId, false);
			}

			mLastTimeVolumeSet = System.currentTimeMillis();
		}
	}
	
	public void setVolume(int volume, long clientId, boolean forceUpdate) {
		if (canSetVolume(clientId) || forceUpdate) {
			
			if (clientId == MASTER_VOLUME_ID) {
				setVolumeForMaster(volume, true);
			} else if (clientId == MultiroomGroupManager.SERVER_NUM_ID) {
				setVolumeForServer(volume, true);
			} else {
				setVolumeForClient(volume, clientId, true);
			}
			
			mLastTimeVolumeSet = System.currentTimeMillis(); 
		}
	}

    private void setVolumeForServer(int volume, boolean inPercent) {

        if (inPercent) {
            volume = RadioNodeUtil.convertPercentVolumeToRadioVolume(volume, mVolSteps);
        }

        setVolumeForAll(new NodeSysAudioVolume((long) volume));
    }

    private void setVolumeForClient(int volume, long clientId, boolean inPercent) {
        Radio radio = mNetRemoteManager.getCurrentRadio();

        if (!mNetRemoteManager.checkConnection(radio))
            return;

        if (inPercent) {
            volume = RadioNodeUtil.convertPercentVolumeToRadioVolume(volume, mVolSteps);
        }

        if (clientId == 0) {
            setVolumeForAll(new NodeMultiroomClientVolume0((long) volume));
        }

        if (clientId == 1) {
            setVolumeForAll(new NodeMultiroomClientVolume1((long) volume));
        }

        if (clientId == 2) {
            setVolumeForAll(new NodeMultiroomClientVolume2((long) volume));
        }

        if (clientId == 3) {
            setVolumeForAll(new NodeMultiroomClientVolume3((long) volume));
        }
    }

    private void setVolumeForMaster(int volume, boolean inPercent) {

        if (inPercent) {
            volume = RadioNodeUtil.convertPercentVolumeToRadioVolume(volume, mVolSteps);
        }

        setVolumeForAll(new NodeMultiroomGroupMasterVolume((long) volume));
    }

	private void setVolumeForAll(NodeInfo volumeNode) {
		mVolumeNode = volumeNode;

		if (mServerRequestInProgress) {
//            log.d("VOLUME_ALL", "IN_PROGRESS");
			mNewVolumeShouldBeSent = true;
			return;
		} else {
//            log.d("VOLUME_ALL", "RUNNING");
			Radio radio = mNetRemoteManager.getCurrentRadio();

			if (!mNetRemoteManager.checkConnection(radio))
				return;

			mServerRequestInProgress = true;
			RadioNodeUtil.setNodeToRadioAsync(radio, mVolumeNode, new RadioNodeUtil.INodeSetResultListener() {
				@Override
				public void onNodeSetResult(boolean success) {
					mServerRequestInProgress = false;

					if (mNewVolumeShouldBeSent) {
//                        log.d("VOLUME_ALL", "NewVolumShouldBeSentForServer1: " + mNewVolumeShouldBeSent);
						mNewVolumeShouldBeSent = false;
						setVolumeForAll(mVolumeNode);
					}
				}
			});

		}
	}

	public void setMute(boolean mute, long clientId) {
		if (clientId == MASTER_VOLUME_ID) {
			//no master mute
		} else if (clientId == MultiroomGroupManager.SERVER_NUM_ID) {
			setMuteForServer(mute);
		} else {
			setMuteForClient(mute, clientId);
		}			
	}

	private void setMuteForClient(boolean mute, long clientId) {
		long muteValue = 0;
		if (mute)
			muteValue = 1;

		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (!mNetRemoteManager.checkConnection(radio))
			return;
		
		if (clientId == 0) {
			radio.setNode(new NodeMultiroomClientMute0(muteValue), false);
		}
		
		if (clientId == 1) {
            radio.setNode(new NodeMultiroomClientMute1(muteValue), false);
		}
		
		if (clientId == 2) {
            radio.setNode(new NodeMultiroomClientMute2(muteValue), false);
		}
		
		if (clientId == 3) {
            radio.setNode(new NodeMultiroomClientMute3(muteValue), false);
		}
	}

	private void setMuteForServer(boolean mute) {
		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (!mNetRemoteManager.checkConnection(radio))
			return;

        NodeSysAudioMute.Ord ord = mute ? NodeSysAudioMute.Ord.MUTE : NodeSysAudioMute.Ord.NOT_MUTE;
        radio.setNode(new NodeSysAudioMute(ord), false);
	}

	public int getVolumeInSteps(long clientId) {
		long volume = 0;

		if (clientId == MASTER_VOLUME_ID) {
			volume = getMasterVolume();
		} else if (clientId == MultiroomGroupManager.SERVER_NUM_ID) {
			volume = getServerVolume();
		} else {
			volume = getClientVolume(clientId);
		}

		return (int)volume;
	}
	
	public int getVolume(long clientId) {
		long volume = 0;
		
		if (clientId == MASTER_VOLUME_ID) {
			volume = getMasterVolume();
		} else if (clientId == MultiroomGroupManager.SERVER_NUM_ID) {
			volume = getServerVolume();
		} else {
			volume = getClientVolume(clientId);
		}
		
		volume = RadioNodeUtil.convertValueToPercent(volume, mVolSteps);
		
		return (int)volume;
	}
	
	private long getServerVolume() {
		long volume = 0;
		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (!mNetRemoteManager.checkConnection(radio))
			return volume;
	
		NodeSysAudioVolume volumeNode = (NodeSysAudioVolume) radio.
                getNodeSyncGetter(NodeSysAudioVolume.class).get();
		if (volumeNode != null)
			volume = volumeNode.getValue();
		
		return volume;
	}

	private long getClientVolume(long clientId) {
		long volume = 0;
		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (!mNetRemoteManager.checkConnection(radio))
			return volume;
		
		if (clientId == 0) {
			NodeMultiroomClientVolume0 volNode = (NodeMultiroomClientVolume0) radio.
                    getNodeSyncGetter(NodeMultiroomClientVolume0.class).get();
		
			if (volNode != null)
				volume = volNode.getValue();
		}
		
		if (clientId == 1) {
			NodeMultiroomClientVolume1 volNode = (NodeMultiroomClientVolume1) radio.
                    getNodeSyncGetter(NodeMultiroomClientVolume1.class).get();
			
			if (volNode != null)
				volume = volNode.getValue();
		}
		
		if (clientId == 2) {
			NodeMultiroomClientVolume2 volNode = (NodeMultiroomClientVolume2) radio.
                    getNodeSyncGetter(NodeMultiroomClientVolume2.class).get();
			
			if (volNode != null)
				volume = volNode.getValue();
		}
		
		if (clientId == 3) {
			NodeMultiroomClientVolume3 volNode = (NodeMultiroomClientVolume3) radio.
                    getNodeSyncGetter(NodeMultiroomClientVolume3.class).get();
			
			if (volNode != null)
				volume = volNode.getValue();
		}
		
		return volume;
	}

	private long getMasterVolume() {
		long volume = 0;
		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (!mNetRemoteManager.checkConnection(radio))
			return volume;
		
		NodeMultiroomGroupMasterVolume volNode = (NodeMultiroomGroupMasterVolume) radio.
                getNodeSyncGetter(NodeMultiroomGroupMasterVolume.class).get();
		
		if (volNode != null)
			volume = volNode.getValue();		
		
		return volume;
	}

	public boolean getMute(long clientId) {
		boolean mute = false;
		
		if (clientId == MASTER_VOLUME_ID) {
			mute = getMasterMute();
		} else if (clientId == MultiroomGroupManager.SERVER_NUM_ID) {
			mute = getServerMute();
		} else {
			mute = getClientMute(clientId);
		}
		
		return mute;
	}

	private boolean getServerMute() {
		boolean mute = false;

		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (mNetRemoteManager.checkConnection(radio)) {
			NodeSysAudioMute muteNode = (NodeSysAudioMute) radio.
                    getNodeSyncGetter(NodeSysAudioMute.class).get();

			if (muteNode != null) {
				mute = (muteNode.getValue() == NodeSysAudioMute.Ord.MUTE.ordinal());
			}
		}
		
		return mute;
	}

	private boolean getClientMute(long clientId) {
		boolean mute = false;

		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (!mNetRemoteManager.checkConnection(radio))
			return mute;
		
		if (clientId == 0) {
			NodeMultiroomClientMute0 muteNode = (NodeMultiroomClientMute0) radio.
                    getNodeSyncGetter(NodeMultiroomClientMute0.class).get();
		
			if (muteNode != null)
				mute = (muteNode.getValue() == NodeMultiroomClientMute0.Ord.MUTE.ordinal());
		}
		
		if (clientId == 1) {
			NodeMultiroomClientMute1 muteNode = (NodeMultiroomClientMute1) radio.
                    getNodeSyncGetter(NodeMultiroomClientMute1.class).get();
			
			if (muteNode != null)
				mute = (muteNode.getValue() == NodeMultiroomClientMute1.Ord.MUTE.ordinal());
		}
		
		if (clientId == 2) {
			NodeMultiroomClientMute2 muteNode = (NodeMultiroomClientMute2) radio.
                    getNodeSyncGetter(NodeMultiroomClientMute2.class).get();
			
			if (muteNode != null)
				mute = (muteNode.getValue() == NodeMultiroomClientMute2.Ord.MUTE.ordinal());
		}
		
		if (clientId == 3) {
			NodeMultiroomClientMute3 muteNode = (NodeMultiroomClientMute3) radio.
                    getNodeSyncGetter(NodeMultiroomClientMute3.class).get();
			
			if (muteNode != null)
				mute = (muteNode.getValue() == NodeMultiroomClientMute3.Ord.MUTE.ordinal());
		}
		
		return mute;
	}

	private boolean getMasterMute() {
		boolean mute = getServerMute();

		int maxClients = (int) mMultiroomManager.getMaxNumClients();
		for (int clientId = 0; clientId < maxClients; clientId++) {
			if (mMultiroomManager.findClient(clientId) != null) {
				mute = (mute && getClientMute(clientId));
			}
		}

		return mute;
	}

	private boolean getMasterMuteMultiple() {
		boolean mute = false;
		Radio radio = mNetRemoteManager.getCurrentRadio();

		if (mNetRemoteManager.checkConnection(radio)) {
			Map<Class, NodeInfo> muteNodes = radio.getNodesSyncGetter(new Class[] {NodeSysAudioMute.class,
					NodeMultiroomClientMute0.class, NodeMultiroomClientMute1.class,
                    NodeMultiroomClientMute2.class, NodeMultiroomClientMute3.class}).get();

			if (muteNodes != null) {
				NodeSysAudioMute serverMute = (NodeSysAudioMute) muteNodes.get(NodeSysAudioMute.class);
				NodeMultiroomClientMute0 client0Mute = (NodeMultiroomClientMute0) muteNodes.get(NodeMultiroomClientMute0.class);
				NodeMultiroomClientMute1 client1Mute = (NodeMultiroomClientMute1) muteNodes.get(NodeMultiroomClientMute1.class);
				NodeMultiroomClientMute2 client2Mute = (NodeMultiroomClientMute2) muteNodes.get(NodeMultiroomClientMute2.class);
				NodeMultiroomClientMute3 client3Mute = (NodeMultiroomClientMute3) muteNodes.get(NodeMultiroomClientMute3.class);

				if (serverMute != null) {
					mute = serverMute.getValue() == NodeSysAudioMute.Ord.MUTE.ordinal();
				}

				if (client0Mute != null) {
					mute = mute && (client0Mute.getValue() == NodeMultiroomClientMute0.Ord.MUTE.ordinal());
				}

				if (client1Mute != null) {
					mute = mute && (client1Mute.getValue() == NodeMultiroomClientMute1.Ord.MUTE.ordinal());
				}

				if (client2Mute != null) {
					mute = mute && (client2Mute.getValue() == NodeMultiroomClientMute2.Ord.MUTE.ordinal());
				}

				if (client3Mute != null) {
					mute = mute && (client3Mute.getValue() == NodeMultiroomClientMute3.Ord.MUTE.ordinal());
				}
			}
		}

		return mute;
	}

}
