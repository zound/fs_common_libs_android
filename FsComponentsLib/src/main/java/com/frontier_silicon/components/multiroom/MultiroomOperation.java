package com.frontier_silicon.components.multiroom;

public class MultiroomOperation {

    public enum MultiroomGroupEditOperation {
        DESTROY_GROUP,
        CREATE_GROUP,
        ADD_REMOVE_SPEAKER_TO_FROM_GROUP
    }

    public String mMultiroomDeviceUDN;

    public String mOldGroupId;
    public String mGroupId;
    public String mOldGroupName;
    public String mGroupName;
    public Boolean mTransportOptimisation = null;

    public MultiroomGroupEditOperation mOperationType;

    public String toString() {
        return "MultiroomOperation: " + mOperationType + " " + mMultiroomDeviceUDN + " groupId: " + mGroupId + " old groupId: " + mOldGroupId;
    }

}
