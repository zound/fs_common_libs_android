package com.frontier_silicon.components.multiroom;

import com.frontier_silicon.components.connection.AfterConnectionNodesSetter;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.Iterator;
import java.util.List;

public class MultiroomOperationsQueueExecutor {

    private static final long WAIT_DEVICE_LIST_ALL_STABILIZE_MS = 2000;
    private MultiroomGroupManager mMultiroomManager;
    private List<MultiroomOperation> mOperationsList;

    public MultiroomOperationsQueueExecutor(List<MultiroomOperation> operationsList) {
        mMultiroomManager = MultiroomGroupManager.getInstance();
        mOperationsList = operationsList;
    }

    public MultiroomOperationResult execute() {
        MultiroomOperationResult operationResult = null;

        mMultiroomManager.setOperationInProgress(true);

        try {
            Iterator<MultiroomOperation> it = mOperationsList.iterator();
            while (it.hasNext()) {
                MultiroomOperation operation = it.next();

                operationResult = executeOperation(operation);

                if (operation.mOperationType == MultiroomOperation.MultiroomGroupEditOperation.CREATE_GROUP &&
                        operationResult.mOperationResult == MultiroomOperationResult.OperationResult.SUCCESS) {
                    updateOperationsGroupId(operationResult);
                }

                boolean isCriticalError = false;
                switch (operationResult.mOperationResult) {
                    case SUCCESS:
                        break;

                    // abort all operations if critical error
                    case CREATE_GROUP_ERR:
                    case ADD_CLIENT_MAX_CLIENT_ERR:
                    case TIMEOUT_ERR:
                    case DIFFERENT_MR_VERSION_ERR:
                    case MR_NOT_SUPPORTED_ERR:
                    case STREAMABLE_MODE_NEEDED_ERR:
                        isCriticalError = true;
                        break;

                    default:
                        break;
                }

                if (isCriticalError) {
                    break;
                }
            }
        } finally {
            mMultiroomManager.setOperationInProgress(false);
        }


        try {
            Thread.sleep(WAIT_DEVICE_LIST_ALL_STABILIZE_MS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        MultiroomRescanUtil.getInstance().rescanMultiroomDevicesAndGroups(false);

        dispose();

        return operationResult;
    }

    private void updateOperationsGroupId(MultiroomOperationResult operationResult) {
        String newGroupId = operationResult.mDevice.mGroupId;

        for (MultiroomOperation op : mOperationsList) {
            op.mGroupId = newGroupId;
        }
    }

    private MultiroomOperationResult executeOperation(MultiroomOperation operation) {
        MultiroomOperationResult operationResult = new MultiroomOperationResult();
        operationResult.mOperationResult = MultiroomOperationResult.OperationResult.SUCCESS;

        FsLogger.log(">>>>> Executing " + operation);

        if (operation.mOperationType == MultiroomOperation.MultiroomGroupEditOperation.CREATE_GROUP) {
            operationResult = executeCreateGroupOperation(operation);
        } else if (operation.mOperationType == MultiroomOperation.MultiroomGroupEditOperation.ADD_REMOVE_SPEAKER_TO_FROM_GROUP) {
            // removing a device == ADD to NO_GROUP_ID group
            operationResult = executeAddOrRemoveClientOperation(operation);
        } else if (operation.mOperationType == MultiroomOperation.MultiroomGroupEditOperation.DESTROY_GROUP) {
            operationResult = mMultiroomManager.destroyGroup(operation.mGroupId);
        }

        return operationResult;
    }

    private MultiroomOperationResult executeAddOrRemoveClientOperation(MultiroomOperation operation) {
        MultiroomOperationResult operationResult = new MultiroomOperationResult();
        operationResult.mOperationResult = MultiroomOperationResult.OperationResult.ADD_CLIENT_ERR;

        MultiroomDeviceModel device = mMultiroomManager.getDeviceByUdn(operation.mMultiroomDeviceUDN);
        if (device != null) {
            device.mOldGroupId = operation.mOldGroupId;

            MultiroomGroupModel group;
            if (operation.mGroupId.equals(MultiroomGroupManager.NO_GROUP_ID)) {
                // is remove operation
                group = new MultiroomGroupModel();
                group.mGroupId = MultiroomGroupManager.NO_GROUP_ID;
                group.mGroupName = MultiroomGroupManager.NO_GROUP_NAME;
                group.mNumClients = 0;
            } else {
                group = getGroup(operation.mGroupId, operation.mGroupName);
            }

            if (group != null) {
                if (operation.mTransportOptimisation != null) {
                    AfterConnectionNodesSetter.setTransportOptimisationSync(device.mRadio, operation.mTransportOptimisation);
                }
                operationResult = mMultiroomManager.addDeviceToGroup(device, group);
            } else {
                FsLogger.log("Group not found " + operation, LogLevel.Error);
                operationResult.mOperationResult = MultiroomOperationResult.OperationResult.ADD_CLIENT_ERR;
            }
        } else {
            FsLogger.log("Device not found " + operation, LogLevel.Error);
        }

        return operationResult;
    }

    private MultiroomOperationResult executeCreateGroupOperation(MultiroomOperation operation) {
        MultiroomOperationResult operationResult = new MultiroomOperationResult();
        operationResult.mOperationResult = MultiroomOperationResult.OperationResult.CREATE_GROUP_ERR;

        MultiroomDeviceModel device = mMultiroomManager.getDeviceByUdn(operation.mMultiroomDeviceUDN);
        if (device != null) {
            device.mOldGroupId = operation.mOldGroupId;

            MultiroomGroupModel group = prepareNewGroup(operation);
            if (group != null) {
                operationResult = mMultiroomManager.addDeviceToGroup(device, group);
            } else {
                FsLogger.log("Prepare new group error " + operation, LogLevel.Error);
                operationResult.mOperationResult = MultiroomOperationResult.OperationResult.CREATE_GROUP_ERR;
            }
        } else {
            FsLogger.log("Device not found " + operation, LogLevel.Error);
        }

        return operationResult;
    }

    private MultiroomGroupModel prepareNewGroup(MultiroomOperation operation) {
        MultiroomGroupModel group = null;

        if (MultiroomGroupManager.TEMP_GROUP_ID.equals(operation.mGroupId)) {
            group = new MultiroomGroupModel();
            group.mGroupId = operation.mGroupId;
            group.mGroupName = operation.mGroupName;
        }

        return group;
    }

    private MultiroomGroupModel getGroup(String groupId, String groupName) {
        MultiroomGroupModel group = mMultiroomManager.findGroup(groupId);

        if (group != null && (group.mGroupId != null) && (group.mGroupName != null)) {
            if (group.mGroupId.equalsIgnoreCase(groupId) && group.mGroupName.equals(groupName)) {

                MultiroomGroupModel groupCopy = new MultiroomGroupModel();
                groupCopy.mGroupId = group.mGroupId;
                groupCopy.mGroupName = group.mGroupName;
                groupCopy.mNumClients = group.mNumClients;
                return groupCopy;
            }
        }

        return null;
    }

    private void dispose() {
        mMultiroomManager = null;
        mOperationsList = null;
    }
}
