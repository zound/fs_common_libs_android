package com.frontier_silicon.components.multiroom;

import com.frontier_silicon.loggerlib.LogLevel;
import com.frontier_silicon.loggerlib.FsLogger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MultiroomRescanUtil {

    private static MultiroomRescanUtil mInstance;

    private final int corePoolSize = 0;
    private final int maxPoolSize = 1;
    // Sets the amount of time an idle thread waits before terminating
    private final int KEEP_ALIVE_TIME = 1;
    private final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private BlockingQueue<Runnable> mRebuildRunnablesQueue = new LinkedBlockingQueue<>(2);
    private ExecutorService mThreadPoolExecutor;

    public static MultiroomRescanUtil getInstance() {
        if (mInstance == null) {
            mInstance = new MultiroomRescanUtil();
        }
        return mInstance;
    }

    private MultiroomRescanUtil() {
        mThreadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT, mRebuildRunnablesQueue, new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                FsLogger.log("MultiroomRescanUtil: thread execution was rejected", LogLevel.Error);
            }
        });
    }

    synchronized public boolean rescanMultiroomDevicesAndGroups(boolean forceRescanSSDP) {

        if (mRebuildRunnablesQueue.size() > 1) {
            FsLogger.log("MultiroomRescanUtil: rebuild queue is full", LogLevel.Info);
            return false;
        }

        FsLogger.log("MultiroomRescanUtil: adding task in rebuild queue", LogLevel.Info);
        mThreadPoolExecutor.submit(new RescanRunnable(forceRescanSSDP));

        return true;
    }

    private class RescanRunnable implements Runnable {
        private boolean mForceRescan;

        RescanRunnable(boolean forceRescan) {
            mForceRescan = forceRescan;
        }

        @Override
        public void run() {
            MultiroomGroupManager.getInstance().rescanMultiroomGroupsAndDevices(mForceRescan);
        }
    }
}
