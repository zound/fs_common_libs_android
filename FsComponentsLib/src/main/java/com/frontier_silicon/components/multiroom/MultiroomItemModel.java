package com.frontier_silicon.components.multiroom;

import android.support.annotation.IntDef;
import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class MultiroomItemModel {

    @IntDef({SimpleSpeaker, MultiroomSpeaker, MultiroomGroup, SingleGroupSpeaker, SingleGroup})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MultiroomItemModelType {}
    /**
     * Speaker without multiroom technology
     */
    public final static int SimpleSpeaker = 0;

    /**
     * Speaker with the original multiroom technology
     */
    public final static int MultiroomSpeaker = 1;

    /**
     * Multiroom group
     */
    public final static int MultiroomGroup = 2;

    /**
     * Speaker with SG flavour of multiroom
     */
    public final static int SingleGroupSpeaker = 3;

    /**
     * Group with SG flavour of multiroom
     */
    public final static int SingleGroup = 4;

	private boolean mIsGroupHeader = false;
	private String mGroupName;
	private String mGroupId;
    private List<MultiroomDeviceModel> mMultiroomDeviceModels;
	private Radio mRadio = null;
    private MultiroomDeviceModel mDeviceModel;
    private @MultiroomItemModelType int mMultiroomItemModelType;
	
	public MultiroomItemModel(String groupName, String groupId, List<MultiroomDeviceModel> devicesList) {
		mIsGroupHeader = true;
		mGroupName = groupName;
		mGroupId = groupId;
        mMultiroomDeviceModels = devicesList;

        if (mMultiroomDeviceModels.size() > 0) {
            MultiroomDeviceModel multiroomDeviceModel = mMultiroomDeviceModels.get(0);

            mMultiroomItemModelType = multiroomDeviceModel.mRadio.isSG() ? SingleGroup : MultiroomGroup;
        }
	}
	
	public MultiroomItemModel(MultiroomDeviceModel deviceModel) {
        mDeviceModel = deviceModel;
		mIsGroupHeader = false;
		mGroupName = deviceModel.mGroupName;
		mGroupId = deviceModel.mGroupId;
		mRadio = deviceModel.mRadio;

        if (mRadio.isSG()) {
            mMultiroomItemModelType = SingleGroupSpeaker;
        } else {
            mMultiroomItemModelType = mRadio.isMultiroomCapable() ? MultiroomSpeaker : SimpleSpeaker;
        }
	}
	
	public boolean getIsGroupHeader() {
		return mIsGroupHeader;
	}
	
	public String getGroupName() {
		return mGroupName;
	}
	
	public String getGroupId() {
		return mGroupId;
	}

	// This list will not change in case group structure changes.
    // It's needed by RecyclerView which works with stateless data sets
	public List<MultiroomDeviceModel> getFrozenMultiroomDeviceModels() {
        return mMultiroomDeviceModels;
    }

	public @MultiroomItemModelType int getType() {
        return mMultiroomItemModelType;
    }
	
	public Radio getRadio() {
		return mRadio;
	}

	public MultiroomDeviceModel getDeviceModel() {
        return mDeviceModel;
    }
	
	public boolean getIsActive() {
		if (mIsGroupHeader) {
			return MultiroomGroupManager.getInstance().isCurrentDeviceInGroup(mGroupId);
		} else {
			Radio currentRadio = NetRemoteManager.getInstance().getCurrentRadio();
			if ((currentRadio != null) && (mRadio != null)) {
				return currentRadio.getSN().equals(mRadio.getSN());
			}
		}

		return false;
	}

	public String toString() {
		if (mIsGroupHeader) {
			return mGroupName;
		} else {
            if (mDeviceModel.isStereoCapable() && !TextUtils.isEmpty(mDeviceModel.mStereoSystemName)) {
                // return the name of Stereo system
                return mDeviceModel.mStereoSystemName;
            } else {
                return mRadio.getFriendlyName();
            }
		}
	}

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        MultiroomItemModel other = (MultiroomItemModel)obj;
        if (mGroupId == null) {
            if (other.mGroupId != null)
                return false;
        } else if (!mGroupId.equals(other.mGroupId))
            return false;

        if (mMultiroomItemModelType != other.getType())
            return false;

        if (mDeviceModel == null) {
            if (other.mDeviceModel != null) {
                return false;
            }
        } else if (!mDeviceModel.equals(other.mDeviceModel)) {
            return false;
        }

        return true;
    }
}
