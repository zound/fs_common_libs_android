package com.frontier_silicon.components.multiroom;

public class MultiroomVolumeModel {

	public String mRadioFriendlyName;
	public long mClientId;
	public String mClientUDN;
	public int mVolume = 50;
	public boolean mMute = false;
	public boolean mSyncEnabled = false;
	public boolean mInTouchMode = false;
	public boolean mEqualizerSupported = false;
	
	public boolean isServer() {
		return (mClientId == MultiroomGroupManager.SERVER_NUM_ID);
	}
	
	public boolean canChangeVolume() {
		return !mInTouchMode;
	}
	
}
