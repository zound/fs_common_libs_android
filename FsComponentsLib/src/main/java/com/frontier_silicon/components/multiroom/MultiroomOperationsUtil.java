package com.frontier_silicon.components.multiroom;

import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomCapsMaxClients;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceClientIndex;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupAddClient;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupAttachedClients;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupCreate;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupDestroy;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupId;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupRemoveClient;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupStreamable;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomSinglegroupState;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.List;

class MultiroomOperationsUtil {

	private static final long WAIT_BEFORE_GET_CLIENT_ID_MS = 1000;
    private static final long WAIT_SHORT = 500;
	private MultiroomGroupManager mMultiroomManager = null;
    private NetRemoteManager mNetRemoteManager;

	public MultiroomOperationsUtil(MultiroomGroupManager multiroomManager, NetRemoteManager netRemoteManager){
		mMultiroomManager = multiroomManager;
        mNetRemoteManager = netRemoteManager;
	}

	public MultiroomOperationResult performMultiroomOperation(MultiroomDeviceModel device, MultiroomGroupModel group) {
		if (MultiroomGroupManager.TEMP_GROUP_ID.equals(group.mGroupId)) {
			return createGroupAndAddDevice(device, group);
		} else if (MultiroomGroupManager.NO_GROUP_ID.equals(group.mGroupId)) {
			return ungroupDevice(device);
		} else if (MultiroomGroupManager.NO_MR_GROUP_ID.equals(group.mGroupId)) {
			return ungroupDevice(device);
		} else {
			return addDeviceToExistingGroup(device, group);
		}		
	}

	private MultiroomOperationResult ungroupDevice(MultiroomDeviceModel device) {
		MultiroomOperationResult opResult = new MultiroomOperationResult();

		boolean ungroupedOk = false;
		
		String clientUDN = device.mRadio.getUDN();
		String multiroomUDN = device.mMultiroomUDN;
		FsLogger.log("Ungrouping device " + clientUDN + " with multiroom udn " + multiroomUDN);

		// server of another group
		if (device.isServer()) {
			ungroupedOk = removeServerOfGroup(device.mOldGroupId);

		} else if (device.isClient()) {
			ungroupedOk = removeClientFromGroup(multiroomUDN, device.mOldGroupId);
		}

		if (ungroupedOk) {
			opResult.mOperationResult = MultiroomOperationResult.OperationResult.SUCCESS;

			// set new group info here, don't wait for listall update
			device.mGroupName = MultiroomGroupManager.NO_GROUP_NAME;
			device.mGroupRole = NodeMultiroomGroupState.Ord.NO_GROUP;
			device.mGroupId = MultiroomGroupManager.NO_GROUP_ID;

		} else {
			opResult.mOperationResult = MultiroomOperationResult.OperationResult.UNGROUP_DEVICE_ERR;
		}

		opResult.mDevice = device;

		return opResult;
	}

	private boolean removeServerOfGroup(String groupId) {
		boolean ungroupedOk = false;
		
		FsLogger.log("Remove server of group: " + groupId, LogLevel.Warning);

		Radio serverRadio = null;
		MultiroomDeviceModel serverDevice = mMultiroomManager.getServerDeviceFromGroupId(groupId);
		if (serverDevice != null) {
			serverRadio = serverDevice.mRadio;
		}
		
		if (serverRadio != null) {
			long numClients = 0;
			NodeMultiroomGroupAttachedClients groupNumClientsNode = (NodeMultiroomGroupAttachedClients) serverRadio.
                    getNodeSyncGetter(NodeMultiroomGroupAttachedClients.class, true).get();

			if (groupNumClientsNode != null) {
				numClients = groupNumClientsNode.getValue();
			}
			
			if (numClients > 0) {
                // other clients in group => make the first found client the new server
				boolean becomeOk = false;

                List<MultiroomDeviceModel> devicesInCurrentGroup = mMultiroomManager.getAllDevicesForGroupId(groupId);
                for (MultiroomDeviceModel device : devicesInCurrentGroup) {
                    if (device.mGroupRole == NodeMultiroomGroupState.Ord.CLIENT) {
                        becomeOk = makeClientIntoServer(device.mRadio);
                       if (becomeOk) {
						   // Update internal node structure to make client into server and old server into client
						   serverDevice.mGroupRole = NodeMultiroomGroupState.Ord.CLIENT;
						   device.mGroupRole = NodeMultiroomGroupState.Ord.SERVER;
                           break;
                       }
                    }
                }

				if (becomeOk) {
					// Wait for 2 seconds to give time to the speaker to internally change the old server into a client
					RadioNodeUtil.waitMs(4000);
					ungroupedOk = removeClientFromGroup(serverDevice.mMultiroomUDN, groupId);
				}
				
			} else {
                // no other devices in group => destroy group
				ungroupedOk = destroyGroup(mMultiroomManager.findGroup(groupId));
			}
		}
		
		return ungroupedOk;
	}

	private boolean removeClientFromGroup(String clientUDN, String groupId) {
		boolean removedOk = false;
		
		FsLogger.log("Remove client " + clientUDN + " from group " + groupId, LogLevel.Warning);

		Radio radio = null;
		MultiroomDeviceModel serverDevice = mMultiroomManager.getServerDeviceFromGroupId(groupId);
		if (serverDevice != null) {
			radio = serverDevice.mRadio;
		}

		if (radio != null) {
			removedOk = radio.getNodeSyncSetter(new NodeMultiroomGroupRemoveClient(clientUDN)).set();

			if (!removedOk) {
				FsLogger.log("Error removing client " + clientUDN, LogLevel.Error);
			}
		}
		
		return removedOk;
	}

	private MultiroomOperationResult addDeviceToExistingGroup(MultiroomDeviceModel device, MultiroomGroupModel group) {
		MultiroomOperationResult opResult = new MultiroomOperationResult();
		MultiroomOperationResult.OperationResult result = MultiroomOperationResult.OperationResult.ADD_CLIENT_ERR;

		FsLogger.log("Add device to existing group " + group.mGroupName + " " + device.mRadio);

		// change device role if changing group => findServerForGroup returns the real group server
		if (device.isServer()) {
            device.mGroupRole = NodeMultiroomGroupState.Ord.NO_GROUP;
        }

		// find server for that group
		MultiroomDeviceModel serverDevice = mMultiroomManager.findServerForGroup(group);

		if (serverDevice != null) {
			if (isMultiroomVersionCompatible(device, serverDevice)) {
				boolean addedOk = serverDevice.mRadio.getNodeSyncSetter(new NodeMultiroomGroupAddClient(device.mMultiroomUDN)).set();

				if (addedOk) {
					result = MultiroomOperationResult.OperationResult.SUCCESS;

					// set new group info here, don't wait for listall update
					device.mGroupName = group.mGroupName;
					device.mGroupId = group.mGroupId;
					device.mGroupRole = NodeMultiroomGroupState.Ord.CLIENT;

					if (device.mRadio != null) {
						try {
							Thread.sleep(WAIT_BEFORE_GET_CLIENT_ID_MS);
						} catch (InterruptedException e) {
						}
						NodeMultiroomDeviceClientIndex clientIdNode = (NodeMultiroomDeviceClientIndex) device.mRadio.
                                getNodeSyncGetter(NodeMultiroomDeviceClientIndex.class, true).get();
						if (clientIdNode != null) {
							device.mClientId = clientIdNode.getValue();
						} else {
							device.mClientId = MultiroomGroupManager.SERVER_NUM_ID;
						}
					}
				}
			} else {
				result = MultiroomOperationResult.OperationResult.DIFFERENT_MR_VERSION_ERR;
			}
		} else {
			FsLogger.log("No server found for group " + group.mGroupName + " " + group.mGroupId, LogLevel.Error);
		}

		opResult.mDevice = device;
		opResult.mOperationResult = result;
		
		return opResult;
	}
	
	public boolean destroyGroup(MultiroomGroupModel group) {
		boolean destroyedOk = false;
		if (group != null) {

			FsLogger.log("Destroy group " + group.mGroupName + " " + group.mGroupId, LogLevel.Warning);

			MultiroomDeviceModel serverDevice = mMultiroomManager.findServerForGroup(group);

			if (serverDevice != null) {
				destroyedOk = serverDevice.mRadio.getNodeSyncSetter(new NodeMultiroomGroupDestroy(NodeMultiroomGroupDestroy.Ord.DESTROY)).set();

                try {
                    Thread.sleep(WAIT_SHORT);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
		}
		
		return destroyedOk;
	}

	private boolean isMultiroomVersionCompatible(MultiroomDeviceModel device1, MultiroomDeviceModel device2) {
		boolean isCompatible = false;
		
		if (device1.mRadio != null && device2.mRadio != null) {
			String versionDevice1 = device1.mRadio.getMultiroomVersion();
			String versionDevice2 = device2.mRadio.getMultiroomVersion();
			
			if (versionDevice1 != null && versionDevice2 != null) {
				isCompatible = versionDevice1.equals(versionDevice2);
			}
		}
		
		return isCompatible;
	}
	
	private MultiroomOperationResult createGroupAndAddDevice(MultiroomDeviceModel device, MultiroomGroupModel group) {
		MultiroomOperationResult opResult = new MultiroomOperationResult(); 
		MultiroomOperationResult.OperationResult result = MultiroomOperationResult.OperationResult.CREATE_GROUP_ERR;

		Radio radio = device.mRadio;

		FsLogger.log("createGroupAndAddDevice: " + group.mGroupName + " for " + radio);

		if (radio != null) {
			if (isCurrentModeStreamable(radio)) {
				boolean createdOk = radio.getNodeSyncSetter(new NodeMultiroomGroupCreate(group.mGroupName)).set();

				if (createdOk) {
					result = MultiroomOperationResult.OperationResult.SUCCESS;

					// set new group info here, don't wait for listall update
					device.mGroupName = group.mGroupName;
					device.mGroupRole = NodeMultiroomGroupState.Ord.SERVER;

                    try {
                        Thread.sleep(WAIT_SHORT);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    NodeMultiroomGroupId groupIdNode = (NodeMultiroomGroupId) radio.
                            getNodeSyncGetter(NodeMultiroomGroupId.class, true).get();
					if (groupIdNode != null) {
						device.mGroupId = groupIdNode.getValue();
					} else {
						device.mGroupId = MultiroomGroupManager.NO_GROUP_ID;
					}
				}
			} else {
				result = MultiroomOperationResult.OperationResult.STREAMABLE_MODE_NEEDED_ERR;
			}
		}

		opResult.mDevice = device;
		opResult.mOperationResult = result;
		
		return opResult;
	}

	private boolean isCurrentModeStreamable(Radio radio) {
		boolean isStreamable = false;
		NodeMultiroomGroupStreamable streamableNode = (NodeMultiroomGroupStreamable) radio.
                getNodeSyncGetter(NodeMultiroomGroupStreamable.class, true).get();

		if (streamableNode != null) {
			isStreamable = (streamableNode.getValueEnum() == NodeMultiroomGroupStreamable.Ord.TRUE);
		}

		return isStreamable;
	}
	
	public long getMaxNumClients() {
		long maxClients = 0;
		Radio radio = mNetRemoteManager.getCurrentRadio();
		
		if (!mNetRemoteManager.checkConnection(radio))
			return maxClients;
		
		NodeMultiroomCapsMaxClients maxClientsNode = (NodeMultiroomCapsMaxClients) radio.
                getNodeSyncGetter(NodeMultiroomCapsMaxClients.class).get();
		if (maxClientsNode != null) {
			maxClients = maxClientsNode.getValue();
		}
		
		return maxClients;
	}

	public boolean makeClientIntoServer(Radio radio) {
		
		FsLogger.log("makeClientIntoServer " + radio);

        boolean result = radio.getNodeSyncSetter(new NodeMultiroomGroupState(NodeMultiroomGroupState.Ord.SERVER)).set();

		return result;
	}

    public void sendSingleGroupCommand(Radio radio, boolean checked) {
        NodeMultiroomSinglegroupState.Ord ord = checked ? NodeMultiroomSinglegroupState.Ord.MULTIROOM :
                NodeMultiroomSinglegroupState.Ord.SINGLE;

        if (radio != null) {
            radio.setNode(new NodeMultiroomSinglegroupState(ord), false);
        }
    }
}
