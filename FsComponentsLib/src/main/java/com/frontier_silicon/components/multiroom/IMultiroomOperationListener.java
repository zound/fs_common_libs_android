package com.frontier_silicon.components.multiroom;

/**
 * Created by lsuhov on 14/09/16.
 */
public interface IMultiroomOperationListener {
    void onOperationComplete(MultiroomOperationResult result);
}
