package com.frontier_silicon.components.multiroom;

public class MultiroomOperationResult {

	public enum OperationResult {
		SUCCESS,
		CREATE_GROUP_ERR,
		ADD_CLIENT_ERR,
		ADD_CLIENT_MAX_CLIENT_ERR,
		UNGROUP_DEVICE_ERR,
		DESTROY_GROUP_ERR,
		TIMEOUT_ERR,
		DIFFERENT_MR_VERSION_ERR,
		MR_NOT_SUPPORTED_ERR,
		STREAMABLE_MODE_NEEDED_ERR
	}

	public OperationResult mOperationResult;
	public MultiroomDeviceModel mDevice;
	public MultiroomGroupModel mGroup;
	
}
