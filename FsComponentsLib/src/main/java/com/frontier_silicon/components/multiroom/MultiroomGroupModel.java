package com.frontier_silicon.components.multiroom;

public class MultiroomGroupModel {

	public String mGroupName;
	public String mGroupId;
	public int mNumClients;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((mGroupId == null) ? 0 : mGroupId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MultiroomGroupModel other = (MultiroomGroupModel) obj;
		if (mGroupId == null) {
			if (other.mGroupId != null)
				return false;
		} else if (!mGroupId.equals(other.mGroupId))
			return false;
		return true;
	}
}
