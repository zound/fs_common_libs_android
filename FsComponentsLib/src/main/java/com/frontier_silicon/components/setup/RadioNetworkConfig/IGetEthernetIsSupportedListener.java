package com.frontier_silicon.components.setup.RadioNetworkConfig;

/**
 * Created by lsuhov on 01/02/16.
 */
public interface IGetEthernetIsSupportedListener {
    void onEthernetSupported(boolean isSupported);
}
