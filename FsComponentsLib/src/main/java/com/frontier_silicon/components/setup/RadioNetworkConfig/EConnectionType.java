package com.frontier_silicon.components.setup.RadioNetworkConfig;

/**
 * Created by lsuhov on 5/11/2015.
 */
public enum EConnectionType {
    WiFi,
    Ethernet,
    WPS
}
