package com.frontier_silicon.components.setup.RadioNetworkConfig;

import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWiredInterfaceEnable;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 09/11/15.
 */
public class GetEthernetIsSupportedTask extends AsyncTask<Void, Integer, Boolean> {

    private List<IGetEthernetIsSupportedListener> mListeners = new ArrayList<>();
    private Radio mRadio;

    public GetEthernetIsSupportedTask(Radio radio, IGetEthernetIsSupportedListener listener) {
        setInterfaceListener(listener);
        mRadio = radio;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        if (mRadio == null)
            return true;

        NodeSysNetWiredInterfaceEnable node = (NodeSysNetWiredInterfaceEnable) mRadio.
                getNodeSyncGetter(NodeSysNetWiredInterfaceEnable.class).get();

        return node != null;
    }

    @Override
    protected void onPostExecute(Boolean isEthernetSupported) {
        notifyListeners(isEthernetSupported);

        mListeners.clear();
        mRadio = null;
    }

    private void notifyListeners(Boolean isEthernetSupported) {
        for (IGetEthernetIsSupportedListener listener : mListeners) {
            listener.onEthernetSupported(isEthernetSupported);
        }
    }

    public void setInterfaceListener(IGetEthernetIsSupportedListener interfaceListener) {
        if (interfaceListener != null)
            mListeners.add(interfaceListener);
    }
}
