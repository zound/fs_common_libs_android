package com.frontier_silicon.components.setup.RadioNetworkConfig;

public class StaticIPAddressesParam {

	public String mIPAdress = "";
	public String mGatewayAdress = "";
	public String mPrimaryDNSAdress = "";
	public String mSecondaryDNSAdress = "";
	public String mSubnetMask = "";
	
}
