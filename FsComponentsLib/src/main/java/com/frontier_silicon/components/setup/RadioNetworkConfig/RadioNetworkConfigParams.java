package com.frontier_silicon.components.setup.RadioNetworkConfig;

public class RadioNetworkConfigParams {

    public EConnectionType mConnectionType = EConnectionType.Ethernet;
    public boolean mUseDhcp = true;
	public long mRegionId;
	public String mWiFiPasscode = "";
	
	public long mSelectedAPKeyId = -1;

	public StaticIPAddressesParam mStaticIPAdresses = null;
	
	// manual settings:
	public String mSSID = "";
	public long mAuthType = 0;
	public long mEncryptionType = 0;
}
