package com.frontier_silicon.components.setup.RadioNetworkConfig;

import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRegion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanScan;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanScanList;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.nodeCommunication.IListNodeListener;
import com.frontier_silicon.loggerlib.FsLogger;

import java.util.ArrayList;
import java.util.List;

public class RadioWiFiNetworkScanner {

	public static final int MAX_SECONDS_TO_SCAN = 30;
	public static final int MAX_SECONDS_TO_SCAN_AND_RETRIEVE = MAX_SECONDS_TO_SCAN + 4;

	private boolean mScanWiFiIdle = false;
	private int mScanRetriesCount = 0;

	List<NodeSysNetWlanScanList.ListItem> mAvailableNetworks = new ArrayList<>();
	
	public RadioWiFiNetworkScanner() {
		
	}
	
	public synchronized List<NodeSysNetWlanScanList.ListItem> scanAvailableWiFiNetworks(Radio radio, long regionId, final AsyncTask asyncTask) {
		if (radio != null) {
			mScanWiFiIdle = false;	
			mScanRetriesCount = 0;

			radio.addNotificationListener(new INodeNotificationCallback() {
				@Override
				public void onNodeUpdated(NodeInfo node) {
                    if (node instanceof NodeSysNetWlanScan) {
                        NodeSysNetWlanScan scanNode = (NodeSysNetWlanScan) node;

                        FsLogger.log("NodeSysNetWlanScan notif callback: " + scanNode.getName() + " value= " + scanNode.toString());
                        mScanWiFiIdle = true;
                    }
				}
			});

            radio.setNode(new NodeSysNetWlanRegion(regionId), true);
			
			RadioNodeUtil.waitDefaultMs();
			if (asyncTask.isCancelled())
				return mAvailableNetworks;

            radio.setNode(new NodeSysNetWlanScan(NodeSysNetWlanScan.Ord.SCAN), true);
			
			while (!mScanWiFiIdle) {
				try {
					Thread.sleep(1000);
					mScanRetriesCount++;
					if (asyncTask.isCancelled())
						return mAvailableNetworks;
					
					//probably the notify event was lost
					if (mScanRetriesCount > MAX_SECONDS_TO_SCAN) {
						break;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			boolean blocking = true;
			RadioNodeUtil.getListNodeItems(radio, NodeSysNetWlanScanList.class, blocking, new IListNodeListener<NodeListItem>() {
				@Override
				public void onListNodeResult(List resultList, boolean isListComplete) {
					if (resultList != null) {
						mAvailableNetworks = resultList;
					}
				}
			});
		}
		
		return mAvailableNetworks;
	}
	
}
