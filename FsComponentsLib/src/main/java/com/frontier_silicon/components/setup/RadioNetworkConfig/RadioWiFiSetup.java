package com.frontier_silicon.components.setup.RadioNetworkConfig;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWiredInterfaceEnable;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetCommitChanges;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetIpConfigAddress;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetIpConfigDhcp;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetIpConfigDnsPrimary;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetIpConfigDnsSecondary;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetIpConfigGateway;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetIpConfigSubnetMask;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetKeepConnected;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanInterfaceEnable;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanPerformWPS;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRegion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanSelectAP;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanSetAuthType;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanSetEncType;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanSetPassphrase;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanSetSSID;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysRsaPublicKey;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysRsaStatus;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.ISetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.loggerlib.LogLevel;


public class RadioWiFiSetup {

	private static final int MAX_WAIT_FOR_RSA_KEY_SEC = 60;
	final private Radio mRadio;
    boolean commitSucces = false;
	
	public RadioWiFiSetup(Radio radio) {
		mRadio = radio;
	}
	
	public boolean setupNetwork(RadioNetworkConfigParams configParams) {
		try {
			FsLogger.log("RadioNetworkSetup: START");

            if (configParams.mConnectionType == EConnectionType.Ethernet) {
                mRadio.setNode(new NodeSysNetWiredInterfaceEnable(NodeSysNetWiredInterfaceEnable.Ord.INTERFACE_ENABLE), true);
                RadioNodeUtil.waitDefaultMs();

                mRadio.setNode(new NodeSysNetKeepConnected(NodeSysNetKeepConnected.Ord.YES), true);
                RadioNodeUtil.waitDefaultMs();

                chooseBetweenDhcpAndStatic(configParams);
            } else {

                mRadio.setNode(new NodeSysNetWlanInterfaceEnable(NodeSysNetWlanInterfaceEnable.Ord.INTERFACE_ENABLE), true);
                RadioNodeUtil.waitDefaultMs();

                mRadio.setNode(new NodeSysNetKeepConnected(NodeSysNetKeepConnected.Ord.YES), true);
                RadioNodeUtil.waitDefaultMs();

                mRadio.setNode(new NodeSysNetWlanRegion(configParams.mRegionId), true);
                RadioNodeUtil.waitDefaultMs();

                if (configParams.mConnectionType == EConnectionType.WPS) {
                    return connectWithWPS();
                }

                NodeSysRsaStatus rsaStatusNode = waitForRSAKeyGenerationAndReturnStatus(MAX_WAIT_FOR_RSA_KEY_SEC);
                if (rsaStatusNode == null || rsaStatusNode.getValueEnum() != NodeSysRsaStatus.Ord.KEY_AVAILABLE)
                    return false;

                NodeSysRsaPublicKey rsaKeyNode = (NodeSysRsaPublicKey) mRadio.
                        getNodeSyncGetter(NodeSysRsaPublicKey.class, true).get();

                String rsaPublicKey = rsaKeyNode.getValue();

                byte[] encBytes = RadioNodeUtil.encryptPassphrase(configParams.mWiFiPasscode, rsaPublicKey);
                String encyptedPassw = RadioNodeUtil.getBase16(encBytes);

                chooseBetweenDhcpAndStatic(configParams);

                boolean isAutoConfig = (configParams.mSelectedAPKeyId >= 0);

                if (isAutoConfig)
                    configNetworkAuto(configParams, encyptedPassw);
                else
                    configNetworkManual(configParams, encyptedPassw);
            }
			
			RadioNodeUtil.waitMs(1000);


            //We don't need notifications after this point. We got unneeded connection lost events
            mRadio.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean closeSetupNetwork()
	{
		mRadio.setNode(new NodeSysNetCommitChanges(NodeSysNetCommitChanges.Ord.YES), true, new ISetNodeCallback() {
			@Override
			public void setNodeSuccess(NodeInfo node) {
				commitSucces = true;
			}

			@Override
			public void setNodeError(NodeInfo node, NodeErrorResponse error) {
				if (error.getErrorCode() == ErrorResponse.ErrorCode.NetworkTimeout ||
						error.getErrorCode() == ErrorResponse.ErrorCode.NetworkProblem) {
					commitSucces = true;
				} else {
					commitSucces = false;
				}
			}
		});

		if (commitSucces) {
			FsLogger.log("RadioNetworkSetup: COMPLETED");
			return true;
		}
		else {
			FsLogger.log("RadioNetworkSetup: failed on Commit", LogLevel.Error);
			return false;
		}
	}

    private boolean connectWithWPS() {
		NodeSysNetWlanPerformWPS wpsStateNode = (NodeSysNetWlanPerformWPS)mRadio.
                getNodeSyncGetter(NodeSysNetWlanPerformWPS.class, true).get();

		if (wpsStateNode == null || wpsStateNode.getValueEnum() != NodeSysNetWlanPerformWPS.Ord.WPS_IDLE)
			return false;

        mRadio.setNode(new NodeSysNetWlanPerformWPS(NodeSysNetWlanPerformWPS.Ord.WPS_PBC), false);
		
		return true;
	}

	private void configNetworkAuto(RadioNetworkConfigParams configParams, String encyptedPassw) {
        mRadio.setNode(new NodeSysNetWlanSetPassphrase(encyptedPassw), true);
		RadioNodeUtil.waitDefaultMs();

        mRadio.setNode(new NodeSysNetWlanSelectAP(configParams.mSelectedAPKeyId), true);
	}
	
	private void configNetworkManual(RadioNetworkConfigParams configParams, String encyptedPassw) {
        mRadio.setNode(new NodeSysNetWlanSetAuthType(configParams.mAuthType), true);
		RadioNodeUtil.waitDefaultMs();

        mRadio.setNode(new NodeSysNetWlanSetEncType(configParams.mEncryptionType), true);
		RadioNodeUtil.waitDefaultMs();

        mRadio.setNode(new NodeSysNetWlanSetPassphrase(encyptedPassw), true);
		RadioNodeUtil.waitDefaultMs();

        mRadio.setNode(new NodeSysNetWlanSetSSID(configParams.mSSID), true);
	}

    private void chooseBetweenDhcpAndStatic(RadioNetworkConfigParams configParams) {
        if (configParams.mUseDhcp) {
            mRadio.setNode(new NodeSysNetIpConfigDhcp(NodeSysNetIpConfigDhcp.Ord.YES), true);
            RadioNodeUtil.waitDefaultMs();
        } else {
            mRadio.setNode(new NodeSysNetIpConfigDhcp(NodeSysNetIpConfigDhcp.Ord.NO), true);
            RadioNodeUtil.waitDefaultMs();

            configStaticIPAdresses(configParams.mStaticIPAdresses);
        }
    }

	private void configStaticIPAdresses(StaticIPAddressesParam staticIPAdresses) {		
		long ipAddress = RadioNodeUtil.getIPNumericValue(staticIPAdresses.mIPAdress);
		if (ipAddress > 0) {
            mRadio.setNode(new NodeSysNetIpConfigAddress(ipAddress), true);
			RadioNodeUtil.waitDefaultMs();
		}
		
		long gatewayAddress = RadioNodeUtil.getIPNumericValue(staticIPAdresses.mGatewayAdress);
		if (gatewayAddress > 0) {
            mRadio.setNode(new NodeSysNetIpConfigGateway(gatewayAddress), true);
			RadioNodeUtil.waitDefaultMs();
		}
		
		long dns1Address = RadioNodeUtil.getIPNumericValue(staticIPAdresses.mPrimaryDNSAdress);
		if (dns1Address > 0) {
            mRadio.setNode(new NodeSysNetIpConfigDnsPrimary(dns1Address), true);
			RadioNodeUtil.waitDefaultMs();
		}
		
		long dns2Address = RadioNodeUtil.getIPNumericValue(staticIPAdresses.mSecondaryDNSAdress);
		if (dns2Address > 0) {
            mRadio.setNode(new NodeSysNetIpConfigDnsSecondary(dns2Address), true);
			RadioNodeUtil.waitDefaultMs();
		}
		
		long subnetMask = RadioNodeUtil.getIPNumericValue(staticIPAdresses.mSubnetMask);
		if (subnetMask > 0) {
            mRadio.setNode(new NodeSysNetIpConfigSubnetMask(subnetMask), true);
			RadioNodeUtil.waitDefaultMs();
		}
	}
	
	private NodeSysRsaStatus waitForRSAKeyGenerationAndReturnStatus(int maxWaitSeconds) {
		int count = 0;		
		NodeSysRsaStatus rsaStatusNode = (NodeSysRsaStatus) mRadio.
                getNodeSyncGetter(NodeSysRsaStatus.class, true).get();

		while (rsaStatusNode.getValueEnum() == NodeSysRsaStatus.Ord.GENERATION_IN_PROGRESS && count < maxWaitSeconds) {
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			rsaStatusNode = (NodeSysRsaStatus) mRadio.getNodeSyncGetter(NodeSysRsaStatus.class, true).get();

			count++;
		}
		return rsaStatusNode;
	}
	
}
