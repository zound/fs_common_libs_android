package com.frontier_silicon.components.widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

public class NoJumpSeekBar extends SeekBar {

    private static final int DELTA_PROGRESS_ON_TAP = 4;
    private Drawable mThumb;
    private OnSeekBarChangeListener mOnSeekBarChangeListener;

    public NoJumpSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NoJumpSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoJumpSeekBar(Context context) {
        super(context);
    }

    @Override
    public void setThumb(Drawable thumb) {
        super.setThumb(thumb);
        mThumb = thumb;
    }

    @Override
    public void setOnSeekBarChangeListener(OnSeekBarChangeListener l) {
        mOnSeekBarChangeListener = l;

        super.setOnSeekBarChangeListener(l);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            int paddingWidth = mThumb.getBounds().width();
            int paddingHeight = mThumb.getBounds().height();

            // if tapped outside of the thumb (+padding for playing nicely with big fingers)
            if ( event.getX() < (mThumb.getBounds().left - paddingWidth) ||
                event.getX() > (mThumb.getBounds().right + paddingWidth) ||
                event.getY() > (mThumb.getBounds().bottom + paddingHeight) ||
                event.getY() < (mThumb.getBounds().top - paddingHeight) ) {

                    int progressDiff;
                    if (event.getX() < (mThumb.getBounds().left - paddingWidth)) {
                        progressDiff = -DELTA_PROGRESS_ON_TAP;
                    } else {
                        progressDiff = DELTA_PROGRESS_ON_TAP;
                    }

                    setProgress(getProgress() + progressDiff);

                    //force call on progresschanged with fromUser "true"
                    if (mOnSeekBarChangeListener != null) {
                        boolean fromUser = true;
                        mOnSeekBarChangeListener.onProgressChanged(NoJumpSeekBar.this, getProgress(), fromUser);
                    }

                    return false;
            }
        }

        return super.onTouchEvent(event);
    }
}

