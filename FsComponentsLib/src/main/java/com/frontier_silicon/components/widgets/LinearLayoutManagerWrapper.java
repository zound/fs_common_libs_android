package com.frontier_silicon.components.widgets;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by lsuhov on 05/11/2016.
 *
 * Used to avoid a crash when scrolling and pressing back
 */

public class LinearLayoutManagerWrapper extends LinearLayoutManager {
    public LinearLayoutManagerWrapper(Context context) {
        super(context);
    }

    @Override
    public boolean supportsPredictiveItemAnimations() {
        return false;
    }
}
