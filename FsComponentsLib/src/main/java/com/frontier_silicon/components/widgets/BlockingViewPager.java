package com.frontier_silicon.components.widgets;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class BlockingViewPager extends ViewPager {

    private static final String TAG = "BlockingViewPager";
	private static final float MAX_X_VALUE_CHANGE = 0;
	private float mStartDragX = -1;
	private float mPrevX = -1;
	private boolean swipeLeftToRight = false;
	
	private boolean mMultiplePointerCnt = false;
    private final Rect mTempRect = new Rect();

	public BlockingViewPager(Context context){
        super(context);
    }

    public BlockingViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev){
    	if (isEnabled()) {
            return super.onInterceptTouchEvent(ev);
        }
    	
    	// block any swipe right->left
//    	boolean blockEvent = false;
//    	float x = ev.getX();
//    	if (ev.getAction() == MotionEvent.ACTION_DOWN) {
//    		mStartDragX = x;
//    		blockEvent = false;
//    	}
//
//        if (ev.getAction() == MotionEvent.ACTION_MOVE) {
//        	if (mStartDragX > x + MAX_X_VALUE_CHANGE) {
//        		blockEvent = true;
//        	}
//        }
//
//        if (!blockEvent) {
//        	return super.onInterceptTouchEvent(ev);
//        }
//        else
        	return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev){
    	if (isEnabled()) {
            return super.onTouchEvent(ev);
        }

    	// block any slow right->left movement. allow left->right => smooth one-sided page change 
//    	boolean blockEvent = true;
//    	float x = ev.getX();
//        if (ev.getAction() == MotionEvent.ACTION_MOVE) {
//	    	if (mPrevX >= 0) {
//	    		// swipe left -> right (prev. page)
//		    	float deltaX = x - mPrevX;
//		    	if (deltaX > 0) {
//		    		blockEvent = false;
//		    		swipeLeftToRight = true;
//		        }
//	    	} else {	// first touch
//	    		mPrevX = x;
//	    		blockEvent = true;
//	    		swipeLeftToRight = false;
//
//	            if (ev.getPointerCount() == 1) {
//	            	mMultiplePointerCnt = false;
//	            }
//	    	}
//        } else if (ev.getAction() == MotionEvent.ACTION_UP) {
//        	mPrevX = -1;
//        	blockEvent = !swipeLeftToRight;
//        }
//
//        // use this to avoid "pointerIndex out of range" exception
//        if (ev.getPointerCount() > 1) {
//        	mMultiplePointerCnt = true;
//        }
//
//        if (!blockEvent && !mMultiplePointerCnt) {
//        	return super.onTouchEvent(ev);
//        }
//        else
        	return false;
    }

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
	}

    @Override
    public boolean arrowScroll(int direction) {
        View currentFocused = findFocus();
        if (currentFocused == this) {
            currentFocused = null;
        } else if (currentFocused != null) {
            boolean isChild = false;
            for (ViewParent parent = currentFocused.getParent(); parent instanceof ViewGroup;
                 parent = parent.getParent()) {
                if (parent == this) {
                    isChild = true;
                    break;
                }
            }
            if (!isChild) {
                // This would cause the focus search down below to fail in fun ways.
                final StringBuilder sb = new StringBuilder();
                sb.append(currentFocused.getClass().getSimpleName());
                for (ViewParent parent = currentFocused.getParent(); parent instanceof ViewGroup;
                     parent = parent.getParent()) {
                    sb.append(" => ").append(parent.getClass().getSimpleName());
                }
                Log.e(TAG, "arrowScroll tried to find focus based on non-child " +
                        "current focused view " + sb.toString());
                currentFocused = null;
            }
        }

        boolean handled = false;

        View nextFocused = FocusFinder.getInstance().findNextFocus(this, currentFocused,
                direction);
        if (nextFocused != null && nextFocused != currentFocused) {
            if (direction == View.FOCUS_LEFT) {
                // If there is nothing to the left, or this is causing us to
                // jump to the right, then what we really want to do is page left.
                final int nextLeft = getChildRectInPagerCoordinates(mTempRect, nextFocused).left;
                final int currLeft = getChildRectInPagerCoordinates(mTempRect, currentFocused).left;
                if (currentFocused != null && nextLeft >= currLeft) {
                    //handled = pageLeft();
                } else {
                    handled = nextFocused.requestFocus();
                }
            } else if (direction == View.FOCUS_RIGHT) {
                // If there is nothing to the right, or this is causing us to
                // jump to the left, then what we really want to do is page right.
                final int nextLeft = getChildRectInPagerCoordinates(mTempRect, nextFocused).left;
                final int currLeft = getChildRectInPagerCoordinates(mTempRect, currentFocused).left;
                if (currentFocused != null && nextLeft <= currLeft) {
                    //handled = pageRight();
                } else {
                    handled = nextFocused.requestFocus();
                }
            }
        } else if (direction == FOCUS_LEFT || direction == FOCUS_BACKWARD) {
            // Trying to move left and nothing there; try to page.
            //handled = pageLeft();
        } else if (direction == FOCUS_RIGHT || direction == FOCUS_FORWARD) {
            // Trying to move right and nothing there; try to page.
            //handled = pageRight();
        }
        if (handled) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(direction));
        }
        return handled;
    }

    private Rect getChildRectInPagerCoordinates(Rect outRect, View child) {
        if (outRect == null) {
            outRect = new Rect();
        }
        if (child == null) {
            outRect.set(0, 0, 0, 0);
            return outRect;
        }
        outRect.left = child.getLeft();
        outRect.right = child.getRight();
        outRect.top = child.getTop();
        outRect.bottom = child.getBottom();

        ViewParent parent = child.getParent();
        while (parent instanceof ViewGroup && parent != this) {
            final ViewGroup group = (ViewGroup) parent;
            outRect.left += group.getLeft();
            outRect.right += group.getRight();
            outRect.top += group.getTop();
            outRect.bottom += group.getBottom();

            parent = group.getParent();
        }
        return outRect;
    }
}
